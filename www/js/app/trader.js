var deletedNodes = [];
var url = new URL((''+document.location));
var obj = url.searchParams.get("obj");

$(function() {
    var lastClick = 0;
    var jstreeOptions = {
        "load_all": true,
        "core" : {
            "data" : {
                "url" : function (node) {
                    return getUrl({
                        'obj' : 'all',
                        'r': dataUrl,
                        'trader_id': url.searchParams.get("id"),
                        'for': url.searchParams.get("for") || obj
                    });
                },
                "data": function (node) {
                    return { 'id' : node.id };
                }
            },
            // 'callback' : {
            //     'beforemove' : function (NODE, REF_NODE, TYPE, TREE_OBJ) {
            //         var i = true;
            //     }
            // },
            "check_callback" :  function (op, node, par, pos, more) {
                if ((op === "move_node" || op === "copy_node") && more.dnd && node.type && node.type === 'pack' && node.parent && node.parent !== more.ref.parent) {
                    return false;
                }
                return true;
            },
            'animation': 0,
            'themes': { 'dots': true },
            'keyboard' : {
                'space': function (e) {
                    e.type = "click";
                    $(e.currentTarget).trigger(e);
                    var tree = $('#jstree-product').jstree(true);
                    var curNodeId = '#' + tree.get_selected();
                    $(curNodeId + ' .p-check-box').click();
                    // var curNode = tree.get_node(curNodeId);
                    // if(curNode.type && curNode.type === 'pack') {
                    //     $(curNodeId).find('i.jstree-checkbox').click();
                    // }
                },
                'f2': function (e) {
                }
            }
        },
        "plugins": ["grid","contextmenu","dnd","unique","types"/*,"checkbox"*/],
        // "checkbox" : {
        //     'whole_node' : false,
        //     'tie_selection' : false,
        //     'visible': false
        // },
        'grid': {
            'columns': [
                {
                    'width': 'auto',
                    'minWidth': '330px',
                    'header': "&nbsp;"},
                {
                    'width': 'auto',
                    'minWidth': '100px',
                    'header': "Цена",
                    'value': 'price',
                    'wideCellClass': 'priceValue'
                },
                {
                    'width': 'auto',
                    'minWidth': '100px',
                    'header': "Поставщик",
                    'title': "Цена этой же Торговой марки и этой же Фасовки от другого поставщика",
                    'value': 'otherTrader',
                    'wideCellClass': 'priceInfo'
                },
                {
                    'width': 'auto',
                    'minWidth': '100px',
                    'header': "Фасовка",
                    'title': "Цена этой же Торговой марки (пересчитанная на объем этой фасовки)",
                    'value': 'otherPack',
                    'wideCellClass': 'priceInfo'
                },
                {
                    'width': 'auto',
                    'minWidth': '100px',
                    'header': "Вид",
                    'title': "Цена этого же Вида товара (пересчитанная на объем этой фасовки)",
                    'value': 'thisType',
                    'wideCellClass': 'priceInfo'
                },
                {
                    'width': 'auto',
                    'minWidth': '100px',
                    'header': "Минимум",
                    'title': "Цена этого же Вида товара (пересчитанная на объем этой фасовки)",
                    'value': 'min',
                    'wideCellClass': 'priceInfo'
                },
                {
                    'width': 'auto',
                    'minWidth': '100px',
                    'header': "Максимум",
                    'title': "Цена этого же Вида товара (пересчитанная на объем этой фасовки)",
                    'value': 'max',
                    'wideCellClass': 'priceInfo'
                }
            ],
            'resizable': true,
            'draggable': false,
            'gridcontextmenu': false /*function (grid,tree,node,val,col,t,target) {
                t.deselect_all();
                t.select_node(node.attr('id'));
                if(col.value === 'price' && /pk\d+/.test(node.attr('id'))) {
                    return {
                        "edit": {
                            label: "Изменить цену",
                            'icon': 'fa fa-ruble',
                            "action": function (data) {
                                var obj = t.get_node(node);
                                grid._edit(obj, col, target);
                            }
                        }
                    }
                }
                return {};
            }*/,
            'headerContextMenu': false,
            'width': '100%'

        },
        'open_node': '#branch_1',
        "types": {
            'root': {
                'valid_children': ['group'],
                'children_type': "group",
                "check_hide" : true,
                'add_text': "Новая группа",
                'addFuncName': 'Group'
            },
            'group': {
                'valid_children': ['type'],
                'children_type': "type",
                'add_text': "Новый вид",
                'addFuncName': 'Type',
                'otherFuncName': 'Group',
                'icon': 'fa fa-envira c1'
            },
            'type': {
                'valid_children': ['tmark'],
                'children_type': "tmark",
                'add_text': "Новая торговая марка",
                'addFuncName': 'TMark',
                'otherFuncName': 'Type',
                'icon': 'fa fa-tag c2'
            },
            'tmark': {
                'valid_children': ['pack'],
                'children_type': "pack",
                'add_text': "Новая упаковка",
                'addFuncName': 'Pack',
                'otherFuncName': 'TMark',
                'icon': 'fa fa-trademark c3'
            },
            'pack': {
                'max_children': 0,
                'add_text': "Новая упаковка",
                'otherFuncName': 'Pack',
                'icon': 'fa fa-cube c4'
            }
        },
        "unique" : {
            "error_callback" : function (n, p, f) {
                alert("Duplicate node `" + n + "` with function `" + f + "`!");
            }
        },

        'contextmenu': {
            'items': function ($node) {
                var tree = $('#jstree-product').jstree(true);
                var typeRules = tree.settings.types;
                var items = {
                    "create": {
                        label: typeRules[$node.type].add_text,
                        "separator_after"	: true,
                        'icon' : 'fa fa-plus',
                        action: function(obj) {
                            var i = 0;
                            var originNode = $node;
                            do {
                                var sufix = (i > 0) ? ' (Копия ' + i + ')' : '';
                                $node = tree.create_node(originNode, {text: typeRules[originNode.type].add_text + sufix, type: typeRules[originNode.type].children_type, icon: typeRules[typeRules[originNode.type].children_type].icon});
                                i++;
                                console.log($('#jstree-product').jstree(true).last_error());
                            } while(!$node && i < 100);
                            treeObj = obj;
                            if($node) {
                                tree.deselect_all();
                                tree.select_node($node);
                                var funcName = 'add' + typeRules[originNode.type].addFuncName;
                                eval(funcName)(tree, $node);
                            }
                        }
                    },
                    "edit": {
                        label: 'Изменить',
                        'icon' : 'fa fa-edit',
                        action: function(obj) {
                            tree.deselect_all();
                            tree.select_node($node);
                            var funcName = 'edit' + typeRules[$node.type].otherFuncName;
                            eval(funcName)(tree, $node);
                        }
                    },
                    'clone': {
                        label: 'Создать Копию',
                        'icon' : 'fa fa-clone',
                        action: function(obj) {
                            var i = 1;
                            var newNodeId = null;
                            var parentNode = tree.get_node($node.parent);
                            do {
                                var sufix = (i > 1) ? ' (Копия ' + i + ')' : ' (Копия)';
                                newNodeId = tree.create_node(parentNode, {text: $node.text + sufix, type: $node.type, icon: $node.icon });
                                i++;
                                console.log($('#jstree-product').jstree(true).last_error());
                            } while(!newNodeId && i < 100);
                            tree.deselect_all();
                            tree.select_node(newNodeId);
                            var newNode = tree.get_node(newNodeId);
                            var funcName = 'add' + typeRules[$node.type].otherFuncName;
                            eval(funcName)(tree, newNode);
                        }
                    },
                    'delete': {
                        label: 'Удалить',
                        'icon' : 'fa fa-trash-o',
                        action: function(obj) {
                            tree.deselect_all();
                            var funcName = 'delete' + typeRules[$node.type].otherFuncName;
                            eval(funcName)(tree, $node);
                            var pos = deletedNodes.indexOf($node.id);
                            if(pos === -1) {
                                deletedNodes.push($node.id);
                            }
                            $('#' + $node.id).addClass('deleted');
                            $node.li_attr.class = 'deleted';
                            $node.data.deleted = '1';
                            if($('#viewDeleted .fa').hasClass('fa-eye-slash')) {
                                tree.hide_node($node);
                            }
                        }
                    },
                    'undelete': {
                        label: 'Восстановить',
                        'icon' : 'fa fa-undo',
                        action: function(obj) {
                            var funcName = 'undelete' + typeRules[$node.type].otherFuncName;
                            eval(funcName)(tree, $node);
                            var pos = deletedNodes.indexOf($node.id);
                            if(pos !== -1) {
                                deletedNodes.splice(pos, 1);
                            }
                            $('#' + $node.id).removeClass('deleted');
                            $node.li_attr.class = '';
                            $node.data.deleted = '0';
                        }
                    }
                };

                if($node.type === 'root') {
                    delete items.edit;
                    delete items.clone;
                    delete items.delete;
                }
                if($node.type === 'pack') {
                    delete items.create;
                }

                if($node.data && $node.data.deleted === '1') {
                    delete items.delete;
                    delete items.create;
                    delete items.edit;
                    if($node.type !== 'group') {
                        var parent = tree.get_node($node.parent);
                        if(parent.data && parent.data.deleted === '1') {
                            delete items.undelete;
                            delete items.clone;
                        }
                    }
                } else {
                    delete items.undelete;
                }

                return items;
            }
        }
    };

    if(typeof permit !== 'undefined') {
        if(!permit.canEditFullProducts) {
            if(!permit.canEditTmarkPackProducts) {
                jstreeOptions.core.check_callback = false;
                jstreeOptions.plugins = ["grid","unique","types"]
            } else {
                jstreeOptions.core.check_callback = function (op, node, par, pos, more) {
                    if ((op === "move_node" || op === "copy_node") && more.dnd && node.type ) {
                        if(node.type !== 'pack' && node.type !== 'tmark') {
                            return false;
                        }
                        if (node.type === 'pack' && node.parent && node.parent !== more.ref.parent) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
    }

    $('#jstree-product').jstree(jstreeOptions).bind("move_node.jstree", function(e, data) {
        var tree = data.instance;
        var funcName = 'changePos' + tree.settings.types[data.node.type].otherFuncName;
        eval(funcName)(data.node, data.parent, data.position);
    }).on("after_open.jstree", function(e, data) {
        var t = setTimeout(function() {
            setCheckboxes(data);
            setCeelParams(data);
            t = null;
        }, 50);
    }).on("redraw.jstree", function(e, data) {
        setCheckboxes(data);
        setCeelParams(data);
    }).on('refresh.jstree', function(e, data) {
        var tree = $('#jstree-product').jstree(true);
        if($('#viewDeleted .fa').hasClass('fa-eye') && deletedNodes.length > 0) {
            deletedNodes.forEach(function(item) {
                tree.show_node(item);
            });
        } else {
            deletedNodes.forEach(function(item) {
                tree.hide_node(item);
            });
        }
    }).on('loaded.jstree', function(e, data) {
        if(typeof treeWithCheckboxes !== 'undefined' && treeWithCheckboxes) {
            data.instance.settings.dnd.is_draggable = function (node, e) {
                if($(e.target).hasClass('tree-price')) {
                    e.preventDefault();
                    return false;
                }
                return true;
            };
        }
    }).on('update_cell.jstree-grid', function(e, data) {
        if(!/^(\d+|\d{1,}(\.|\,)|\d{0,}(\.|\,)\d{0,5})$/.test(data.value)) { // .45   ,45   123   123.45   123,45
            data.node.data.price = data.old;
        } else {
            // var curCheckbox = $('#' + data.node.id).find('.p-check-box');
            // if(!curCheckbox.prop('checked')) {
            //     curCheckbox.prop('checked', true);
            // }
            data.node.data.price = Number(data.value.replace(',','.')).toFixed(2);
//            data.node.data.checkboxState = 1;
            data.instance = $('#jstree-product').jstree(true);
            setCheckboxes(data);
            data.node.data.tmark_id = data.instance.get_node(data.node.parent).data.id;
            savePrice(data.node.data);
            if((obj !== 'all' || url.searchParams.get("for")) && data.node.data.main_price) {
                var priceCeel = $('.jstree-grid-col-1[data-jstreegrid="' + data.node.id + '"]').removeClass('bgC3').removeClass('bgC6');
                if (data.node.data.price > data.node.data.main_price) {
                    priceCeel.addClass('bgC3');
                } else if (data.node.data.price < data.node.data.main_price) {
                    priceCeel.addClass('bgC6');
                }
            }
        }
    }).on('ready.jstree', function(e,data) {
        var tree = data.instance;
        Object.values(tree._model.data).forEach(function(item) {
            if(item.data && item.data.deleted == '1') {
               deletedNodes.push(item.id);
               tree.hide_node(item.id);
               item.li_attr.class = 'deleted';
               $('#' + item.id).removeClass('hidden');
            }
            if (item.type === 'pack' && item.data) {
                if(item.data.checkboxState) {
                    //jstree_133_grid_pk26_col1
                    item.parents.forEach( function(parent) {
                        var node = tree.get_node(parent);
                        tree.open_node(node);
                    });
                }
            }
        });
        setCheckboxes(data);

    }).on('click.jstree','.p-check-box', function(e) {
        var nodeId = $(e.currentTarget).closest('.jstree-node').attr('id');
        var tree = $('#jstree-product').jstree(true);
        var node = tree.get_node(nodeId);
        if(typeof tripleState !== 'undefined' && tripleState) {
            if (this.readOnly) {
                this.readOnly = false;
                this.checked = true;
                node.data.checkboxState = 1;
            } else if (this.checked) {
                this.readOnly = this.indeterminate = true;
                node.data.checkboxState = 2;
            } else {
                node.data.checkboxState = 0;
            }
        } else {
            node.data.checkboxState = this.checked ? 1 : 0;
        }
//        if(node.data.price) {
            node.data.tmark_id = tree.get_node(node.parent).data.id;
            savePrice(node.data);
//        }
        e.stopImmediatePropagation();
    }).on("click.jstree",  function(e,ev) {
        var time = new Date().getTime();
        if (time-lastClick < 400) {
            lastClick = 0;
            if((typeof permit !== 'undefined') && permit.canChangePriceProduct) {
                e.preventDefault();
                if (ev && $(ev.currentTarget).hasClass('jstree-grid-col-1')) {
                    ev.preventDefault();
                    var tree = $('#jstree-product').jstree(true);
                    var nodeId = $(ev.currentTarget).data('jstreegrid');
                    var node = tree.get_node(nodeId);
                    var col = tree.settings.grid.columns[1];
                    var target = ev.target;
                    tree._edit(node, col, target);
                }
            }
        } else {
            lastClick = time;
        }
    });

});


function setCheckboxes(data) {
    var tree = data.instance;
    if(!data.node) {
        data.node = { children_d: Object.keys(data.instance._model.data) };
    }
    data.node.children_d.forEach(function (item) {
        var node = tree.get_node(item);
        if (node.type === 'pack' && node.data) {
            var checkbox = $('#' + node.id).find('.p-check-box');
            if((typeof permit !== 'undefined') && permit.canCheckedProduct) {
                if (typeof tripleState !== 'undefined' && tripleState) {
                    if (node.data.checkboxState === 1) {
                        checkbox.prop('readOnly', false);
                        checkbox.prop('checked', true);
                        checkbox.prop('value', 'true');
                    } else if (node.data.checkboxState === 2 || node.data.checkboxState === undefined) {
                        checkbox.prop('readOnly', true);
                        checkbox.prop('indeterminate', true);
                        checkbox.prop('value', 'indeterminate');
                    } else {
                        checkbox.prop('checked', false);
                        checkbox.prop('value', 'false');
                    }
                } else {
                    if (node.data.checkboxState === 1) {
                        checkbox.prop('checked', true);
                        checkbox.prop('value', 'true');
                    } else {
                        checkbox.prop('value', 'false');
                    }
                }
            } else {
                checkbox.addClass('hidden');
            }
        }
    });
}

function setCeelParams(data) {
    var tree = data.instance;
    if(!data.node) {
        data.node = { children_d: Object.keys(data.instance._model.data) };
    }
    data.node.children_d.forEach(function (item) {
        var node = tree.get_node(item);
        if (node.type === 'pack' && node.data && node.data.price && node.data.main_price) {
            var priceCeel = $('.jstree-grid-col-1[data-jstreegrid="' + node.id + '"]').removeClass('bgC3').removeClass('bgC6');
            if(node.data.price > node.data.main_price) {
                priceCeel.addClass('bgC3');
            } else if(node.data.price < node.data.main_price) {
                priceCeel.addClass('bgC6');
            }
            if(typeof businessobj !== 'undefined' && node.data.min_obj !== '-' && node.data.max_obj !== '-') {
                $('.jstree-grid-col-5[data-jstreegrid="' + node.id + '"]').prop('title', eval('businessobj.' + node.data.min_obj));
                $('.jstree-grid-col-6[data-jstreegrid="' + node.id + '"]').prop('title', eval('businessobj.' + node.data.max_obj));
            }
        }
    });
    $('.jstree-grid-column-1 .jstree-grid-header').attr('title', "Цена");
    $('.jstree-grid-column-2 .jstree-grid-header').attr('title', "Минимальная цена этой же Торговой марки \nи этой же Фасовки от другого поставщика");
    $('.jstree-grid-column-3 .jstree-grid-header').attr('title', "Минимальная цена этой же Торговой марки \n(пересчитанная на объем этой фасовки)");
    $('.jstree-grid-column-4 .jstree-grid-header').attr('title', "Минимальная цена этого же Вида товара \n(пересчитанная на объем этой фасовки)");
    $('.jstree-grid-column-5 .jstree-grid-header').attr('title', "Минимальная цена для объектов");
    $('.jstree-grid-column-6 .jstree-grid-header').attr('title', "Максимальная цена для объектов");

}


function savePrice(data) {
    var url = new URL((''+document.location));
    var forObj = url.searchParams.get("for");
    var trader_id = url.searchParams.get("id");
    $.ajax({
        method: "POST",
        url: getUrl({'r': 'trader/savePrice'}),
        data: {
            'forObj' : forObj,
            'trader_id': trader_id,
            'tmark_id': data.tmark_id,
            'pack_id': data.id,
            'price': data.price || null,
            'checkboxState': data.checkboxState,
            'smallest_unit_value': data.smallest_unit_value
        }
    }).done(function() {
        $('.offline').addClass('hidden');
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
}


$('.addTraderBtn').on('click', function() {
    var form = $("#traderForm");
    resetForm(form);
    $("#traderModal").modal('show').on('hide.bs.modal', function() {
        resetForm(form);
        clearInterval(timerLockId);
        $(this).off('hide.bs.modal');
    });
});

$('.editTraderBtn').on('click', function() {
    var form = $("#traderForm");
    var id = form.find('.field-id').val();
    timerLockId = setTimeout(function tick() {
        lockData(form, 'trader', id);
        timerLockId = setTimeout(tick, 600000);
    }, 100);     // 10 min. = 600 000 msec.

    $("#traderModal").modal('show').on('hide.bs.modal', function() {
        clearInterval(timerLockId);
        unlockData('trader', id);
        $(this).off('hide.bs.modal');
    });
});

$('.editTraderObjBtn').on('click', function() {
    var form = $("#traderObjForm");
    var id = form.find('.field-id').val();
    timerLockId = setTimeout(function tick() {
        lockData(form, 'traderObj', id);
        timerLockId = setTimeout(tick, 600000);
    }, 100);     // 10 min. = 600 000 msec.

    $("#traderObjModal").modal('show').on('hide.bs.modal', function() {
        clearInterval(timerLockId);
        unlockData('traderObj', id);
        $(this).off('hide.bs.modal');
    });
});


var timerLockId;

function lockData(form, ctrl, lockRowId) {
    $.ajax({
        method: "POST",
        url: getUrl({'r': ctrl + '/lock'}),
        data: {id: lockRowId}
    }).done(function(data) {
        if(data.content && data.content.lock !== null) {
            form.find('.lockData').removeClass('hidden').text(data.content.lock);
            form.find(".canHide").addClass('hidden');
            form.find(".canLock").attr('disabled',true);
        } else {
            form.find('.lockData').addClass('hidden');
            form.find(".canHide").removeClass('hidden');
            form.find(".canLock").attr('disabled',false);
        }
        form.find('.offline').addClass('hidden');
    }).fail(function() {
        form.find('.offline').removeClass('hidden');
    });
}


function unlockData(ctrl, lockRowId) {
    $.ajax({
        method: "POST",
        url: getUrl({'r': ctrl + '/unlock'}),
        data: {id: lockRowId}
    }).done(function() {
        clearTimeout(timerLockId);
        $('.offline').addClass('hidden');
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
}

$('#viewDeleted').on('click', function(e) {
    var tree = $('#jstree-product').jstree(true);
    var keyViewDeleted = $(e.currentTarget).find('.fa');
    keyViewDeleted.toggleClass('fa-eye-slash').toggleClass('fa-eye');
    if(deletedNodes.length > 0) {
        if (keyViewDeleted.hasClass('fa-eye')) {
            deletedNodes.forEach(function (item) {
                tree.show_node(item);
            });
        } else {
            deletedNodes.forEach(function (item) {
                tree.hide_node(item);
            });
        }
    }
});


function resetForm(form) {
    form.find('input').each(function() {
        $(this).val('');
    });
    form.find('.variable').each(function() {
        $(this).text('');
    });
    form.find("input[type=\"checkbox\"]").each(function() {
        if($(this).prop('checked')) {
            $(this).click();
        }
    });
    form.find(".requiredValue").each(function() {
        $(this).closest('.form-group').removeClass('has-error');
    });
}


$('.check-wDay').on('click', function(e) {
    var wDayDelivery = $(e.currentTarget).closest('.form-group');
    wDayDelivery.find('.deliveryTime').toggleClass('hidden');
    wDayDelivery.find('input[type="time"]').toggleClass('requiredValue');
});

$('#prepayment').on('click', function(e) {
    $('#paymentDelay input').toggleClass('requiredValue').prop('disabled', function(i, v) { return !v; });
});



$('.saveTraderBtn').on('click', function(e) {

    var error = false;

    var form = $(e.currentTarget).closest('.modal').find('form');
    var ctrl = $(e.currentTarget).data('ctrl');

    form.find('.form-group').removeClass('has-error');
    form.find(".requiredValue").each(function(key, item) {
        if(!$(item).val()) {
            error = true;
            $(item).closest('.form-group').addClass('has-error');
        } else if(!error) {
            $(item).closest('.form-group').removeClass('has-error');
        }
    });

    event.preventDefault();

    if(error) {
        $('#traderModal').animate({scrollTop: 0}, 'slow');
        return false;
    }

    var data = form.serializeArray();
    console.log( data );
    $.ajax({
        method: "POST",
        url: getUrl({'r': ctrl + '/save'}),
        data: data
    }).done(function( data ) {
        if(data.error  !== undefined && data.error !== 1) {
            unlockData(ctrl, data.content.id);
            $(e.currentTarget).closest('.modal').modal('hide');
            if(data.has_child === undefined) {
//                alert( "Data Saved: " + JSON.stringify(data.content) );
                window.location.reload();
            }
        } else {
            alert("Data Saved Error: " + JSON.stringify(data.content));
        }
    });
});

$('.saveObjTraderBtn').on('click', function(e) {

    var error = false;

    var form = $(e.currentTarget).closest('.modal').find('form');
    var ctrl = $(e.currentTarget).data('ctrl');

    form.find('.form-group').removeClass('has-error');
    form.find(".requiredValue").each(function(key, item) {
        if(!$(item).val()) {
            error = true;
            $(item).closest('.form-group').addClass('has-error');
        } else if(!error) {
            $(item).closest('.form-group').removeClass('has-error');
        }
    });

    event.preventDefault();

    if(error) {
        return false;
    }

    var data = form.serializeArray();
    console.log( data );
    $.ajax({
        method: "POST",
        url: getUrl({
            'r': ctrl + '/save',
            'obj': url.searchParams.get("for") || obj
        }),
        data: data
    }).done(function( data ) {
        if(data.error  !== undefined && data.error !== 1) {
            unlockData(ctrl, data.content.id);
            $(e.currentTarget).closest('.modal').modal('hide');
            if(data.has_child === undefined) {
//                alert( "Data Saved: " + JSON.stringify(data.content) );
                window.location.reload();
            }
        } else {
            alert("Data Saved Error: " + JSON.stringify(data.content));
        }
    });
});
