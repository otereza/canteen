"use strict";

var deletedNodes = [];
var searchRes = {};
var url = new URL((''+document.location));
var obj = url.searchParams.get("obj");

var lastClick = 0;

var jstreeOptions = {
    "load_all": true,
    "core" : {
        "data" : {
            "url" : function (node) {
                return getUrl({
                    'obj' : 'all',
                    'r': dataUrl,
                    'trader_id': $("#trader").val(),
                    'for': url.searchParams.get("for") || obj
                });
            }
        },
        "check_callback" :  function (op, node, par, pos, more) {
            if ((op === "move_node" || op === "copy_node") && more.dnd && node.type && node.type === 'pack' && node.parent && node.parent !== more.ref.parent) {
                return false;
            }
            return true;
        },
        'animation': 0,
        'themes': { 'dots': true },
        'keyboard' : {
            'f2': function (e) {
            }
        }
    },
    "plugins": ["grid","contextmenu","dnd","unique","types"],
    'grid': {
        'columns': [
            {
                'width': 'auto',
                'minWidth': '330px',
                'header': "&nbsp;"},
            {
                'width': 'auto',
                'minWidth': '100px',
                'header': "Цена",
                'value': function(data) {
                    if(data.type === 'pack') {
                        return data.data.price ? (data.data.price * window.priceCoef).toFixed(2) : '';
                    }
                    return '';
                },
                'wideCellClass': 'priceValue'
            },
            {
                'width': 'auto',
                'minWidth': '100px',
                'header': "Поставщик",
                'title': "Цена этой же Торговой марки и этой же Фасовки от другого поставщика",
                'value': 'otherTrader',
                'wideCellClass': 'priceInfo'
            },
            {
                'width': 'auto',
                'minWidth': '100px',
                'header': "Фасовка",
                'title': "Цена этой же Торговой марки (пересчитанная на объем этой фасовки)",
                'value': 'otherPack',
                'wideCellClass': 'priceInfo'
            },
            {
                'width': 'auto',
                'minWidth': '100px',
                'header': "Вид",
                'title': "Цена этого же Вида товара (пересчитанная на объем этой фасовки)",
                'value': 'thisType',
                'wideCellClass': 'priceInfo'
            },
            {
                'width': 'auto',
                'minWidth': '100px',
                'header': "Минимум",
                'title': "Цена этого же Вида товара (пересчитанная на объем этой фасовки)",
                'value': 'min',
                'wideCellClass': 'priceInfo'
            },
            {
                'width': 'auto',
                'minWidth': '100px',
                'header': "Максимум",
                'title': "Цена этого же Вида товара (пересчитанная на объем этой фасовки)",
                'value': 'max',
                'wideCellClass': 'priceInfo'
            }
        ],
        'resizable': true,
        'draggable': false,
        'gridcontextmenu': false,
        'headerContextMenu': false,
        'width': '100%'

    },
    'open_node': '#branch_1',
    "types": {
        'root': {
            'valid_children': ['group'],
            'children_type': "group",
            "check_hide" : true,
            'add_text': "Новая группа",
            'addFuncName': 'Group'
        },
        'group': {
            'valid_children': ['type'],
            'children_type': "type",
            'add_text': "Новый вид",
            'addFuncName': 'Type',
            'otherFuncName': 'Group',
            'icon': 'fa fa-envira c1'
        },
        'type': {
            'valid_children': ['tmark'],
            'children_type': "tmark",
            'add_text': "Новая торговая марка",
            'addFuncName': 'TMark',
            'otherFuncName': 'Type',
            'icon': 'fa fa-tag c2'
        },
        'tmark': {
            'valid_children': ['pack'],
            'children_type': "pack",
            'add_text': "Новая упаковка",
            'addFuncName': 'Pack',
            'otherFuncName': 'TMark',
            'icon': 'fa fa-trademark c3'
        },
        'pack': {
            'max_children': 0,
            'add_text': "Новая упаковка",
            'otherFuncName': 'Pack',
            'icon': 'fa fa-cube c4'
        }
    },
    "unique" : {
        "error_callback" : function (n, p, f) {
            alert("Duplicate node `" + n + "` with function `" + f + "`!");
        }
    }

};

jstreeOptions.core.check_callback = false;
jstreeOptions.plugins = ["grid","unique","types", "search"];
jstreeOptions.search = {show_only_matches: true, show_only_matches_children: true};

$(document).ready(function() {
    window.tmpOrder = new DocStorage('order');
    window.priceCoef = tmpOrder.getDoc()['priceCoef'] || 1;

    if($.jstree) {
        $('#jstree-product').jstree(jstreeOptions)
            .bind("move_node.jstree", jstreeMoveNode)
            .on("after_open.jstree", jstreeAfterOpen)
            .on("redraw.jstree", jstreeRedraw)
            .on('refresh.jstree', jstreeRefresh)
            .on('ready.jstree', jstreeReady)
            .on("click.jstree", jstreeClick)
            .on("search.jstree", jstreeSearch);
    }

    $('#viewDeleted').addClass('hidden');
});

function jstreeMoveNode(e, data) {
    var tree = data.instance;
    var funcName = 'changePos' + tree.settings.types[data.node.type].otherFuncName;
    eval(funcName)(data.node, data.parent, data.position);
}

function jstreeAfterOpen(e, data) {
    var t = setTimeout(function () {
        if (typeof treeWithCheckboxes === 'undefined' || !treeWithCheckboxes) {
            $('.p-check-box').remove();
        }
        setCeelParams(data);
        t = null;
    }, 10);
}

function jstreeRedraw(e, data) {
    var tree = data.instance;
    if (typeof treeWithCheckboxes === 'undefined' || !treeWithCheckboxes) {
        $('.p-check-box').remove();
    }
    setCeelParams(data);
}

function jstreeRefresh(e, data) {
    var tree = $('#jstree-product').jstree(true);
    if ($('#viewDeleted .fa').hasClass('fa-eye') && deletedNodes.length > 0) {
        deletedNodes.forEach(function (item) {
            tree.show_node(item);
        });
    } else {
        deletedNodes.forEach(function (item) {
            tree.hide_node(item);
        });
    }
}

function jstreeReady(e, data) {
    var tree = data.instance;
    Object.values(tree._model.data).forEach(function (item) {
        if (item.data && item.data.deleted == '1') {
            deletedNodes.push(item.id);
            tree.hide_node(item.id);
            item.li_attr.class = 'deleted';
            $('#' + item.id).removeClass('hidden');
        }
        if (item.type === 'pack' && item.data) {
            if (item.data.price && item.data.price > 0) {
                item.parents.forEach(function (parent) {
                    var node = tree.get_node(parent);
                    tree.open_node(node);
                });
            } else {
                tree.hide_node(item.id);
            }
        }
    });
}

function jstreeClick(e) {
    var time = new Date().getTime();
    if (time - lastClick < 400) {
        lastClick = 0;
        addNewRow();
    } else {
        lastClick = time;
    }
}

function jstreeSearch(e, data) {
    var words = $('#prodName').val().replace(/[^a-zA-Zа-яА-Я0-9%]+/g, ' ').trim().split(" ").slice(1);
    if (words.length && data.res.length) {
        data.res.forEach(function (item) {
            words.forEach(function (value) {
                var searchResEl = eval('searchRes.' + item + '_' + value);
                if (searchResEl === undefined) {
                    searchRes[item + '_' + value] = true;
                    data.instance.search(value, false, true, item, true);
                }
            });
        });
    }
}



function setCeelParams(data) {
    var tree = data.instance;
    if(!data.node) {
        data.node = { children_d: Object.keys(data.instance._model.data) };
    }
    data.node.children_d.forEach(function (item) {
        var node = tree.get_node(item);
        if (node.type === 'pack' && node.data && node.data.price && node.data.main_price) {

            // раскраска ячеек
            // var priceCeel = $('.jstree-grid-col-1[data-jstreegrid="' + node.id + '"]').removeClass('bgC3').removeClass('bgC6');
            // if(node.data.price > node.data.main_price) {
            //     priceCeel.addClass('bgC3');
            // } else if(node.data.price < node.data.main_price) {
            //     priceCeel.addClass('bgC6');
            // }

            // title для колонок минимум / максимум для обьектов
            if(typeof businessobj !== 'undefined' && node.data.min_obj !== '-' && node.data.max_obj !== '-') {
                $('.jstree-grid-col-5[data-jstreegrid="' + node.id + '"]').prop('title', eval('businessobj.' + node.data.min_obj));
                $('.jstree-grid-col-6[data-jstreegrid="' + node.id + '"]').prop('title', eval('businessobj.' + node.data.max_obj));
            }
        }
    });

    // title для колонок
    $('.jstree-grid-column-1 .jstree-grid-header').attr('title', "Цена со скидкой");
    $('.jstree-grid-column-2 .jstree-grid-header').attr('title', "Цена этой же Торговой марки и этой же Фасовки \nот другого поставщика");
    $('.jstree-grid-column-3 .jstree-grid-header').attr('title', "Цена этой же Торговой марки \n(пересчитанная на объем этой фасовки)");
    $('.jstree-grid-column-4 .jstree-grid-header').attr('title', "Цена этого же Вида товара \n(пересчитанная на объем этой фасовки)");
    $('.jstree-grid-column-5 .jstree-grid-header').attr('title', "Минимальная цена для объектов");
    $('.jstree-grid-column-6 .jstree-grid-header').attr('title', "Максимальная цена для объектов");
}



$('#orderStateTab a').on('click', function(e) {
    e.preventDefault();
    $(this).tab('show');
});

$('.viewOrder').on('click', function(e) {
    var urlParams = {'r': 'order/edit'};
    urlParams.id = $(e.currentTarget).data('id');
    if (obj === 'all' ) {
        urlParams.for = $(e.currentTarget).data('obj');
    }
    window.location.href = getUrl(urlParams);

});

$('#obj').on('change', changeObj);

function changeObj(e) {
    var curObj = $(e.currentTarget).val();
    var traderList = $('#trader');
    traderList.find('option:not(".hidden")').remove();

    $.ajax({
        method: "POST",
        url: getUrl({'r': 'trader/getTradersForObj', 'obj': curObj})
    }).done(function(data) {
        if(data.error !== undefined && !data.error) {
            Object.values(data.content).forEach(function(item) {
                traderList.append('<option value="'+ item.id +'">' + item.internal_name + '</option>');
            });

        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
}

$('.addOrderBtn').on('click', function() {
    var trader = $('#trader');
    var traderId = trader.val();
    var objName = $('#obj').val();

    if(+traderId) {
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'order/new'}),
            data: {
                obj: (!!objName) ? objName : obj,
                trader_id: traderId
            }
        }).done(function(data) {
            if(data.error !== undefined && !data.error) {
                var urlParams = {'r': 'order/edit', 'id': data.content.id};
                if(!!objName) {
                    urlParams.for = objName;
                }
                window.location.href = getUrl(urlParams);
            }
        }).fail(function() {
            $('.offline').removeClass('hidden');
        });
    } else {
        trader.closest('.form-group').addClass('has-error').find('.help-block').removeClass('hidden');
    }
});


$('#dateShipment').on('change', function(e) {
    $(e.currentTarget).removeClass('bgC6');
    var deliveryTime = $('.deliveryTime').val('').removeClass('bgC6');
    var curOption = $(e.currentTarget).find(':selected');
    if(curOption.data('from') && curOption.data('to')) {
        $(e.currentTarget).addClass('bgC6');
        $(e.currentTarget).find('option:not(.bgC6)').addClass('bgC0');
        $('#timeFrom').val(curOption.data('from')).data('original', curOption.data('from'));
        $('#timeTo').val(curOption.data('to')).data('original', curOption.data('to'));
        deliveryTime.addClass('bgC6');
//        console.log('change date');
        saveOrderInfo();
    }
});

$('#timeFrom, #timeTo').on('change', function (e) {
    var curEl = $(e.currentTarget);
    var originalValue = curEl.data('original');
    if(originalValue !== curEl.val()) {
        curEl.removeClass('bgC6');
    } else {
        curEl.addClass('bgC6');
    }
}).on('focusout', function (e) {
    e.stopPropagation();
//    console.log('change time');
    saveOrderInfo();
});

function saveOrderInfo() {
    var date = $('#dateShipment').val();
    var timeFrom = $('#timeFrom').val();
    var timeTo = $('#timeTo').val();
    if(!!date && !!timeFrom && !!timeTo) {
        tmpOrder.saveDocInfo({
            date_shipment: date,
            delivery_time: { from: timeFrom, to: timeTo }
        });
    }

}

// перетаскивать только за якорь
$(function() {
    sortableRows();
});

function sortableRows() {
    $('.prodList tbody').sortable({
        items: 'tr:not(#newRow)',
        cursor: "move",
        handle: 'td.dragAnchor',
        stop: function(e) {
            var rows = $('.prodList').find('.prodListItem:not(.hidden)');
            rows.each(function (key, el) {
                var rowId = $(el).data('id');
                tmpOrder.saveRow(rowId, { pos: key + 1 });
            });
        }
    });
}

var to = false;
$('#prodName').on('click', clickProdName).keyup(keyupProdName);

function clickProdName(e) {
    $('#jstreePanel').removeClass('hidden').offset({ top: $(e.currentTarget).offset().top + $(e.currentTarget).outerHeight() + 5 });
}

function keyupProdName() {
    if(to) { clearTimeout(to); }
    to = setTimeout(function () {
        var tree = $('#jstree-product').jstree(true);
        var str = $('#prodName').val().replace(/[^a-zA-Zа-яА-Я0-9%]+/g, ' ').trim().split(" ");
        if(str.length  && str[0].length > 2) {
            searchRes = {};
            tree.search(str[0]);
        } else {
            tree.clear_search();
            tree.show_all();
            deletedNodes.forEach(function(item) {
                tree.hide_node(item);
            });
        }
    }, 100);
}

$('.priceText').on('click', clickPrice);


var priceLastClick = 0;
var priceFirstClickTimer = null;

function clickPrice(e) {
    var time = new Date().getTime();
    var priceLabel = $(e.currentTarget).siblings('.priceLabel');
    var inputPrice = $(e.currentTarget).siblings('.price');
    var originPrice = (+inputPrice.data('origin')).toFixed(2);
    var prevPriceValue = +inputPrice.val();
    var row = $(e.currentTarget).closest('tr.prodListItem');
    var unit = row.find('.packUnit').val();
    var amount = row.find('.amount');
    var unitValue = +amount.data('unit-value');


    if (time - priceLastClick < 400) {
        priceLastClick = 0;
        $(e.currentTarget).addClass('hidden');
        inputPrice.removeClass('hidden').val((+inputPrice.data('user')).toFixed(2)).focus();
        priceLabel.removeClass('hidden');
        inputPrice.one('focusout', function (e) {
            var newTraderPrice  = +$(e.currentTarget).val();
            var priceText = $(e.currentTarget).siblings('.priceText');
            priceText.removeClass('bgC6').removeClass('bgC3');
            $(e.currentTarget).addClass('hidden').data('user', newTraderPrice);
            priceLabel.addClass('hidden');
            priceText.removeClass('hidden');

            var newPrice = ((newTraderPrice * priceCoef * 100000).toFixed() / 100000);
            if(newTraderPrice > originPrice) {
                priceText.addClass('bgC3');
            } else if(newTraderPrice < originPrice) {
                priceText.addClass('bgC6');
            }
            if (packUnits.includes(unit)) {
                newPrice = (newPrice / unitValue);
                priceText.text(newPrice.toFixed(unitValue.toString().length - 1 + 2).replace(/0+$/,''));     // Если будет маленькое число);
            } else {
                priceText.text(newPrice.toFixed(2));     // Если будет маленькое число);
            }

            var rowId = +row.data('id');
            if(newTraderPrice != prevPriceValue) {
                calcRowPrice(row);
                tmpOrder.saveRow(rowId, {
                    trader_price_user : newTraderPrice,
                    price : newPrice
                });

                // меняем цены во всех строках таблицы с таким товаром
                var items = tmpOrder.findRows('pack_id', row.data('pack-id'));
                if(items.length > 0) {
                    items.forEach(function(item) {
                        if(item.id != rowId) {
                            var itemRow = $('.prodListItem').eq(item.pos - 1);
                            var priceText = itemRow.find('.priceText');
                            var priceInput = itemRow.find('.price');
                            var itemPrice = newPrice;

                            priceInput.data('origin', Number(originPrice).toFixed(2));
                            priceInput.data('user', Number(newTraderPrice).toFixed(2));
                            priceInput.val(Number(newTraderPrice).toFixed(2));

                            priceText.removeClass('bgC6').removeClass('bgC3');
                            if (newTraderPrice > originPrice) {
                                priceText.addClass('bgC3');
                            } else if (newTraderPrice < originPrice) {
                                priceText.addClass('bgC6');
                            }

                            if (packUnits.includes(item.amount_unit)) {
                                itemPrice = (unit === item.amount_unit) ? newPrice : newPrice / unitValue;
                                priceText.text(itemPrice.toFixed(unitValue.toString().length - 1 + 2).replace(/0+$/, ''));     // Если будет маленькое число);
                            } else {
                                itemPrice = (unit === item.amount_unit) ? newPrice : newPrice * unitValue;
                                priceText.text(itemPrice.toFixed(2));
                            }

                            calcRowPrice(itemRow);
                            tmpOrder.saveRow(item.id, {
                                trader_price_user: newTraderPrice,
                                price: itemPrice,
                                unit_value: unitValue
                            });
                        }
                    });
                }
            }
        });
        clearTimeout(priceFirstClickTimer);
        priceFirstClickTimer = null;
    } else {
        priceLastClick = time;
        if(priceFirstClickTimer === null) {
            var event = e;
            priceFirstClickTimer = setTimeout(function () {
                var curVal = (+inputPrice.val()).toFixed(2);
                var userVal = (+inputPrice.data('user')).toFixed(2);
                var origVal = (+inputPrice.data('origin')).toFixed(2);
                var newPrice, newTraderPrice;
                $(event.currentTarget).removeClass('bgC6').removeClass('bgC3');
                if (curVal === userVal) {
                    inputPrice.val(origVal);
                    newPrice = ((origVal * priceCoef * 100000).toFixed() / 100000);
                    newTraderPrice = origVal;
                } else {
                    inputPrice.val(userVal);
                    if(userVal > origVal) {
                        $(event.currentTarget).addClass('bgC3');
                    } else if(userVal < origVal) {
                        $(event.currentTarget).addClass('bgC6');
                    }
                    newPrice = ((userVal * priceCoef * 100000).toFixed() / 100000);
                    newTraderPrice = userVal;
                }

                if (packUnits.includes(unit)) {
                    newPrice = (newPrice / unitValue);
                    $(event.currentTarget).text(newPrice.toFixed(unitValue.toString().length - 1 + 2).replace(/0+$/,''));     // Если будет маленькое число);
                } else {
                    $(event.currentTarget).text(newPrice.toFixed(2));     // Если будет маленькое число);
                }

                if(userVal !== origVal) {
                    calcRowPrice(row);
                    tmpOrder.saveRow(row.data('id'), {
                        trader_price_user : newTraderPrice,
                        price : newPrice
                    });
                }

                priceFirstClickTimer = null;
            }, 400);
        }
    }
}

$('.price').on('changed', changedPrice).on('keypress', enterBtnPress);

function changedPrice(e) {
    var curPrice = $(e.currentTarget).val();
    $(e.currentTarget).data('user', curPrice);
}

function enterBtnPress(e) {
    if (e.which == null) { // IE
        if(e.keyCode === 13) {
            $(e.currentTarget).blur();
        }
    }

    if (e.which !== 0 && e.charCode !== 0) { // все кроме IE
        if(e.which === 13) {
            $(e.currentTarget).blur();
        }
    }
}

function addNewRow() {
    var tree = $('#jstree-product').jstree(true);
    var nodeId = tree.get_selected()[0];
    var node = tree.get_node(nodeId);
    if (node.type === 'pack' && node.data.price > 0) {
        var tmarkNode = tree.get_node(node.parent);
        var packName = node.data.by_weight === '0'
            ? node.data.name + ' (' + node.data.value + node.data.pack_unit + ')'
            : node.data.name + ' (' + node.data.pack_unit + ', на вес)';
        var packUnit = node.data.by_weight === '0'
            ? node.data.value + node.data.pack_unit
            : node.data.pack_unit;
        var tmarkName = tmarkNode.data.name;
        var prodName = tmarkName + ', ' + packName;
        var lastRow = $('#newRow').prev('tr');
        var newRow = lastRow.clone();

        if (node.data.id) {
            var userPrice = getUserPriceFromPresentPosition(node.data.id, node.data.price);
            $.ajax({
                method: "POST",
                url: getUrl({'r': 'order/saveProd'}),
                data: {
                    order_id: tmpOrder.doc.id,
                    pos: lastRow.index() + 1,
                    prod_name: prodName,
                    tmark_id: tmarkNode.data.id,
                    pack_id: node.data.id,
                    unit: node.data.pack_unit,
                    pack_unit: packUnit,
                    amount_unit: packUnit,
                    amount: 1,
                    price: ((userPrice * priceCoef * 100000).toFixed() / 100000).toFixed(2)
                }
            }).done(function (data) {
                if (data.error !== undefined && !data.error) {
                    data.content.trader_price = node.data.price;
                    newRow.removeClass('hidden');
                    newRow.find('.price').val(userPrice).data('origin', data.content.trader_price).data('user', userPrice)
                        .on('changed', changedPrice).on('keypress', enterBtnPress);
                    newRow.find('.priceText').text(data.content.price).on('click', clickPrice);
                    if(userPrice > data.content.trader_price) {
                        newRow.find('.priceText').addClass('bgC3');
                    } else if(userPrice < data.content.trader_price) {
                        newRow.find('.priceText').addClass('bgC6');
                    }
                    newRow.find('.onlyUnSignFloatNumbers').on('keypress', onlyUnSignFloatNumbers);
                    newRow.data('row-price', data.content.price);
                    newRow.find('.amount').val(1)
                        .data('unit-value', node.data.value || 1)
                        .on('focusin', changeAmount).on('keypress', enterBtnPress);
                    newRow.find('.packUnitText').text(packUnit);
                    newRow.find('.packUnit').val(packUnit).data('pack-unit', packUnit);
                    newRow.data('id', data.content.id);
                    newRow.data('pack-id', data.content.pack_id);

                    var unitList = '<li><a href="#">' + data.content.unit + '</a></li>';
                    var needAddPackUnit = true;
                    packUnits.forEach(function (item) {
                        if (item === packUnit) {
                            needAddPackUnit = false;
                        }
                    });
                    if (needAddPackUnit) {
                        unitList += '<li><a href="#">' + packUnit + '</a></li>';
                    }
                    newRow.find('.prodName').text(prodName);
                    newRow.find('.unitList').append(unitList);
                    newRow.find('.unitList a').on('click', changeUnit);
                    newRow.find('.delOrderProdBtn').on('click', delOrderProd);

                    $('.prodListItem.hidden').before(newRow);
                    flashBackground(newRow, '#dbffdb');

                    tree.clear_search();
                    tree.show_all();
                    deletedNodes.forEach(function (item) {
                        tree.hide_node(item);
                    });

                    data.content.trader_price_main = prices[data.content.pack_id]['trader_price_main'];
                    data.content.trader_price_user = prices[data.content.pack_id]['trader_price'];
                    tmpOrder.saveRow(data.content.id, data.content);
                    calcOrderTotalPrice();
                }
            }).fail(function () {
                $('.offline').removeClass('hidden');
            });
        }

        $('#prodName').val('');
        $('#jstreePanel').addClass('hidden');
    }
}

function getUserPriceFromPresentPosition(packId, price) {
    var presentPrice = false;
    var rows = tmpOrder.findRows('pack_id', packId);
    if(rows.length > 0) {
        for (var key in rows) {
            presentPrice = rows[key].trader_price_user;
            break;
        }
    }
    return presentPrice || price;
}


$(document).on('click', function(e) {
    var target = $(e.target);
    if(target.closest('.jstree-grid-wrapper').length === 0 && target.attr('id') !== 'prodName') {
        $('#jstreePanel').addClass('hidden');
    }
});

$('.unitList a').on('click', changeUnit);
function changeUnit(e) {
    e.preventDefault();
    var unit = $(e.currentTarget).text();
    var row = $(e.currentTarget).closest('tr.prodListItem');
    var packUnitVal = row.find('.packUnit').val();
    if(packUnitVal !== unit) {
        row.find('.packUnitText').text(unit);
        row.find('.packUnit').val(unit);

        var amount = row.find('.amount');
        var amountValue = +amount.val();
        var unitValue = +amount.data('unit-value');
        var curTraderPriceWithDiscount = ((row.find('.price').val() * priceCoef * 100000).toFixed() / 100000).toFixed(2);
        var newAmount, newPrice;

        if (packUnits.includes(unit)) {
            var roundTo = units[unit]['decimal_places'] || 0;
            newAmount = (amountValue * unitValue).toFixed(roundTo);
            var minimalAmount = 1 / Number(1 + '0'.repeat(roundTo));
            newAmount = (newAmount > 0) ? newAmount : minimalAmount;
            newPrice = curTraderPriceWithDiscount / unitValue;
            row.find('.priceText').text(newPrice.toFixed(unitValue.toString().length - 1 + 2));      // Если будет маленькое число
        } else {
            newAmount = (amountValue / unitValue).toFixed(0);
            newAmount = (newAmount > 0) ? newAmount : 1;
            newPrice = +curTraderPriceWithDiscount;
            row.find('.priceText').text(newPrice.toFixed(2));
        }
        amount.val(newAmount);

        calcRowPrice(row);
        tmpOrder.saveRow(row.data('id'), {
            amount_unit : unit,
            amount : newAmount,
            price : newPrice
        });
    }
}

$('.amount').on('focusin', changeAmount).on('keypress', enterBtnPress);
function changeAmount(e) {
    var prevValue = Number($(e.currentTarget).val());
    var row = $(e.currentTarget).closest('tr.prodListItem');
    $(e.currentTarget).one('focusout', function(e) {
        var newValue = Number($(e.currentTarget).val());
        if(prevValue !== newValue) {
            var unit = row.find('.packUnit').val();
            var roundTo = (units[unit] && units[unit]['decimal_places']) ? units[unit]['decimal_places'] : 0;
            newValue = newValue.toFixed(roundTo);
            $(e.currentTarget).val(newValue);

            calcRowPrice(row);
            tmpOrder.saveRow(row.data('id'), { amount : newValue });
        }
    });
}

$('.delOrderProdBtn').on('click', delOrderProd);
function delOrderProd(e) {
    var row = $(e.currentTarget).closest('.prodListItem');
    var rowId = row.data('id');
    $.ajax({
        method: "POST",
        url: getUrl({'r': 'order/delProd'}),
        data: {id: rowId}
    }).done(function(data) {
        if(data.error !== undefined && !data.error) {
            row.remove();
            tmpOrder.delRow(rowId);
            calcOrderTotalPrice();
        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
}

function calcRowPrice(row) {
    var unit = row.find('.packUnitText').text();
    var amount = row.find('.amount');
    var price = +row.find('.price').val();
    var amountValue = +amount.val();
    var unitValue = +amount.data('unit-value');
    var newPrice = ((price * priceCoef * 100000).toFixed() / 100000).toFixed(2);
    if (packUnits.includes(unit)) {
        row.data('row-price', amountValue * (newPrice / unitValue).toFixed(unitValue.toString().length - 1 + 2));
    } else {
        row.data('row-price', (amountValue * newPrice).toFixed(2));
    }

    calcOrderTotalPrice();
}

function calcOrderTotalPrice() {
    var rows = $('.prodList').find('.prodListItem:not(.hidden)');
    var orderTotalPrice = 0;
    rows.each(function(key, row) {
        orderTotalPrice += +$(row).data('row-price');
    });
    $('#orderTotalPrice').text(orderTotalPrice.toFixed(2));
    tmpOrder.doc.total = orderTotalPrice.toFixed(2);
}

$('#toTraderBtn').on('click', function (e) {
    var href = $(e.currentTarget).data('href');
    order.preSave = 1;
    $.ajax({
        method: "POST",
        url: getUrl({'r': 'order/save'}),
        data: order
    }).done(function(data) {
        if(data.error !== undefined && !data.error) {
            window.location.href = href;
        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
});

$('#saveOrderBtn').on('click', function (e) {
    var form = $('#traderForm');
    var hasError = false;
    form.find('.input-group').removeClass('has-error');
    var changedPriceRows = tmpOrder.getChangedPriceRows();

    if(!order.date_shipment) {
        form.find('#dateShipment').closest('.input-group').addClass('has-error');
        hasError = true;
    }
    if(!order.delivery_time || order.delivery_time.from === '') {
        form.find('#timeFrom').closest('.input-group').addClass('has-error');
        hasError = true;
    }
    if(!order.delivery_time || order.delivery_time.to === '') {
        form.find('#timeTo').closest('.input-group').addClass('has-error');
        hasError = true;
    }
    if(!order.total || order.total == 0) {
        hasError = true;
    }
    if(hasError) {
        return;
    }

    $.ajax({
        method: "POST",
        url: getUrl({'r': 'order/save'}),
        data: order
    }).done(function(data) {
        if(data.error !== undefined && !data.error) {
            if(Object.keys(changedPriceRows).length > 0) {
                if (obj === 'all') {
                    if (permit && permit.canChangePriceProduct) {
                        var forObj = url.searchParams.get("for");
                        changeTraderPrice(obj, changedPriceRows, "Изменить общий прайс поставщика?", forObj);
                    }
                } else if (permit && permit.canChangePriceProduct/* && confirm("Изменить прайс поставщика?")*/) {
                    changeTraderPrice(obj, changedPriceRows, "Изменить прайс поставщика?");
                } else {
                    window.location.href = getUrl({'r': 'order', 'tab': 'work'});
                }
            } else {
                window.location.href = getUrl({'r': 'order', 'tab': 'work'});
            }
        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });

});

// изменение прайса поставщика для определенного обьекта
function changeTraderPrice(obj, orderRows, title, forObj) {
    var priceModal = $('#changedPriceModal');
    if (Object.keys(orderRows).length > 0) {
        var modalTitle = priceModal.find('.modal-title');
        modalTitle.text(title);
        flashBackground(modalTitle, '#aaaaff');
        var priceTable = $('#priceTable');
        var priceTableBody = priceTable.find('tbody');
        priceTableBody.empty();
        for(var key in orderRows) {
            var item = orderRows[key];
            var oldPrice = (+((obj == 'all') ? item.trader_price_main : item.trader_price)).toFixed(2);
            var newPrice = (+item.trader_price_user).toFixed(2);
            var bg = (newPrice > oldPrice) ? 'bgC3' : 'bgC6';
            var priceItem = '<tr class="priceItem">' +
                '<td>' + item.prod_name.replace(/\0/g, '0').replace(/\\(.)/g, "$1") + '</td>' +
                    '<td class="text-right">' + (+((obj == 'all') ? item.trader_price_main : item.trader_price)).toFixed(2) + '</td>' +
                    '<td class="text-right ' + bg + '">' + (+item.trader_price_user).toFixed(2) + '</td>' +
                '</tr>';
            priceTableBody.append(priceItem);
        }

        priceModal.modal('show').on('hide.bs.modal', function() {
            $(this).find('.priceItem').remove();
            window.location.href = getUrl({'r': 'order', 'tab': 'work'});
            $('#savePriceBtn').off('click');
//            $(this).off('hide.bs.modal');
        });

        if(obj == 'all' && forObj !== undefined && permit.canChangeObjPriceProduct) {
            $('#refusePriceBtn').one('click', function (e) {
                e.stopPropagation();
                modalTitle.text("Изменить прайс поставщика для обьекта?");
                flashBackground(modalTitle, '#aaaaff');
                priceTableBody.empty();
                for(var key in orderRows) {
                    var item = orderRows[key];
                    var oldPrice = (+((obj == 'all') ? item.trader_price_main : item.trader_price)).toFixed(2);
                    var newPrice = (+item.trader_price_user).toFixed(2);
                    var bg = (newPrice > oldPrice) ? 'bgC3' : 'bgC6';
                    var priceItem = '<tr class="priceItem">' +
                        '<td>' + item.prod_name.replace(/\0/g, '0').replace(/\\(.)/g, "$1") + '</td>' +
                        '<td class="text-right">' + (+item.trader_price).toFixed(2) + '</td>' +
                        '<td class="text-right ' + bg + '">' + (+item.trader_price_user).toFixed(2) + '</td>' +
                        '</tr>';
                    priceTableBody.append(priceItem);
                }
            });
        }

        $('#savePriceBtn').on('click', function (e) {
            $.ajax({
                method: "POST",
                url: getUrl({'r': 'trader/changePrice'}),
                data: {
                    trader_id: $('#trader').val(),
                    obj: obj,
                    items: orderRows
                }
            }).done(function (data) {
                if (data.error !== undefined && !data.error) {
                    priceModal.modal('hide');
                }
            }).fail(function () {
                $('.offline').removeClass('hidden');
            });
        });
    }
}
