
"use strict";

var deletedNodes = [];
var searchRes = {};
var url = new URL((''+document.location));
var obj = url.searchParams.get("obj");
var forObj = url.searchParams.get("for");
var docId = url.searchParams.get("id");

var lastClick = 0;

var jstreeOptions = {
    "load_all": true,
    "core" : {
        "data" : {
            "url" : function (node) {
                return getUrl({
                    'r': 'invoiceTransfer/getData',
                    'obj': forObj || obj
                });
            }
        },
        "check_callback" :  function (op, node, par, pos, more) {
            if ((op === "move_node" || op === "copy_node") && more.dnd && node.type && node.type === 'pack' && node.parent && node.parent !== more.ref.parent) {
                return false;
            }
            return true;
        },
        'animation': 0,
        'themes': { 'dots': true },
        'keyboard' : {
            'f2': function (e) {
            }
        }
    },
    "plugins": ["grid","contextmenu","dnd","unique","types"],
    'grid': {
        'columns': [
            {
                'width': 'auto',
                'minWidth': '330px',
                'header': "&nbsp;"
            },
            {
                'width': 'auto',
                'minWidth': '100px',
                'header': "Количество",
                'value':  function(data) {
                    if(data.type === 'pack') {
                        return data.data.amount ? data.data.amount : 'нет';
                    }
                    return '';
                }
            },
            {
                'width': 'auto',
                'minWidth': '100px',
                'header': "Цена",
                'value': function(data) {
                    if(data.type === 'pack') {
                        return data.data.price ? (+data.data.price).toFixed(2) : 0;
                    }
                    return '';
                },
                'wideCellClass': 'price'
            },
            {
                'width': 'auto',
                'minWidth': '130px',
                'header': "Срок годности",
                'title': "Срок годности",
                'value': function(data) {
                    if(data.data && data.data.exp_date) {
                        return formatDate(new Date(data.data.exp_date * 1000));
                    }
                    return '';
                },
                'cellClass': 'margin-r10'
            }
        ],
        'resizable': true,
        'draggable': false,
        'gridcontextmenu': false,
        'headerContextMenu': false,
        'width': '100%'

    },
    'open_node': '#branch_1',
    "types": {
        'root': {
            'valid_children': ['group'],
            'children_type': "group",
            "check_hide" : true,
            'add_text': "Новая группа",
            'addFuncName': 'Group'
        },
        'group': {
            'valid_children': ['type'],
            'children_type': "type",
            'add_text': "Новый вид",
            'addFuncName': 'Type',
            'otherFuncName': 'Group',
            'icon': 'fa fa-envira c1'
        },
        'type': {
            'valid_children': ['tmark'],
            'children_type': "tmark",
            'add_text': "Новая торговая марка",
            'addFuncName': 'TMark',
            'otherFuncName': 'Type',
            'icon': 'fa fa-tag c2'
        },
        'tmark': {
            'valid_children': ['pack'],
            'children_type': "pack",
            'add_text': "Новая упаковка",
            'addFuncName': 'Pack',
            'otherFuncName': 'TMark',
            'icon': 'fa fa-trademark c3'
        },
        'pack': {
            'max_children': 0,
            'add_text': "Новая упаковка",
            'otherFuncName': 'Pack',
            'icon': 'fa fa-cube c4'
        }
    },
    "unique" : {
        "error_callback" : function (n, p, f) {
            alert("Duplicate node `" + n + "` with function `" + f + "`!");
        }
    }

};

jstreeOptions.core.check_callback = false;
jstreeOptions.plugins = ["grid","unique","types", "search"];
jstreeOptions.search = {show_only_matches: true, show_only_matches_children: true};

$(document).ready(function() {
    window.tmpDoc = new DocStorage('invoice');
    window.priceCoef = tmpDoc.getDoc()['priceCoef'] || 1;

    if($.jstree) {
        $('#jstree-product').jstree(jstreeOptions)
            .bind("move_node.jstree", jstreeMoveNode)
            .on("after_open.jstree", jstreeAfterOpen)
            .on("redraw.jstree", jstreeRedraw)
            .on('refresh.jstree', jstreeRefresh)
            .on('ready.jstree', jstreeReady)
            .on("click.jstree", jstreeClick)
            .on("search.jstree", jstreeSearch);
    }

    $('#viewDeleted').addClass('hidden');

    var curDate = new Date();
    var minutes = curDate.getMinutes();
    var timePeriodList = $('#timePeriodList');
    var timePeriodText = $('#timePeriodText');
    (minutes >= 30) ? curDate.setMinutes(30) : curDate.setMinutes(0);
    for(var i=0; i < 48; i++ ){
        var timeFrom = formatTime(curDate);
        curDate.setMinutes(curDate.getMinutes() + 30);
        timePeriodList.append('<li><a href="#">' + timeFrom + ' - ' + formatTime(curDate) + '</a></li>');
        if(!i) {
            timePeriodText.text(timeFrom + ' - ' + formatTime(curDate));
        }
    }
    timePeriodList.find('li a').on('click', function(e) {
        var curEl = $(e.currentTarget);
        timePeriodText.text(curEl.text());
        tmpDoc.doc.inv_time_period = curEl.text();
        checkDeliveryTime();
    });

    checkDeliveryTime();
    calcDocTotalPrice();
    sortableRows();

});

function jstreeMoveNode(e, data) {
    var tree = data.instance;
    var funcName = 'changePos' + tree.settings.types[data.node.type].otherFuncName;
    eval(funcName)(data.node, data.parent, data.position);
}

function jstreeAfterOpen(e, data) {
    var t = setTimeout(function () {
        if (typeof treeWithCheckboxes === 'undefined' || !treeWithCheckboxes) {
            $('.p-check-box').remove();
        }
        t = null;
    }, 10);
}

function jstreeRedraw(e, data) {
    var tree = data.instance;
    if (typeof treeWithCheckboxes === 'undefined' || !treeWithCheckboxes) {
        $('.p-check-box').remove();
    }
}

function jstreeRefresh(e, data) {
    var tree = $('#jstree-product').jstree(true);
    if ($('#viewDeleted .fa').hasClass('fa-eye') && deletedNodes.length > 0) {
        deletedNodes.forEach(function (item) {
            tree.show_node(item);
        });
    } else {
        deletedNodes.forEach(function (item) {
            tree.hide_node(item);
        });
    }
}

function jstreeReady(e, data) {
    var tree = data.instance;
    Object.values(tree._model.data).forEach(function (item) {
        if (item.data && +item.data.deleted === 1) {
            deletedNodes.push(item.id);
            tree.hide_node(item.id);
            item.li_attr.class = 'deleted';
            $('#' + item.id).removeClass('hidden');
        }
        if (item.type === 'pack' && item.data) {
            if (item.data.price && item.data.price > 0) {
                item.parents.forEach(function (parent) {
                    var node = tree.get_node(parent);
                    tree.open_node(node);
                });
            } else {
                tree.hide_node(item.id);
            }
        }
    });
}

function jstreeClick(e) {
    var time = new Date().getTime();
    if (time - lastClick < 400) {
        lastClick = 0;
        addNewRow();
    } else {
        lastClick = time;
    }
}

function jstreeSearch(e, data) {
    var words = $('#prodName').val().replace(/[^a-zA-Zа-яА-Я0-9%]+/g, ' ').trim().split(" ").slice(1);
    if (words.length && data.res.length) {
        data.res.forEach(function (item) {
            words.forEach(function (value) {
                var searchResEl = eval('searchRes.' + item + '_' + value);
                if (searchResEl === undefined) {
                    searchRes[item + '_' + value] = true;
                    data.instance.search(value, false, true, item, true);
                }
            });
        });
    }
}


$('#invoiceStateTab a').on('click', function(e) {
    e.preventDefault();
    $(this).tab('show');
});


$(document).on('click', function(e) {
    var target = $(e.target);
    if(target.closest('.jstree-grid-wrapper').length === 0 && target.attr('id') !== 'prodName') {
        $('#jstreePanel').addClass('hidden');
    }
});

function sortableRows() {
    $('.prodList tbody').sortable({
        items: 'tr:not(#newRow)',
        cursor: "move",
        handle: 'td.dragAnchor',
        stop: function(e) {
            var rows = $('.prodList').find('.prodListItem:not(.hidden)');
            rows.each(function (key, el) {
                var rowId = $(el).data('id');
                tmpDoc.saveRow(rowId, { pos: key + 1 });
            });
        }
    });
}




// добавление новой накладной перемещения
$('#addInvoiceTransferBtn').on('click', function(e) {
    var objFrom = $('#objFrom').val();
    var objTo = $('#objTo').val();

    if(!objFrom || !objTo) {
        $(e.currentTarget).closest('.form-group').addClass('has-error').find('.help-block').removeClass('hidden');
        return false;
    }

    $.ajax({
        method: "POST",
        url: getUrl({'r': 'InvoiceTransfer/new'}),
        data: {
            objFrom: objFrom,
            objTo: objTo
        }
    }).done(function (data) {
        if (data.error !== undefined && !data.error) {
            var urlParams = {'r': 'InvoiceTransfer/edit', 'id': data.content.id};
            if(obj === 'all') {
                urlParams.for = objFrom;
            }
            window.location.href = getUrl(urlParams);
        }
    }).fail(function () {
        $('.offline').removeClass('hidden');
    });
});


$('.editInvoice').on('click', function(e) {
    var ctrl = $(e.currentTarget).closest('table').data('ctrl');
    var urlParams = {'r': ctrl + '/edit'};
    urlParams.id = $(e.currentTarget).data('id');
    if (obj === 'all' ) {
        urlParams.for = $(e.currentTarget).data('obj');
    }
    window.location.href = getUrl(urlParams);
});

$('.viewInvoice').on('click', function(e) {
    var ctrl = $(e.currentTarget).closest('table').data('ctrl');
    var urlParams = {'r': ctrl + '/view'};
    urlParams.id = $(e.currentTarget).data('id');
    if (obj === 'all' ) {
        urlParams.for = $(e.currentTarget).data('obj');
    }
    window.location.href = getUrl(urlParams);
});

$('#saveInvoiceBtn').on('click', function (e) {
    tmpDoc.doc.preSave = 1;
    saveInvoice();
});

$('#approveInvoiceBtn').on('click', function (e) {
    e.stopPropagation();
    tmpDoc.doc.approve = 1;
    $.ajax({
        method: "POST",
        url: getUrl({'r': 'invoiceTransfer/save', 'obj': forObj || obj}),
        data: tmpDoc.doc
    }).done(function(data) {
        if(data.error !== undefined && !data.error) {
            if(!tmpDoc.doc.canEditInvoiceObjTo) {
                alert("Накладная будет проведена только после подтверждения представителем второго объекта")
                window.location.href = getUrl({'r': 'invoiceTransfer'});
            } else {
                $.ajax({
                    method: "POST",
                    url: getUrl({'r': 'invoiceTransfer/approveDist'}),
                    data: {
                        'doc_id': tmpDoc.doc.id,
                        'obj_from': tmpDoc.doc.obj_from,
                        'obj_to': tmpDoc.doc.obj_to
                    }
                }).done(function(data) {
                    if(data.error !== undefined && !data.error) {
                        alert("Успешно");
                        window.location.href = getUrl({'r': 'invoiceTransfer'});
                    }
                }).fail(function() {
                    $('.offline').removeClass('hidden');
                });
            }
        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
});

$('.invoiceApproveToBtn').on('click', function (e) {
    e.stopPropagation();
    var curEl = $(e.currentTarget);
    $.ajax({
        method: "POST",
        url: getUrl({'r': 'invoiceTransfer/approveDist'}),
        data: {
            'doc_id': curEl.data('id'),
            'obj_from': curEl.data('obj-from'),
            'obj_to': curEl.data('obj-to')
        }
    }).done(function(data) {
        if(data.error !== undefined && !data.error) {
            alert("Успешно");
            window.location.href = getUrl({'r': 'invoiceTransfer'});
        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
});

function saveInvoice(href) {
    tmpDoc.doc.preSave = 1;
    $.ajax({
        method: "POST",
        url: getUrl({'r': 'invoiceTransfer/save', 'obj': forObj || obj}),
        data: tmpDoc.doc
    }).done(function(data) {
        if(data.error !== undefined && !data.error) {
            if(href) {
                window.location.href = href;
            } else {
                alert("Сохранено");
            }
        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
}

$('#inv_number').on('change', function (e) {
    tmpDoc.doc.inv_number = $(e.currentTarget).val();
});

$('#trader').on('change', function (e) {
    var traderId = +$(e.currentTarget).val();
    var objName = $('#obj').val() || obj;
    var orderList = $('#order');
    var invReceiptList = $('#invReceipt');
    var docList, route;

    if(orderList.length) {
        docList = orderList;
        route = 'order/getTraderOrders';
    } else if(invReceiptList.length) {
        docList = invReceiptList;
        route = 'invoiceReceipt/getTraderInvoice';
    } else {
        return false;
    }

    docList.find('option:not([value="0"])').remove();

    if(!traderId || !objName) {
        return;
    }
    $.ajax({
        method: "POST",
        url: getUrl({'r': route}),
        data: {
            trader_id: traderId,
            forObj: objName
        }
    }).done(function(data) {
        if(data.error !== undefined && !data.error && data.content.length > 0) {
            docList.find('option:not([value="0"])').remove();
            data.content.forEach(function (item) {
                docList.append('<option value="' + item.id + '">' + item.name + '</option>');
            });
        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
});


function scrollDateToActive(e) {
    var dropdown = $(e.currentTarget).siblings('.dateLifeList');
    var activeEl = dropdown.find('.active');
    setTimeout(function () {
        dropdown.scrollTop(0);
        dropdown.scrollTop(activeEl.offset().top - dropdown.offset().top);
    }, 100);
}


$('#prodName').on('click', clickProdName).keyup(keyupProdName);

function clickProdName(e) {
    $('#jstreePanel').removeClass('hidden').offset({ top: $(e.currentTarget).offset().top + $(e.currentTarget).outerHeight() + 5 });
}

$('#obj').on('change', changeObj);
$('.amount').on('focusin', changeAmount).on('keypress', enterBtnPress);
$('.delDocProdBtn').on('click', delDocProd);
$('#date').on('change', checkDeliveryTime );
$('.return').on('click', toggleReturn);
$('#reason').on('change', function(e) {
    tmpDoc.doc.reason = $(e.currentTarget).val();
});


function toggleReturn(e) {
    if(e.target.nodeName === 'TD') {
        var curRow = $(e.currentTarget).closest('.prodListItem');
        var rowId = curRow.data('id');
        var rowPrice = curRow.data('row-price');
        var inAmount = +curRow.find('.inAmount').text();
        var unitValue = +curRow.find('.amount').data('unit-value');
        var inAmountUnit = curRow.find('.inAmountUnit').text();
        var amountUnit = curRow.find('.packUnitText').text();
        var inPrice = +curRow.find('.inPrice').text();
        var newAmount;
        if(rowPrice > 0) {
            curRow.find('.amount').val(0);
            curRow.find('.packUnit').val(inAmountUnit);
            curRow.find('.packUnitText').text(inAmountUnit);
            curRow.find('.priceText').text(inPrice);

            tmpDoc.saveRow(rowId, {'amount':null, 'price':null, 'amount_unit':null});
        } else {
            if(inAmountUnit !== amountUnit) {
                var roundTo = (units[amountUnit] && units[amountUnit]['decimal_places']) ? units[amountUnit]['decimal_places'] : 0;
                if (packUnits.includes(amountUnit)) {
                    newAmount = (inAmount * unitValue).toFixed(roundTo);
                } else {
                    newAmount = (inAmount / unitValue).toFixed(0);
                }
            } else {
                newAmount = inAmount;
            }
            curRow.find('.amount').val(newAmount);
            tmpDoc.saveRow(rowId, {'amount':inAmount, 'price':inPrice, 'amount_unit':inAmountUnit});
        }
        calcRowPrice(curRow);
        curRow.find('.canHide').toggleClass('hidden');
    }
}

function changeObj(e) {
    var curObj = $(e.currentTarget).val();
    var traderList = $('#trader');
    traderList.find('option:not(".hidden")').remove();
    $('#order').find('option:not([value="0"])').remove();

    $.ajax({
        method: "POST",
        url: getUrl({'r': 'trader/getTradersForObj', 'obj': curObj})
    }).done(function(data) {
        if(data.error !== undefined && !data.error) {
            Object.values(data.content).forEach(function(item) {
                traderList.append('<option value="'+ item.id +'">' + item.internal_name + '</option>');
            });

        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
}


function checkDeliveryTime() {
    var deliveryOnTimeBtn = $('#deliveryOnTimeBtn').addClass('hidden');
    var traderDate, traderTime;
    var time = $('#timePeriodText').text();
    if (typeof traderDeliveryDays !== 'undefined' && Object.keys(traderDeliveryDays).length > 0) {
        var isRightDeliveryTime = true;
        var date = $('#date').val();
        traderDate = date;
        traderTime = time;
        var dateArr = date.split('.');
        var curDate = new Date(dateArr[2], dateArr[1] - 1, dateArr[0]);
        var wDay = curDate.getDay() || 7;
        if (traderDeliveryDays[wDay] === undefined) {
            isRightDeliveryTime = false;
            for (var key in traderDeliveryDays) {
                wDay = --wDay || 7;
                curDate.setDate(curDate.getDate() - 1);
                if (traderDeliveryDays[wDay]) {
                    traderDate = formatDate(curDate);
                    traderTime = traderDeliveryDays[key]['from'] + ' - ' + traderDeliveryDays[key]['to'];
                    break;
                }
            }
        } else {
            var tTime = traderDeliveryDays[wDay]['from'] + ' - ' + traderDeliveryDays[wDay]['to'];
            if (tTime !== time) {
                traderDate = formatDate(curDate);
                traderTime = traderDeliveryDays[wDay]['from'] + ' - ' + traderDeliveryDays[wDay]['to'];
                isRightDeliveryTime = false;
            }
        }
        if (!isRightDeliveryTime) {
            deliveryOnTimeBtn.removeClass('hidden').one('click', function (e) {
                $(e.currentTarget).addClass('hidden');
                $('#date').val(traderDate);
                $('#timePeriodText').text(traderTime);
            });
        }
    }
    tmpDoc.doc.inv_time_period = time;
}



var to = false;
function keyupProdName() {
    if(to) { clearTimeout(to); }
    to = setTimeout(function () {
        var tree = $('#jstree-product').jstree(true);
        var str = $('#prodName').val().replace(/[^a-zA-Zа-яА-Я0-9%]+/g, ' ').trim().split(" ");
        if(str.length  && str[0].length > 2) {
            searchRes = {};
            tree.search(str[0]);
        } else {
            tree.clear_search();
            tree.show_all();
            deletedNodes.forEach(function(item) {
                tree.hide_node(item);
            });
        }
    }, 100);
}

function addDateLife(row, life) {
    var dateLifeText = row.find('.dateLifeText');
    var dateLifeList = row.find('.dateLifeList');
    var curDate = new Date();
    var day = curDate.getDate();
    var month = curDate.getMonth();
    var year = curDate.getFullYear();
    for(var i = +life; i >= 0; i-- ){
        curDate = new Date(year, month, day + i);
        dateLifeList.append('<li><a href="#" data-int="' + (curDate.getTime() / 1000).toFixed() + '">' + formatDate(curDate) + '</a></li>');
        if(i === +life) {
            dateLifeText.text(formatDate(curDate));
        }
    }
    dateLifeList.find('a').on('click', changeDateLife);
}

function changeDateLife(e) {
    var curEl = $(e.currentTarget);
    var rowId = curEl.closest('.prodListItem').data('id');
    curEl.closest('.dateLifeList').find('li').removeClass('active');
    curEl.closest('li').addClass('active');
    curEl.closest('.dropdown').find('.dateLifeText').text(curEl.text());
    tmpDoc.saveRow(rowId, {exp_date: curEl.data('int')});
}

function addNewRow() {
    var tree = $('#jstree-product').jstree(true);
    var nodeId = tree.get_selected()[0];
    var node = tree.get_node(nodeId);
    if (node.type === 'pack' && node.data.price > 0) {
        var presentPackIdRow = tmpDoc.findRows('pack_id', node.data.id);
        if(!permit.canEditInvoice) {
            alert("Вы не можете редактировать данные");
        } else if(presentPackIdRow.length === 0) {
            var tmarkNode = tree.get_node(node.parent);
            var packName = node.data.by_weight === '0'
                ? node.data.name + ' (' + node.data.value + node.data.pack_unit + ')'
                : node.data.name + ' (' + node.data.pack_unit + ', на вес)';
            var packUnit = node.data.by_weight === '0'
                ? node.data.value + node.data.pack_unit
                : node.data.pack_unit;
            var tmarkName = tmarkNode.data.name;
            var prodName = tmarkName + ', ' + packName;
            var lastRow = $('#newRow').prev('tr');
            var newRow = lastRow.clone();


            if (node.data.id) {
                $.ajax({
                    method: "POST",
                    url: getUrl({'r': 'docProd/save', 'obj': forObj || obj}),
                    data: {
                        doc_type: +window.docType,
                        doc_id: docId,
                        obj: forObj || obj,
                        pos: lastRow.index() + 1,
                        prod_name: prodName,
                        tmark_id: tmarkNode.data.id,
                        pack_id: node.data.id,
                        unit: node.data.pack_unit,
                        pack_unit: packUnit,
                        in_amount: node.data.amount,
                        amount: 1,
                        amount_unit: packUnit,
                        price: node.data.price,
                        exp_date: node.data.exp_date
                    }
                }).done(function (data) {
                    if (data.error !== undefined && !data.error) {
                        newRow.removeClass('hidden');
                        newRow.find('.prodName').text(prodName);
                        newRow.find('.packUnit').text(packUnit).data('pack-unit', packUnit);
                        newRow.find('.inPrice').text(node.data.price);
                        newRow.find('.inAmount').text(node.data.amount);
                        newRow.find('.onlyUnSignFloatNumbers').on('keypress', onlyUnSignFloatNumbers);
                        newRow.find('.amount').val((1.0).toFixed((units[packUnit] && units[packUnit]['decimal_places']) ? units[packUnit]['decimal_places'] : 0))
                            .prop('max', node.data.amount).on('focusin', changeAmount).on('keypress', enterBtnPress);
                        newRow.find('.delDocProdBtn').on('click', delDocProd);
                        newRow.find('.inTotalPrice').text(node.data.price * node.data.amount);
                        newRow.data('row-price', data.content.price);
                        newRow.data('id', data.content.id);
                        newRow.data('pack-id', data.content.pack_id);

                        $('.prodListItem.hidden').before(newRow);
                        flashBackground(newRow, '#dbffdb');

                        tree.clear_search();
                        tree.show_all();
                        deletedNodes.forEach(function (item) {
                            tree.hide_node(item);
                        });

                        tmpDoc.saveRow(data.content.id, data.content);
                        calcDocTotalPrice();
                    }
                }).fail(function () {
                    $('.offline').removeClass('hidden');
                });
            }
        } else {
            var presentRow = null;
            $('.prodListItem').each(function(index, item) {
                if (+$(item).data('pack-id') === + presentPackIdRow[0].pack_id) {
                    presentRow = $(item);
                }
            });
            if(presentRow) {
                flashBackground(presentRow, '#aaaaff');
            }
        }

        $('#prodName').val('');
        $('#jstreePanel').addClass('hidden');
    }
}

function delDocProd(e) {
    var row = $(e.currentTarget).closest('.prodListItem');
    var rowId = row.data('id');
    $.ajax({
        method: "POST",
        url: getUrl({'r': 'docProd/delete'}),
        data: {id: rowId}
    }).done(function(data) {
        if(data.error !== undefined && !data.error) {
            row.remove();
            tmpDoc.delRow(rowId);
            calcDocTotalPrice();
        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
}

function getUserPriceFromPresentPosition(packId, price) {
    var presentPrice = false;
    var rows = tmpDoc.findRows('pack_id', packId);
    if(rows.length > 0) {
        for (var key in rows) {
            presentPrice = rows[key].trader_price_user;
            break;
        }
    }
    return presentPrice || price;
}

var priceLastClick = 0;
var priceFirstClickTimer = null;

function clickPrice(e) {
    var time = new Date().getTime();
    var priceLabel = $(e.currentTarget).siblings('.priceLabel');
    var inputPrice = $(e.currentTarget).siblings('.price');
    var originPrice = (+inputPrice.data('origin'));
    var prevPriceValue = +inputPrice.val();
    var row = $(e.currentTarget).closest('tr.prodListItem');
    var unit = row.find('.packUnit').val();
    var amount = row.find('.amount');
    var unitValue = +amount.data('unit-value');


    if (time - priceLastClick < 400) {
        priceLastClick = 0;
        $(e.currentTarget).addClass('hidden');
        inputPrice.removeClass('hidden').val((+inputPrice.data('user')).toFixed(2)).focus();
        priceLabel.removeClass('hidden');
        inputPrice.one('focusout', function (e) {
            var newTraderPrice  = +$(e.currentTarget).val();
            var priceText = $(e.currentTarget).siblings('.priceText');
            priceText.removeClass('bgC6').removeClass('bgC3');
            $(e.currentTarget).addClass('hidden').data('user', newTraderPrice);
            priceLabel.addClass('hidden');
            priceText.removeClass('hidden');

            var newPrice = ((newTraderPrice * priceCoef * 100000).toFixed() / 100000);
            if(newTraderPrice > originPrice) {
                priceText.addClass('bgC3');
            } else if(newTraderPrice < originPrice) {
                priceText.addClass('bgC6');
            }
            if (packUnits.includes(unit)) {
                newPrice = (newPrice / unitValue);
                priceText.text(newPrice.toFixed(unitValue.toString().length - 1 + 2).replace(/0+$/,''));     // Если будет маленькое число);
            } else {
                priceText.text(newPrice.toFixed(2));     // Если будет маленькое число);
            }

            var rowId = row.data('id');
            if(newTraderPrice != prevPriceValue) {
                calcRowPrice(row);
                tmpDoc.saveRow(rowId, {
                    trader_price_user : newTraderPrice,
                    price : newPrice
                });

                // меняем цены во всех строках таблицы с таким товаром
                var items = tmpDoc.findRows('pack_id', row.data('pack-id'));
                if(items.length > 0) {
                    items.forEach(function(item) {
                        if(item.id != rowId) {
                            var itemRow = $('.prodListItem').eq(item.pos - 1);
                            var priceText = itemRow.find('.priceText');
                            var priceInput = itemRow.find('.price');
                            var itemPrice = newPrice;

                            priceInput.data('origin', Number(originPrice).toFixed(2));
                            priceInput.data('user', Number(newTraderPrice).toFixed(2));
                            priceInput.val(Number(newTraderPrice).toFixed(2));

                            priceText.removeClass('bgC6').removeClass('bgC3');
                            if (newTraderPrice > originPrice) {
                                priceText.addClass('bgC3');
                            } else if (newTraderPrice < originPrice) {
                                priceText.addClass('bgC6');
                            }

                            if (packUnits.includes(item.amount_unit)) {
                                itemPrice = (unit === item.amount_unit) ? newPrice : newPrice / unitValue;
                                priceText.text(itemPrice.toFixed(unitValue.toString().length - 1 + 2).replace(/0+$/, ''));     // Если будет маленькое число);
                            } else {
                                itemPrice = (unit === item.amount_unit) ? newPrice : newPrice * unitValue;
                                priceText.text(itemPrice.toFixed(2));
                            }

                            calcRowPrice(itemRow);
                            tmpDoc.saveRow(item.id, {
                                trader_price_user: newTraderPrice,
                                price: itemPrice,
                                unit_value: unitValue
                            });
                        }
                    });
                }
            }
        });
        clearTimeout(priceFirstClickTimer);
        priceFirstClickTimer = null;
    } else {
        priceLastClick = time;
        if(priceFirstClickTimer === null) {
            var event = e;
            priceFirstClickTimer = setTimeout(function () {
                var curVal = (+inputPrice.val()).toFixed(2);
                var userVal = (+inputPrice.data('user')).toFixed(2);
                var origVal = (+inputPrice.data('origin')).toFixed(2);
                var newPrice, newTraderPrice;
                $(event.currentTarget).removeClass('bgC6').removeClass('bgC3');
                if (curVal === userVal) {
                    inputPrice.val(origVal);
                    newPrice = ((origVal * priceCoef * 100000).toFixed() / 100000);
                    newTraderPrice = origVal;
                } else {
                    inputPrice.val(userVal);
                    if(userVal > origVal) {
                        $(event.currentTarget).addClass('bgC3');
                    } else if(userVal < origVal) {
                        $(event.currentTarget).addClass('bgC6');
                    }
                    newPrice = ((userVal * priceCoef * 100000).toFixed() / 100000);
                    newTraderPrice = userVal;
                }

                if (packUnits.includes(unit)) {
                    newPrice = (newPrice / unitValue);
                    $(event.currentTarget).text(newPrice.toFixed(unitValue.toString().length - 1 + 2).replace(/0+$/,''));     // Если будет маленькое число);
                } else {
                    $(event.currentTarget).text(newPrice.toFixed(2));     // Если будет маленькое число);
                }

                if(userVal !== origVal) {
                    calcRowPrice(row);
                    tmpDoc.saveRow(row.data('id'), {
                        trader_price_user : newTraderPrice,
                        price : newPrice
                    });
                }

                priceFirstClickTimer = null;
            }, 400);
        }
    }
}


function changedPrice(e) {
    var curPrice = $(e.currentTarget).val();
    $(e.currentTarget).data('user', curPrice);
}

function enterBtnPress(e) {
    if (e.which == null) { // IE
        if(e.keyCode === 13) {
            $(e.currentTarget).blur();
        }
    }

    if (e.which !== 0 && e.charCode !== 0) { // все кроме IE
        if(e.which === 13) {
            $(e.currentTarget).blur();
        }
    }
}

function changeUnit(e) {
    e.preventDefault();
    var unit = $(e.currentTarget).text();
    var row = $(e.currentTarget).closest('tr.prodListItem');
    var packUnitVal = row.find('.packUnit').val();
    if(packUnitVal !== unit) {
        row.find('.packUnitText').text(unit);
        row.find('.packUnit').val(unit);

        var amount = row.find('.amount');
        var maxAmount = +amount.data('max');
        var amountValue = +amount.val();
        var unitValue = +amount.data('unit-value');
        var curTraderPriceWithDiscount = ((row.find('.price').val() * priceCoef * 100000).toFixed() / 100000).toFixed(2);
        var newAmount, newPrice, newMaxAmount;
        var roundTo = (units[unit] && units[unit]['decimal_places']) ? units[unit]['decimal_places'] : 0;

        if (packUnits.includes(unit)) {
            newMaxAmount = (maxAmount * unitValue).toFixed(roundTo);
        } else {
            newMaxAmount = (maxAmount / unitValue).toFixed(0);
        }
        amount.data('max', newMaxAmount);

        if (packUnits.includes(unit)) {
            newAmount = (amountValue * unitValue).toFixed(roundTo);
            newPrice = curTraderPriceWithDiscount / unitValue;
            row.find('.priceText').text(newPrice.toFixed(unitValue.toString().length - 1 + 2));      // Если будет маленькое число
        } else {
            newAmount = (amountValue / unitValue).toFixed(0);
            newPrice = +curTraderPriceWithDiscount;
            row.find('.priceText').text(newPrice.toFixed(2));
        }
        amount.val(newAmount);

        calcRowPrice(row);
        tmpDoc.saveRow(row.data('id'), {
            amount_unit : unit,
            amount : newAmount,
            price : newPrice
        });
    }
}

function changeAmount(e) {
    var prevValue = (+$(e.currentTarget).val());
    var row = $(e.currentTarget).closest('tr.prodListItem');
    $(e.currentTarget).one('focusout', function(e) {
        var newValue = (+$(e.currentTarget).val());
        var maxValue = (+$(e.currentTarget).prop('max'));
        if(row.find('.realAmount').length) {
            if($(e.currentTarget).val() !== '') {
                tmpDoc.saveRow(row.index(), {realAmount: newValue});
            } else {
                tmpDoc.saveRow(row.index(), {realAmount: null});
            }
        } else {
            if (maxValue > 0 && newValue > maxValue) {
                $(e.currentTarget).val(maxValue);
                tmpDoc.saveRow(row.data('id'), {amount: maxValue});
            } else if (prevValue !== newValue) {
                $(e.currentTarget).val(newValue);
                tmpDoc.saveRow(row.data('id'), {amount: newValue});
            }
        }
        calcRowPrice(row);
        if(row.find('.check').length) {
            row.find('.check').prop('checked', $(e.currentTarget).val() !== '');
            tmpDoc.saveRow(row.index(), {'checked': $(e.currentTarget).val() !== '' ? 1 : 0})
        }
    });
}

function calcRowPrice(row) {
    var amount;
    if(row.find('.realAmount').length) {
        var inAmount = row.find('.inAmount').text();
        var realAmount = row.find('.realAmount').val();
        amount = (realAmount === '' || isNaN(+realAmount)) ? 0 : realAmount - inAmount;
    } else {
        amount = +row.find('.amount').val();
    }
    var price = +row.find('.inPrice').text();
    row.data('row-price', (amount * price).toFixed(2));

    calcDocTotalPrice();
}

function calcDocTotalPrice() {
    var rows = $('.prodList').find('.prodListItem:not(.hidden)');
    var docTotalPrice = 0;
    rows.each(function(key, row) {
        docTotalPrice += +$(row).data('row-price');
    });
    $('#docTotalPrice').text(docTotalPrice.toFixed(2));
    tmpDoc.doc.total = docTotalPrice.toFixed(2);
}

// изменение прайса поставщика для определенного обьекта
function changeTraderPrice(obj, docRows, title, forObj) {
    var controller;
    if(tmpDoc.doc.order_id !== undefined) {
        controller = 'invoiceReceipt';
    } else if(tmpDoc.doc.inv_receipt_id !== undefined) {
        controller = 'invoiceReturn';
    } else {
        return false;
    }

    var priceModal = $('#changedPriceModal');
    if (Object.keys(docRows).length > 0) {
        var modalTitle = priceModal.find('.modal-title');
        modalTitle.text(title);
        flashBackground(modalTitle, '#aaaaff');
        var priceTable = $('#priceTable');
        var priceTableBody = priceTable.find('tbody');
        priceTableBody.empty();
        for(var key in docRows) {
            var item = docRows[key];
            var oldPrice = (+((obj == 'all') ? item.trader_price_main : item.trader_price)).toFixed(2);
            var newPrice = (+item.trader_price_user).toFixed(2);
            var bg = (newPrice > oldPrice) ? 'bgC3' : 'bgC6';
            var priceItem = '<tr class="priceItem">' +
                '<td>' + item.prod_name.replace(/\0/g, '0').replace(/\\(.)/g, "$1") + '</td>' +
                '<td class="text-right">' + (+((obj == 'all') ? item.trader_price_main : item.trader_price)).toFixed(2) + '</td>' +
                '<td class="text-right ' + bg + '">' + (+item.trader_price_user).toFixed(2) + '</td>' +
                '</tr>';
            priceTableBody.append(priceItem);
        }

        priceModal.modal('show').on('hide.bs.modal', function() {
            $(this).find('.priceItem').remove();
            window.location.href = getUrl({'r': controller});
            $('#savePriceBtn').off('click');
//            $(this).off('hide.bs.modal');
        });

        if(obj == 'all' && forObj !== undefined && permit.canChangeObjPriceProduct) {
            $('#refusePriceBtn').one('click', function (e) {
                e.stopPropagation();
                modalTitle.text("Изменить прайс поставщика для обьекта?");
                flashBackground(modalTitle, '#aaaaff');
                priceTableBody.empty();
                for(var key in docRows) {
                    var item = docRows[key];
                    var oldPrice = (+((obj == 'all') ? item.trader_price_main : item.trader_price)).toFixed(2);
                    var newPrice = (+item.trader_price_user).toFixed(2);
                    var bg = (newPrice > oldPrice) ? 'bgC3' : 'bgC6';
                    var priceItem = '<tr class="priceItem">' +
                        '<td>' + item.prod_name.replace(/\0/g, '0').replace(/\\(.)/g, "$1") + '</td>' +
                        '<td class="text-right">' + (+item.trader_price).toFixed(2) + '</td>' +
                        '<td class="text-right ' + bg + '">' + (+item.trader_price_user).toFixed(2) + '</td>' +
                        '</tr>';
                    priceTableBody.append(priceItem);
                }
            });
        }

        $('#savePriceBtn').on('click', function (e) {
            $.ajax({
                method: "POST",
                url: getUrl({'r': 'trader/changePrice'}),
                data: {
                    trader_id: $('#trader').val(),
                    obj: obj,
                    items: docRows
                }
            }).done(function (data) {
                if (data.error !== undefined && !data.error) {
                    priceModal.modal('hide');
                }
            }).fail(function () {
                $('.offline').removeClass('hidden');
            });
        });
    }
}
