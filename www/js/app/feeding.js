$(function() {
    window.tmpDoc = new DocStorage('feeds');
    sortableRows();
});


function sortableRows() {
    $('.feedList tbody').sortable({
        items: 'tr.feedItem',
        cursor: "move",
        delay: 150,
        handle: 'td.dragAnchor',
        forceHelperSize: true,
        placeholder:"bgC2",
        start: function(e, ui) {
            if(ui.item.data('type') === 'group') {
                var rows = $('.feedList').find('.feedItem');
                var groups = rows.filter(function(){
                    return $(this).data('type') === 'group';
                });
                groups.each(function(index, group) {
                    var child = $(group).siblings().filter(function(){
                        return +$(this).data('parent') === +$(group).data('id');
                    }).clone();
                    $(group).data('child', child);
                    $(group).siblings().filter(function(){
                        return +$(this).data('parent') === +$(group).data('id');
                    }).remove();
                });
                $(this).sortable( "refreshPositions" );
            }
        },
        stop: function(e, ui) {
            var rows = $('.feedList').find('.feedItem');
            if(ui.item.data('type') === 'group') {
                var groups = rows.filter(function(){
                    return $(this).data('type') === 'group';
                });
                groups.each(function(index, group) {
                    var child = $(group).data('child');
                    $(group).after(child);
                    child.find('.groupObj').on('click', changeGroupCheckbox);
                    child.find('.checkbox input[type="checkbox"]').on('click', changeCheckbox);
                    child.find('.editFeedBtn').on('click', editFeed);
                    child.find('.delFeedBtn').on('click', deleteFeed);

                });
                $(this).sortable( "refreshPositions" );
            }

            if(ui.item.data('type') === 'item') {
                var siblingItem = ui.item.prev();
                if(siblingItem.length === 0) {
                    $(this).sortable( "cancel" );
                }
                var groupId = +siblingItem.data('parent') === 0 ? siblingItem.data('id') : siblingItem.data('parent');
                var oldGroupId = ui.item.data('parent');
                ui.item.data('parent', groupId);

                if(+oldGroupId !== +groupId) {
                    var parent = rows.filter(function () {
                        return +$(this).data('id') === +groupId;
                    });
                    var parentObjs = parent.find('.objList input[type="checkbox"]');
                    var objList = ui.item.find('.objList .checkbox');
                    objList.each(function (i, item) {
                        if (parentObjs.eq(i).prop('checked')) {
                            $(item).removeClass('hidden');
                        } else {
                            $(item).addClass('hidden');
                        }
                        $(item).find('input[type="checkbox"]').prop('checked', false);
                    });
                }

            }

            rows.each(function (key, el) {
                var rowId = $(el).data('id');
                var parentId = $(el).data('parent');
                tmpDoc.saveRow(rowId, { pos: key + 1, parent_id: parentId});
            });

        },
        beforeStop: function( e, ui ) {
        }
    }).disableSelection();
}

$('#saveFeedingBtn').on('click', saveFeeding);
$('#addFeedBtn').on('click', addNewFeed);
$('#saveFeedBtn').on('click', saveFeed);
$('.groupObj').on('click', changeGroupCheckbox);
$('.checkbox input[type="checkbox"]').on('click', changeCheckbox);
$('.editFeedBtn').on('click', editFeed);
$('.delFeedBtn').on('click', deleteFeed);





function addNewFeed() {
    var form = $("#feedForm");
    resetForm(form);
    form.find('#isNew').val(1);
    $("#feedModal").modal('show').on('hide.bs.modal', function() {
        $(this).off('hide.bs.modal');
    });
}

function editFeed(e) {
    var curEl = $(e.currentTarget);
    var id = curEl.closest('tr').data('id');
    var form = $("#feedForm");
    resetForm(form);
    form.find('#categoryBlock').addClass('hidden');
    var row = tmpDoc.getRow(id);
    form.find('#id-value').val(row.id);
    form.find('#pos-value').val(row.pos);
    form.find('#objs-value').val(JSON.stringify(row.objs));
    form.find('#parentGroup').val(row.parent_id);
    form.find('#feedName').val(row.name);

    $("#feedModal").modal('show').on('hide.bs.modal', function() {
        $(this).off('hide.bs.modal');
    });
}

function deleteFeed(e) {
    var curEl = $(e.currentTarget);
    var id = curEl.closest('tr').data('id');
    var name = curEl.closest('tr').find('.feedName').text();
    if(confirm('Удалить категорию "' + name + '" ?')) {
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'feeding/deleteCategory'}),
            data: {id: id}
        }).done(function (data) {
            if (data.error !== undefined && data.error !== 1) {
                var rows = $('.feedItem').filter(function(i, item){
                    return data.content.ids.includes($(item).data('id').toString());
                });
                rows.remove();
                data.content.ids.forEach(function(item) {
                    tmpDoc.delRow(item);
                });
                $('#parentGroup').find('[value="' + id + '"]').remove();
            } else {
                alert("Error: " + data.content);
            }
        });
    }
}

function saveFeed(e) {
    var modal = $('#feedModal');
    var form = $('#feedForm');
    var feedName = $('#feedName');
    var feedItems = $('.feedItem');
    var isNew = $('#isNew').val();
    var hasError = false;

    if(!feedName.val()) {
        feedName.closest('.form-group').addClass('has-error').find('.help-block').removeClass('hidden');
        hasError |= true;
    }

    if(!hasError) {
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'feeding/saveCategory'}),
            data: form.serializeArray()
        }).done(function(data) {
            if(data.error !== undefined && !data.error) {
                modal.modal('hide');
                tmpDoc.saveRow(data.content.id, data.content);
                var row = feedItems.filter(function(i, item){
                    return +$(item).data('id') === +data.content.id;
                }).find('.feedName').text(data.content.name);

                if (+data.content.parent_id === 0) {
                    $('#parentGroup option').each(function(i, item) {
                        if(+$(item).val() === +data.content.id) {
                            $(item).text(data.content.name);
                        }
                    });
                }
                if(!!isNew) {
                    var groupItemEl = $('#groupItem');
                    var itemItemEl = $('#itemItem');
                    if (+data.content.parent_id === 0) {
                        row = groupItemEl.clone();
                        row.prop('id', '');
                        row.data('id', data.content.id);
                        row.find('.feedName').text(data.content.name);
                        row.addClass('feedItem').removeClass('hidden');
                        row.find('.groupObj').on('click', changeGroupCheckbox);
                        row.find('.checkbox input[type="checkbox"]').on('click', changeCheckbox);
                        row.find('.editFeedBtn').on('click', editFeed);
                        row.find('.delFeedBtn').on('click', deleteFeed);

                        groupItemEl.before(row);
                        $('#parentGroup').append('<option value="' + data.content.id + '">' + data.content.name + '</option>');
                        $('.feedList tbody').sortable('refresh');
                    } else if (+data.content.parent_id > 0) {
                        row = itemItemEl.clone();
                        row.prop('id', '');
                        row.data('id', data.content.id);
                        row.data('parent', data.content.parent_id);
                        row.find('.feedName').text(data.content.name);
                        row.addClass('feedItem').removeClass('hidden');
                        row.find('.groupObj').on('click', changeGroupCheckbox);
                        row.find('.checkbox input[type="checkbox"]').on('click', changeCheckbox);
                        row.find('.editFeedBtn').on('click', editFeed);
                        row.find('.delFeedBtn').on('click', deleteFeed);
                        var parent = feedItems.filter(function () {
                            return +$(this).data('id') === +data.content.parent_id;
                        });
                        var parentObjs = parent.find('.objList input[type="checkbox"]');
                        var objList = row.find('.objList .checkbox');
                        objList.each(function (i, item) {
                            if (parentObjs.eq(i).prop('checked')) {
                                $(item).removeClass('hidden');
                            }
                        });
                        groupItemEl.before(row);
                        $('.feedList tbody').sortable('refresh');
                    }
                }
            }
        }).fail(function() {
            $('.offline').removeClass('hidden');
        });
    }

}

// TODO: нужно реализовать
function saveFeeding() {
    var rows = tmpDoc.getRows();
    $.ajax({
        method: "POST",
        url: getUrl({'r': 'feeding/save'}),
        data: {items: rows}
    }).done(function(data) {
        if(data.error !== undefined && !data.error) {
            row.remove();
            tmpDoc.delRow(rowId);
            calcDocTotalPrice();
        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });

}

function changeGroupCheckbox(e) {
    var curEl = $(e.currentTarget);
    var groupId = +curEl.closest('tr').data('id');
    var checkboxIndex = curEl.closest('.checkbox').index();
    var isCheck = curEl.prop('checked');
    var child = curEl.closest('tr').siblings().filter(function(){
        return +$(this).data('parent') === groupId;
    });
    if(isCheck) {
        child.each(function(i, item) {
            $(item).find('.objList .checkbox').eq(checkboxIndex).removeClass('hidden');
        });
    } else {
        child.each(function(i, item) {
            $(item).find('.objList .checkbox').eq(checkboxIndex).addClass('hidden').find('input[type="checkbox"]').prop('checked', false);
        });
    }
}

function changeCheckbox(e) {
    var curEl = $(e.currentTarget);
    var objName = curEl.closest('.checkbox').data('obj');
    var isCheck = curEl.prop('checked');
    var rowId = curEl.closest('tr').data('id');
    var row = tmpDoc.getRow(rowId);

    row.objs[objName] = isCheck ? 1 : 0;
    tmpDoc.saveRow(rowId, { objs : row.objs});
}

function resetForm(form) {
    form.find('input').each(function() {
        $(this).val('');
    });
    form.find('select').each(function() {
        $(this).val(0);
    });
    form.find('#categoryBlock').removeClass('hidden');

}
