    var packUnits = [
        {unit: 'г', group: 'weight'},
        {unit: 'кг', group: 'weight'},
        {unit: 'мл', group: 'volume'},
        {unit: 'л', group: 'volume'},
        {unit: 'шт', group: 'piece'},
        {unit: 'дес', group: 'piece'}
    ];

// Groups events
    function addGroup(tree, node) {
        var form = $("#groupForm");
        form.find('.field-name').val('');
        form.find('.field-name').prop('placeholder', tree.get_node(node).text);
        $("#groupModal").modal('show').on('hide.bs.modal', function() {
            resetForm(form);
            if(form.data('new')) {
                form.data('tree').delete_node(form.data('node'));
            }
            $(this).off('hide.bs.modal');
        });
        form.data('tree', tree).data('node', node).data('new', true);
    }
    function editGroup(tree, node) {
        var form = $("#groupForm");
        var id = tree.get_node(node).data.id;
        form.find('.field-id').val(id);
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'prodGroup/edit'}),
            data: {id: id}
        }).done(function( data ) {
            if(data.error  !== undefined && data.error !== 1) {
                if(data.content.lock !== null) {
                    form.find('.lockData').removeClass('hidden').text(data.content.lock);
                    form.find('.canHide').addClass('hidden');
                    form.find('.canLock').attr('disabled',true);
                } else {
                    form.find('.lockData').addClass('hidden');
                    form.find('.canHide').removeClass('hidden');
                    form.find('.canLock').attr('disabled',false);
                }
                form.find('.field-id').val(id);
                form.find('.field-pos').val(data.content.group_pos);
                form.find(".field-name").val(data.content.name);
            } else {
                alert("Data Error: " + data.content);
            }
        });

        timerLockId = setTimeout(function tick() {
            lockData(form, 'prodGroup', id);
            timerLockId = setTimeout(tick, 600000);
        }, 100);     // 10 min. = 600 000 msec.

        $("#groupModal").modal('show').on('hide.bs.modal', function() {
            resetForm(form);
            clearInterval(timerLockId);
            unlockData('prodGroup', id);
            $(this).off('hide.bs.modal');
        });
        form.data('tree', tree).data('node', node).data('new', true);
    }
    function deleteGroup(tree, node) {
        var name = tree.get_node(node).data.name;
        var id = tree.get_node(node).data.id;
        if(confirm('Удалить группу "' + name + '" ?')) {
            $.ajax({
                method: "POST",
                url: getUrl({'r': 'prodGroup/delete'}),
                data: {id: id}
            }).done(function (data) {
                if (data.error !== undefined && data.error !== 1) {
//                    tree.refresh();
//                    alert('Группа "' + name + '" удалена');
                } else {
                    alert("Error: " + data.content);
                }
            });
        }
    }
    function undeleteGroup(tree, node) {
        var id = tree.get_node(node).data.id;
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'prodGroup/undelete'}),
            data: {id: id}
        }).done(function (data) {
            if (data.error !== undefined && data.error !== 1) {
//                tree.refresh();
            } else {
                alert("Error: " + data.content);
            }
        });
    }
    function changePosGroup(node, parent, position) {
        var tree = $('#jstree-product').jstree(true);
        var parentNode = tree.get_node(parent);
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'prodGroup/changePos'}),
            data: {id: node.data.id, pos: position+1}
        }).done(function (data) {
            if (data.error !== undefined && data.error !== 1) {
//                console.log(JSON.stringify(data.content));
            } else {
                alert("Error: " + JSON.stringify(data.content));
            }
        });
    }

// Type events
    function addType(tree, node) {
        var form = $("#typeForm");
        var groupNode = tree.get_node(tree.get_node(node).parent);
        form.find('.field-name').val('');
        form.find('.field-name').prop('placeholder', tree.get_node(node).text);
        form.find(".field-groupName").val(groupNode.data.name);
        form.find(".field-groupId").val(groupNode.data.id);
        $("#typeModal").modal('show').on('hide.bs.modal', function() {
            resetForm(form);
            if(form.data('new')) {
                form.data('tree').delete_node(form.data('node'));
            }
            $(this).off('hide.bs.modal');
        });
        form.data('tree', tree).data('node', node).data('new', true);
    }
    function editType(tree, node) {
        var form = $("#typeForm");
        var id = tree.get_node(node).data.id;
        var groupName = tree.get_node(tree.get_node(node).parent).data.name;
        form.find('.field-id').val(id);
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'prodType/edit'}),
            data: {id: id}
        }).done(function( data ) {
            if(data.error  !== undefined && data.error !== 1) {
                if(data.content.lock !== null) {
                    form.find('.lockData').removeClass('hidden').text(data.content.lock);
                    form.find('.canHide').addClass('hidden');
                    form.find('.canLock').attr('disabled',true);
                } else {
                    form.find('.lockData').addClass('hidden');
                    form.find('.canHide').removeClass('hidden');
                    form.find('.canLock').attr('disabled',false);
                }
                form.find('.field-id').val(id);
                form.find('.field-pos').val(data.content.type_pos);
                form.find(".field-groupId").val(data.content.group_id);
                form.find(".field-groupName").val(groupName);
                form.find('.field-name').val(data.content.name);
                form.find(".field-packUnit").val(data.content.pack_unit);
                form.find("#type-packUnitText").text(data.content.pack_unit);
                form.find(".field-roundToPack").prop('checked', data.content.round_to_pack === '1');
                form.find(".field-life").val(data.content.life);
                form.find(".field-tempFrom").val(data.content.temp_from);
                form.find(".field-tempTo").val(data.content.temp_to);
                if(data.content.delivery_possible === '1') {
                    form.find(".field-deliveryPossible").prop('checked', true);
                    form.find('.field-deliveryHour-block').removeClass('hidden');
                }
                form.find(".field-deliveryHour").val(data.content.delivery_to_hour);
            } else {
                alert("Data Error: " + data.content);
            }
        });

        timerLockId = setTimeout(function tick() {
            lockData(form, 'prodType', id);
            timerLockId = setTimeout(tick, 600000);
        }, 100);     // 10 min. = 600 000 msec.

        $("#typeModal").modal('show').on('hide.bs.modal', function() {
            resetForm(form);
            clearTimeout(timerLockId);
            unlockData('prodType', id);
            $(this).off('hide.bs.modal');
        });
        form.data('tree', tree).data('node', node).data('new', true);
    }
    function deleteType(tree, node) {
        var name = tree.get_node(node).data.name;
        var id = tree.get_node(node).data.id;
        if(confirm('Удалить вид продукта "' + name + '" ?')) {
            $.ajax({
                method: "POST",
                url: getUrl({'r': 'prodType/delete'}),
                data: {id: id}
            }).done(function (data) {
                if (data.error !== undefined && data.error !== 1) {
                    tree.refresh();
//                    alert('Вид продукта "' + name + '" удален');
                } else {
                    alert("Error: " + data.content);
                }
            });
        }
    }
    function undeleteType(tree, node) {
        var id = tree.get_node(node).data.id;
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'prodType/undelete'}),
            data: {id: id}
        }).done(function (data) {
            if (data.error !== undefined && data.error !== 1) {
                tree.refresh();
            } else {
                alert("Error: " + data.content);
            }
        });
    }
    function changePosType(node, parent, position) {
        var tree = $('#jstree-product').jstree(true);
        var groupNode = tree.get_node(parent);
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'prodType/changePos'}),
            data: {
                id: node.data.id,
                pos: position + 1,
                group_id: groupNode.data.id
            }
        }).done(function (data) {
            if (data.error !== undefined && data.error !== 1) {
//                console.log(JSON.stringify(data.content));
            } else {
                alert("Error: " + JSON.stringify(data.content));
            }
        });
    }


// TMark events
    function addTMark(tree, node) {
        var form = $("#tmarkForm");
        var typeNode = tree.get_node(tree.get_node(node).parent);
        var groupNode = tree.get_node(typeNode.parent);
        var unitGroup;
        packUnits.forEach( function(item) {
            if(item.unit === typeNode.data.pack_unit) {
                unitGroup = item.group;
            }
        });
        form.find(".field-groupName").val(groupNode.data.name);
        form.find(".field-groupId").val(groupNode.data.id);
        form.find(".field-typeName").val(typeNode.data.name);
        form.find(".field-typeId").val(typeNode.data.id);
        form.find('.field-name').val('');
        form.find('.field-name').prop('placeholder', tree.get_node(node).text);
        form.find(".field-packUnit").val(typeNode.data.pack_unit);
        form.find("#tmark-packUnitText").text(typeNode.data.pack_unit);
        form.find('#tmark-packUnitList li').each(function(key, item) {
            if($(item).hasClass(unitGroup)) {
                $(item).removeClass('hidden');
            } else {
                $(item).addClass('hidden');
            }
        });
        form.find(".field-life").val(typeNode.data.life);
        form.find(".field-tempFrom").val(typeNode.data.temp_from);
        form.find(".field-tempTo").val(typeNode.data.temp_to);
        $("#tmarkModal").modal('show').on('hide.bs.modal', function() {
            resetForm(form);
            if(form.data('new')) {
                form.data('tree').delete_node(form.data('node'));
            }
            $(this).off('hide.bs.modal');
        });
        form.data('tree', tree).data('node', node).data('new', true);
    }
    function editTMark(tree, node) {
        var form = $("#tmarkForm");
        var id = tree.get_node(node).data.id;
        var typeNode = tree.get_node(tree.get_node(node).parent);
        var groupNode = tree.get_node(typeNode.parent);
        form.find('.field-id').val(id);
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'prodTMark/edit'}),
            data: {id: id}
        }).done(function( data ) {
            if(data.error  !== undefined && data.error !== 1) {
                if(data.content.lock !== null) {
                    form.find('.lockData').removeClass('hidden').text(data.content.lock);
                    form.find('.canHide').addClass('hidden');
                    form.find('.canLock').attr('disabled',true);
                } else {
                    form.find('.lockData').addClass('hidden');
                    form.find('.canHide').removeClass('hidden');
                    form.find('.canLock').attr('disabled',false);
                }
                form.find('.field-id').val(id);
                form.find('.field-pos').val(data.content.tmark_pos);
                form.find(".field-groupName").val(groupNode.data.name);
                form.find(".field-groupId").val(groupNode.data.id);
                form.find(".field-typeName").val(typeNode.data.name);
                form.find(".field-typeId").val(typeNode.data.id);
                form.find('.field-name').val(data.content.name);
                form.find(".field-packUnit").val(data.content.pack_unit);
                form.find("#tmark-packUnitText").text(data.content.pack_unit);
                form.find(".field-life").val(data.content.life).attr('def-life', typeNode.data.life);
                form.find(".field-tempFrom").val(data.content.temp_from).attr('def-temp_from', typeNode.data.temp_from);
                form.find(".field-tempTo").val(data.content.temp_to).attr('def-temp_to', typeNode.data.temp_to);
                form.find(".field-hasChaild").val(node.children.length > 0);
                var unitGroup;
                packUnits.forEach( function(item) {
                    if(item.unit === typeNode.data.pack_unit) {
                        unitGroup = item.group;
                    }
                });
                form.find('#tmark-packUnitList li').each(function(key, item) {
                    if($(item).hasClass(unitGroup)) {
                        $(item).removeClass('hidden');
                    } else {
                        $(item).addClass('hidden');
                    }
                });
            } else {
                alert("Data Error: " + JSON.stringify(data.content));
            }
        });

        timerLockId = setTimeout(function tick() {
            lockData(form, 'prodTMark', id);
            timerLockId = setTimeout(tick, 600000);
        }, 100);     // 10 min. = 600 000 msec.

        $("#tmarkModal").modal('show').on('hide.bs.modal', function() {
            resetForm(form);
            clearTimeout(timerLockId);
            unlockData('prodTMark', id);
            $(this).off('hide.bs.modal');
        });
        form.data('tree', tree).data('node', node).data('new', true);
    }
    function deleteTMark(tree, node) {
        var name = tree.get_node(node).data.name;
        var id = tree.get_node(node).data.id;
        if(confirm('Удалить торговую марку "' + name + '" ?')) {
            $.ajax({
                method: "POST",
                url: getUrl({'r': 'prodTMark/delete'}),
                data: {id: id}
            }).done(function (data) {
                if (data.error !== undefined && data.error !== 1) {
                    tree.refresh();
//                    alert('Торговая марка "' + name + '" удалена');
                } else {
                    alert("Error: " + data.content);
                }
            });
        }
    }
    function undeleteTMark(tree, node) {
        var id = tree.get_node(node).data.id;
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'prodTMark/undelete'}),
            data: {id: id}
        }).done(function (data) {
            if (data.error !== undefined && data.error !== 1) {
                tree.refresh();
            } else {
                alert("Error: " + data.content);
            }
        });
    }
    function changePosTMark(node, parent, position) {
        var tree = $('#jstree-product').jstree(true);
        var typeNode = tree.get_node(parent);
        var groupNode = tree.get_node(typeNode.parent);
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'prodTMark/changePos'}),
            data: {
                id: node.data.id,
                pos: position + 1,
                group_id: groupNode.data.id,
                type_id: typeNode.data.id
            }
        }).done(function (data) {
            if (data.error !== undefined && data.error !== 1) {
//                console.log(JSON.stringify(data.content));
            } else {
                alert("Error: " + JSON.stringify(data.content));
            }
        });
    }

// Pack events
    function addPack(tree, node) {
        var form = $("#packForm");
        var tmarkNode = tree.get_node(tree.get_node(node).parent);
        var typeNode = tree.get_node(tmarkNode.parent);
        var groupNode = tree.get_node(typeNode.parent);
        var unitGroup;
        packUnits.forEach( function(item) {
            if(item.unit === typeNode.data.pack_unit) {
                unitGroup = item.group;
            }
        });

        form.find(".field-groupId").val(groupNode.data.id);
        form.find(".field-typeId").val(typeNode.data.id);
        form.find(".field-tmarkName").val(tmarkNode.data.name);
        form.find(".field-tmarkId").val(tmarkNode.data.id);
        form.find('.field-name').val('');
        form.find('.field-name').prop('placeholder', tree.get_node(node).text);
        form.find(".field-packUnit").val(typeNode.data.pack_unit);
        form.find("#pack-packUnitText").text(typeNode.data.pack_unit);
        form.find('#pack-packUnitList li').each(function(key, item) {
            if($(item).hasClass(unitGroup)) {
                $(item).removeClass('hidden');
            } else {
                $(item).addClass('hidden');
            }
        });
        form.find(".field-life").val(tmarkNode.data.life);
        form.find(".field-tempFrom").val(tmarkNode.data.temp_from);
        form.find(".field-tempTo").val(tmarkNode.data.temp_to);
        $("#packModal").modal('show').on('hide.bs.modal', function() {
            resetForm(form);
            if(form.data('new')) {
                form.data('tree').delete_node(form.data('node'));
            }
            $(this).off('hide.bs.modal');
        });
        form.data('tree', tree).data('node', node).data('new', true);
    }
    function editPack(tree, node) {
        var form = $("#packForm");
        var id = tree.get_node(node).data.id;
        var tmarkNode = tree.get_node(tree.get_node(node).parent);
        var typeNode = tree.get_node(tmarkNode.parent);
        var groupNode = tree.get_node(typeNode.parent);
        form.find('.field-id').val(id);
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'prodPack/edit'}),
            data: {id: id}
        }).done(function( data ) {
            if(data.error  !== undefined && data.error !== 1) {
                if(data.content.lock !== null) {
                    form.find('.lockData').removeClass('hidden').text(data.content.lock);
                    form.find('.canHide').addClass('hidden');
                    form.find('.canLock').attr('disabled',true);
                } else {
                    form.find('.lockData').addClass('hidden');
                    form.find('.canHide').removeClass('hidden');
                    form.find('.canLock').attr('disabled',false);
                }
                form.find('.field-id').val(id);
                form.find('.field-pos').val(data.content.pack_pos);
                form.find(".field-groupId").val(groupNode.data.id);
                form.find(".field-typeId").val(typeNode.data.id);
                form.find(".field-tmarkId").val(tmarkNode.data.id);
                form.find(".field-tmarkName").val(tmarkNode.data.name);
                form.find('.field-name').val(data.content.name);
                form.find(".field-packValue").val(data.content.value);
                var unitGroup;
                packUnits.forEach( function(item) {
                    if(item.unit === tmarkNode.data.pack_unit) {
                        unitGroup = item.group;
                    }
                });
                form.find('#pack-packUnitList li').each(function(key, item) {
                    if($(item).hasClass(unitGroup)) {
                        $(item).removeClass('hidden');
                    } else {
                        $(item).addClass('hidden');
                    }
                });
                if(data.content.by_weight === "1") {
                    form.find(".field-byWeight").prop('checked', true);
                    form.find(".field-packValue").removeClass('requiredValue ').val('1').attr('readonly', true);
                }
                form.find(".field-packUnit").val(data.content.pack_unit);
                form.find("#pack-packUnitText").text(data.content.pack_unit);
                form.find(".field-life").val(data.content.life).attr('def-life', tmarkNode.data.life);
                form.find(".field-tempFrom").val(data.content.temp_from).attr('def-temp_from', tmarkNode.data.temp_from);
                form.find(".field-tempTo").val(data.content.temp_to).attr('def-temp_to', tmarkNode.data.temp_to);
            } else {
                alert("Data Error: " + JSON.stringify(data.content));
            }
        });

        timerLockId = setTimeout(function tick() {
            lockData(form, 'prodPack', id);
            timerLockId = setTimeout(tick, 600000);
        }, 100);     // 10 min. = 600 000 msec.

        $("#packModal").modal('show').on('hide.bs.modal', function() {
            resetForm(form);
            clearTimeout(timerLockId);
            unlockData('prodPack', id);
            $(this).off('hide.bs.modal');
        });
        form.data('tree', tree).data('node', node).data('new', true);
    }
    function deletePack(tree, node) {
        var name = tree.get_node(node).data.name;
        var id = tree.get_node(node).data.id;
        if(confirm('Удалить фасовку "' + name + '" ?')) {
            $.ajax({
                method: "POST",
                url: getUrl({'r': 'prodPack/delete'}),
                data: {id: id}
            }).done(function (data) {
                if (data.error !== undefined && data.error !== 1) {
                    tree.refresh();
//                    alert('Фасовка "' + name + '" удалена');
                } else {
                    alert("Error: " + data.content);
                }
            });
        }
    }
    function undeletePack(tree, node) {
        var id = tree.get_node(node).data.id;
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'prodPack/undelete'}),
            data: {id: id}
        }).done(function (data) {
            if (data.error !== undefined && data.error !== 1) {
                tree.refresh();
            } else {
                alert("Error: " + data.content);
            }
        });
    }
    function changePosPack(node, parent, position) {
        var tree = $('#jstree-product').jstree(true);
        var tmarkNode = tree.get_node(parent);
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'prodPack/changePos'}),
            data: {
                tmark_id: tmarkNode.data.id,
                id: node.data.id,
                pos: position + 1
            }
        }).done(function (data) {
            if (data.error !== undefined && data.error !== 1) {
//                console.log(JSON.stringify(data.content));
            } else {
                alert("Error: " + JSON.stringify(data.content));
            }
        });
    }



// form TMark
    $('#tmark-packUnitList a').on('click', function (e) {
        var unit = $(e.currentTarget).text();
        var form = $(e.currentTarget).closest('.modal').find('form');
        form.find('#tmark-packUnitText').text(unit);
        form.find('.field-packUnit').val(unit);
    });

// form Type
    $('#type-packUnitList a').on('click', function (e) {
        var unit = $(e.currentTarget).text();
        var form = $(e.currentTarget).closest('.modal').find('form');
        form.find('#type-packUnitText').text(unit);
        form.find('.field-packUnit').val(unit);
    });

    $('.field-deliveryPossible').on('click', function (e) {
        var form = $(e.currentTarget).closest('.modal').find('form');
        form.find('.field-deliveryHour').closest('.input-group').toggleClass('hidden');
    });

// form pack
    $('#pack-packUnitList a').on('click', function (e) {
        var unit = $(e.currentTarget).text();
        var form = $(e.currentTarget).closest('.modal').find('form');
        form.find('#pack-packUnitText').text(unit);
        form.find('.field-packUnit').val(unit);
    });

    $('#pack-byWeight').on('click', function (e) {
        var form = $(e.currentTarget).closest('.modal').find('form');
        var packValue = form.find('.field-packValue');
        packValue.toggleClass('requiredValue');
        if( !packValue.hasClass('requiredValue') ) {
            packValue.val('1').attr('readonly', true);
        } else {
            packValue.val('').attr('readonly', false);
        }
    });



// For ALL form
    $('.saveBtn').on('click', function(e) {

        var error = 0;

        var form = $(e.currentTarget).closest('.modal').find('form');
        var ctrl = $(e.currentTarget).data('ctrl');

        form.find(".requiredValue").each(function(key, item) {
            if(!$(item).val()) {
                error = 1;
                $(item).closest('.form-group').addClass('has-error');
            } else if(!error) {
                $(item).closest('.form-group').removeClass('has-error');
            }
        });

        if(form.find("[def-life]") && form.find("[def-life]").val() === '') {
            form.find("[def-life]").val(form.find("[def-life]").attr('def-life'));
        }
        if((form.find("[def-temp_from]") && form.find("[def-temp_from]").val() === '') || (form.find("[def-temp_to]") && form.find("[def-temp_to]").val() === '') ) {
            form.find("[def-temp_from]").val(form.find("[def-temp_from]").attr('def-temp_from'));
            form.find("[def-temp_to]").val(form.find("[def-temp_to]").attr('def-temp_to'));
        }

        event.preventDefault();
//        console.log( data );

        if(error) {
            return false;
        }

        var tree = form.data('tree');
        var node = form.data('node');

        var data = form.serializeArray();
        $.ajax({
            method: "POST",
            url: getUrl({'r': ctrl + '/save'}),
            data: data
        }).done(function( data ) {
            if(data.error  !== undefined && data.error !== 1) {
                unlockData(ctrl, data.content.id);
                form.data('new', false);
                // tree.set_text(node, data.content.name.toString().replace(/\\"/g, '"'));
                // tree.get_node(node).data = data.content;
                tree.refresh();
                $(e.currentTarget).closest('.modal').modal('hide');
                if(data.has_child === undefined) {
                    alert( "Data Saved: " + JSON.stringify(data.content) );
                }
            } else {
                alert("Data Saved Error: " + JSON.stringify(data.content));
            }
        });
    });
