var deletedNodes = [];
var url = new URL((''+document.location));
var obj = url.searchParams.get("obj");

$(function() {
    var jstreeOptions = {
        "load_all": true,
        "core" : {
            "data" : {
                "url" : function (node) {
                    return getUrl({'r': dataUrl});
                },
                "data": function (node) {
                    return { 'id' : node.id };
                }
            },
            "check_callback" :  function (op, node, par, pos, more) {
                if ((op === "move_node" || op === "copy_node") && more.dnd && node.type && node.type === 'pack' && node.parent && node.parent !== more.ref.parent) {
                    return false;
                }
                return true;
            },
            'themes': { 'dots': true },
            'keyboard' : {
                'f2': false
            }
        },
        "plugins": ["contextmenu","dnd","unique","types"],
        'open_node': '#branch_1',
        "types": {
            'root': {
                'valid_children': ['group'],
                'children_type': "group",
                "check_hide" : true,
                'add_text': "Новая группа",
                'addFuncName': 'Group'
            },
            'group': {
                'valid_children': ['type'],
                'children_type': "type",
                'add_text': "Новый вид",
                'addFuncName': 'Type',
                'otherFuncName': 'Group',
                'icon': 'fa fa-envira c1'
            },
            'type': {
                'valid_children': ['tmark'],
                'children_type': "tmark",
                'add_text': "Новая торговая марка",
                'addFuncName': 'TMark',
                'otherFuncName': 'Type',
                'icon': 'fa fa-tag c2'
            },
            'tmark': {
                'valid_children': ['pack'],
                'children_type': "pack",
                'add_text': "Новая упаковка",
                'addFuncName': 'Pack',
                'otherFuncName': 'TMark',
                'icon': 'fa fa-trademark c3'
            },
            'pack': {
                'max_children': 0,
                'add_text': "Новая упаковка",
                'otherFuncName': 'Pack',
                'icon': 'fa fa-cube c4'
            }
        },
        "unique" : {
            "error_callback" : function (n, p, f) {
                alert("Duplicate node `" + n + "` with function `" + f + "`!");
            }
        },

        'contextmenu': {
            'items': function ($node) {
                var tree = $('#jstree-product').jstree(true);
                var typeRules = tree.settings.types;
                var items = {
                    "create": {
                        label: typeRules[$node.type].add_text,
                        "separator_after"	: true,
                        'icon' : 'fa fa-plus',
                        action: function(obj) {
                            var i = 0;
                            var originNode = $node;
                            do {
                                var sufix = (i > 0) ? ' (Копия ' + i + ')' : '';
                                $node = tree.create_node(originNode, {text: typeRules[originNode.type].add_text + sufix, type: typeRules[originNode.type].children_type, icon: typeRules[typeRules[originNode.type].children_type].icon});
                                i++;
//                                console.log($('#jstree-product').jstree(true).last_error());
                            } while(!$node && i < 100);
                            treeObj = obj;
                            if($node) {
                                tree.deselect_all();
                                tree.select_node($node);
                                var funcName = 'add' + typeRules[originNode.type].addFuncName;
                                eval(funcName)(tree, $node);
                            }
                        }
                    },
                    "edit": {
                        label: 'Изменить',
                        'icon' : 'fa fa-edit',
                        action: function(obj) {
                            tree.deselect_all();
                            tree.select_node($node);
                            var funcName = 'edit' + typeRules[$node.type].otherFuncName;
                            eval(funcName)(tree, $node);
                        }
                    },
                    'clone': {
                        label: 'Создать Копию',
                        'icon' : 'fa fa-clone',
                        action: function(obj) {
                            var i = 1;
                            var newNodeId = null;
                            var parentNode = tree.get_node($node.parent);
                            do {
                                var sufix = (i > 1) ? ' (Копия ' + i + ')' : ' (Копия)';
                                newNodeId = tree.create_node(parentNode, {text: $node.text + sufix, type: $node.type, icon: $node.icon });
                                i++;
//                                console.log($('#jstree-product').jstree(true).last_error());
                            } while(!newNodeId && i < 100);
                            tree.deselect_all();
                            tree.select_node(newNodeId);
                            var newNode = tree.get_node(newNodeId);
                            var funcName = 'add' + typeRules[$node.type].otherFuncName;
                            eval(funcName)(tree, newNode);
                        }
                    },
                    'delete': {
                        label: 'Удалить',
                        'icon' : 'fa fa-trash-o',
                        action: function(obj) {
                            tree.deselect_all();
                            var funcName = 'delete' + typeRules[$node.type].otherFuncName;
                            eval(funcName)(tree, $node);
                            var pos = deletedNodes.indexOf($node.id);
                            if(pos === -1) {
                                deletedNodes.push($node.id);
                            }
                            $('#' + $node.id).addClass('deleted');
                            $node.li_attr.class = 'deleted';
                            $node.data.deleted = '1';
                            if($('#viewDeleted .fa').hasClass('fa-eye-slash')) {
                                tree.hide_node($node);
                            }
                        }
                    },
                    'undelete': {
                        label: 'Восстановить',
                        'icon' : 'fa fa-undo',
                        action: function(obj) {
                            var funcName = 'undelete' + typeRules[$node.type].otherFuncName;
                            eval(funcName)(tree, $node);
                            var pos = deletedNodes.indexOf($node.id);
                            if(pos !== -1) {
                                deletedNodes.splice(pos, 1);
                            }
                            $('#' + $node.id).removeClass('deleted');
                            $node.li_attr.class = '';
                            $node.data.deleted = '0';
                        }
                    }
                };

                if($node.type === 'root') {
                    delete items.edit;
                    delete items.clone;
                    delete items.delete;
                }
                if($node.type === 'pack') {
                    delete items.create;
                }
                if($node.type === 'type' && obj !== 'all') {
                    delete items.create;
                }
                if($node.type === 'tmark' && obj !== 'all') {
                    delete items.create;
                }

                if($node.data && $node.data.deleted === '1') {
                    delete items.delete;
                    delete items.create;
                    delete items.edit;
                    if($node.type !== 'group') {
                        var parent = tree.get_node($node.parent);
                        if(parent.data && parent.data.deleted === '1') {
                            delete items.undelete;
                            delete items.clone;
                        }
                    }
                } else {
                    delete items.undelete;
                }

                return items;
            }
        }
    };

    if(typeof permit !== 'undefined') {
        if(!permit.canEditFullProducts) {
            if(!permit.canEditTmarkPackProducts) {
                jstreeOptions.core.check_callback = false;
                jstreeOptions.plugins = ["unique","types"]
            } else {
                jstreeOptions.core.check_callback = function (op, node, par, pos, more) {
                    if ((op === "move_node" || op === "copy_node") && more.dnd && node.type ) {
                        if(node.type !== 'pack' && node.type !== 'tmark') {
                            return false;
                        }
                        if (node.type === 'pack' && node.parent && node.parent !== more.ref.parent) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
    }

    $('#jstree-product').jstree(jstreeOptions).bind("move_node.jstree", function(e, data) {
        var tree = $('#jstree-product').jstree(true);
        var funcName = 'changePos' + tree.settings.types[data.node.type].otherFuncName;
        eval(funcName)(data.node, data.parent, data.position);
//        console.log("Drop node " + data.node.id + " to " + data.parent);
    }).on('refresh.jstree', function() {
        var tree = $('#jstree-product').jstree(true);
        if($('#viewDeleted .fa').hasClass('fa-eye') && deletedNodes.length > 0) {
            deletedNodes.forEach(function(item) {
                tree.show_node(item);
            });
        } else {
            deletedNodes.forEach(function(item) {
                tree.hide_node(item);
            });
        }
    }).on('ready.jstree', function(e,data) {
        var tree = data.instance;
        Object.values(tree._model.data).forEach(function(item) {
            if(item.data && item.data.deleted == '1') {
                deletedNodes.push(item.id);
                tree.hide_node(item.id);
                item.li_attr.class = 'deleted';
                $('#' + item.id).removeClass('hidden');
            }
            if(item.type === 'pack' && item.data && item.data.price > 0) {
                item.parents.forEach( function(parent) {
                    var node = tree.get_node(parent);
                    tree.open_node(node);
                });
            }
        });
    });
});


var timerLockId;

function lockData(form, ctrl, lockRowId) {
    var url = new URL((''+document.location));
    var obj = url.searchParams.get("obj");
    var path = url.origin + url.pathname;
    $.ajax({
        method: "POST",
        url: getUrl({'r': ctrl + '/lock'}),
        data: {id: lockRowId}
    }).done(function(data) {
        if(data.content.lock !== null) {
            form.find('.lockData').removeClass('hidden').text(data.content.lock);
            form.find(".canHide").addClass('hidden');
            form.find(".canLock").attr('disabled',true);
        } else {
            form.find('.lockData').addClass('hidden');
            form.find(".canHide").removeClass('hidden');
            form.find(".canLock").attr('disabled',false);
        }
        form.find('.offline').addClass('hidden');
    }).fail(function() {
        form.find('.offline').removeClass('hidden');
    });
}


function unlockData(ctrl, lockRowId) {
    $.ajax({
        method: "POST",
        url: getUrl({'r': ctrl + '/unlock'}),
        data: {id: lockRowId}
    }).done(function() {
        clearTimeout(timerLockId);
        $('.offline').addClass('hidden');
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
}

$('#viewDeleted').on('click', function(e) {
    var tree = $('#jstree-product').jstree(true);
    var keyViewDeleted = $(e.currentTarget).find('.fa');
    keyViewDeleted.toggleClass('fa-eye-slash').toggleClass('fa-eye');
    if(deletedNodes.length > 0) {
        if (keyViewDeleted.hasClass('fa-eye')) {
            deletedNodes.forEach(function (item) {
                tree.show_node(item);
            });
        } else {
            deletedNodes.forEach(function (item) {
                tree.hide_node(item);
            });
        }
    }
});


function resetForm(form) {
    form.find('input').each(function() {
        $(this).val('');
    });
    form.find('.variable').each(function() {
        $(this).text('');
    });
    form.find("input[type=\"checkbox\"]").each(function() {
        if($(this).prop('checked')) {
            $(this).click();
        }
    });
    form.find(".requiredValue").each(function() {
        $(this).closest('.form-group').removeClass('has-error');
    });
}

