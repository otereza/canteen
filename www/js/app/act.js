
"use strict";

var deletedNodes = [];
var searchRes = {};
var url = new URL((''+document.location));
var obj = url.searchParams.get("obj");
var forObj = url.searchParams.get("for");
var docId = url.searchParams.get("id");
var lastClick = 0;


var jstreeOptions = {
    "load_all": true,
    "core" : {
        "data" : {
            "url" : function (node) {
                return getUrl({
                    'r': 'actWritingOff/getData',
                    'obj': actObj || obj
                });
            }
        },
        "check_callback" :  function (op, node, par, pos, more) {
            if ((op === "move_node" || op === "copy_node") && more.dnd && node.type && node.type === 'pack' && node.parent && node.parent !== more.ref.parent) {
                return false;
            }
            return true;
        },
        'animation': 0,
        'themes': { 'dots': true },
        'keyboard' : {
            'f2': function (e) {
            }
        }
    },
    "plugins": ["grid","contextmenu","dnd","unique","types"],
    'grid': {
        'columns': [
            {
                'width': 'auto',
                'minWidth': '330px',
                'header': "&nbsp;"
            },
            {
                'width': 'auto',
                'minWidth': '100px',
                'header': "Количество",
                'value':  function(data) {
                    if(data.type === 'pack') {
                        return data.data.amount ? data.data.amount : 'нет';
                    }
                    return '';
                }
            },
            {
                'width': 'auto',
                'minWidth': '100px',
                'header': "Цена",
                'value': function(data) {
                    if(data.type === 'pack') {
                        return data.data.price ? (+data.data.price).toFixed(2) : 0;
                    }
                    return '';
                },
                'wideCellClass': 'price'
            },
            {
                'width': 'auto',
                'minWidth': '130px',
                'header': "Срок годности",
                'title': "Срок годности",
                'value': function(data) {
                    if(data.data && data.data.exp_date) {
                        return formatDate(new Date(data.data.exp_date * 1000));
                    }
                    return '';
                },
                'cellClass': 'margin-r10'
            }
        ],
        'resizable': true,
        'draggable': false,
        'gridcontextmenu': false,
        'headerContextMenu': false,
        'width': '100%'

    },
    'open_node': '#branch_1',
    "types": {
        'root': {
            'valid_children': ['group'],
            'children_type': "group",
            "check_hide" : true,
            'add_text': "Новая группа",
            'addFuncName': 'Group'
        },
        'group': {
            'valid_children': ['type'],
            'children_type': "type",
            'add_text': "Новый вид",
            'addFuncName': 'Type',
            'otherFuncName': 'Group',
            'icon': 'fa fa-envira c1'
        },
        'type': {
            'valid_children': ['tmark'],
            'children_type': "tmark",
            'add_text': "Новая торговая марка",
            'addFuncName': 'TMark',
            'otherFuncName': 'Type',
            'icon': 'fa fa-tag c2'
        },
        'tmark': {
            'valid_children': ['pack'],
            'children_type': "pack",
            'add_text': "Новая упаковка",
            'addFuncName': 'Pack',
            'otherFuncName': 'TMark',
            'icon': 'fa fa-trademark c3'
        },
        'pack': {
            'max_children': 0,
            'add_text': "Новая упаковка",
            'otherFuncName': 'Pack',
            'icon': 'fa fa-cube c4'
        }
    },
    "unique" : {
        "error_callback" : function (n, p, f) {
            alert("Duplicate node `" + n + "` with function `" + f + "`!");
        }
    }

};

jstreeOptions.core.check_callback = false;
jstreeOptions.plugins = ["grid","unique","types", "search"];
jstreeOptions.search = {show_only_matches: true, show_only_matches_children: true};

$(document).ready(function() {
    window.tmpDoc = new DocStorage('act');
    window.priceCoef = tmpDoc.getDoc()['priceCoef'] || 1;

    if($.jstree) {
        $('#jstree-product').jstree(jstreeOptions)
            .bind("move_node.jstree", jstreeMoveNode)
            .on("after_open.jstree", jstreeAfterOpen)
            .on("redraw.jstree", jstreeRedraw)
            .on('refresh.jstree', jstreeRefresh)
            .on('ready.jstree', jstreeReady)
            .on("click.jstree", jstreeClick)
            .on("search.jstree", jstreeSearch);
    }

    $('#viewDeleted').addClass('hidden');

    var curDate = new Date();
    var minutes = curDate.getMinutes();
    var timePeriodList = $('#timePeriodList');
    var timePeriodText = $('#timePeriodText');
    (minutes >= 30) ? curDate.setMinutes(30) : curDate.setMinutes(0);
    for(var i=0; i < 48; i++ ){
        var timeFrom = formatTime(curDate);
        curDate.setMinutes(curDate.getMinutes() + 30);
        timePeriodList.append('<li><a href="#">' + timeFrom + ' - ' + formatTime(curDate) + '</a></li>');
        if(!i) {
            timePeriodText.text(timeFrom + ' - ' + formatTime(curDate));
        }
    }
    timePeriodList.find('li a').on('click', function(e) {
        var curEl = $(e.currentTarget);
        timePeriodText.text(curEl.text());
        tmpDoc.doc.inv_time_period = curEl.text();
        if(tmpDoc.doc.order_id !== undefined) {
            checkTimePeriod();
        } else if(tmpDoc.doc.inv_receipt_id !== undefined) {
            checkTimePeriod();
        }
    });

    if(tmpDoc.doc.order_id !== undefined) {
        checkTimePeriod();
    }
    calcDocTotalPrice();
    sortableRows();
});

function jstreeMoveNode(e, data) {
    var tree = data.instance;
    var funcName = 'changePos' + tree.settings.types[data.node.type].otherFuncName;
    eval(funcName)(data.node, data.parent, data.position);
}

function jstreeAfterOpen(e, data) {
    var t = setTimeout(function () {
        if (typeof treeWithCheckboxes === 'undefined' || !treeWithCheckboxes) {
            $('.p-check-box').remove();
        }
        t = null;
    }, 10);
}

function jstreeRedraw(e, data) {
    var tree = data.instance;
    if (typeof treeWithCheckboxes === 'undefined' || !treeWithCheckboxes) {
        $('.p-check-box').remove();
    }
}

function jstreeRefresh(e, data) {
    var tree = $('#jstree-product').jstree(true);
    if ($('#viewDeleted .fa').hasClass('fa-eye') && deletedNodes.length > 0) {
        deletedNodes.forEach(function (item) {
            tree.show_node(item);
        });
    } else {
        deletedNodes.forEach(function (item) {
            tree.hide_node(item);
        });
    }
}

function jstreeReady(e, data) {
    var tree = data.instance;
    Object.values(tree._model.data).forEach(function (item) {
        if (item.data && +item.data.deleted === 1) {
            deletedNodes.push(item.id);
            tree.hide_node(item.id);
            item.li_attr.class = 'deleted';
            $('#' + item.id).removeClass('hidden');
        }
        if (item.type === 'pack' && item.data) {
            if (item.data.price && item.data.price > 0) {
                item.parents.forEach(function (parent) {
                    var node = tree.get_node(parent);
                    tree.open_node(node);
                });
            } else {
                tree.hide_node(item.id);
            }
        }
    });
}

function jstreeClick(e) {
    var time = new Date().getTime();
    if (time - lastClick < 400) {
        lastClick = 0;
        addNewRow();
    } else {
        lastClick = time;
    }
}

function jstreeSearch(e, data) {
    var words = $('#prodName').val().replace(/[^a-zA-Zа-яА-Я0-9%]+/g, ' ').trim().split(" ").slice(1);
    if (words.length && data.res.length) {
        data.res.forEach(function (item) {
            words.forEach(function (value) {
                var searchResEl = eval('searchRes.' + item + '_' + value);
                if (searchResEl === undefined) {
                    searchRes[item + '_' + value] = true;
                    data.instance.search(value, false, true, item, true);
                }
            });
        });
    }
}



$('#actStateTab a').on('click', function(e) {
    e.preventDefault();
    $(this).tab('show');
});


$(document).on('click', function(e) {
    var target = $(e.target);
    if(target.closest('.jstree-grid-wrapper').length === 0 && target.attr('id') !== 'prodName') {
        $('#jstreePanel').addClass('hidden');
    }
});


function sortableRows() {
    $('.prodList tbody').sortable({
        items: 'tr:not(#newRow)',
        cursor: "move",
        handle: 'td.dragAnchor',
        stop: function(e) {
            var rows = $('.prodList').find('.prodListItem:not(.hidden)');
            rows.each(function (key, el) {
                var rowId = $(el).data('id');
                tmpDoc.saveRow(rowId, { pos: key + 1 });
            });
        }
    });
}



$('#prodName').on('click', clickProdName).keyup(keyupProdName);
$('#addActBtn').on('click', addAct);
$('#obj').on('change', changeObj);
$('.viewAct').on('click', viewAct);
$('.amount, .realAmount').on('focusin', changeAmount).on('keypress', enterBtnPress);
$('.delDocProdBtn').on('click', delDocProd);
$('#date').on('change', checkTimePeriod );
$('#reason').on('change', function(e) {
    tmpDoc.doc.reason = $(e.currentTarget).val();
});
$('#commission').on('change', function(e) {
    tmpDoc.doc.commission = $(e.currentTarget).val();
});
$('#saveActBtn').on('click', preSave);
$('#saveBalanceActBtn').on('click', saveBalance);
$('#approveActBtn').on('click', approve);


function viewAct(e) {
    var curEl = $(e.currentTarget);
    var controller = curEl.data('cnt');
    var urlParams = {
        'r': controller + '/edit',
        'id': curEl.data('id')
    };
    if(obj === 'all') {
        urlParams.for = curEl.data('obj');
    }
    window.location.href = getUrl(urlParams);
}

function clickProdName(e) {
    $('#jstreePanel').removeClass('hidden').offset({ top: $(e.currentTarget).offset().top + $(e.currentTarget).outerHeight() + 5 });
}

function addAct(e) {
    var curEl = $(e.currentTarget);
    var objName = $('#obj').val();
    var typeBalance = $('#typeBalance').length ? $('#typeBalance').val() : null;
    var controller = curEl.data('cnt');

    if(!!objName) {
        $.ajax({
            method: "POST",
            url: getUrl({'r': controller + '/new'}),
            data: {
                obj: objName,
                typeBalance: typeBalance
            }
        }).done(function (data) {
            if (data.error !== undefined && !data.error) {
                var urlParams = {'r': controller + '/edit', 'id': data.content.id};
                if (obj === 'all' && !!objName) {
                    urlParams.for = objName;
                }
                window.location.href = getUrl(urlParams);
            }
        }).fail(function () {
            $('.offline').removeClass('hidden');
        });
    } else {
        var formBlock = $(e.currentTarget).closest('.form-group');
        formBlock.addClass('has-error').find('.help-block').removeClass('hidden');
    }
}

function changeObj(e) {
    var curEl = $(e.currentTarget);
    var formBlock = curEl.closest('.form-group');
    formBlock.removeClass('has-error').find('.help-block').addClass('hidden');
}


function checkTimePeriod(e) {
    var curEl = $(e.currentTarget);
    var deliveryOnTimeBtn = $('#deliveryOnTimeBtn').addClass('hidden');
    var traderDate, traderTime;
    var time = $('#timePeriodText').text();
    if (typeof traderDeliveryDays !== 'undefined' && Object.keys(traderDeliveryDays).length > 0) {
        var isRightDeliveryTime = true;
        var date = curEl.val();
        traderDate = date;
        traderTime = time;
        var dateArr = date.split('.');
        var curDate = new Date(dateArr[2], dateArr[1] - 1, dateArr[0]);
        var wDay = curDate.getDay() || 7;
        if (traderDeliveryDays[wDay] === undefined) {
            isRightDeliveryTime = false;
            for (var key in traderDeliveryDays) {
                wDay = --wDay || 7;
                curDate.setDate(curDate.getDate() - 1);
                if (traderDeliveryDays[wDay]) {
                    traderDate = formatDate(curDate);
                    traderTime = traderDeliveryDays[key]['from'] + ' - ' + traderDeliveryDays[key]['to'];
                    break;
                }
            }
        } else {
            var tTime = traderDeliveryDays[wDay]['from'] + ' - ' + traderDeliveryDays[wDay]['to'];
            if (tTime !== time) {
                traderDate = formatDate(curDate);
                traderTime = traderDeliveryDays[wDay]['from'] + ' - ' + traderDeliveryDays[wDay]['to'];
                isRightDeliveryTime = false;
            }
        }
        if (!isRightDeliveryTime) {
            deliveryOnTimeBtn.removeClass('hidden').one('click', function (e) {
                $(e.currentTarget).addClass('hidden');
                curEl.val(traderDate);
                $('#timePeriodText').text(traderTime);
            });
        }
    }
    tmpDoc.doc.inv_time_period = time;
}




var to = false;
function keyupProdName(e) {
    var curEl = $(e.currentTarget);
    if(to) { clearTimeout(to); }
    to = setTimeout(function () {
        var tree = $('#jstree-product').jstree(true);
        var str = curEl.val().replace(/[^a-zA-Zа-яА-Я0-9%]+/g, ' ').trim().split(" ");
        if(str.length  && str[0].length > 2) {
            searchRes = {};
            tree.search(str[0]);
        } else {
            tree.clear_search();
            tree.show_all();
            deletedNodes.forEach(function(item) {
                tree.hide_node(item);
            });
        }
    }, 100);
}


function addNewRow() {
    var tree = $('#jstree-product').jstree(true);
    var nodeId = tree.get_selected()[0];
    var node = tree.get_node(nodeId);
    if (node.type === 'pack' && node.data.price > 0) {
        var presentPackIdRow = tmpDoc.findRows('pack_id', node.data.id);
        if(!permit.canEditAct) {
            alert("Вы не можете редактировать данные");
        } else if(presentPackIdRow.length === 0) {
            var tmarkNode = tree.get_node(node.parent);
            var packName = node.data.by_weight === '0'
                ? node.data.name + ' (' + node.data.value + node.data.pack_unit + ')'
                : node.data.name + ' (' + node.data.pack_unit + ', на вес)';
            var packUnit = node.data.by_weight === '0'
                ? node.data.value + node.data.pack_unit
                : node.data.pack_unit;
            var tmarkName = tmarkNode.data.name;
            var prodName = tmarkName + ', ' + packName;
            var lastRow = $('#newRow').prev('tr');
            var newRow = lastRow.clone();


            if (node.data.id) {
                $.ajax({
                    method: "POST",
                    url: getUrl({'r': 'docProd/save', obj: (obj !== 'all') ? obj : forObj}),
                    data: {
                        doc_type: +window.docType,
                        doc_id: +docId,
                        obj: (obj !== 'all') ? obj : forObj,
                        pos: lastRow.index() + 1,
                        prod_name: prodName,
                        tmark_id: +tmarkNode.data.id,
                        pack_id: +node.data.id,
                        unit: node.data.pack_unit,
                        pack_unit: packUnit,
                        in_amount: node.data.amount,
                        amount: 1,
                        amount_unit: packUnit,
                        price: node.data.price,
                        exp_date: node.data.exp_date
                    }
                }).done(function (data) {
                    if (data.error !== undefined && !data.error) {
                        newRow.removeClass('hidden');
                        newRow.find('.prodName').text(prodName);
                        newRow.find('.packUnit').text(packUnit).data('pack-unit', packUnit);
                        newRow.find('.inPrice').text(node.data.price);
                        newRow.find('.inAmount').text(node.data.amount);
                        newRow.find('.onlyUnSignFloatNumbers').on('keypress', onlyUnSignFloatNumbers);
                        newRow.find('.amount').val((1.0).toFixed((units[packUnit] && units[packUnit]['decimal_places']) ? units[packUnit]['decimal_places'] : 0))
                            .on('focusin', changeAmount).on('keypress', enterBtnPress);
                        newRow.find('.delDocProdBtn').on('click', delDocProd);
                        newRow.find('.inTotalPrice').text(node.data.price * node.data.amount);
                        newRow.data('row-price', data.content.price);
                        newRow.data('id', data.content.id);
                        newRow.data('pack-id', data.content.pack_id);

                        $('.prodListItem.hidden').before(newRow);
                        flashBackground(newRow, '#dbffdb');

                        tree.clear_search();
                        tree.show_all();
                        deletedNodes.forEach(function (item) {
                            tree.hide_node(item);
                        });

                        tmpDoc.saveRow(data.content.id, data.content);
                        calcDocTotalPrice();
                    }
                }).fail(function () {
                    $('.offline').removeClass('hidden');
                });
            }
        } else {
            var presentRow = null;
            $('.prodListItem').each(function(index, item) {
                if (+$(item).data('pack-id') === + presentPackIdRow[0].pack_id) {
                    presentRow = $(item);
                }
            });
            if(presentRow) {
                flashBackground(presentRow, '#aaaaff');
            }
        }

        $('#prodName').val('');
        $('#jstreePanel').addClass('hidden');
    }
}

function delDocProd(e) {
    var row = $(e.currentTarget).closest('.prodListItem');
    var rowId = row.data('id');
    $.ajax({
        method: "POST",
        url: getUrl({'r': 'docProd/delete'}),
        data: {id: rowId}
    }).done(function(data) {
        if(data.error !== undefined && !data.error) {
            row.remove();
            tmpDoc.delRow(rowId);
            calcDocTotalPrice();
        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
}


function enterBtnPress(e) {
    if (e.which == null) { // IE
        if(e.keyCode === 13) {
            $(e.currentTarget).blur();
        }
    }

    if (e.which !== 0 && e.charCode !== 0) { // все кроме IE
        if(e.which === 13) {
            $(e.currentTarget).blur();
        }
    }
}


function changeAmount(e) {
    var prevValue = (+$(e.currentTarget).val());
    var row = $(e.currentTarget).closest('tr.prodListItem');
    $(e.currentTarget).one('focusout', function(e) {
        var newValue = (+$(e.currentTarget).val());
        var maxValue = (+$(e.currentTarget).prop('max'));
        if(row.find('.realAmount').length) {
            if($(e.currentTarget).val() !== '') {
                tmpDoc.saveRow(row.index(), {realAmount: newValue});
            } else {
                tmpDoc.saveRow(row.index(), {realAmount: null});
            }
        } else {
            if (maxValue > 0 && newValue > maxValue) {
                $(e.currentTarget).val(maxValue);
                tmpDoc.saveRow(row.data('id'), {amount: maxValue});
            } else if (prevValue !== newValue) {
                $(e.currentTarget).val(newValue);
                tmpDoc.saveRow(row.data('id'), {amount: newValue});
            }
        }
        calcRowPrice(row);
        if(row.find('.check').length) {
            row.find('.check').prop('checked', $(e.currentTarget).val() !== '');
            tmpDoc.saveRow(row.index(), {'checked': $(e.currentTarget).val() !== '' ? 1 : 0})
        }
    });
}

function calcRowPrice(row) {
    var amount;
    if(row.find('.realAmount').length) {
        var inAmount = row.find('.inAmount').text();
        var realAmount = row.find('.realAmount').val();
        amount = (realAmount === '' || isNaN(+realAmount)) ? 0 : realAmount - inAmount;
    } else {
        amount = +row.find('.amount').val();
    }
    var price = +row.find('.inPrice').text();
    row.data('row-price', (amount * price).toFixed(2));

    calcDocTotalPrice();
}

function calcDocTotalPrice() {
    var rows = $('.prodList').find('.prodListItem:not(.hidden)');
    var docTotalPrice = 0;
    rows.each(function(key, row) {
        docTotalPrice += +$(row).data('row-price');
    });
    $('#docTotalPrice').text(docTotalPrice.toFixed(2));
    tmpDoc.doc.total = docTotalPrice.toFixed(2);
}

function saveBalance() {
    tmpDoc.doc.preSave = 1;
    $.ajax({
        method: "POST",
        url: getUrl({'r': 'actBalance/save', 'obj': forObj || obj}),
        data: tmpDoc.doc
    }).done(function(data) {
        if(data.error !== undefined && !data.error) {
            window.location = getUrl({'r': 'actBalance'});
        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
}

function preSave() {
    tmpDoc.doc.preSave = 1;
    $.ajax({
        method: "POST",
        url: getUrl({'r': 'actWritingOff/save', 'obj': forObj || obj}),
        data: tmpDoc.doc
    }).done(function(data) {
        if(data.error !== undefined && !data.error) {
            alert("Сохранено");
        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
}

function approve() {
    tmpDoc.doc.approve = 1;
    $.ajax({
        method: "POST",
        url: getUrl({'r': 'actWritingOff/save', 'obj': forObj || obj}),
        data: tmpDoc.doc
    }).done(function(data) {
        if(data.error !== undefined && !data.error) {
            window.location.href = getUrl({'r': 'actWritingOff'});
        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
}

