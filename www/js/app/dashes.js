
var isChangedData = false;
var deletedNodes = [];
var searchRes = {};
var lastClick = 0;

var url = new URL((''+document.location));
var obj = url.searchParams.get("obj");
var forObj = url.searchParams.get("for");
var docId = url.searchParams.get("id");

var jstreeOptions = {
    "load_all": true,
    "core" : {
        "data" : {
            "url" : function (node) {
                return getUrl({
                    'r': 'product/getData'
                });
            }
        },
        "check_callback" :  false,
        'themes': { 'dots': true },
        'keyboard' : {
            'f2': function (e) {
            }
        }
    },
    "plugins": ["types", "search"],
    'open_node': '#branch_1',
    "types": {
        'root': {
            'valid_children': ['group'],
            'children_type': "group",
            "check_hide" : true,
            'add_text': "Новая группа",
            'addFuncName': 'Group'
        },
        'group': {
            'valid_children': ['type'],
            'children_type': "type",
            'add_text': "Новый вид",
            'addFuncName': 'Type',
            'otherFuncName': 'Group',
            'icon': 'fa fa-envira c1'
        },
        'type': {
            'valid_children': ['tmark'],
            'children_type': "tmark",
            'add_text': "Новая торговая марка",
            'addFuncName': 'TMark',
            'otherFuncName': 'Type',
            'icon': 'fa fa-tag c2'
        },
        'tmark': {
            'valid_children': ['pack'],
            'children_type': "pack",
            'add_text': "Новая упаковка",
            'addFuncName': 'Pack',
            'otherFuncName': 'TMark',
            'icon': 'fa fa-trademark c3'
        },
        'pack': {
            'max_children': 0,
            'add_text': "Новая упаковка",
            'otherFuncName': 'Pack',
            'icon': 'fa fa-cube c4'
        }
    }
};

jstreeOptions.search = {show_only_matches: true, show_only_matches_children: true};

$(function() {
    window.tmpDoc = new DocStorage('dish');

//    $('.dishesList:not(".in")').find('ul').fadeIn();
    $('.dishesGroup.in').find('ul').fadeOut();

    $('.dishesGroup').on('click', function(e) {
        var curEl = $(e.currentTarget);
        curEl.toggleClass('in');
        curEl.siblings('i.fa').toggleClass('fa-chevron-down').toggleClass('fa-chevron-right');
        if(curEl.hasClass('in')) {
            curEl.siblings('ul').fadeIn();
        } else {
            curEl.siblings('ul').fadeOut();
        }
    });

    // tinymce.init({
    //     selector:'#cooking',
    //     language: 'ru',
    //     branding: false,
    //     height: 500,
    //     // autosave_ask_before_unload: false,
    //     // powerpaste_allow_local_images: true,
    //     // plugins: [
    //     //     "advlist anchor autolink autoresize autosave bbcode charmap code codesample " +
    //     //     "directionality emoticons fullpage fullscreen help hr image imagetools importcss insertdatetime " +
    //     //     "legacyoutput link lists media nonbreaking noneditable pagebreak paste preview print  save " +
    //     //     "searchreplace spellchecker tabfocus table template textpattern toc visualblocks visualchars wordcount"
    //     // ],
    //     // toolbar:
    //     //     "insertfile a11ycheck undo redo | bold italic | forecolor backcolor | template codesample | alignleft aligncenter alignright alignjustify | bullist numlist | link image tinydrive",
    //     // spellchecker_dialog: true,
    //     setup : function(ed){
    //         ed.on('Change', function(e){
    //             isChangedData = true;
    //             tmpDoc.doc.cooking = ed.getContent();
    //         });
    //     }
    // });

    CKEDITOR.replace( 'cooking', {
        language: 'ru'
    });

    window.tmpDoc = new DocStorage('dish');
    window.priceCoef = tmpDoc.doc.priceCoef || 1;

    if($.jstree) {
        $('#jstree-product').jstree(jstreeOptions)
            .on('refresh.jstree', jstreeRefresh)
            .on('ready.jstree', jstreeReady)
            .on("click.jstree", jstreeClick)
            .on("search.jstree", jstreeSearch);
    }

    $('#viewDeleted').addClass('hidden');

    sortableRows();

});


function jstreeRefresh(e, data) {
    var tree = $('#jstree-product').jstree(true);
    if ($('#viewDeleted .fa').hasClass('fa-eye') && deletedNodes.length > 0) {
        deletedNodes.forEach(function (item) {
            tree.show_node(item);
        });
    } else {
        deletedNodes.forEach(function (item) {
            tree.hide_node(item);
        });
    }
}

function jstreeReady(e, data) {
    var tree = data.instance;
    Object.values(tree._model.data).forEach(function (item) {
        if (item.data && (+item.data.deleted === 1 || item.type === 'tmark' || item.type === 'pack')) {
            deletedNodes.push(item.id);
            tree.hide_node(item.id);
            item.li_attr.class = 'deleted';
            $('#' + item.id).removeClass('hidden');
        }
    });
}

function jstreeClick(e) {
    var time = new Date().getTime();
    if (time - lastClick < 400) {
        lastClick = 0;
        addNewRow();
    } else {
        lastClick = time;
    }
}

function jstreeSearch(e, data) {
    var words = $('#prodName').val().replace(/[^a-zA-Zа-яА-Я0-9%]+/g, ' ').trim().split(" ").slice(1);
    if (words.length && data.res.length) {
        data.res.forEach(function (item) {
            words.forEach(function (value) {
                var searchResEl = eval('searchRes.' + item + '_' + value);
                if (searchResEl === undefined) {
                    searchRes[item + '_' + value] = true;
                    data.instance.search(value, false, true, item, true);
                }
            });
        });
    }
}



$(document).on('click', function(e) {
    var target = $(e.target);
    var targetProdBlock = target.closest('#jstree-product');
    if(targetProdBlock.length === 0 && target.attr('id') !== 'prodName') {
        $('#jstreePanel').addClass('hidden');
    }
});

$(window).bind('beforeunload', function(e){
    if(isChangedData) {
        return true;
    } else {
        e = null;
    }
});


$('#btnAddDish').on('click', addDish);

$('#groups').on('change', changeGroup);
$('#recipes').on('change', changeRecipe);
$('#saveNewDishBtn').on('click', addNewDish);
$('.dishesItem').on('click', changeDish);
$('#btnAddNewCollectionRecipes').on('click', addNewCollectionRecipes);
$('#btnAddNewDishGroup').on('click', addNewDishGroup);
$('.unitSelect a').on('click', changeUnit);
$('.unitSelectProd a').on('click', changeProdUnit);
$('#tabTechMap li a').on('click', changeTechMap);
$('#prodName').on('click', clickProdName).keyup(keyupProdName);
$('#objList input[type="checkbox"]').on('click', changeObjList);
$('input').on('change', function() {
    isChangedData = true;
});
$('.standartOutput').on('change', changeStandartOutput);
$('#newDishForm').on('submit', function() {
    isChangedData = false;
});
$('#copyRowsCalculationMap').on('click', copyRowsCalculationMap);
$('.fieldBrutto').on('change', changeBrutto);
$('.fieldNetto').on('change', changeNetto);
$('#mapTypeOutput1').on('change', changeCalcValue);
$('#mapTypeOutput2').on('change', changeWorkValue);
$('.calcValue').on('change', changeOutletCalcValue);
$('.workValue').on('change', changeOutletWorkValue);
$('#saveDishBtn').on('click', saveForm);
$('.delDishProdBtn').on('click', delDishProd);
$('.changeable').on('change', changeChangeable);



function changeChangeable(e) {
    var curEl = $(e.currentTarget);
    var name = curEl.prop('name');
    tmpDoc.doc[name] = curEl.val();
}

function saveForm(e) {
    var data = tmpDoc.doc;
    data.cooking = CKEDITOR.instances.cooking.getData();
    $.ajax({
        method: "POST",
        url: getUrl({'r': 'dishes/save', 'obj': (!forObj) ? obj : forObj}),
        data: data
    }).done(function (data) {
        if (data.error !== undefined && !data.error) {
            isChangedData = false;
            window.location.reload();
        }
    }).fail(function () {
        $('.offline').removeClass('hidden');
    });

    e.preventDefault();
    return false;
}

function changeOutletCalcValue(e) {
    var curEl = $(e.currentTarget);
    var standartValue = $('#mapTypeOutput1').val();
    var newCoef = curEl.val() / (standartValue > 0 ? standartValue : 1);
    curEl.data('coef', newCoef);
    var feeding_id = curEl.closest('tr').data('id');
    tmpDoc.doc.outlet[feeding_id]['calculation'] = curEl.val();
}

function changeOutletWorkValue(e) {
    var curEl = $(e.currentTarget);
    var standartValue = $('#mapTypeOutput2').val();
    var newCoef = curEl.val() / (standartValue > 0 ? standartValue : 1);
    curEl.data('coef', newCoef);
    var feeding_id = curEl.closest('tr').data('id');
    tmpDoc.doc.outlet[feeding_id]['working'] = curEl.val();
}


function changeCalcValue(e) {
    var newValue = $(e.currentTarget).val();
    $('#feedingDishOutlet').find('.calcValue').each(function(key, item) {
        var roundTo = ($(item).prop('step').match(/0/g) || []).length;
        var coef = $(item).data('coef');
        var value = (newValue * coef).toFixed(roundTo);
        $(item).val(value);
        var feeding_id = $(item).closest('tr').data('id');
        tmpDoc.doc.outlet[feeding_id]['calculation'] = value;
    });
}

function changeWorkValue(e) {
    var newValue = $(e.currentTarget).val();
    $('#feedingDishOutlet').find('.workValue').each(function(key, item) {
        var roundTo = ($(item).prop('step').match(/0/g) || []).length;
        var coef = $(item).data('coef');
        var value = (newValue * coef).toFixed(roundTo);
        $(item).val(value);
        var feeding_id = $(item).closest('tr').data('id');
        tmpDoc.doc.outlet[feeding_id]['working'] = value;
    });
}


function changeBrutto(e) {
    var curEl = $(e.currentTarget);
    var rowId = curEl.closest('.prodListItem').data('id');
    tmpDoc.saveRow(rowId, {brutto: curEl.val()});
}

function changeNetto(e) {
    var curEl = $(e.currentTarget);
    var rowId = curEl.closest('.prodListItem').data('id');
    tmpDoc.saveRow(rowId, {netto: curEl.val()});
}


function copyRowsCalculationMap(e) {
    if(confirm("Текущая рабочая карта будет удалена. Все данные будут взяты с калькуляционной карты.")) {
        var rowsCalcMap = tmpDoc.findRows('map_type', mapCalc);
        var rowsWorkMap = tmpDoc.findRows('map_type', mapWork);
        rowsWorkMap.forEach(function (item) {
            tmpDoc.delRow(item.id);
        });

        $('.prodList .prodListItem').each(function (key, item) {
            if (+$(item).data('map-type') === +mapWork) {
                $(item).remove();
            }
        });

        $.ajax({
            method: "POST",
            url: getUrl({'r': 'dishes/copyCalculationMapProd', 'obj': (obj !== 'all') ? obj : forObj}),
            data: {rows: rowsCalcMap, dish_id: tmpDoc.doc.id}
        }).done(function (data) {
            if (data.error !== undefined && !data.error) {
                data.content.rows.forEach(function (item) {
                    appendRow(item);
                    tmpDoc.saveRow(item.id, item);
                });
            }
        }).fail(function () {
            $('.offline').removeClass('hidden');
        });
    }
    return false;
}

function changeStandartOutput(e) {
    var curEl = $(e.currentTarget);
    var techMapOutput = '#' + curEl.attr('id').replace('mapTypeOutput', 'techMapOutput');
    $(techMapOutput).text(curEl.val());
}

function changeObjList(e) {
    var curEl = $(e.currentTarget);
    var objTabs = $('#objTabs');
    if(curEl.is(":checked")) {
        tmpDoc.doc.objs[curEl.data('obj')] = 1;
        objTabs.find('li[data-obj="' + curEl.data('obj') + '"]').removeClass('hidden');
    } else {
        tmpDoc.doc.objs[curEl.data('obj')] = 0;
        objTabs.find('li[data-obj="' + curEl.data('obj') + '"]').addClass('hidden');
    }
    isChangedData = true;
}

var to = false;
function keyupProdName() {
    if(to) { clearTimeout(to); }
    to = setTimeout(function () {
        var tree = $('#jstree-product').jstree(true);
        var str = $('#prodName').val().replace(/[^a-zA-Zа-яА-Я0-9%]+/g, ' ').trim().split(" ");
        if(str.length  && str[0].length > 2) {
            searchRes = {};
            tree.search(str[0]);
        } else {
            tree.clear_search();
            tree.show_all();
            deletedNodes.forEach(function(item) {
                tree.hide_node(item);
            });
        }
    }, 100);
}

function addNewDishGroup() {
    var newGroupName = $('#newGroupName');
    var dishGroupList = $('#groups');
    if(!!newGroupName.val()) {
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'dishes/newDishGroup'}),
            data: {
                name: newGroupName.val()
            }
        }).done(function (data) {
            if (data.error !== undefined && !data.error) {
                dishGroupList.find('option:last').before('<option value="' + data.content + '" selected>' + newGroupName.val() + '</option>');
                newGroupName.addClass('hidden');
                $('#btnAddNewDishGroup').addClass('hidden');
                isChangedData = true;
            }
        }).fail(function () {
            $('.offline').removeClass('hidden');
        });
    }

    return false;
}


function addNewRow() {
    var tree = $('#jstree-product').jstree(true);
    var nodeId = tree.get_selected()[0];
    var node = tree.get_node(nodeId);
    if (node.type === 'type') {
        var prodName = node.data.name;
        var lastRow = $('#newRow').prev('tr');
        var newRow;

        if (node.data.id) {
            var mapType = $('#tabTechMap li.active a').data('type');

            $.ajax({
                method: "POST",
                url: getUrl({'r': 'dishes/saveProd', 'obj': (obj !== 'all') ? obj : forObj }),
                data: {
                    dish_id: docId,
                    map_type: mapType,
                    pos: lastRow.index() + 1,
                    prod_name: prodName,
                    unit: node.data.pack_unit
                }
            }).done(function (data) {
                if (data.error !== undefined && !data.error) {
                    newRow = appendRow(data.content);
                    flashBackground(newRow, '#dbffdb');

                    tree.clear_search();
                    tree.show_all();
                    deletedNodes.forEach(function (item) {
                        tree.hide_node(item);
                    });

                    tmpDoc.saveRow(data.content.id, data.content);
                    $('input').on('change', function() { isChangedData = true; });
                }
            }).fail(function () {
                $('.offline').removeClass('hidden');
            });
        }

        $('#prodName').val('');
        $('#jstreePanel').addClass('hidden');
    }
}

function appendRow(data) {
    var lastRow = $('#newRow').prev('tr');
    var newRow = lastRow.clone();
    newRow.removeClass('hidden').data('map-type', data.map_type);
    newRow.find('.onlyUnSignFloatNumbers').on('keypress', onlyUnSignFloatNumbers);
    newRow.find('.packUnitText').text(data.unit);
    newRow.find('.packUnit').val(data.unit).prop('name', 'techMap[' + data.id + '][unit]');
    newRow.find('.fieldBrutto').val(data.brutto).prop('name', 'techMap[' + data.id + '][brutto]').on('change', changeBrutto);
    newRow.find('.fieldNetto').val(data.netto).prop('name', 'techMap[' + data.id + '][netto]').on('change', changeNetto);
    newRow.data('id', data.id);
    newRow.find('.prodName').text(data.prod_name);
    var step = packUnits[data.unit] ? packUnits[data.unit].step : 1;
    newRow.find('input[type="number"]').prop('step', step);

    var unitGroup;
    Object.values(packUnits).forEach( function(item) {
        if(item.name === data.unit) {
            unitGroup = item.group;
        }
    });
    newRow.find('.unitSelectProd li').each(function(key, item) {
        if($(item).hasClass(unitGroup)) {
            $(item).removeClass('hidden');
        } else {
            $(item).addClass('hidden');
        }
    });

    newRow.find('.unitSelectProd a').on('click', changeProdUnit);
    newRow.find('.delDishProdBtn').on('click', delDishProd);

    lastRow.before(newRow);
    return newRow;
}

function delDishProd(e) {
    var row = $(e.currentTarget).closest('.prodListItem');
    var rowId = row.data('id');
    $.ajax({
        method: "POST",
        url: getUrl({'r': 'dishes/deleteProd'}),
        data: {id: rowId}
    }).done(function(data) {
        if(data.error !== undefined && !data.error) {
            row.remove();
            tmpDoc.delRow(rowId);
            isChangedData = true;
        }
    }).fail(function() {
        $('.offline').removeClass('hidden');
    });
}

function sortableRows() {
    $('.prodList tbody').sortable({
        items: 'tr:not(#newRow)',
        cursor: "move",
        handle: 'td.dragAnchor',
        stop: function(e) {
            var rows = $('.prodList').find('.prodListItem:not(.hidden)');
            rows.each(function (key, el) {
                var rowId = $(el).data('id');
                tmpDoc.saveRow(rowId, { pos: key + 1 });
            });
            isChangedData = true;
        }
    });
}

function clickProdName(e) {
    $('#jstreePanel').removeClass('hidden').offset({ top: $(e.currentTarget).offset().top + $(e.currentTarget).outerHeight() + 5 });
}

function changeTechMap(e) {
    var curEl = $(e.currentTarget);
    var mapType = curEl.data('type');
    $('.prodListItem').each(function(index, item) {
        if(+$(item).data('map-type') === +mapType) {
            $(item).removeClass('hidden');
        } else {
            $(item).addClass('hidden');
        }
    });

    curEl.closest('#tabTechMap').find('li.active').removeClass('active');
    curEl.closest('li').addClass('active');
    $('#techMapOutput' + mapType).removeClass('hidden').siblings('.techMapOutput').addClass('hidden');
    if(mapType === mapWork) {
        $('#copyRowsCalculationMap').removeClass('hidden');
    } else {
        $('#copyRowsCalculationMap').addClass('hidden');
    }
    e.preventDefault();
}

function changeUnit(e) {
    var curEl = $(e.currentTarget);
    var unitValue = curEl.closest('.dropdown-menu').siblings('button');
    unitValue = unitValue.length ? unitValue : curEl.closest('.dropdown-menu').siblings('.packUnitText');
    if(unitValue.hasClass('mainUnit')) {
        var step = packUnits[curEl.text()] ? packUnits[curEl.text()].step : 1;
        $('.mainUnit').text(curEl.text()).closest('.input-group').find('input[type="number"]').prop('step', step);
    } else {
        unitValue.text(curEl.text());
    }
    e.preventDefault();
    isChangedData = true;
}

function changeProdUnit(e) {
    e.preventDefault();
    var newUnit = $(e.currentTarget).text();
    var row = $(e.currentTarget).closest('tr.prodListItem');
    var oldUnit = row.find('.packUnit').val();
    if(oldUnit !== newUnit) {
        row.find('.packUnitText').text(newUnit);
        row.find('.packUnit').val(newUnit);

        var fieldBrutto = row.find('.fieldBrutto');
        var fieldNetto = row.find('.fieldNetto');
        var valueBrutto = +fieldBrutto.val() * packUnits[oldUnit]['to_smallest'] / packUnits[newUnit]['to_smallest'];
        var valueNetto = +fieldNetto.val() * packUnits[oldUnit]['to_smallest'] / packUnits[newUnit]['to_smallest'];

        var roundTo = +packUnits[newUnit]['decimal_places'];

        fieldBrutto.val(valueBrutto.toFixed(roundTo)).prop('step', packUnits[newUnit]['step']);
        fieldNetto.val(valueNetto.toFixed(roundTo)).prop('step', packUnits[newUnit]['step']);
        tmpDoc.saveRow(row.data('id'), {
            unit : newUnit,
            brutto : valueBrutto,
            netto : valueNetto
        });
    }
}


function addNewCollectionRecipes() {
    var newCollectionRecipes = $('#newCollectionRecipes');
    var recipesList = $('#recipes');
    if(!!newCollectionRecipes.val()) {
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'dishes/newCollectionRecipes'}),
            data: {
                name: newCollectionRecipes.val()
            }
        }).done(function (data) {
            if (data.error !== undefined && !data.error) {
                recipesList.find('option:last').before('<option value="' + data.content + '" selected>' + newCollectionRecipes.val() + '</option>');
                newCollectionRecipes.addClass('hidden');
                $('#btnAddNewCollectionRecipes').addClass('hidden');
            }
        }).fail(function () {
            $('.offline').removeClass('hidden');
        });
    }
    isChangedData = true;

    return false;
}

function changeDish(e) {
    var curEl = $(e.currentTarget);
    var dishId = curEl.data('id');
    window.location.href = getUrl({'r': 'dishes/edit', 'id': dishId})
}

function addDish(e) {
    $('#newDishModal').modal('show');
    $('#groupName').removeClass('has-error').find('.help-block').addClass('hidden');
    $('#dish').removeClass('has-error').find('.help-block').addClass('hidden');
    $('#newGroupName').addClass('hidden');
    var form = $('#newDishForm');
    form.find('input, select').val('');
}

function changeGroup(e) {
    $('#groupName').removeClass('has-error').find('.help-block').addClass('hidden');
    $('#dish').removeClass('has-error').find('.help-block').addClass('hidden');
    var value = $(e.currentTarget).val();
    if(!value) {
        $('#newGroupName').removeClass('hidden').focus();
        $('#btnAddNewDishGroup').removeClass('hidden');
    } else {
        $('#newGroupName').addClass('hidden');
        $('#btnAddNewDishGroup').addClass('hidden');
    }
    isChangedData = true;
}

function changeRecipe(e) {
    var value = $(e.currentTarget).val();
    if(!value) {
        $('#newCollectionRecipes').removeClass('hidden').focus();
        $('#btnAddNewCollectionRecipes').removeClass('hidden');
    } else {
        $('#newCollectionRecipes').addClass('hidden');
        $('#btnAddNewCollectionRecipes').addClass('hidden');
    }
    isChangedData = true;
}


function addNewDish(e) {
    var modal = $('#newDishModal');
    var form = $('#newDishForm');
    var group = $('#groups');
    var newGroupName =  $('#newGroupName');
    var dishName = $('#dishName');
    var hasError = false;

    if(!group.val()) {
        if(!newGroupName.val()) {
            $('#groupName').addClass('has-error').find('.help-block').removeClass('hidden');
            hasError &= true;
        }
    }

    if(!dishName.val()) {
        $('#dish').addClass('has-error').find('.help-block').removeClass('hidden');
        hasError &= true;
    }

    if(!hasError) {
        $.ajax({
            method: "POST",
            url: getUrl({'r': 'dishes/new'}),
            data: form.serializeArray()
        }).done(function(data) {
            if(data.error !== undefined && !data.error) {
                modal.modal('hide');
                window.location.href = getUrl({'r': 'dishes/edit', 'id': data.content.id})
            }
        }).fail(function() {
            $('.offline').removeClass('hidden');
        });
    }

}
