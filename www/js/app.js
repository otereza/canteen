$('.onlySignNumbers').on('keypress', function(e) {
    if (e.which == null) { // IE
        if(e.keyCode < 32 || !/^((\+|\-)?(\d+|\d+\.|\d+\.\d+)|(\+|\-))$/.test($(e.currentTarget).val() + String.fromCharCode(e.keyCode))) {
            return false;
        }
    }

    if (e.which != 0 && e.charCode != 0) { // все кроме IE
        if(e.which < 32 || !/^((\+|\-)?(\d+|\d+\.|\d+\.\d+)|(\+|\-))$/.test($(e.currentTarget).val() + String.fromCharCode(e.which))) {
            return false;
        }
    }

});

$('.onlyUnSignNumbers').on('keypress', function(e) {
    if (e.which == null) { // IE
        if(e.keyCode < 32 || !/^\d+$/.test($(e.currentTarget).val() + String.fromCharCode(e.keyCode))) {
            return false;
        }
    }

    if (e.which != 0 && e.charCode != 0) { // все кроме IE
        if(e.which < 32 || !/^\d+$/.test($(e.currentTarget).val() + String.fromCharCode(e.which))) {
            return false;
        }
    }

});

$('.onlyUnSignFloatNumbers').on('keypress', onlyUnSignFloatNumbers);

function onlyUnSignFloatNumbers(e) {
    if (e.which == null) { // IE
        if(e.keyCode < 32 || !/^(\d+|\d{1,}(\.|\,)|\d{0,}(\.|\,)\d{0,})$/.test($(e.currentTarget).val() + String.fromCharCode(e.keyCode))) {
            return false;
        }
    }

    if (e.which != 0 && e.charCode != 0) { // все кроме IE
        if(e.which < 32 || !/^(\d+|\d{1,}(\.|\,)|\d{0,}(\.|\,)\d{0,})$/.test($(e.currentTarget).val() + String.fromCharCode(e.which))) {
            return false;
        }
    }

}

var url = new URL((''+document.location));
var obj = url.searchParams.get("obj");

function getUrl(params) {
    var path = url.origin + url.pathname;
    var args = '?obj=' + (params.obj || obj);
    Object.entries(params).forEach(function(item) {
        if(item[0] !== 'obj') {
            args += '&' + item[0] + '=' + item[1];
        }
    } );
    return path + args;
}

function flashBackground(el, color) {
    el.animate({ 'background-color': color }, 500);
    setTimeout(function() {
        el.animate({ 'background-color': 'initial' }, 500);
        setTimeout(function() {
            el.css('background-color', '');
        }, 600);
    }, 1000);
}

function DocStorage (type) {
    window[type] = window[type] || {};
    this.doc = window[type];

    this.getDoc = function() {
        return this.doc;
    };

    this.saveDocInfo = function(data) {
        for (var key in data) {
            if (key != 'items') {
                this.doc[key] = data[key];
            }
        }
    };

    this.getRows = function(){
        return this.doc.items;
    };

    this.getChangedPriceRows = function() {
        var changedRows = {};
        var rows = this.getRows();
        Object.values(rows).forEach(function(row) {
            if(row.trader_price != row.trader_price_user) {
                changedRows["" + row.pack_id] = row;
            }
        });
        return changedRows;
    };

    this.findRows = function(key, value) {
        var findedRow = [];
        var rows = this.getRows();
        Object.values(rows).forEach(function(row) {
            if(row[key] == value) {
                findedRow.push(row);
            }
        });
        return findedRow;
    };

    this.getRow = function(rowId) {
        var rows = this.getRows();
        return rows[rowId];
    };

    this.saveRow = function(rowId, data) {
        var row = this.getRow(rowId);
        if(!row) {
            this.doc.items[rowId] = data;
        } else {
            for (var key in data) {
                if (key == 'price' && row[key] && row[key] != data[key] && !row.originPrice) {
                    row.originPrice = row[key];
                }
                row[key] = data[key];
            }
        }
    };

    this.delRow = function(rowId) {
        var row = this.getRow(rowId);
        if(row) {
            delete this.doc.items[rowId];
            return true;
        }
        return false;
    };
}

function formatTime(date) {
    var hours = (date.getHours() < 10 ? '0' : '') + date.getHours();
    var minutes = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    return hours + ':' + minutes;
}

function formatDate(date) {
    var day = ("0" + date.getDate()).slice(-2);
    var month = ("0" + (date.getMonth()+1)).slice(-2);
    var year = date.getFullYear();
    return day + '.' + month + '.' + year;
}
