<?
/* ПЕРВЫЕ 111 строк - это "заглушка" */

/* Класс User, заглушка */
class User {
    var $uid = false;
    var $screenName = '';
    var $tmpList = array(1 => 'Петр Иванов', 2 => 'Дима Сидоров', 3 => 'Мария Петрова');

    function __construct($id) {
        $this->uid = $id;
        $this->screenName = $this->tmpList[$id];
    }

    /* Есть ли разрешение what = 300..399: true / false */
    function havePermit($what, $param1 = false, $param2 = false, $param3 = false) {
        // Это просто примеры
        if ($what == 302) return true;                        // может редактировать "Продукты питания"
        if ($what == 307 && $param1 == 4) return true;	// может заказывать продукты для Объекта с id=4, это Детский сад
        return false;						// больше ничего делать НЕ может
    }

    /* Возвращает имена пользователей с uid = array(uid1, uid2, ...) */
    function getScreenName($uids) {
        $res = array();
        foreach($uids as $k=>$v) if ($this->tmpList[$v]) $res[] = $this->tmpList[$v];
        return $res;
    }
}

/* Класс Page, заглушка */
class Page {
    var $title;							// TITLE страницы по умолчанию
    var $paths      = array('imgs'  => '/img',
        'css'   => '/css',
        'js'    => '/js');			// пути к картинкам, css, Javascript
    var $css        = array('files' => array(),			// список включаемых css-файлов
        'text'  => '');				// включаемый текст css
    var $js         = array('files' => array(
        'head' => array(),
        'end'  => array()), 			// список включаемых js-файлов в <HEAD> и в конце <BODY>
        'text'  => array(
            'head' => '',
            'end'  => '')); 			// список включаемого js-текста в <HEAD> и в конце <BODY>
    var $html       = array('head'   => '',				// текст вверху
        'body'   => '',				// основной текст (меню, слайдер, текстовка)
        'hidden' => '');                          // скрытый текст (доп. слои)

    function __construct($title = '') {
        if ($title) $this->title = $title;
        else $this->title = 'Учет продуктов';

        array_push($this->css['files'], 'style_no.css');		// Подключили стиль (пример)
        array_push($this->js['files']['head'], 'basic_v3.js');		// Подключили JS-файл
        array_push($this->js['files']['end'], 'jquery.min.js');        // Подключили JQuery
    }

    // Вывод страницы на печать
    function go() {
        $tmpMenu = '<div style="width:100%;padding:5px 10px;background:#eee;">Это временное меню: <a href="#">Список продуктов</a> <a href="#">Список поставщиков</a> <a href="#">Список ещё какой-то хрени...</a></div>';

        global $user;
        echo '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>'.$this->title.'</title>';

        $a = array_unique ($this->css['files']);						// Подключение файлов CSS
        foreach($a as $key => $value) {
            $txt = substr($value,0,7) != 'http://' && substr($value,0,8) != 'https://'    ? $this->paths['css'].'/' : '';
            echo '<link href="'.$txt.$value.'" rel="stylesheet" media="screen">';
        }

        $a = array_unique ($this->js['files']['head']);                                        // Подключение файлов JS в <HEAD> ... </HEAD>
        foreach($a as $key => $value) {
            $txt = substr($value,0,7) != 'http://' && substr($value,0,8) != 'https://'    ? $this->paths['js'].'/' : '';
            echo '<script type="text/javascript" src="'.$txt.$value.'"></script>';
        }

        if ($this -> css['text'])                                                              // Вставка кода CSS
            echo "<style type=\"text/css\">\n".$this->css['text']."\n</style>";
        if ($this -> js['text']['head']) 							// Вставка кода JS в <HEAD> ... </HEAD>
            echo "<script type=\"text/javascript\">\n".$this->js['text']['head']."\n</script>";

        echo '</head><body>'.
            $tmpMenu.			// Текст меню
            $this->html['body'];		// Тело страницы (формирует Ваш скрипт)

        $a = array_unique($this->js['files']['end']);                                        // Подключение файлов JS в конце страницы
        foreach($a as $key => $value) {
            $txt = substr($value,0,7) != 'http://' && substr($value,0,8) != 'https://'    ? $this->paths['js'].'/' : '';
            echo '<script type="text/javascript" src="'.$txt.$value.'"></script>';
        }

        if ($this -> js['text']['end']) 							// Вставка кода JS в конце страницы
            echo "<script type=\"text/javascript\">\n".$this->js['text']['end']."\n</script>";

        echo '</body></html>';
    }
}

$user = new User(1);				// Текущий пользователь - Петр Иванов, uid=1
$page = new Page('Модуль питание');

$businessobj = array(
    'u2'=>'1. ОЛ "Звездный"',
    'u3'=>'2. ОЛ "Юность"',
    'u4'=>'3. Детский сад',
    'u5'=>'4. Офис',
    'u6'=>'5. Школа',
    'u7'=>'6. ОЛ "Горизонт"');

$FILES_ROOT = $_SERVER['DOCUMENT_ROOT'].'/food';      // Полный путь до каталога, где лежит скрипт
$ACTIONPAGE = '/food/';              			// URL, как вызвать скрипт
if ($_GET['cmd'] == 'food') food_module_start();	// вызов Вашего модуля
if ($_GET['cmd'] == 'go') my_module_start();
$page->go();

/******* А ЗДЕСЬ УЖЕ НАЧИНАЕТСЯ ВАШ МОДУЛЬ **************/
// Все классы и функции, естественно, в отдельных файлах будут :-)
function food_module_start() {
    global $user, $page, $ACTIONPAGE, $FILES_ROOT, $businessobj;
    $page->title = 'Это тестовый запуск';
    $page->html['body'] .= '<div style="padding:10px;">Это тестовый HTML от Вашего модуля. На стили внимание не обращайте :-)</div>';
}


// Это, например, другой модуль: вывод кода этого файла
function my_module_start() {
    $txt = file_get_contents('index.php');
    header('Content-type: text/plain');
    echo $txt;
    exit;
}
?>