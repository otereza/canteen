<?
global $ACTIONPAGE;

$forObj = empty($_GET['for']) ? null : $_GET['for'];

$obj = empty($_GET['obj']) ? '' : $_GET['obj'];

?>

<h2>Готовые блюда</h2>


<div class="container-fluid">
    <? if(!empty($permit['canEditTrader']) && $obj == 'all') { ?>
        <button type="button" class="btn btn-primary btn-sm canHide margin-b20 addTraderBtn" style="margin-left: 30px;">Добавить поставщика</button>
    <? } ?>



    <button class="btn btn-success btn-sm newDish" id="btnAddDish">Добавить блюдо</button>

    <div class="row">
        <div class="col-md-6">
            <ul id="sTree">

                <? foreach($list as $groupId => $item) { ?>
                    <li class="dishesList">
                        <? if(!empty($item['dishes'])) { ?>
                            <i class="fa fa-chevron-right"></i>
                        <? } ?>
                        <div class="dishesGroup" data-id="<?= $groupId ?>"><?= $item['name'] ?></div>

                        <? if(!empty($item['dishes'])) { ?>
                            <ul style="display: none;">
                                <? foreach($item['dishes'] as $dishId => $dish) { ?>
                                    <li>
                                        <i class="fa fa-cutlery c6"></i>
                                        <div class="dishesItem" data-id="<?= $dishId ?>"><?= $dish['dish_name'] ?></div>
                                    </li>
                                <? } ?>
                            </ul>
                        <? } ?>

                    </li>
                <? } ?>

            </ul>
        </div>
    </div>

</div>

<!--  ФОРМЫ  -->
<? use \Food\Core\View; ?>

<?= View::renderPart('dishes/forms/groupModal', array('groups' => $groups)); ?>
