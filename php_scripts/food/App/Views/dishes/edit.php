<?
global $ACTIONPAGE, $businessobj;

use \Food\App\Helpers\Units;
use Food\Core\View;
use \Food\App\Models\Repositories\TechMapProdRep;

$forObj = empty($_GET['for']) ? null : $_GET['for'];
$obj = empty($_GET['obj']) ? '' : $_GET['obj'];

$groups = isset($groups) ? $groups : array();
$unitList = Units::getAll();

?>

<script>
    mapCalc = <?= TechMapProdRep::TECH_MAP_TYPE_CALC ?>;
    mapWork = <?= TechMapProdRep::TECH_MAP_TYPE_WORK ?>;
</script>

<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= $ACTIONPAGE . '?obj=' . $obj . '&r=dishes' ?>">Список готовых блюд</a></li>
        <li class="breadcrumb-item active">Редактирование блюда</li>
    </ol>
</nav>

<form role="form" id="newDishForm" action="<?= $ACTIONPAGE . '?obj=' . $obj . '&r=dishes/save' ?>" method="post">
    <? if(!empty($objDish['dish_name'])) {
        $dish_name = $objDish['dish_name'];
        $isChanged =  $objDish['dish_name'] != $dish['dish_name'];
    } else {
        $dish_name = empty($dish['dish_name']) ? '' : $dish['dish_name'];
        $isChanged = false;
    } ?>
    <h2><input type="text" class="changeable <?= $isChanged ? 'bgC2' : '' ?>" value="<?= $dish_name ?>" name="dish_name"></h2>

    <div class="form-inline margin-b20" id="groupName">
        <? if(!empty($objDish['group_id'])) {
            $group_id = $objDish['group_id'];
            $isChanged =  $objDish['group_id'] != $dish['group_id'];
        } else {
            $group_id = empty($dish['group_id']) ? '' : $dish['group_id'];
            $isChanged = false;
        } ?>
        <label for="group" class="control-label"> Группа: </label>
        <select class="form-control requiredValue changeable <?= $isChanged ? 'bgC2' : '' ?>" id="groups" name="group_id">
            <option value="" selected class="hidden"></option>
            <? foreach($groups as $group) { ?>
                <option class="bgC0" value="<?= $group['id'] ?>" <?= $group_id == $group['id'] ? 'selected' : '' ?>><?= $group['name'] ?></option>
            <? } ?>
            <option value="" class="c3 bgC0">-- новая --</option>
        </select>
        <input type="text" id="newGroupName" class="form-control hidden" placeholder="Название новой группы">
        <button class="btn btn-sm btn-success hidden" id="btnAddNewDishGroup"> Добавить группу </button>
        <span class="help-block hidden">Выберите или создайте новую группу</span>
        <button type="submit" id="saveDishBtn" class="btn btn-primary btn-sm margin-l20">Сохранить блюдо</button>
    </div>


    <? if($obj == 'all') { ?>
        <ul class="nav nav-tabs" id="objTabs">
            <li<?= empty($forObj) ? ' class="active"' : '' ?>><a href="<?= $ACTIONPAGE ?>?obj=<?= $obj ?>&r=dishes/edit&id=<?= empty($dish['id']) ? 0 : $dish['id'] ?>">Общая информация</a></li>
            <? foreach ($businessobj as $b_obj => $name) { ?>
                    <li class="<?= ($forObj == $b_obj) ? 'active' : '' ?> <?= (!empty($dish['objs']) && !empty($dish['objs'][$b_obj])) ? '' : ' hidden' ?>" data-obj="<?= $b_obj ?>">
                        <a href="<?= $ACTIONPAGE ?>?obj=<?= $obj ?>&r=dishes/edit&id=<?= empty($dish['id']) ? 0 : $dish['id'] ?>&for=<?= $b_obj ?>"><?= $name ?></a>
                    </li>
            <? } ?>
        </ul>
    <? } else { ?>
        <hr class="margin-b5">
    <? } ?>


    <div class="container-fluid">
        <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
        <div class="alert alert-warning fade in lockData hidden"></div>


        <div class="row">
            <? if(!empty($objDish['collection_recipe_id'])) {
                $collection_recipe_id = $objDish['collection_recipe_id'];
                $isChanged =  $objDish['collection_recipe_id'] != $dish['collection_recipe_id'];
            } else {
                $collection_recipe_id = $dish['collection_recipe_id'];
                $isChanged = false;
            } ?>
            <div class="form-group form-inline">
                <div class="col-md-2">
                    <label for="recipe" class="control-label"> Сборник рецептур: </label>
                </div>
                <div class="col-md-10">
                    <select name="collection_recipe_id" class="form-control requiredValue changeable <?= $isChanged ? 'bgC2' : '' ?>" id="recipes" style="width: 60%;">
                        <option value="" selected class="hidden"></option>
                        <? foreach($recipes as $recipe) { ?>
                            <option class="bgC0" value="<?= $recipe['id'] ?>" <?= $collection_recipe_id == $recipe['id'] ? 'selected' : '' ?>><?= $recipe['name_n_year'] ?></option>
                        <? } ?>
                        <option value="" class="bgC0 c3">-- новый --</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group form-inline">
                <div class="col-md-2"> </div>
                <div class="col-md-10 hidden"  id="newCollectionRecipes">
                    <input type="text" class="form-control" placeholder="Название и год издания" style="width: 60%;">
                    <button class="btn btn-sm btn-success hidden" id="btnAddNewCollectionRecipes"> Добавить </button>
                    <span class="help-block small"> Название и год издания </span>
                </div>
            </div>
        </div>


        <div class="row margin-b20">
            <? if(!empty($objDish['recipe_number'])) {
                $recipe_number = $objDish['recipe_number'];
                $isChanged =  $objDish['recipe_number'] != $dish['recipe_number'];
            } else {
                $recipe_number = empty($dish['recipe_number']) ? '' : $dish['recipe_number'];
                $isChanged = false;
            } ?>
            <div class="form-group form-inline">
                <div class="col-md-2">
                    <label for="recipeNumber" class="control-label"> Номер рецепта в сборнике: </label>
                </div>
                <div class="col-md-10">
                    <input type="number" id="recipeNumber" class="form-control changeable <?= $isChanged ? 'bgC2' : '' ?>" name="recipe_number" value="<?= $recipe_number ?>">
                </div>
            </div>
        </div>

        <div class="row margin-b10">
            <? if(!empty($objDish['creator_technological_map'])) {
                $creator_technological_map = $objDish['creator_technological_map'];
                $isChanged =  $objDish['creator_technological_map'] != $dish['creator_technological_map'];
            } else {
                $creator_technological_map = empty($dish['creator_technological_map']) ? '' : $dish['creator_technological_map'];
                $isChanged = false;
            } ?>
            <div class="form-group">
                <div class="col-md-2">
                    <label for="creatorTechnologicalMap" class="control-label"> ФИО технолога и дата разработки рабочей технологической карты: </label>
                </div>
                <div class="col-md-10">
                    <input type="text" id="creatorTechnologicalMap" class="form-control changeable <?= $isChanged ? 'bgC2' : '' ?>" name="creator_technological_map" value="<?= $creator_technological_map ?>">
                </div>
            </div>
        </div>

        <? if($obj == 'all' && empty($forObj)) { ?>
            <div class="row margin-b20">
                <div class="form-group form-inline">
                    <div class="col-md-2">
                        <label class="control-label">Список объектов:</label>
                    </div>
                    <div class="col-md-10" id="objList">
                        <? foreach ($businessobj as $b_obj => $name) { ?>
                            <div class="checkbox">
                                <label class="margin-r30">
                                    <input type="checkbox" class="canLock" data-obj="<?= $b_obj ?>" <?= (!empty($dish['objs']) && !empty($dish['objs'][$b_obj])) ? ' checked' : '' ?> name="objs[]">
                                    <?= $name ?>
                                </label>
                            </div>
                        <? } ?>
                    </div>
                </div>
            </div>
        <? } ?>

        <div class="row margin-b30">
            <div class="form-group form-inline">
                <div class="col-md-2">
                    <label class="control-label"> Стандартные выходы блюда: </label>
                </div>
                <div class="col-md-10">
                    <? if(!empty($objDish['standart_calculation_output'])) {
                        $output_unit = $objDish['output_unit'] ? 'г' : $objDish['output_unit'];
                        $standart_calculation_output = $objDish['standart_calculation_output'];
                        $isChanged =  $objDish['standart_calculation_output'] != $dish['standart_calculation_output'] || $objDish['output_unit'] != $dish['output_unit'];
                    } else {
                        $output_unit = empty($dish['output_unit']) ? 'г' : $dish['output_unit'];
                        $standart_calculation_output = empty($dish['standart_calculation_output']) ? '' : $dish['standart_calculation_output'];
                        $isChanged = false;
                    } ?>
                    <div class="input-group">
                        <input type="number"
                               step="<?= Units::getStep($output_unit) ?>"
                               min="0" id="mapTypeOutput<?= TechMapProdRep::TECH_MAP_TYPE_CALC ?>"
                               class="form-control standartOutput changeable <?= $isChanged ? 'bgC2' : '' ?>"
                               name="standart_calculation_output"
                               value="<?= $standart_calculation_output ?>">
                        <small class="help-block small"> Калькуляционный </small>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle mainUnit <?= $isChanged ? 'bgC2' : '' ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?= empty($dish['output_unit']) ? 'г' : $dish['output_unit'] ?>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right unitSelect">
                                <? foreach ($unitList as $unit) { ?>
                                    <li><a href="#"><?= $unit['name'] ?></a></li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>

                    <div class="input-group">
                        <? if(!empty($objDish['standart_working_output'])) {
                            $output_unit = $objDish['output_unit'] ? 'г' : $objDish['output_unit'];
                            $standart_working_output = $objDish['standart_working_output'];
                            $isChanged =  $objDish['standart_working_output'] != $dish['standart_working_output'] || $objDish['output_unit'] != $dish['output_unit'];
                        } else {
                            $output_unit = empty($dish['output_unit']) ? 'г' : $dish['output_unit'];
                            $standart_working_output = empty($dish['standart_working_output']) ? '' : $dish['standart_working_output'];
                            $isChanged = false;
                        } ?>
                        <input type="number"
                               step="<?= Units::getStep($dish['output_unit']) ?>"
                               min="0" id="mapTypeOutput<?= TechMapProdRep::TECH_MAP_TYPE_WORK ?>"
                               class="form-control standartOutput changeable <?= $isChanged ? 'bgC2' : '' ?>"
                               name="standart_working_output"
                               value="<?= $standart_working_output ?>">
                        <small class="help-block small"> Рабочий </small>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle mainUnit <?= $isChanged ? 'bgC2' : '' ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?= empty($dish['output_unit']) ? 'г' : $dish['output_unit'] ?>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right unitSelect">
                                <? foreach ($unitList as $unit) { ?>
                                    <li><a href="#"><?= $unit['name'] ?></a></li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <hr>

        <div class="row">

            <div class="col-md-7">
                <ul class="nav nav-tabs" id="tabTechMap">
                    <li class="active"><a href="#" data-type="<?= TechMapProdRep::TECH_MAP_TYPE_CALC ?>">Калькуляционная карта</a></li>
                    <li><a href="#" data-type="<?= TechMapProdRep::TECH_MAP_TYPE_WORK ?>">Рабочая карта</a></li>
                </ul>
                <table class="table table-hover prodList <?= isset($objDish['isChangedTechMap']) && $objDish['isChangedTechMap'] ? 'bgC2' : '' ?>">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Наименование</th>
                        <th>Ед.изм.</th>
                        <th>вес Брутто </th>
                        <th>вес Нетто</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? $totalPrice = 0; ?>

                    <? if (!empty($rows)) { ?>
                        <? foreach ($rows as $row) { ?>

                            <tr class="prodListItem <?= $row['map_type'] == TechMapProdRep::TECH_MAP_TYPE_CALC ? '' : 'hidden' ?>" data-map-type="<?= $row['map_type'] ?>" data-id="<?= $row['id'] ?>">
                                <td class="dragAnchor" style="width: 30px"><i class="fa fa-ellipsis-v"></i></td>
                                <td style="width: 40%"><span class="prodName"><?= $row['prod_name'] ?></span></td>
                                <td style="width: 20%">
                                    <input type="hidden" class="form-control requiredValue packUnit" value="<?= $row['unit'] ?>"
                                           name="techMap[<?= $row['id'] ?>][unit]">
                                    <div class="dropdown form-control canLock center-block text-left" style="width: 100px">
                                        <span class="variable packUnitText"><?= $row['unit'] ?></span>
                                        <a href="#" data-toggle="dropdown" class="dropdown-toggle pull-right"><b class="caret"></b></a>
                                        <ul class="dropdown-menu unitSelectProd">
                                            <li class="weight <?= Units::getUnitGroup($row['unit']) == 'weight' ? '' : 'hidden' ?>"><a href="#">кг</a></li>
                                            <li class="weight <?= Units::getUnitGroup($row['unit']) == 'weight' ? '' : 'hidden' ?>"><a href="#">г</a></li>
                                            <li class="volume <?= Units::getUnitGroup($row['unit']) == 'volume' ? '' : 'hidden' ?>"><a href="#">л</a></li>
                                            <li class="volume <?= Units::getUnitGroup($row['unit']) == 'volume' ? '' : 'hidden' ?>"><a href="#">мл</a></li>
                                            <li class="piece <?= Units::getUnitGroup($row['unit']) == 'piece' ? '' : 'hidden' ?>"><a href="#">дес</a></li>
                                            <li class="piece <?= Units::getUnitGroup($row['unit']) == 'piece' ? '' : 'hidden' ?>"><a href="#">шт</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td style="width: 20%">
                                    <input type="number" step="<?= Units::getUnitStep($row['unit']) ?>" min="0"
                                           class="form-control onlyUnSignFloatNumbers text-right fieldBrutto"
                                           name="techMap[<?= $row['id'] ?>][brutto]"
                                           value="<?= $row['brutto'] ?>" size="10">
                                </td>
                                <td style="width: 20%">
                                    <input type="number" step="<?= Units::getUnitStep($row['unit']) ?>" min="0"
                                           class="form-control onlyUnSignFloatNumbers text-right fieldNetto"
                                           name="techMap[<?= $row['id'] ?>][netto]"
                                           value="<?= $row['netto'] ?>" size="10">
                                </td>
                                <td class="text-center" style="width: 30px"><a type="button" class="btn btn-link btn-sm delDishProdBtn" data-id="<?= $row['id'] ?>"><i class="fa fa-remove c2"></i></a></td>
                            </tr>
                        <? } ?>
                    <? } ?>

                    <tr class="prodListItem hidden">
                        <td class="dragAnchor" style="width: 30px"><i class="fa fa-ellipsis-v"></i></td>
                        <td style="width: 40%"><input type="hidden" value=""><span class="prodName"></span></td>
                        <td style="width: 20%">
                            <input type="hidden" class="form-control requiredValue packUnit" value="">
                            <div class="dropdown form-control canLock center-block text-left" style="width: 100px">
                                <span class="variable packUnitText"></span>
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle pull-right"><b class="caret"></b></a>
                                <ul class="dropdown-menu unitSelectProd">
                                    <li class="weight"><a href="#">кг</a></li>
                                    <li class="weight"><a href="#">г</a></li>
                                    <li class="volume hidden"><a href="#">л</a></li>
                                    <li class="volume hidden"><a href="#">мл</a></li>
                                    <li class="piece hidden"><a href="#">дес</a></li>
                                    <li class="piece hidden"><a href="#">шт</a></li>
                                </ul>
                            </div>
                        </td>
                        <td style="width: 20%">
                            <input type="number" step="<?= !empty($row['unit']) ? Units::getStep($row['unit']) : 1 ?>" min="0"
                                   class="form-control onlyUnSignFloatNumbers text-right fieldBrutto"
                                   value="" size="10">
                        </td>
                        <td style="width: 20%">
                            <input type="number" step="<?= !empty($row['unit']) ? Units::getStep($row['unit']) : 1 ?>" min="0"
                                   class="form-control onlyUnSignFloatNumbers text-right fieldNetto"
                                   value="" size="10">
                        </td>
                        <td style="width: 30px"><a type="button" class="btn btn-link btn-sm delDishProdBtn"><i class="fa fa-remove c2"></i></a></td>
                    </tr>

                    <tr id="newRow" class="active">
                        <td>&nbsp;</td>
                        <td><input type="text" id="prodName" style="width: 100%"></td>
                        <td>&nbsp;</td>
                        <td class="text-right">&nbsp;Выход блюда: </td>
                        <td>
                            <span class="text-right h2 techMapOutput" id="techMapOutput<?= TechMapProdRep::TECH_MAP_TYPE_CALC?>">
                                <?= $dish['standart_calculation_output'] ?>
                            </span>
                            <span class="text-right h2 techMapOutput hidden" id="techMapOutput<?= TechMapProdRep::TECH_MAP_TYPE_WORK?>">
                                <?= $dish['standart_working_output'] ?>
                            </span>
                            <span class="mainUnit text-left mainUnit h2">
                                <?= $dish['output_unit'] ?>
                            </span>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
                <button class="btn btn-primary pull-right hidden" id="copyRowsCalculationMap">скопировать калькуляционную карту</button>
            </div>

            <div class="col-md-5">
                <h4 class="text-center"> Выход блюда </h4>
                <hr>
                <table class="table table-sm table-hover" id="feedingDishOutlet">
                <thead>
                <tr>
                    <th style="width: auto">Категория</th>
                    <th style="width: auto">Подкатегория</th>
                    <th style="width: 150px">калькуляционный</th>
                    <th style="width: 150px">рабочий</th>
                </tr>
                </thead>
                <tbody>
                <? if(!empty($feeds) && !empty($feeding_dish_outlet)) { ?>
                    <? foreach ($feeds as $feed) { ?>
                        <? if(($obj == 'all' && !$forObj) || ($forObj && !empty($feed['objs'][$forObj])) || ($obj != 'all' && !empty($feed['objs'][$obj])) ) { ?>
                            <? if($feed['parent_id'] == 0) { ?>
                                <tr data-id="<?= $feed['id'] ?>">
                                    <td><?= $feed['name'] ?></td>
                                    <td></td>
                                    <td class="l-border">
                                        <div class="input-group">
                                            <input type="number" min="0" size="10"
                                                   class="form-control border-0 text-right onlyUnSignFloatNumbers calcValue <?= !empty($feeding_dish_outlet[$feed['id']]['isChangedCalculation']) ? 'bgC2' : '' ?>"
                                                   step="<?= empty($dish['output_unit']) ? '1' : Units::getStep($dish['output_unit']) ?>"
                                                   value="<?= empty($feeding_dish_outlet[$feed['id']]['calculation']) ? $dish['standart_calculation_output'] : $feeding_dish_outlet[$feed['id']]['calculation'] ?>"
                                                   data-coef="<?= empty($feeding_dish_outlet[$feed['id']]['calculation']) ? 1 :  $feeding_dish_outlet[$feed['id']]['calculation'] / ($dish['standart_calculation_output'] > 0 ? $dish['standart_calculation_output'] : 1) ?>">
                                            <span class="input-group-addon mainUnit border-0 padding-0 bgC0 text-left mainUnit">
                                                <?= empty($dish['output_unit']) ? 'г' : $dish['output_unit'] ?>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <input type="number" min="0" size="10"
                                                   class="form-control border-0 text-right onlyUnSignFloatNumbers workValue <?= !empty($feeding_dish_outlet[$feed['id']]['isChangedWorking']) ? 'bgC2' : '' ?>"
                                                   step="<?= empty($dish['output_unit']) ? '1' : Units::getStep($dish['output_unit']) ?>"
                                                   value="<?= empty($feeding_dish_outlet[$feed['id']]['working']) ? $dish['standart_working_output'] : $feeding_dish_outlet[$feed['id']]['working'] ?>"
                                                   data-coef="<?= empty($feeding_dish_outlet[$feed['id']]['working']) ? 1 :  $feeding_dish_outlet[$feed['id']]['working'] / ($dish['standart_working_output'] > 0 ? $dish['standart_working_output'] : 1) ?>">
                                            <span class="input-group-addon mainUnit border-0 padding-0 bgC0 text-left mainUnit">
                                                <?= empty($dish['output_unit']) ? 'г' : $dish['output_unit'] ?>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                            <? } else { ?>
                                <tr data-id="<?= $feed['id'] ?>">
                                    <td></td>
                                    <td><?= $feed['name'] ?></td>
                                    <td class="l-border">
                                        <div class="input-group">
                                            <input type="number" min="0" size="10"
                                                   class="form-control border-0 text-right onlyUnSignFloatNumbers calcValue <?= !empty($feeding_dish_outlet[$feed['id']]['isChangedCalculation']) ? 'bgC2' : '' ?>"
                                                   step="<?= empty($dish['output_unit']) ? '1' : Units::getStep($dish['output_unit']) ?>"
                                                   value="<?= empty($feeding_dish_outlet[$feed['id']]['calculation']) ? $dish['standart_calculation_output'] : $feeding_dish_outlet[$feed['id']]['calculation'] ?>"
                                                   data-coef="<?= empty($feeding_dish_outlet[$feed['id']]['calculation']) ? 1 :  $feeding_dish_outlet[$feed['id']]['calculation'] / ($dish['standart_calculation_output'] > 0 ? $dish['standart_calculation_output'] : 1) ?>">
                                            <span class="input-group-addon mainUnit border-0 padding-0 bgC0 text-left mainUnit">
                                                <?= empty($dish['output_unit']) ? 'г' : $dish['output_unit'] ?>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <input type="number" min="0" size="10"
                                                   class="form-control border-0 text-right onlyUnSignFloatNumbers workValue <?= !empty($feeding_dish_outlet[$feed['id']]['isChangedWorking']) ? 'bgC2' : '' ?>"
                                                   step="<?= empty($dish['output_unit']) ? '1' : Units::getStep($dish['output_unit']) ?>"
                                                   value="<?= empty($feeding_dish_outlet[$feed['id']]['working']) ? $dish['standart_working_output'] : $feeding_dish_outlet[$feed['id']]['working'] ?>"
                                                   data-coef="<?= empty($feeding_dish_outlet[$feed['id']]['working']) ? 1 :  $feeding_dish_outlet[$feed['id']]['working'] / ($dish['standart_working_output'] > 0 ? $dish['standart_working_output'] : 1) ?>">
                                            <span class="input-group-addon mainUnit border-0 padding-0 bgC0 text-left mainUnit">
                                                <?= empty($dish['output_unit']) ? 'г' : $dish['output_unit'] ?>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                            <? } ?>
                        <? } ?>
                    <? } ?>
                <? } ?>
                </tbody>
            </table>
            </div>

        </div>

        <? if(!empty($objDish['cooking'])) {
            $cooking = $objDish['cooking'];
            $isChanged =  $objDish['cooking'] != $dish['cooking'];
        } else {
            $cooking = empty($dish['cooking']) ? '' : $dish['cooking'];
            $isChanged = false;
        } ?>
        <div class="row padding-20 <?= $isChanged ? 'bgC2' : '' ?>">
            <h4 class="text-center"> Технология приготовления </h4>
            <textarea id="cooking" name="cooking"><?= $cooking ?></textarea>
        </div>


    </div>
</form>

<div id="jstreePanel" class="col-md-9 margin-t20 hidden panel panel-primary">
    <div class="alert alert-warning fade hidden in offline"> Нет связи с сервером… </div>
    <div class="alert alert-warning fade hidden in lockData"></div>
    <?= View::renderPart('products/tree'); ?>
</div>

<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>