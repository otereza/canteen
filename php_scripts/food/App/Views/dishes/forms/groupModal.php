<div id="newDishModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Новое блюдо</h4>
            </div>
            <div class="modal-body">

                <form role="form" id="newDishForm" action="" method="post">
                    <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
                    <div class="alert alert-warning fade in lockData hidden"></div>

                    <div class="form-group" id="groupName">
                        <label for="group" class="control-label">Выберите группу</label>
                        <select name="group" class="form-control requiredValue" id="groups">
                            <option value="" selected class="hidden"></option>
                            <? foreach($groups as $group) { ?>
                                <option value="<?= $group['id'] ?>"><?= $group['name'] ?></option>
                            <? } ?>
                            <option value="" class="c3">-- новая --</option>
                        </select>
                        <input type="text" id="newGroupName" class="form-control hidden" name="newGroupName" placeholder="Название новой группы">
                        <span class="help-block hidden">Выберите или создайте новую группу</span>
                    </div>

                    <div class="form-group" id="dish">
                        <label for="dishName" class="control-label">Название блюда</label>
                        <input type="text" class="form-control requiredValue" name="name" id="dishName">
                        <span class="help-block hidden">Введите название блюда</span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="saveNewDishBtn">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>
