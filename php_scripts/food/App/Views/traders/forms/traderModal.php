<? global $businessobj; ?>

<div id="traderModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Поставщик</h4>
            </div>
            <div class="modal-body">

                <form role="form" id="traderForm">

                    <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
                    <div class="alert alert-warning fade in lockData hidden"></div>

                    <input type="hidden" class="form-control field-id" name="id" value="<?= isset($trader['id']) ? $trader['id'] : '' ?>">

                    <br>
                    <div class="form-group form-inline">
                        <label for="internalName" class="control-label">Внутреннее наименование:</label>
                        <input type="text" id="internalName" class="form-control requiredValue canLock" name="internal_name" value="<?= isset($trader['internal_name']) ? htmlspecialchars($trader['internal_name']) : '' ?>">
                    </div>

                    <div class="form-group">
                        <label for="legalName" class="control-label">Юридическое наименование:</label>
                        <input type="text" id="legalName" class="form-control requiredValue canLock" name="legal_name" value="<?= isset($trader['legal_name']) ? htmlspecialchars($trader['legal_name']) : '' ?>" size="50">
                    </div>

                    <div class="form-group">
                        <label for="details" class="control-label">Банковские реквизиты:</label>
                        <textarea id="details" class="form-control canLock" name="bank_details" rows="5"><?= isset($trader['bank_details']) ? $trader['bank_details'] : '' ?></textarea>
                    </div>

                    <div class="form-group">
                        <label for="address" class="control-label">Адрес:</label>
                        <input type="text" id="address" class="form-control canLock" name="address" value="<?= isset($trader['address']) ? $trader['address'] : '' ?>" size="50">
                    </div>

                    <div class="form-group form-inline">
                        <label for="mainPhone" class="control-label">Телефон:</label>
                        <input type="text" id="mainPhone" class="form-control canLock" name="main_phone" value="<?= isset($trader['main_phone']) ? $trader['main_phone'] : '' ?>" size="15">
                    </div>
                    <div class="form-group form-inline">
                        <label for="mainEmail" class="control-label">Email:</label>
                        <input type="text" id="mainEmail" class="form-control canLock" name="main_email" value="<?= isset($trader['main_email']) ? $trader['main_email'] : '' ?>" size="30">
                    </div>

                    <div class="form-group">
                        <label for="contract" class="control-label">Номер и дата договора:</label>
                        <input type="text" id="contract" class="form-control requiredValue canLock" name="contract" value="<?= isset($trader['contract']) ? $trader['contract'] : '' ?>" size="50">
                    </div>

                    <hr>
                    <div class="form-group margin-t10">
                        <label for="contactFio" class="control-label">Контактное лицо:</label>
                        <input type="text" id="contactFio" class="form-control requiredValue canLock" name="fio" value="<?= isset($trader['fio']) ? $trader['fio'] : '' ?>" size="50">
                    </div>

                    <div class="form-group form-inline">
                        <label for="contactPhone" class="control-label">Телефон:</label>
                        <input type="text" id="contactPhone" class="form-control requiredValue canLock" name="phone" value="<?= isset($trader['phone']) ? $trader['phone'] : '' ?>" size="15">
                    </div>
                    <div class="form-group form-inline">
                        <label for="contactEmail" class="control-label">Email:</label>
                        <input type="text" id="contactEmail" class="form-control requiredValue canLock" name="email" value="<?= isset($trader['email']) ? $trader['email'] : '' ?>" size="30">
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-xs-7">
                            <div class="form-group form-inline">
                                <label for="paymentDelay" class="control-label">Отсрочка платежа:</label>
                                <div class="input-group" id="paymentDelay">
                                    <input type="text" class="form-control canLock" name="payment_delay" value="<?= isset($trader['payment_delay']) ? $trader['payment_delay'] : '' ?>">
                                    <span class="input-group-addon"> дней </span>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" class="canLock" name="prepayment" <?= !empty($trader['prepayment']) ? 'checked' : '' ?>> Предоплата </label>
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <label for="dicsount" class="control-label">Скидка от прайса:</label>
                                <div class="input-group" id="discount">
                                    <input type="text" class="form-control onlySignFloatNumbers canLock" name="discount" value="<?= isset($trader['discount']) ? $trader['discount'] : '' ?>">
                                    <span class="input-group-addon"> % </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-5">
                            <div class="form-group">
                                <label class="control-label">С кем работает:</label>
                                <? foreach ($businessobj as $b_obj => $name) { ?>
                                    <div class="checkbox">
                                        <label><input type="checkbox" class="canLock" name="business_obj[<?= $b_obj ?>]" <?= (isset($trader['business_obj']) && in_array($b_obj, $trader['business_obj'])) ? ' checked' : '' ?>> <?= $name ?> </label>
                                    </div>
                                <? } ?>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary saveTraderBtn canHide" data-ctrl="trader">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>
