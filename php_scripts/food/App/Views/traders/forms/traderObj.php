<div id="traderObjModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Поставщик</h4>
            </div>
            <div class="modal-body">

            <form role="form" id="traderObjForm">

                <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
                <div class="alert alert-warning fade in lockData hidden"></div>

                <input type="hidden" class="form-control field-id" name="id" value="<?= $trader['id'] ?>">
                <input type="hidden" class="form-control field-for_obj" name="obj" value="<?= $forObj ?>">

                <label>Осуществляет доставку:</label>
                <? $arrWeek = array(
                    1 => 'Понедельник',
                    2 => 'Вторник',
                    3 => 'Среда',
                    4 => 'Четверг',
                    5 => 'Пятница',
                    6 => 'Суббота',
                    7 => 'Воскресенье'
                ); ?>
                <? foreach ($arrWeek as $wDay => $wName) { ?>
                    <div class="row form-group margin-b5">
                        <div class="col-xs-3">
                            <div class="checkbox">
                                <label><input type="checkbox" class="canLock check-wDay"<?= isset($trader['obj']['delivery_days'][$wDay]) ? ' checked' : '' ?>> <?= $wName ?> </label>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class='input-group deliveryTime<?= isset($trader['obj']['delivery_days'][$wDay]) ? '' : ' hidden' ?>'>
                                <span class="input-group-addon"> с </span>
                                <input type='time' class="form-control timeFrom<?= isset($trader['obj']['delivery_days'][$wDay]) ? ' requiredValue' : '' ?>" name="wday[<?= $wDay ?>][from]" value="<?= isset($trader['obj']['delivery_days'][$wDay]) ? $trader['obj']['delivery_days'][$wDay]['from'] : '' ?>">
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="input-group deliveryTime<?= isset($trader['obj']['delivery_days'][$wDay]) ? '' : ' hidden' ?>">
                                <span class="input-group-addon"> по </span>
                                <input type='time' class="form-control timeTo<?= isset($trader['obj']['delivery_days'][$wDay]) ? ' requiredValue' : '' ?>" name="wday[<?= $wDay ?>][to]" value="<?= isset($trader['obj']['delivery_days'][$wDay]) ? $trader['obj']['delivery_days'][$wDay]['to'] : '' ?>">
                            </div>
                        </div>
                    </div>
                <? } ?>

                <br>
                <div class="form-group form-inline">
                    <label for="dicsount" class="control-label">Минимальная сумма доставки:</label>
                    <div class="input-group" id="discount">
                        <input type="text" class="form-control canLock" name="min_price" value="<?= isset($trader['obj']['min_price']) ? $trader['obj']['min_price'] : '' ?>" size="7">
                    </div>
                </div>

                <div class="form-group">
                    <label for="contract" class="control-label">Номер и дата договора:</label>
                    <input type="text" id="contract" class="form-control requiredValue canLock" name="contract" value="<?= isset($trader['obj']['contract']) ? $trader['obj']['contract'] : '' ?>" size="50">
                </div>

                <hr>
                <div class="form-group margin-t10">
                    <label for="contactFio" class="control-label">Контактное лицо:</label>
                    <input type="text" id="contactFio" class="form-control requiredValue canLock" name="fio" value="<?= isset($trader['obj']['fio']) ? $trader['obj']['fio'] : '' ?>" size="50">
                </div>

                <div class="form-group form-inline">
                    <label for="contactPhone" class="control-label">Телефон:</label>
                    <input type="text" id="contactPhone" class="form-control requiredValue canLock" name="phone" value="<?= isset($trader['obj']['phone']) ? $trader['obj']['phone'] : '' ?>" size="15">
                </div>
                <div class="form-group form-inline">
                    <label for="contactEmail" class="control-label">Email:</label>
                    <input type="text" id="contactEmail" class="form-control requiredValue canLock" name="email" value="<?= isset($trader['obj']['email']) ? $trader['obj']['email'] : '' ?>" size="30">
                </div>
                <hr>

                <div class="form-group form-inline">
                    <label for="paymentDelay" class="control-label">Отсрочка платежа:</label>
                    <div class="input-group" id="paymentDelay">
                        <input type="text" class="form-control canLock <?= empty($trader['obj']['prepayment']) ? 'requiredValue' : '' ?>" name="payment_delay" value="<?= isset($trader['obj']['payment_delay']) ? $trader['obj']['payment_delay'] : '' ?>" size="4" <?= !empty($trader['obj']['prepayment']) ? 'disabled' : '' ?>>
                        <span class="input-group-addon"> дней </span>
                    </div>
                    <div class="checkbox pull-right">
                        <label><input type="checkbox" id="prepayment" class="canLock" name="prepayment" <?= !empty($trader['obj']['prepayment']) ? 'checked' : '' ?>> Предоплата </label>
                    </div>
                </div>
                <div class="form-group form-inline">
                    <label for="dicsount" class="control-label">Скидка от прайса:</label>
                    <div class="input-group" id="discount">
                        <input type="text" class="form-control onlyUnSignFloatNumbers canLock" name="discount" value="<?= isset($trader['obj']['discount']) ? $trader['obj']['discount'] : '' ?>" size="6">
                        <span class="input-group-addon"> % </span>
                    </div>
                </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary saveObjTraderBtn canHide" data-ctrl="traderObj">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>
