<?
    global $ACTIONPAGE;
?>

<h2>Поставщики</h2>


<div class="container-fluid">
    <? if(!empty($permit['canEditTrader']) && $obj == 'all') { ?>
        <button type="button" class="btn btn-primary btn-sm canHide margin-b20 addTraderBtn" style="margin-left: 30px;">Добавить поставщика</button>
    <? } ?>
    <div class="row">
        <div class="col-md-6">
            <div class="list-group">
                <? foreach ($traders as $trader) { ?>
                    <a href="<?= $ACTIONPAGE ?>?obj=<?= $obj ?>&r=trader&id=<?= $trader['id'] ?>" class="list-group-item"> <?= $trader['internal_name'] ?> </a>
                <? } ?>
            </div>
        </div>
    </div>

</div>

<?= \Food\Core\View::renderPart('traders/forms/traderModal', array('trader' => $traders)); ?>
