<?
    global $ACTIONPAGE, $businessobj;

    $forObj = empty($_GET['for']) ? null : $_GET['for'];

    $obj = empty($_GET['obj']) ? '' : $_GET['obj'];

    if(empty($trader['obj'])) {
        $trader['obj'] = array(
            'contract' => $trader['contract'],
            'fio' => $trader['fio'],
            'phone' => $trader['phone'],
            'email' => $trader['email'],
            'prepayment' => $trader['prepayment'],
            'payment_delay' => $trader['payment_delay'],
            'discount' => $trader['discount']
        );
    }


?>

<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= $ACTIONPAGE . '?obj=' . $obj . '&r=trader' ?>">Список поставщиков</a></li>
        <li class="breadcrumb-item active">Поставщик</li>
    </ol>
</nav>

<h2><?= isset($trader['internal_name']) ? $trader['internal_name'] : 'Поставщики' ?></h2>

<? if(isset($_GET['ref'])) { ?>
    <a href="<?= urldecode($_GET['ref']) ?>"> << Назад <?= isset($_GET['refmsg']) ? urldecode($_GET['refmsg']) : '' ?></a>
<? } ?>

<? if($obj == 'all') { ?>
<ul class="nav nav-tabs">
    <li<?= empty($forObj) ? ' class="active"' : '' ?>><a href="<?= $ACTIONPAGE ?>?obj=<?= $obj ?>&r=trader&id=<?= $trader['id'] ?>">Общая информация</a></li>
    <? foreach ($businessobj as $b_obj => $name) { ?>
        <? if(in_array($b_obj, $trader['business_obj'])) { ?>
            <li<?= ($forObj == $b_obj) ? ' class="active"' : '' ?>><a href="<?= $ACTIONPAGE ?>?obj=<?= $obj ?>&r=trader&id=<?= $trader['id'] ?>&for=<?= $b_obj ?>"><?= $name ?></a></li>
        <? } ?>
    <? } ?>
</ul>
<? } else { ?>
    <hr class="margin-b5">
<? } ?>

<?
$prepayment = '';
if(isset($trader['prepayment']) && $trader['prepayment']) {
    $prepayment = 'Предоплата';
} elseif(isset($trader['payment_delay'])) {
    $prepayment = declineForNum($trader['payment_delay'], 'день', 'дня', 'дней');
}
$prepaymentObj = '';
if(isset($trader['obj']['prepayment']) && $trader['obj']['prepayment']) {
    $prepaymentObj = 'Предоплата';
} elseif(isset($trader['obj']['payment_delay'])) {
    $prepaymentObj = declineForNum($trader['obj']['payment_delay'], 'день', 'дня', 'дней');
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 trader-info">

                <h4><?= $trader['legal_name'] ?></h4>
                <p>
                    <label>Банковские реквизиты:</label><br><span class="viewValue"><?= $trader['bank_details'] ?></span><br>
                </p>
                <p>
                    <label>Адрес:</label><span class="viewValue"><?= $trader['address'] ?></span><br>
                    <label>Телефон:</label><span class="viewValue"><?= $trader['main_phone'] ?></span><span class="viewSeparator"></span>
                    <label>Email:</label><span class="viewValue"><?= $trader['main_email'] ?></span><br>
                </p>
                <p>
                    <label>Номер и дата договора:</label><br><span class="viewValue"><?= $trader['contract'] ?></span>
                </p>
                <? if(empty($forObj) && $obj == 'all') { ?>
                    <p>
                        <label>Контактное лицо:</label><span class="viewValue"><?= $trader['fio'] ?></span><br>
                        <label>Телефон:</label><span class="viewValue"><?= $trader['phone'] ?></span><span class="viewSeparator"></span>
                        <label>Email:</label><span class="viewValue"><?= $trader['email'] ?></span>
                    </p>
                    <p>
                        <label>Отсрочка платежа:</label><span class="viewValue"><?= $prepayment ?></span><br>
                        <label>Скидка от прайса:</label><span class="viewValue"><?= $trader['discount'] ?> %</span>
                    </p>
                    <p>
                        <label>Работает с:</label><br>
                        <? foreach ($businessobj as $b_obj => $name) { ?>
                            <? if(in_array($b_obj, $trader['business_obj'])) { ?>
                                <span class="viewValue"><?= $name ?></span><br>
                            <? } ?>
                        <? } ?>
                    </p>

                    <? if(!empty($permit['canEditTrader'])) { ?>
                        <button type="button" class="btn btn-sm btn-primary pull-right editTraderBtn canHide margin-t10" data-ctrl="trader">Редактировать</button>
                    <? } ?>
                <? } else { ?>
                    <hr>
                        <table class="table-condensed">
                            <tr><td colspan="2"><label>Осуществляет доставку:</label></td></tr>
                            <? $arrWeek = array(
                                1 => 'Понедельник',
                                2 => 'Вторник',
                                3 => 'Среда',
                                4 => 'Четверг',
                                5 => 'Пятница',
                                6 => 'Суббота',
                                7 => 'Воскресенье'
                            ); ?>
                                <? foreach ($arrWeek as $wDay => $wName) { ?>
                                    <tr>
                                        <td><span class="viewValue"><?= $wName ?>: </span></td>
                                        <? if(isset($trader['obj']['delivery_days'][$wDay])) { ?>
                                            <td>
                                                <span class="viewValue">с <?= $trader['obj']['delivery_days'][$wDay]['from'] ?></span>,
                                                <span class="viewValue">по <?= $trader['obj']['delivery_days'][$wDay]['to'] ?></span>
                                            </td>
                                        <? } else { ?>
                                            <td><span class="viewValue"> - </span></td>
                                        <? } ?>
                                    </tr>
                                <? } ?>
                            </table>

                        <p>
                            <label>Минимальная сумма доставки:</label><span class="viewValue"><?= isset($trader['obj']['min_price']) ? $trader['obj']['min_price'] : '' ?></span>
                        </p>
                        <p>
                            <label>Номер и дата договора:</label><br><span class="viewValue"><?= isset($trader['obj']['contract']) ? $trader['obj']['contract'] : '' ?></span>
                        </p>
                        <p>
                            <label>Контактное лицо:</label><span class="viewValue"><?= isset($trader['obj']['fio']) ? $trader['obj']['fio'] : '' ?></span><br>
                            <label>Телефон:</label><span class="viewValue"><?= isset($trader['obj']['phone']) ? $trader['obj']['phone'] : '' ?></span><span class="viewSeparator"></span>
                            <label>Email:</label><span class="viewValue"><?= isset($trader['obj']['email']) ? $trader['obj']['email'] : '' ?></span>
                        </p>
                        <p>
                            <label>Отсрочка платежа:</label><span class="viewValue"><?= $prepaymentObj ?></span><br>
                            <label>Скидка от прайса:</label><span class="viewValue"><?= isset($trader['obj']['discount']) ? $trader['obj']['discount'] : '' ?> %</span>
                        </p>
                    <? if(!empty($permit['canEditTrader'])) { ?>
                        <button type="button" class="btn btn-sm btn-primary pull-right editTraderObjBtn canHide margin-t10" data-ctrl="trader">Редактировать</button>
                    <? } ?>
                <? } ?>

        </div>

        <div class="col-md-9">
            <div class="alert alert-warning fade hidden in offline"> Нет связи с сервером… </div>
            <div class="alert alert-warning fade hidden in lockData"></div>
            <?= \Food\Core\View::renderPart('products/tree'); ?>
        </div>
    </div>

</div>

<?= \Food\Core\View::renderPart('traders/forms/traderModal', array('trader' => $trader)); ?>
<?= \Food\Core\View::renderPart('traders/forms/traderObj', array('trader' => $trader, 'forObj' => (empty($forObj) || $obj != 'all') ? $obj : $forObj)); ?>
