<?
global $ACTIONPAGE, $businessobj;

use Food\Core\View;
use \Food\App\Helpers\Units;

$packUnits = Units::getAll();

$ref_url = urlencode($_SERVER['REQUEST_URI']);
$ref_msg = urlencode('к приходной накладной');

// переменные передаются с контроллера, поэтому здесь они как неопределенные. Определим )))
$trader = isset($trader) ? $trader : array();
$order = isset($order) ? $order : array();
$invoice = isset($invoice) ? $invoice : array();
$rows = isset($rows) ? $rows : array();

$forObj = empty($_GET['for']) ? null : $_GET['for'];
$obj = empty($_GET['obj']) ? '' : $_GET['obj'];

?>

<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= $ACTIONPAGE . '?obj=' . $obj . '&r=invoiceReceipt&tab=approved' ?>">Список накладных</a></li>
        <li class="breadcrumb-item active">Просмотр накладной</li>
    </ol>
</nav>

<h2 class="inline">Накладная (приход)
    <button data-href="<?= $ACTIONPAGE ?>?obj=<?= $_GET['obj'] ?>&r=trader&id=<?= $trader['id'] ?><?= !empty($_GET['for']) ? '&for=' . $_GET['for'] : '' ?>&ref=<?= $ref_url ?>&refmsg=<?= $ref_msg ?>" id="toTraderBtn" class="btn btn-default margin-l20">
        <?= $trader['internal_name'] ?>
    </button>
</h2>

<? if($_GET['obj'] == 'all' && isset($businessobj[$invoice['obj']])) { ?>
    <h3 class="text-info"><?= $businessobj[$invoice['obj']] ?></h3>
<? } ?>


<? if(!empty($permit['canEditInvoice'])) { ?>
    <div class="text-nowrap well">
        <label class="margin-l20"> Накладная №: </label>
        <span> <?= isset($invoice['inv_number']) ? $invoice['inv_number'] : '' ?> </span>

        <label class="margin-l20"> Заявка </label>
        <span> <?= empty($order['id']) ? 'Без заявки' : ' от ' . date('d.m.Y', $order['date_open']) .' № ' . $order['id'] ?> </span>

        <label class="margin-l20"> Дата поставки: </label>
        <span> <?= isset($invoice['inv_date']) ? date('d.m.Y', $invoice['inv_date']) : '' ?>, <?= isset($invoice['inv_time_period']) ? $invoice['inv_time_period'] : '' ?> </span>

        <label class="margin-l20"> Проведена: </label>
        <span> <?= isset($invoice['date_approve']) ? date('d.m.Y H:i', $invoice['date_approve']) : '' ?> </span>
    </div>
<? } ?>



<script>
    var units = <?= json_encode($packUnits) ?>;
    var packUnits = <?= json_encode(array_keys($packUnits)) ?>;
</script>

<table class="table table-hover prodList">
    <thead>
    <tr>
        <th rowspan="2">&nbsp;</th>
        <th rowspan="2" style="width: 500px">Наименование</th>
        <th colspan="3" class="text-center"> Заказано </th>
        <th colspan="3" class="text-center"> Получено </th>
        <th rowspan="2" class="text-center"> Срок годности </th>
        <th rowspan="2"> </th>
    </tr>
    <tr>
        <th class="text-center">Ед.изм.</th>
        <th class="text-center">Цена <span class="help-block" style="font-size:smaller; margin:0;"> (со скидкой) за ед.изм. </span> </th>
        <th class="text-center">Количество </th>
        <th class="text-center l-border">Ед.изм.</th>
        <th class="text-center">Цена <span class="help-block" style="font-size:smaller; margin:0;"> (со скидкой) за ед.изм. </span> </th>
        <th class="text-center">Количество</th>
    </tr>
    </thead>
    <tbody>
    <? $totalPrice = 0; ?>
    <? if(!empty($rows)) { ?>
        <? foreach ($rows as $row) {
        $rowPrice = round($row['price'], ($row['amount_unit'] != $row['pack_unit']) ? (strlen($row['unit_value']) - 1 + 2) : 2) * $row['amount'];
        $totalPrice += $rowPrice;

        $bgPrice = '';
        if($row['trader_price_user'] > $row['trader_price']) {
            $bgPrice = 'bgC3';
        } elseif($row['trader_price_user'] < $row['trader_price']) {
            $bgPrice = 'bgC6';
        }
    ?>
        <tr class="prodListItem">
            <td style="width: 30px"><i class="fa fa-ellipsis-v"></i></td>
            <td style="width:500px"><span class="prodName"><?= $row['prod_name'] ?></span></td>
            <td class="text-center"><?= $row['in_amount_unit'] ?></td>
            <td class="text-center"><?= $row['in_price'] ? sprintf('%.' . (($row['in_amount_unit'] != $row['pack_unit']) ? (strlen($row['unit_value']) - 1 + 2) : 2) . 'f', $row['in_price']) : '' ?></td>
            <? $decimalNum = isset($packUnits[$row['in_amount_unit']]) ? $packUnits[$row['in_amount_unit']]['decimal_places'] : 0 ?>
            <td class="text-center"><?= $row['in_amount'] ? sprintf('%.' . $decimalNum . 'f', $row['in_amount']) : '' ?></td>
            <td class="text-center l-border"><?= $row['amount_unit'] ?></td>
            <td class="text-center"><?= sprintf('%.' . (($row['amount_unit'] != $row['pack_unit']) ? (strlen($row['unit_value']) - 1 + 2) : 2) . 'f', $row['price']) ?></td>
            <? $decimalNum = isset($packUnits[$row['amount_unit']]) ? $packUnits[$row['amount_unit']]['decimal_places'] : 0 ?>
            <td class="text-center"><?= sprintf('%.' . $decimalNum . 'f', $row['amount']) ?></td>
            <td class="text-center"><?= date('d.m.Y', $row['exp_date']); ?></td>
            <td class="text-center" style="width: 30px"></td>
        </tr>
    <? } ?>
    <? } ?>

    <tr class="prodListItem hidden">
        <td class="dragAnchor" style="width: 30px"><i class="fa fa-ellipsis-v"></i></td>
        <td style="width:500px"><span class="prodName"></span></td>
        <td class="text-center inAmountUnit"></td>
        <td class="text-center inPrice"></td>
        <td class="text-center inAmount"></td>
        <td class="text-center l-border">
            <input type="hidden" class="form-control requiredValue packUnit" value="">
            <div class="dropdown form-control canLock center-block text-left" style="width: 100px">
                <span class="variable packUnitText"></span>
                <a href="#" data-toggle="dropdown" class="dropdown-toggle pull-right"><b class="caret"></b></a>
                <ul class="dropdown-menu unitList"></ul>
            </div>
        </td>
        <td class="text-center">
            <input type="text" class="onlyUnSignFloatNumbers text-right price hidden" value="" size="10">
            <span class="help-block priceLabel hidden" style="font-size: smaller;"> цена без скидки за упаковку </span>
            <span class="priceText"></span>
        </td class="text-center">
        <td class="text-center"><input type="text" class="onlyUnSignFloatNumbers amount" value="" size="8"></td>
        <td class="text-center">
            <div class="input-group input-group-center">
                <div class="dropdown form-control">
                    <span data-toggle="dropdown" class="dropdown-toggle dateLifeText"></span>
                    <ul class="dropdown-menu scroll-y dateLifeList"></ul>
                </div>
            </div>
        </td>
        <td class="text-center" style="width: 30px"><a type="button" class="btn btn-link btn-sm delDocProdBtn" data-id=""><i class="fa fa-remove c2"></i></a></td>
    </tr>

    <tr id="newRow" class="active">
        <td>&nbsp;</td>
        <td> </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="text-center">&nbsp;<span class="h2 text-center"><?= $invoice['total'] ?></span></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>

    </tbody>
</table>
