<?
global $ACTIONPAGE, $businessobj;

use Food\Core\View;
use \Food\App\Helpers\Units;

$packUnits = Units::getAll();

$ref_url = urlencode($_SERVER['REQUEST_URI']);
$ref_msg = urlencode('к приходной накладной');

// переменные передаются с контроллера, поэтому здесь они как неопределенные. Определим )))
$trader = isset($trader) ? $trader : array();
$order = isset($order) ? $order : array();
$invoice = isset($invoice) ? $invoice : array();
$rows = isset($rows) ? $rows : array();

$forObj = empty($_GET['for']) ? null : $_GET['for'];
$obj = empty($_GET['obj']) ? '' : $_GET['obj'];

?>

<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= $ACTIONPAGE . '?obj=' . $obj . '&r=invoiceReceipt' ?>">Список накладных</a></li>
        <li class="breadcrumb-item active">Редактирование накладной</li>
    </ol>
</nav>


<h2 class="inline">Накладная (приход)
    <button data-href="<?= $ACTIONPAGE ?>?obj=<?= $_GET['obj'] ?>&r=trader&id=<?= $trader['id'] ?><?= !empty($_GET['for']) ? '&for=' . $_GET['for'] : '' ?>&ref=<?= $ref_url ?>&refmsg=<?= $ref_msg ?>" id="toTraderBtn" class="btn btn-default margin-l20">
        <?= $trader['internal_name'] ?>
    </button>
</h2>

<? if($_GET['obj'] == 'all' && isset($businessobj[$invoice['obj']])) { ?>
    <h3 class="text-info"><?= $businessobj[$invoice['obj']] ?></h3>
<? } ?>


<? if(!empty($permit['canEditInvoice'])) { ?>
    <form action="<?= $ACTIONPAGE ?>?obj=<?= $_GET['obj'] ?>&r=invoiceReceipt/edit">
        <div class="form-group form-inline">

            <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
            <div class="alert alert-warning fade in lockData hidden"></div>

            <input type="hidden" id="trader" name="trader" value="<?= $trader['id'] ?>">

            <div class="input-group">
                <span class="input-group-addon"> № </span>
                <input type="text" id="inv_number" class="form-control" value="<?= isset($invoice['inv_number']) ? $invoice['inv_number'] : '' ?>">
            </div>

            <input type="hidden" id="#order" name="order" value="<?= $order['id'] ?>">
            <div class="input-group">
                <span class="input-group-addon"> Заявка </span>
                <input type="text" class="form-control" value="<?= $order['name'] ?>" readonly style="background-color:initial">
            </div>

            <div class="input-group">
                <select class="form-control requiredValue" id="date" name="date">
                    <? for ($i = 0; $i < 60; $i++) { ?>
                        <? $day = date('d.m.Y', strtotime("-" . $i . " day")); ?>
                        <option value="<?= $day ?>">
                            <?= $day ?>
                        </option>
                    <? } ?>
                </select>
            </div>

            <div class="input-group">
                <div class="dropdown form-control">
                    <span data-toggle="dropdown" class="dropdown-toggle" id="timePeriodText">00:00 - 00:00</span>
                    <ul class="dropdown-menu scroll-y" id="timePeriodList"></ul>
                </div>
            </div>

            <button type="button" id="deliveryOnTimeBtn" class="btn btn-warning btn-sm hidden">Поставщик привез вовремя</button>
            <button type="button" id="saveInvoiceBtn" class="btn btn-primary btn-sm margin-l20">Сохранить</button>
            <button type="button" id="approveInvoiceBtn" class="btn btn-success btn-sm canHide">Провести</button>
            <span class="help-block hidden">Выберите поставщика</span>
        </div>
    </form>
<? } ?>



<script>
    var units = <?= json_encode($packUnits) ?>;
    var packUnits = <?= json_encode(array_keys($packUnits)) ?>;
</script>

<table class="table table-hover prodList">
    <thead>
    <tr>
        <th rowspan="2">&nbsp;</th>
        <th rowspan="2" style="width: 500px">Наименование</th>
        <th colspan="3" class="text-center"> Заказано </th>
        <th colspan="3" class="text-center"> Получено </th>
        <th rowspan="2" class="text-center"> Срок годности </th>
        <th rowspan="2"> </th>
    </tr>
    <tr>
        <th class="text-center">Ед.изм.</th>
        <th class="text-center">Цена <span class="help-block" style="font-size:smaller; margin:0;"> (со скидкой) за ед.изм. </span> </th>
        <th class="text-center">Количество </th>
        <th class="text-center l-border">Ед.изм.</th>
        <th class="text-center">Цена <span class="help-block" style="font-size:smaller; margin:0;"> (со скидкой) за ед.изм. </span> </th>
        <th class="text-center">Количество</th>
    </tr>
    </thead>
    <tbody>
    <? $totalPrice = 0; ?>
    <? if(!empty($rows)) { ?>
        <? foreach ($rows as $row) {
        $rowPrice = round($row['price'], ($row['amount_unit'] != $row['pack_unit']) ? (strlen($row['unit_value']) - 1 + 2) : 2) * $row['amount'];
        $totalPrice += $rowPrice;

        $bgPrice = '';
        if($row['trader_price_user'] > $row['trader_price']) {
            $bgPrice = 'bgC3';
        } elseif($row['trader_price_user'] < $row['trader_price']) {
            $bgPrice = 'bgC6';
        }
    ?>
        <tr class="prodListItem chackRow" data-row-price="<?= $rowPrice ?>" data-id="<?= $row['id'] ?>" data-pack-id="<?= $row['pack_id']?>">
            <td class="dragAnchor" style="width: 30px"><i class="fa fa-ellipsis-v"></i></td>
            <td style="width:500px"><span class="prodName"><?= $row['prod_name'] ?></span></td>
            <td class="text-center inAmountUnit"><?= $row['in_amount_unit'] ?></td>
            <td class="text-center inPrice"><?= $row['in_price'] ?></td>
            <td class="text-center inAmount">
                <?= $row['in_amount'] ?>
                <?= isset($row['remained']) ? '<span class="text-danger" title="Количество, которое нужно дозаказать, чтобы закрыть заявку"> ('.$row['remained'].') </span>' : '' ?>
            </td>
            <td class="text-center l-border">
                <input type="hidden" class="form-control requiredValue packUnit" value="<?= $row['amount_unit'] ?>" data-pack-unit="<?= $row['pack_unit'] ?>">
                <div class="dropdown form-control canLock center-block text-left" style="width: 100px">
                    <span class="variable packUnitText"><?= $row['amount_unit'] ?></span>
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle pull-right"><b class="caret"></b></a>
                    <ul class="dropdown-menu unitList" data-unit="<?= $row['unit'] ?>">
                        <? if($row['unit'] != $row['pack_unit']) { ?>
                            <li><a href="#"><?= $row['unit'] ?></a></li>
                        <? } ?>
                        <li><a href="#"><?= $row['pack_unit'] ?></a></li>
                    </ul>
                </div>

            </td>
            <td class="text-center">
                <input type="text"
                       class="onlyUnSignFloatNumbers text-right price hidden"
                       value="<?= $row['trader_price_user'] ?>"
                       data-origin="<?= $row['trader_price'] ?>"
                       data-user="<?= $row['trader_price_user'] ?>"
                       size="10">
                <span class="help-block priceLabel hidden" style="font-size: smaller;"> цена без скидки за упаковку </span>
                <span class="priceText <?= $bgPrice ?>"><?= sprintf('%.' . (($row['amount_unit'] != $row['pack_unit']) ? (strlen($row['unit_value']) - 1 + 2) : 2) . 'f', $row['price']) ?></span>
            </td>
            <? $decimalNum = isset($packUnits[$row['amount_unit']]) ? $packUnits[$row['amount_unit']]['decimal_places'] : 0 ?>
            <td class="text-center">
                <input type="text"
                       class="amount onlyUnSignFloatNumbers"
                       value="<?= sprintf('%.' . $decimalNum . 'f', $row['amount']) ?>"
                       data-unit-value="<?= $row['unit_value'] ?>"
                       data-max="<?= $row['in_amount'] ?>"
                       size="8">
            </td>
            <td class="text-center">
                <div class="input-group input-group-center">
                    <div class="dropdown form-control">
                        <? $expDate = date('d.m.Y', $row['exp_date']); ?>
                        <span data-toggle="dropdown" class="dropdown-toggle dateLifeText"><?= $expDate ?></span>
                        <ul class="dropdown-menu scroll-y dateLifeList">
                            <? for ($i = $row['life']; $i >= 0; $i--) { ?>
                                <? $day = date('d.m.Y', strtotime("+" . $i . " day")); ?>
                                <li<?= ($expDate == $day) ? ' class="active"' : '' ?>><a href="#" data-int="<?= strtotime("+" . $i . " day") ?>"><?= $day ?></a></li>
                            <? } ?>
                        </ul>
                    </div>
                </div>
            </td>
            <td class="text-center" style="width: 30px">
            <? if(empty($row['in_amount_unit'])) { ?>
                <a type="button" class="btn btn-link btn-sm delDocProdBtn" data-id="<?= $row['id'] ?>"><i class="fa fa-remove c2"></i></a>
            <? } ?>
            </td>
        </tr>
    <? } ?>
    <? } ?>

    <tr class="prodListItem hidden chackRow">
        <td class="dragAnchor" style="width: 30px"><i class="fa fa-ellipsis-v"></i></td>
        <td style="width:500px"><span class="prodName"></span></td>
        <td class="text-center inAmountUnit"></td>
        <td class="text-center inPrice"></td>
        <td class="text-center inAmount"></td>
        <td class="text-center l-border">
            <input type="hidden" class="form-control requiredValue packUnit" value="">
            <div class="dropdown form-control canLock center-block text-left" style="width: 100px">
                <span class="variable packUnitText"></span>
                <a href="#" data-toggle="dropdown" class="dropdown-toggle pull-right"><b class="caret"></b></a>
                <ul class="dropdown-menu unitList"></ul>
            </div>
        </td>
        <td class="text-center">
            <input type="text" class="onlyUnSignFloatNumbers text-right price hidden" value="" size="10">
            <span class="help-block priceLabel hidden" style="font-size: smaller;"> цена без скидки за упаковку </span>
            <span class="priceText"></span>
        </td class="text-center">
        <td class="text-center"><input type="text" class="onlyUnSignFloatNumbers amount" value="" size="8"></td>
        <td class="text-center">
            <div class="input-group input-group-center">
                <div class="dropdown form-control">
                    <span data-toggle="dropdown" class="dropdown-toggle dateLifeText"></span>
                    <ul class="dropdown-menu scroll-y dateLifeList"></ul>
                </div>
            </div>
        </td>
        <td class="text-center" style="width: 30px"><a type="button" class="btn btn-link btn-sm delDocProdBtn" data-id=""><i class="fa fa-remove c2"></i></a></td>
    </tr>

    <tr id="newRow" class="active">
        <td>&nbsp;</td>
        <td><input type="text" id="prodName" style="width: 100%"></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="text-center">&nbsp;<span id="docTotalPrice" class="h2 text-center"><?= sprintf("%.2f", $totalPrice) ?></span></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>

    </tbody>
</table>

<div id="jstreePanel" class="col-md-9 margin-t20 hidden panel panel-primary">
    <div class="alert alert-warning fade hidden in offline"> Нет связи с сервером… </div>
    <div class="alert alert-warning fade hidden in lockData"></div>
    <?= View::renderPart('products/tree'); ?>
</div>

<div id="changedPriceModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <table id="priceTable" class="table">
                    <thead>
                    <tr>
                        <th rowspan="2">Наименование</th>
                        <th colspan="2" class="text-center">Цена</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" id="savePriceBtn" class="btn btn-primary">Изменить</button>
                <button type="button" id="refusePriceBtn" class="btn btn-default" data-dismiss="modal">Отказаться</button>
            </div>
        </div>
    </div>
</div>
