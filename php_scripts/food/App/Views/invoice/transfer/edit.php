<?
global $ACTIONPAGE, $businessobj;

use Food\Core\View;
use \Food\App\Helpers\Units;

$packUnits = Units::getAll();

// переменные передаются с контроллера, поэтому здесь они как неопределенные. Определим )))
$invoice = isset($invoice) ? $invoice : array();
$rows = isset($rows) ? $rows : array();

$forObj = empty($_GET['for']) ? null : $_GET['for'];
$obj = empty($_GET['obj']) ? '' : $_GET['obj'];

//dd($invoice);
?>

<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= $ACTIONPAGE . '?obj=' . $obj . '&r=InvoiceTransfer' ?>">Список Накладных (перемещения) </a></li>
        <li class="breadcrumb-item active">Редактирование накладной</li>
    </ol>
</nav>

<? if(empty($permit['canEditInvoice'])) { ?>
    <div class="alert alert-warning fade in lockData"> Нет разрешения на редактирование. Только просмотр. </div>
<? } ?>

<h2 class="inline">Накладная перемещения № <?= isset($invoice['inv_number']) ? $invoice['inv_number'] : 'без номера' ?></h2>

<div class="h4 margin-b20">
    <span class="text-muted"> с объекта: </span>
    <span class="text-info"><?= $businessobj[$invoice['obj_from']] ?></span><br>
    <span class="text-muted"> на объект: </span>
    <span class="text-info"><?= $businessobj[$invoice['obj_to']] ?></span>
</div>

<?// if(!empty($permit['canEditInvoice'])) { ?>
    <form action="<?= $ACTIONPAGE ?>?obj=<?= $_GET['obj'] ?>&r=actWritingOff/edit">
        <div class="form-group form-inline">

            <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
            <div class="alert alert-warning fade in lockData hidden"></div>

            <input type="hidden" value="<?= isset($invoice['inv_number']) ? $invoice['inv_number'] : '' ?>">

            <div class="input-group">
                <? if(!empty($permit['canEditInvoice'])) { ?>
                    <select class="form-control requiredValue" id="date" name="date">
                        <? for ($i = 0; $i < 60; $i++) { ?>
                            <? $day = date('d.m.Y', strtotime("-" . $i . " day")); ?>
                            <option value="<?= $day ?>"<?= (!empty($invoice['inv_date']) && date('d.m.Y', $invoice['inv_date']) == $day) ? ' selected' : '' ?>>
                                <?= $day ?>
                            </option>
                        <? } ?>
                    </select>
                <? } else { ?>
                    <div class="form-control">
                        <?= isset($invoice['inv_date']) ? date('d.m.Y', $invoice['inv_date']) : '' ?>
                    </div>
                <? } ?>
            </div>

            <div class="input-group">
                <div class="dropdown form-control">
                    <? if(!empty($permit['canEditInvoice'])) { ?>
                        <span data-toggle="dropdown" class="dropdown-toggle" id="timePeriodText"><?= empty($invoice['inv_time_period']) ? '00:00 - 00:30' : $invoice['inv_time_period'] ?></span>
                        <ul class="dropdown-menu scroll-y" id="timePeriodList"></ul>
                    <? } else { ?>
                            <?= isset($invoice['inv_time_period']) ? $invoice['inv_time_period'] : '' ?>
                    <? } ?>
                </div>
            </div>

            <? if(!empty($permit['canEditInvoice'])) { ?>
                <button type="button" id="saveInvoiceBtn" class="btn btn-primary btn-sm margin-l20">Сохранить</button>
                <button type="button" id="approveInvoiceBtn" class="btn btn-warning btn-sm canHide">Переместить</button>
            <? } ?>
        </div>
        <div class="form-group form-inline">
            <div class="input-group">
                <span class="input-group-addon"> Причина перемещения </span>
                <textarea id="reason" class="form-control <?= !empty($permit['canEditInvoice']) ? '' : 'bgC0'?>" cols="80" rows="4" <?= !empty($permit['canEditInvoice']) ? 'placeholder="Введите по какой причине перемещается товар"' : 'readonly' ?>><?= isset($invoice['reason']) ? $invoice['reason'] : '' ?></textarea>
            </div>
        </div>
    </form>



<script>
    var units = <?= json_encode($packUnits) ?>;
    var packUnits = <?= json_encode(array_keys($packUnits)) ?>;
</script>

<table class="table table-hover prodList">
    <thead>
    <tr>
        <th rowspan="2">&nbsp;</th>
        <th rowspan="2" style="width: 500px">Наименование</th>
        <th colspan="4" class="text-center"> На складе <span class="help-block" style="font-size:smaller; margin:0;"> текущее состояние </span> </th>
        <th rowspan="2" class="text-center l-border""> Списано <span class="help-block" style="font-size:smaller; margin:0;"> количество </span> </th>
        <th rowspan="2">&nbsp;</th>
    </tr>
    <tr>
        <th class="text-center">Ед.изм.</th>
        <th class="text-center">Цена <span class="help-block" style="font-size:smaller; margin:0;"> за ед.изм. </span> </th>
        <th class="text-center">Количество </th>
        <th class="text-center">Сумма </th>
    </tr>
    </thead>
    <tbody>
    <? $totalPrice = 0; ?>
    <? if(!empty($rows)) { ?>
        <? foreach ($rows as $row) {
            $rowPrice = $row['price'] * $row['amount'];
            $totalPrice += $rowPrice;
            ?>
            <tr class="prodListItem" data-row-price="<?= $rowPrice ?>" data-id="<?= $row['id'] ?>" data-pack-id="<?= $row['pack_id']?>">
                <td class="<?= !empty($permit['canEditInvoice']) ? 'dragAnchor' : '' ?>" style="width: 30px"><i class="fa fa-ellipsis-v"></i></td>
                <td style="width:500px"><span class="prodName"><?= $row['prod_name'] ?></span></td>
                <td class="text-center packUnit"><?= $row['amount_unit'] ?></td>
                <td class="text-center inPrice"><?= sprintf("%.2f", $row['price']) ?></td>
                <td class="text-center inAmount"><?= $row['in_amount'] ?></td>
                <td class="text-center inTotalPrice"><?= sprintf("%.2f", $row['price'] * $row['in_amount']) ?></td>
                <? $decimalNum = isset($packUnits[$row['unit']]) ? $packUnits[$row['unit']]['decimal_places'] : 0 ?>
                <td class="text-center l-border">
                    <? if(!empty($permit['canEditInvoice'])) { ?>
                        <input type="number" min="1" max="<?= $row['in_amount'] ?>"
                                   class="amount border-0 onlyUnSignFloatNumbers"
                                   value="<?= $row['amount'] ?>"
                                   style="width: 5em">
                    <? } else { ?>
                        <?= isset($row['amount']) ? $row['amount'] : '' ?>
                    <? } ?>
                </td>
                <td class="text-center return" style="width: 30px">
                <? if(!empty($permit['canEditInvoice'])) { ?>
                    <a type="button" class="btn btn-link btn-sm delDocProdBtn" data-id="<?= $row['id'] ?>"><i class="fa fa-remove c2"></i></a>
                <? } ?>
                </td>
            </tr>
        <? } ?>
    <? } ?>

    <tr class="prodListItem hidden" data-row-price="" data-id="" data-pack-id="">
        <td class="dragAnchor" style="width: 30px"><i class="fa fa-ellipsis-v"></i></td>
        <td style="width:500px"><span class="prodName"></span></td>
        <td class="text-center packUnit"></td>
        <td class="text-center inPrice"></td>
        <td class="text-center inAmount"></td>
        <td class="text-center inTotalPrice"></td>
        <td class="text-center l-border">
            <input type="number" min="1" max=""
                   class="amount border-0 onlyUnSignFloatNumbers"
                   value=""
                   style="width: 5em">
        </td>
        <td class="text-center return" style="width: 30px">
            <a type="button" class="btn btn-link btn-sm delDocProdBtn" data-id=""><i class="fa fa-remove c2"></i></a>
        </td>
    </tr>

    <tr id="newRow" class="active">
        <td>&nbsp;</td>
        <td>
            <? if(!empty($permit['canEditInvoice'])) { ?>
                <input type="text" id="prodName" style="width: 100%">
            <? } ?>
        </td>
        <td colspan="4" class="h3 text-right"> Сумма списания: </td>
        <td class="text-center">&nbsp;<span id="docTotalPrice" class="h2 text-center"><?= sprintf("%.2f", $totalPrice) ?></span></td>
        <td>&nbsp;</td>
    </tr>

    </tbody>
</table>

<div id="jstreePanel" class="col-md-9 margin-t20 hidden panel panel-primary">
    <div class="alert alert-warning fade hidden in offline"> Нет связи с сервером… </div>
    <div class="alert alert-warning fade hidden in lockData"></div>
    <?= View::renderPart('products/tree'); ?>
</div>
