<?
global $ACTIONPAGE, $businessobj;

use Food\Core\View;
use \Food\App\Helpers\Units;
use \Food\App\Models\InvoiceTransferModel;

$packUnits = Units::getAll();

// переменные передаются с контроллера, поэтому здесь они как неопределенные. Определим )))
$invoice = isset($invoice) ? $invoice : array();
$rows = isset($rows) ? $rows : array();

$forObj = empty($_GET['for']) ? null : $_GET['for'];
$obj = empty($_GET['obj']) ? '' : $_GET['obj'];

//dd($invoice);
?>

<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= $ACTIONPAGE . '?obj=' . $obj . '&r=InvoiceTransfer' ?>">Список Накладных (перемещения) </a></li>
        <li class="breadcrumb-item active">Редактирование накладной</li>
    </ol>
</nav>

<? if(empty($permit['canEditInvoice'])) { ?>
    <div class="alert alert-warning fade in lockData"> Нет разрешения на редактирование. Только просмотр. </div>
<? } ?>

<h2 class="inline">Накладная перемещения № <?= isset($invoice['inv_number']) ? $invoice['inv_number'] : 'без номера' ?></h2>

<div class="h4 margin-b20">
    <span class="text-muted"> с объекта: </span>
    <span class="text-info"><?= $businessobj[$invoice['obj_from']] ?></span><br>
    <span class="text-muted"> на объект: </span>
    <span class="text-info"><?= $businessobj[$invoice['obj_to']] ?></span>
</div>

<?// if(!empty($permit['canEditInvoice'])) { ?>
    <form>
        <div class="form-group form-inline">

            <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
            <div class="alert alert-warning fade in lockData hidden"></div>

            <input type="hidden" value="<?= isset($invoice['inv_number']) ? $invoice['inv_number'] : '' ?>">

            <div class="input-group">
                <div class="form-control">
                    <?= isset($invoice['inv_date']) ? date('d.m.Y', $invoice['inv_date']) : '' ?>
                </div>
            </div>

            <div class="input-group">
                <div class="dropdown form-control">
                    <?= isset($invoice['inv_time_period']) ? $invoice['inv_time_period'] : '' ?>
                </div>
            </div>

            <? if(!empty($invoice['canEditInvoiceObjTo']) && $invoice['status'] == InvoiceTransferModel::STATUS_NEED_APPROVE) { ?>
                <button class="btn btn-sm btn-success invoiceApproveToBtn <?= empty($invoice['canEditInvoiceObjTo']) ? 'hidden' : '' ?>"
                        data-id="<?= $invoice['id'] ?>"
                        data-obj-from="<?= $invoice['obj_from'] ?>"
                        data-obj-to="<?= $invoice['obj_to'] ?>">
                    Подтвердить
                </button>
            <? } ?>
        </div>
        <div class="form-group form-inline">
            <div class="input-group">
                <span class="input-group-addon"> Причина перемещения </span>
                <textarea id="reason" class="form-control bgC0" cols="80" rows="4" readonly><?= isset($invoice['reason']) ? $invoice['reason'] : '' ?></textarea>
            </div>
        </div>
    </form>



<script>
    var units = <?= json_encode($packUnits) ?>;
    var packUnits = <?= json_encode(array_keys($packUnits)) ?>;
</script>

<table class="table table-hover prodList">
    <thead>
    <tr>
        <th rowspan="2">&nbsp;</th>
        <th rowspan="2" style="width: 500px">Наименование</th>
        <th colspan="4" class="text-center"> На складе <span class="help-block" style="font-size:smaller; margin:0;"> текущее состояние </span> </th>
        <th rowspan="2" class="text-center l-border""> Списано <span class="help-block" style="font-size:smaller; margin:0;"> количество </span> </th>
        <th rowspan="2">&nbsp;</th>
    </tr>
    <tr>
        <th class="text-center">Ед.изм.</th>
        <th class="text-center">Цена <span class="help-block" style="font-size:smaller; margin:0;"> за ед.изм. </span> </th>
        <th class="text-center">Количество </th>
        <th class="text-center">Сумма </th>
    </tr>
    </thead>
    <tbody>
    <? $totalPrice = 0; ?>
    <? if(!empty($rows)) { ?>
        <? foreach ($rows as $row) {
            $rowPrice = $row['price'] * $row['amount'];
            $totalPrice += $rowPrice;
            ?>
            <tr class="prodListItem" data-row-price="<?= $rowPrice ?>" data-id="<?= $row['id'] ?>" data-pack-id="<?= $row['pack_id']?>">
                <td style="width: 30px"><i class="fa fa-ellipsis-v"></i></td>
                <td style="width:500px"><span class="prodName"><?= $row['prod_name'] ?></span></td>
                <td class="text-center packUnit"><?= $row['amount_unit'] ?></td>
                <td class="text-center inPrice"><?= sprintf("%.2f", $row['price']) ?></td>
                <td class="text-center inAmount"><?= $row['in_amount'] ?></td>
                <td class="text-center inTotalPrice"><?= sprintf("%.2f", $row['price'] * $row['in_amount']) ?></td>
                <? $decimalNum = isset($packUnits[$row['unit']]) ? $packUnits[$row['unit']]['decimal_places'] : 0 ?>
                <td class="text-center l-border">
                    <?= isset($row['amount']) ? $row['amount'] : '' ?>
                </td>
                <td class="text-center return" style="width: 30px">
                </td>
            </tr>
        <? } ?>
    <? } ?>

    <tr id="newRow" class="active">
        <td>&nbsp;</td>
        <td> </td>
        <td colspan="4" class="h3 text-right"> Сумма списания: </td>
        <td class="text-center">&nbsp;<span id="docTotalPrice" class="h2 text-center"><?= sprintf("%.2f", $totalPrice) ?></span></td>
        <td>&nbsp;</td>
    </tr>

    </tbody>
</table>
