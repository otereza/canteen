<?
global $ACTIONPAGE, $businessobj;

$forObj = empty($_GET['for']) ? null : $_GET['for'];
$obj = empty($_GET['obj']) ? '' : $_GET['obj'];

$tab = isset($_GET['tab']) ? $_GET['tab'] : 'draft';

?>

<h2>Накладные (перемещения)</h2>


<div class="container-fluid">
    <? if(!empty($permit['canEditInvoice'])) { ?>
        <form action="<?= $ACTIONPAGE ?>">
            <div class="form-group form-inline">
                <? if($obj == 'all') { ?>
                    <div class="input-group">
                        <span class="input-group-addon"> Списать с </span>
                        <select class="form-control requiredValue" id="objFrom">
                            <option value="" selected class="hidden"> Объект </option>
                            <? foreach ($businessobj as $bObj => $bName) { ?>
                                <option value="<?= $bObj ?>"><?= $bName ?></option>
                            <? } ?>
                        </select>
                    </div>
                <? } else { ?>
                    <input type="hidden" name="objTo" id="objTo" value="<?= $obj ?>">
                <? } ?>
                <div class="input-group">
                    <span class="input-group-addon"> Переместить на </span>
                    <select class="form-control requiredValue" id="objTo">
                        <option value="" selected class="hidden"> Объект </option>
                        <? foreach ($businessobj as $bObj => $bName) { ?>
                            <option value="<?= $bObj ?>"><?= $bName ?></option>
                        <? } ?>
                    </select>
                </div>
                <button type="button" id="addInvoiceTransferBtn" class="btn btn-primary btn-sm canHide">Создать накладную</button>
                <span class="help-block hidden">Выберите объект</span>
            </div>
        </form>
    <? } ?>

    <ul class="nav nav-tabs" id="invoiceStateTab">
        <li class="<?= $tab=='draft' ? 'active' : '' ?>"><a href="#tabDraft">Черновики<? if(!empty($list['draft'])) { ?> <span class="badge"><?= count($list['draft']) ?></span><? } ?></a></li>
        <li class="<?= $tab=='approvedFrom' ? 'active' : '' ?>"><a href="#tabApprovedFrom">Отгружено<? if(!empty($list['approvedFrom'])) { ?> <span class="badge"><?= count($list['approvedFrom']) ?></span><? } ?></a></li>
        <li class="<?= $tab=='approvedTo' ? 'active' : '' ?>"><a href="#tabApprovedTo">Получено<? if(!empty($list['approvedTo'])) { ?> <span class="badge"><?= count($list['approvedTo']) ?></span><? } ?></a></li>
        <li class="<?= $tab=='needApprove' ? 'active' : '' ?>"><a href="#tabNeedApprove">Для подтверждения<? if(!empty($list['needApprove'])) { ?> <span class="badge"><?= count($list['needApprove']) ?></span><? } ?></a></li>
    </ul>

    <div class="tab-content">
        <div id="tabDraft" class="tab-pane fade <?= $tab=='draft' ? 'in active' : '' ?>">
            <? if(!empty($list['draft'])) { ?>

                <table class="table table-hover" data-ctrl="InvoiceTransfer">
                    <thead>
                    <tr>
                        <th class="text-left">Дата накладной</th>
                        <th>Номер</th>
                        <?if($_GET['obj'] == 'all') { ?>
                            <th> Откуда </th>
                            <th> Куда </th>
                        <? } ?>
                        <th>Сумма</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($list['draft'] as $invoice) { ?>
                        <tr class="editInvoice" data-id="<?= $invoice['id'] ?>" data-obj="<?= $invoice['obj_from'] ?>">
                            <td class="text-left">
                                <span> <?= date('d.m.Y H:i', $invoice['inv_date']) ?> </span>
                            </td>
                            <td class="text-center"><?= $invoice['inv_number'] ?></td>
                            <?if($_GET['obj'] == 'all') { ?>
                                <td class="text-center"><?= $businessobj[$invoice['obj_from']] ?></td>
                                <td class="text-center"><?= $businessobj[$invoice['obj_to']] ?></td>
                            <? } ?>
                            <td class="text-center"><?= $invoice['total'] ?></td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            <? } else { ?>
                Нет актов
            <? } ?>
        </div>

        <div id="tabApprovedFrom" class="tab-pane fade <?= $tab=='approvedFrom' ? 'in active' : '' ?>">
            <? if(!empty($list['approvedFrom'])) { ?>

                <table class="table table-hover" data-ctrl="InvoiceTransfer">
                    <thead>
                    <tr>
                        <th class="text-left">Дата накладной</th>
                        <th>Номер</th>
                        <?if($_GET['obj'] == 'all') { ?>
                            <th> Откуда </th>
                            <th> Куда </th>
                        <? } else { ?>
                            <th> Куда </th>
                        <? } ?>
                        <th>Сумма</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($list['approvedFrom'] as $invoice) { ?>
                        <tr class="viewInvoice" data-id="<?= $invoice['id'] ?>">
                            <td class="text-left">
                                <span> <?= date('d.m.Y H:i', $invoice['inv_date']) ?> </span>
                            </td>
                            <td class="text-center"><?= $invoice['inv_number'] ?></td>
                            <?if($_GET['obj'] == 'all') { ?>
                                <td class="text-center"><?= $businessobj[$invoice['obj_from']] ?></td>
                                <td class="text-center"><?= $businessobj[$invoice['obj_to']] ?></td>
                            <? } else { ?>
                                <td class="text-center"><?= $businessobj[$invoice['obj_to']] ?></td>
                            <? } ?>
                            <td class="text-center"><?= $invoice['total'] ?></td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            <? } else { ?>
                Нет актов
            <? } ?>
        </div>

        <div id="tabApprovedTo" class="tab-pane fade <?= $tab=='approvedTo' ? 'in active' : '' ?>">
            <? if(!empty($list['approvedTo'])) { ?>

                <table class="table table-hover" data-ctrl="InvoiceTransfer">
                    <thead>
                    <tr>
                        <th class="text-left">Дата накладной</th>
                        <th>Номер</th>
                        <?if($_GET['obj'] == 'all') { ?>
                            <th> Откуда </th>
                            <th> Куда </th>
                        <? } else { ?>
                            <th> Откуда </th>
                        <? } ?>
                        <th>Сумма</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($list['approvedTo'] as $invoice) { ?>
                        <tr class="viewInvoice" data-id="<?= $invoice['id'] ?>">
                            <td class="text-left">
                                <span> <?= date('d.m.Y H:i', $invoice['inv_date']) ?> </span>
                            </td>
                            <td class="text-center"><?= $invoice['inv_number'] ?></td>
                            <?if($_GET['obj'] == 'all') { ?>
                                <td class="text-center"><?= $businessobj[$invoice['obj_from']] ?></td>
                                <td class="text-center"><?= $businessobj[$invoice['obj_to']] ?></td>
                            <? } else { ?>
                                <td class="text-center"><?= $businessobj[$invoice['obj_from']] ?></td>
                            <? } ?>
                            <td class="text-center"><?= $invoice['total'] ?></td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            <? } else { ?>
                Нет актов
            <? } ?>
        </div>

        <div id="tabNeedApprove" class="tab-pane fade <?= $tab=='needApprove' ? 'in active' : '' ?>">
            <? if(!empty($list['needApprove'])) { ?>

                <table class="table table-hover" data-ctrl="InvoiceTransfer">
                    <thead>
                    <tr>
                        <th class="text-left">Дата накладной</th>
                        <th>Номер</th>
                        <?if($_GET['obj'] == 'all') { ?>
                            <th> Откуда </th>
                            <th> Куда </th>
                        <? } else { ?>
                            <th> Откуда </th>
                        <? } ?>
                        <th>Сумма</th>
                        <th style="width:100px;">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($list['needApprove'] as $invoice) { ?>
                        <tr class="viewInvoice" data-id="<?= $invoice['id'] ?>">
                            <td class="text-left">
                                <span> <?= date('d.m.Y H:i', $invoice['inv_date']) ?> </span>
                            </td>
                            <td class="text-center"><?= $invoice['inv_number'] ?></td>
                            <?if($_GET['obj'] == 'all') { ?>
                                <td class="text-center"><?= $businessobj[$invoice['obj_from']] ?></td>
                                <td class="text-center"><?= $businessobj[$invoice['obj_to']] ?></td>
                            <? } else { ?>
                                <td class="text-center"><?= $businessobj[$invoice['obj_from']] ?></td>
                            <? } ?>
                            <td class="text-center"><?= $invoice['total'] ?></td>
                            <td class="text-center">
                                <button class="btn btn-sm btn-success invoiceApproveToBtn <?= empty($invoice['canEditInvoiceObjTo']) ? 'hidden' : '' ?>"
                                        data-id="<?= $invoice['id'] ?>"
                                        data-obj-from="<?= $invoice['obj_from'] ?>"
                                        data-obj-to="<?= $invoice['obj_to'] ?>">
                                    Подтвердить
                                </button>
                            </td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            <? } else { ?>
                Нет актов
            <? } ?>
        </div>
    </div>


    </div>
