<?
    global $ACTIONPAGE, $businessobj;

    $obj = isset($_GET['obj']) ? $_GET['obj'] : 'all';
    $forObj = isset($_GET['for']) ? $_GET['for'] : false;
    $tab = isset($_GET['tab']) ? $_GET['tab'] : 'draft';

    $traders = isset($traders) ? $traders : array();
    $orders = isset($orders) ? $orders : array();

?>

<h2>Накладные (возврат) </h2>

<? if(!empty($permit['canEditInvoice'])) { ?>
    <form action="<?= $ACTIONPAGE ?>?obj=<?= $obj ?>&r=order/edit">
        <input type="hidden" name="obj" value="<?= $obj ?>">
        <input type="hidden" name="r" value="order/edit">
        <div class="form-group form-inline">
            <? if($obj == 'all') { ?>
            <div class="input-group<?= $obj == 'all' ? '' : ' hidden'?>">
                <select class="form-control requiredValue" id="obj">
                    <option value="0" selected class="hidden"> Объект </option>
                    <? foreach ($businessobj as $bObj => $bName) { ?>
                        <option value="<?= $bObj ?>"><?= $bName ?></option>
                    <? } ?>
                </select>
            </div>
            <? } ?>
            <div class="input-group">
                <select class="form-control requiredValue" id="trader" name="trader_id">
                    <option value="0" selected class="hidden"> Поставщик </option>
                    <? foreach ($objTraders as $trader) { ?>
                        <option value="<?= $trader['id'] ?>"><?= $trader['internal_name'] ?></option>
                    <? } ?>
                </select>
            </div>
            <div class="input-group">
                <select class="form-control requiredValue" id="invReceipt"></select>
            </div>
            <button type="button" id="addInvoiceBtn" class="btn btn-primary btn-sm canHide">Создать накладную</button>
            <span class="help-block hidden">Выберите<?= $obj == 'all' ? ' объект и' : ''?> поставщика</span>
        </div>
    </form>
<? } ?>

<ul class="nav nav-tabs" id="invoiceStateTab">
    <li class="<?= $tab=='draft' ? 'active' : '' ?>"><a href="#tabDraft">Черновики</a></li>
    <li class="<?= $tab=='approved' ? 'active' : '' ?>"><a href="#tabApproved">Проведенные</a></li>
</ul>

<div class="container-fluid">
    <div class="tab-content">
        <div id="tabDraft" class="tab-pane fade <?= $tab=='draft' ? 'in active' : '' ?>">
            <? if(!empty($invoices['draft'])) { ?>

                <table class="table table-hover" data-ctrl="invoiceReturn">
                    <thead>
                        <tr>
                            <th>Дата накладной</th>
                            <?if($_GET['obj'] == 'all') { ?>
                                <th>Объект</th>
                            <? } ?>
                            <th>Номер</th>
                            <th>Сумма</th>
                            <th>Поставщик</th>
                            <th>Проведена</th>
                        </tr>
                    </thead>
                    <tbody>
                    <? foreach ($invoices['draft'] as $invoice) { ?>
                        <tr class="viewInvoice" data-id="<?= $invoice['id'] ?>" data-obj="<?= $invoice['obj'] ?>">
                            <td>
                                <span> <?= date('d.m.Y H:i', $invoice['inv_date']) ?> </span>
                            </td>
                            <?if($_GET['obj'] == 'all') { ?>
                                <td><?= $businessobj[$invoice['obj']] ?></td>
                            <? } ?>
                            <td><?= $invoice['inv_number'] ?></td>
                            <td><?= $invoice['total'] ?></td>
                            <td><?= isset($traders[$invoice['trader_id']]) ? $traders[$invoice['trader_id']]['internal_name'] : '' ?></td>
                            <td><?= empty($invoice['date_approve']) ? 'в работе' : date('d.m.Y H:i', $invoice['date_approve']) ?></td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            <? } else { ?>
                Нет накладных
            <? } ?>
        </div>
        <div id="tabApproved" class="tab-pane fade <?= $tab=='approved' ? 'in active' : '' ?>">
            <? if(!empty($invoices['approved'])) { ?>
                <table class="table table-hover" data-ctrl="invoiceReturn">
                    <thead>
                    <tr>
                        <th>Дата накладной</th>
                        <?if($_GET['obj'] == 'all') { ?>
                            <th>Объект</th>
                        <? } ?>
                        <th>Номер</th>
                        <th>Сумма</th>
                        <th>Поставщик</th>
                        <th>Проведена</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($invoices['approved'] as $invoice) { ?>
                        <tr class="viewInvoice" data-id="<?= $invoice['id'] ?>" data-obj="<?= $invoice['obj'] ?>">
                            <td>
                                <span> <?= date('d.m.Y H:i', $invoice['inv_date']) ?> </span>
                            </td>
                            <?if($_GET['obj'] == 'all') { ?>
                                <td><?= $businessobj[$invoice['obj']] ?></td>
                            <? } ?>
                            <td><?= $invoice['inv_number'] ?></td>
                            <td><?= $invoice['total'] ?></td>
                            <td><?= isset($traders[$invoice['trader_id']]) ? $traders[$invoice['trader_id']]['internal_name'] : '' ?></td>
                            <td><?= empty($invoice['date_approve']) ? '' : date('d.m.Y H:i', $invoice['date_approve']) ?></td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            <? } else { ?>
                Нет накладных
            <? } ?>
        </div>

</div>
