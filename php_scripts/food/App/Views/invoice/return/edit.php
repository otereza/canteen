<?
global $ACTIONPAGE, $businessobj;

use Food\Core\View;
use \Food\App\Helpers\Units;

$packUnits = Units::getAll();

$ref_url = urlencode($_SERVER['REQUEST_URI']);
$ref_msg = urlencode('к приходной накладной');

// переменные передаются с контроллера, поэтому здесь они как неопределенные. Определим )))
$trader = isset($trader) ? $trader : array();
$order = isset($order) ? $order : array();
$invoice = isset($invoice) ? $invoice : array();
$rows = isset($rows) ? $rows : array();

//dd($invoice);
?>

<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= $ACTIONPAGE . '?obj=' . $obj . '&r=invoiceReturn' ?>">Список накладных</a></li>
        <li class="breadcrumb-item active">Редактирование накладной</li>
    </ol>
</nav>

<h2 class="inline">Накладная (возврат) <?= $trader['internal_name'] ?></h2>

<? if($_GET['obj'] == 'all' && isset($businessobj[$invoice['obj']])) { ?>
    <h3 class="text-info"><?= $businessobj[$invoice['obj']] ?></h3>
<? } ?>


<? if(!empty($permit['canEditInvoice'])) { ?>
    <form action="<?= $ACTIONPAGE ?>?obj=<?= $_GET['obj'] ?>&r=invoiceReceipt/edit">
        <div class="form-group form-inline">

            <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
            <div class="alert alert-warning fade in lockData hidden"></div>

            <input type="hidden" id="trader" name="trader" value="<?= $trader['id'] ?>">

            <div class="input-group">
                <span class="input-group-addon"> № </span>
                <input type="text" id="inv_number" class="form-control" value="<?= isset($invoice['inv_number']) ? $invoice['inv_number'] : '' ?>">
            </div>

            <div class="input-group">
                <span class="input-group-addon"> Приходная Накладная </span>
                <input type="text" class="form-control" value="<?= $invoice['receipt_name'] ?>" readonly style="background-color:initial">
            </div>

            <div class="input-group">
                <select class="form-control requiredValue" id="date" name="date">
                    <? for ($i = 0; $i < 60; $i++) { ?>
                        <? $day = date('d.m.Y', strtotime("-" . $i . " day")); ?>
                        <option value="<?= $day ?>">
                            <?= $day ?>
                        </option>
                    <? } ?>
                </select>
            </div>

            <div class="input-group">
                <div class="dropdown form-control">
                    <span data-toggle="dropdown" class="dropdown-toggle" id="timePeriodText">00:00 - 00:00</span>
                    <ul class="dropdown-menu scroll-y" id="timePeriodList"></ul>
                </div>
            </div>

            <button type="button" id="deliveryOnTimeBtn" class="btn btn-warning btn-sm hidden">Поставщик привез вовремя</button>
            <button type="button" id="saveInvoiceBtn" class="btn btn-primary btn-sm margin-l20">Сохранить</button>
            <button type="button" id="approveInvoiceBtn" class="btn btn-success btn-sm canHide">Провести</button>
            <span class="help-block hidden">Выберите поставщика</span>
        </div>
        <div class="form-group form-inline">
            <div class="input-group">
                <span class="input-group-addon"> Причина возврата </span>
                <textarea id="reason" class="form-control" cols="80" rows="4" placeholder="Введите по какой причине возвращается товар"><?= isset($invoice['reason']) ? $invoice['reason'] : '' ?></textarea>
            </div>
        </div>
    </form>
<? } ?>



<script>
    var units = <?= json_encode($packUnits) ?>;
    var packUnits = <?= json_encode(array_keys($packUnits)) ?>;
</script>

<table class="table table-hover prodList">
    <thead>
    <tr>
        <th rowspan="2">&nbsp;</th>
        <th rowspan="2" style="width: 500px">Наименование</th>
        <th colspan="4" class="text-center"> Поставлено </th>
        <th colspan="3" class="text-center"> Возврат </th>
        <th rowspan="2"> </th>
    </tr>
    <tr>
        <th class="text-center">Ед.изм.</th>
        <th class="text-center">Цена <span class="help-block" style="font-size:smaller; margin:0;"> (со скидкой) за ед.изм. </span> </th>
        <th class="text-center">Количество </th>
        <th class="text-center"> Срок годности </th>
        <th class="text-center l-border">Ед.изм.</th>
        <th class="text-center">Цена <span class="help-block" style="font-size:smaller; margin:0;"> (со скидкой) за ед.изм. </span> </th>
        <th class="text-center">Количество</th>
    </tr>
    </thead>
    <tbody>
    <? $totalPrice = 0; ?>
    <? if(!empty($rows)) { ?>
        <? foreach ($rows as $row) {
        $rowPrice = round($row['in_price'], ($row['amount_unit'] != $row['pack_unit']) ? (strlen($row['unit_value']) - 1 + 2) : 2) * $row['amount'];
        $totalPrice += $rowPrice;

//        $bgPrice = '';
//        if($row['trader_price_user'] > $row['trader_price']) {
//            $bgPrice = 'bgC3';
//        } elseif($row['trader_price_user'] < $row['trader_price']) {
//            $bgPrice = 'bgC6';
//        }
    ?>
        <tr class="prodListItem" data-row-price="<?= $rowPrice ?>" data-id="<?= $row['id'] ?>" data-pack-id="<?= $row['pack_id']?>">
            <td class="dragAnchor" style="width: 30px"><i class="fa fa-ellipsis-v"></i></td>
            <td style="width:500px"><span class="prodName"><?= $row['prod_name'] ?></span></td>
            <td class="text-center inAmountUnit"><?= $row['in_amount_unit'] ?></td>
            <td class="text-center inPrice"><?= $row['in_price'] ?></td>
            <td class="text-center inAmount"><?= $row['in_amount'] ?></td>
            <td class="text-center"><?= date('d.m.Y', $row['exp_date']) ?></td>
            <td class="text-center l-border return">
                <input type="hidden" class="form-control requiredValue packUnit" value="<?= $row['amount_unit'] ?>" data-pack-unit="<?= $row['pack_unit'] ?>">
                <div class="dropdown form-control canLock center-block text-left canHide<?= empty($row['amount']) ? ' hidden' : '' ?>" style="width: 100px">
                    <span class="variable packUnitText"><?= $row['in_amount_unit'] ?></span>
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle pull-right"><b class="caret"></b></a>
                    <ul class="dropdown-menu unitList" data-unit="<?= $row['unit'] ?>">
                        <? if($row['unit'] != $row['pack_unit']) { ?>
                            <li><a href="#"><?= $row['unit'] ?></a></li>
                        <? } ?>
                        <li><a href="#"><?= $row['pack_unit'] ?></a></li>
                    </ul>
                </div>

            </td>
            <td class="text-center return">
                <input type="hidden" class="price" value="<?= $row['trader_price_user'] ?>">
                <span class="priceText noClick canHide<?= empty($row['amount']) ? ' hidden' : '' ?>"><?= sprintf('%.' . (($row['in_amount_unit'] != $row['pack_unit']) ? (strlen($row['unit_value']) - 1 + 2) : 2) . 'f', $row['in_price']) ?></span>
            </td>
            <? $decimalNum = isset($packUnits[$row['amount_unit']]) ? $packUnits[$row['amount_unit']]['decimal_places'] : 0 ?>
            <td class="text-center return">
                <input type="text"
                       class="amount onlyUnSignFloatNumbers canHide<?= empty($row['amount']) ? ' hidden' : '' ?>"
                       value="<?= sprintf('%.' . $decimalNum . 'f', $row['in_amount']) ?>"
                       data-unit-value="<?= $row['unit_value'] ?>"
                       data-max="<?= $row['in_amount'] ?>"
                       size="8">
            </td>
            <td class="text-center return" style="width: 30px">
            </td>
        </tr>
    <? } ?>
    <? } ?>

    <tr id="newRow" class="active">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="text-center">&nbsp;<span id="docTotalPrice" class="h2 text-center"><?= sprintf("%.2f", $totalPrice) ?></span></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>

    </tbody>
</table>
