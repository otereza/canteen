<?
global $ACTIONPAGE, $businessobj;

$forObj = empty($_GET['for']) ? null : $_GET['for'];

$obj = empty($_GET['obj']) ? '' : $_GET['obj'];

?>

<div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
<div class="alert alert-warning fade in lockData hidden"></div>


<h2>Категории питающихся</h2>
<button type="button" id="addFeedBtn" class="btn btn-success btn-sm margin-l20">Добавить</button>
<button type="button" id="saveFeedingBtn" class="btn btn-primary btn-sm margin-l20">Сохранить</button>

<div class="container-fluid">


    <table class="table table-sm table-hover feedList">
        <thead>
            <tr>
                <th style="width: 30px"></th>
                <th style="width: 20%">Категория</th>
                <th style="width: 20% ">Подкатегория</th>
                <th style="">Список объектов</th>
                <th style=""></th>
            </tr>
        </thead>
        <tbody>
            <? if(!empty($feeds)) { ?>
                <? foreach ($feeds as $feed) { ?>

                    <? if($feed['parent_id'] == 0) { ?>
                        <? $groups[$feed['id']] = $feed; ?>
                        <tr class="feedItem active" data-type="group" data-parent="<?= $feed['parent_id'] ?>" data-id="<?= $feed['id'] ?>">
                            <td class="dragAnchor"><i class="fa fa-ellipsis-v"></i></td>
                            <td class="feedName"><?= $feed['name'] ?></td>
                            <td></td>
                            <td class="l-border">
                                <div class="form-group form-inline">
                                    <div class="objList">
                                        <? foreach ($businessobj as $b_obj => $name) { ?>
                                            <div class="checkbox" data-obj="<?= $b_obj ?>">
                                                <label class="margin-r30"><input type="checkbox" class="canLock groupObj" <?= !empty($feed['objs'][$b_obj]) ? ' checked' : '' ?>> <?= $name ?> </label>
                                            </div>
                                        <? } ?>
                                    </div>
                                </div>
                            </td>
                            <td class="text-center  text-nowrap" style="width: 30px">
                                <a type="button" class="btn btn-link btn-sm editFeedBtn"><i class="fa fa-edit"></i></a>
                                <a type="button" class="btn btn-link btn-sm delFeedBtn"><i class="fa fa-remove c2"></i></a>
                            </td>
                        </tr>
                    <? } else { ?>
                        <tr class="feedItem" data-type="item" data-parent="<?= $feed['parent_id'] ?>" data-id="<?= $feed['id'] ?>">
                            <td class="dragAnchor"><i class="fa fa-ellipsis-v"></i></td>
                            <td></td>
                            <td class="feedName"><?= $feed['name'] ?></td>
                            <td class="l-border">
                                <div class="form-group form-inline">
                                    <div class="objList">
                                        <? foreach ($businessobj as $b_obj => $name) { ?>
                                            <div class="checkbox <?= !empty($feeds[$feed['parent_id']]['objs'][$b_obj]) ? '' : 'hidden' ?>" data-obj="<?= $b_obj ?>">
                                                <label class="margin-r30"><input type="checkbox" class="canLock groupObj" <?= !empty($feed['objs'][$b_obj]) ? ' checked' : '' ?>> <?= $name ?> </label>
                                            </div>
                                        <? } ?>
                                    </div>
                                </div>
                            </td>
                            <td class="text-center  text-nowrap" style="width: 30px">
                                <a type="button" class="btn btn-link btn-sm editFeedBtn"><i class="fa fa-edit"></i></a>
                                <a type="button" class="btn btn-link btn-sm delFeedBtn"><i class="fa fa-remove c2"></i></a>
                            </td>
                        </tr>
                    <? } ?>
                <? } ?>
            <? } ?>
            <tr id="groupItem" class="active hidden" data-type="group" data-parent="0" data-id="">
                <td class="dragAnchor"><i class="fa fa-ellipsis-v"></i></td>
                <td class="feedName"></td>
                <td></td>
                <td class="l-border">
                    <div class="form-group form-inline">
                        <div class="objList">
                            <? foreach ($businessobj as $b_obj => $name) { ?>
                                <div class="checkbox" data-obj="<?= $b_obj ?>">
                                    <label class="margin-r30"><input type="checkbox" class="canLock groupObj"> <?= $name ?> </label>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                </td>
                <td class="text-center text-nowrap" style="width: 30px">
                    <a type="button" class="btn btn-link btn-sm editFeedBtn"><i class="fa fa-edit"></i></a>
                    <a type="button" class="btn btn-link btn-sm delFeedBtn"><i class="fa fa-remove c2"></i></a>
                </td>
            </tr>
            <tr id="itemItem" class="hidden" data-type="item" data-parent="" data-id="">
                <td class="dragAnchor"><i class="fa fa-ellipsis-v"></i></td>
                <td></td>
                <td class="feedName"></td>
                <td class="l-border">
                    <div class="form-group form-inline">
                        <div class="objList">
                            <? foreach ($businessobj as $b_obj => $name) { ?>
                                <div class="checkbox hidden" data-obj="<?= $b_obj ?>">
                                    <label class="margin-r30"><input type="checkbox" class="canLock"> <?= $name ?> </label>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                </td>
                <td class="text-center  text-nowrap" style="width: 30px">
                    <a type="button" class="btn btn-link btn-sm editFeedBtn"><i class="fa fa-edit"></i></a>
                    <a type="button" class="btn btn-link btn-sm delFeedBtn"><i class="fa fa-remove c2"></i></a>
                </td>
            </tr>
        </tbody>
    </table>

</div>

<!--  ФОРМЫ  -->
<? use \Food\Core\View; ?>

<?= View::renderPart('feeding/forms/feedModal', array('groups' => $groups)); ?>
