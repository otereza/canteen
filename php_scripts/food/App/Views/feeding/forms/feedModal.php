<div id="feedModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Категория питающихся</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="feedForm">

                    <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
                    <div class="alert alert-warning fade in lockData hidden"></div>

                    <input type="hidden" id="id-value" name="id">
                    <input type="hidden" id="pos-value" name="pos">
                    <input type="hidden" id="objs-value" name="objs">

                    <input type="hidden" id="isNew">


                    <div class="form-inline margin-b20" id="categoryBlock">
                        <label for="parentGroup" class="control-label"> Родительская категоря: </label>
                        <select id="parentGroup" class="form-control requiredValue" name="parent_id">
                            <option value="0" selected class="с2"> -- основная -- </option>
                            <? foreach($groups as $group) { ?>
                                <option value="<?= $group['id'] ?>"><?= $group['name'] ?></option>
                            <? } ?>
                        </select>
                        <span class="help-block"> Выберите категорию, к которой относится данная категория </span>
                    </div>


                    <div class="form-group">
                        <label for="tmark-name" class="control-label">Название категории:</label>
                        <input type="text" class="form-control field-name canLock" name="name" id="feedName">
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary canHide" id="saveFeedBtn">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>
