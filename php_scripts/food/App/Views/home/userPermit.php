<?
global $user, $businessobj, $ACTIONPAGE;

$getParams = $_REQUEST;
if(isset($getParams['cmd'])) { unset($getParams['cmd']); }

$allPermits = array(

    300 => 'Пользователь может <b>просматривать</b> данные.',
    301 => 'Может редактировать объект <b>Продукты питания</b>, все уровни',
    302 => 'Может редактировать объект <b>Продукты питания</b>, ТОЛЬКО уровни  «Торговая марка» и «Фасовка»',
    303 => 'Может редактировать объект <b>Поставщики</b>: общая информация о поставщике (адрес, телефон, ФИО контактного лица и т.д.)',
    304 => 'Может редактировать объект <b>Поставщики</b>: устанавливать рейтинг поставщика',
    305 => 'Может редактировать объект <b>Поставщики</b>: отмечать продукты, доступные для заказа у данного поставщика',
    306 => 'Может редактировать объект <b>Поставщики</b>: изменять цены на продукты',
    307 => 'Может редактировать объект <b>Заказ продуктов</b> (заявки поставщикам)',
    308 => 'Может редактировать объект <b>Накладная</b>, КРОМЕ акта списания',
    309 => 'Может редактировать объект <b>Накладная</b>: акт списания (утилизация продуктов)',
    310 => 'Может редактировать объект <b>Акт снятия остатков</b>',
    311 => 'Может редактировать объект <b>Готовые блюда</b>',
    312 => 'Может редактировать объект <b>Готовые блюда</b>, КРОМЕ рабочих объемов блюда и рабочих технологических карт',
    313 => 'Может редактировать объект <b>Замена продуктов</b>',
    314 => 'Может редактировать объект <b>Замена продуктов</b>, КРОМЕ рабочих техн./карт',
    315 => 'Может редактировать объект <b>Категории питающихся</b>',
    316 => 'Может редактировать объект <b>Категории питающихся</b>, ТОЛЬКО подкатегории питающихся',
    317 => 'Может редактировать объект <b>Приемы пищи</b>',
    318 => 'Может редактировать объект <b>Перспективное меню</b>',
    319 => 'Может редактировать объект <b>План-меню на день</b> (все возможности)',
    320 => 'Может редактировать объект <b>План-меню на день</b>, кроме изменения калькуляций',
    321 => 'Может редактировать объект <b>План-меню на день</b>, возможна работа только по Перспективным меню',
    322 => 'Может <b>просматривать отчёты о расходе продуктов</b>'

);
if(isset($_GET['obj'])) {
?>

<div id="userPermitModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Доступы пользователя</h4>
            </div>
            <div class="modal-body">

                <form role="form" id="permitForm" action="" method="post">
                    <div class="form-group">
                        Пользователь: <label class="control-label"><?= $user->screenName; ?></label>
                        <li class="dropdown pull-right">
                            <label class="control-label"> Обьект: </label>
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle"> <?= ($_GET['obj'] == 'all') ? 'Все' : $businessobj[$_GET['obj']] ?> <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <? $getParams['obj'] = 'all'; ?>
                                <li><a href="<?= $ACTIONPAGE . '?' . http_build_query($getParams) ?>"><b>all</b> - Все </a></li>
                                <? foreach ($businessobj as $bObj => $name) { ?>
                                    <? $getParams['obj'] = $bObj; ?>
                                    <li><a href="<?= $ACTIONPAGE . '?' . http_build_query($getParams) ?>"><b><?= $bObj ?></b> - <?= $name ?></a></li>
                                <? } ?>
                            </ul>
                        </li>
                        <? foreach ($allPermits as $id => $desc) { ?>
                            <hr style="margin:5px;">
                            <div class="checkbox">
                                <label><input type="checkbox" class="canLock" name="permit[<?= $id ?>]" <?= empty($userPermit[$_GET['obj']][$id]) ? '' : ' checked' ?>> <b><?= $id ?></b> <?= ' - ' .$desc ?> </label>
                            </div>
                        <? } ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary savePermitBtn">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>
    <script>
        $(function() {

            $('.editPermitBtn').on('click', function () {
                $("#userPermitModal").modal('show');
            });

            $('.savePermitBtn').on('click', function (e) {
                var url = new URL((''+document.location));
                var obj = url.searchParams.get("obj");
                var path = url.origin + url.pathname;

                var error = 0;

                var form = $(e.currentTarget).closest('.modal').find('form');

                event.preventDefault();

                var data = form.serializeArray();
                console.log( data );
                $.ajax({
                    method: "POST",
                    url: path + '?obj=' + obj + '&r=home/savePermit',
                    data: data
                }).done(function( data ) {
                    if(data.error  !== undefined && data.error !== 1) {
                        window.location.reload();
                    } else {
                        alert("Data Saved Error: " + JSON.stringify(data.content));
                    }
                });
            });

        });
    </script>
<? } ?>