<!-- поле с выбором единицы измерения -->
<div class="input-group">
    <input type="number" step="0.001" min="0" class="form-control border-0 text-right" value="123">
    <div class="input-group-btn">
        <button type="button" class="btn btn-default dropdown-toggle border-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">г</button>
        <ul class="dropdown-menu dropdown-menu-right unitSelect">
            <li><a href="#">г</a></li>
            <li><a href="#">кг</a></li>
            <li><a href="#">мл</a></li>
            <li><a href="#">л</a></li>
            <li><a href="#">шт</a></li>
            <li><a href="#">дес</a></li>
        </ul>
    </div>
</div>
