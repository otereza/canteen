<? if(DEBUG) {
    global $FILES_ROOT;

    $userPermit = array();
    if(file_exists($FILES_ROOT . '/userpermits.txt')) {
        $data = file_get_contents($FILES_ROOT . '/userpermits.txt');
        if(!empty($data)) {
            $userPermit = json_decode($data, true);
        }
    }
    echo '<button type="button" class="btn btn-link editPermitBtn" data-ctrl="trader" style="position:fixed;right:0;z-index:1000;"> <i class="fa fa-gear" style="font-size:24px"></i> </button>';
    echo \Food\Core\View::renderPart('home/userPermit', array('userPermit' => $userPermit));
} ?>

<p></p>
<div class="container-fluid">
    {{ CONTENT }}
</div>
