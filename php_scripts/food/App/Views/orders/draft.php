<?
global $ACTIONPAGE, $businessobj;

use \Food\App\Helpers\Units;

$packUnits = Units::getAll();

$ref_url = urlencode($_SERVER['REQUEST_URI']);
$ref_msg = urlencode('к заявке');

// переменные передаются с контроллера, поэтому здесь они как неопределенные. Определим )))
$trader = isset($trader) ? $trader : array();
$order = isset($order) ? $order : array();
$deliveryDays = isset($deliveryDays) ? $deliveryDays : array();

$forObj = empty($_GET['for']) ? null : $_GET['for'];
$obj = empty($_GET['obj']) ? '' : $_GET['obj'];

?>

<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= $ACTIONPAGE . '?obj=' . $obj . '&r=order' ?>">Список заявок</a></li>
        <li class="breadcrumb-item active">Черновик завки</li>
    </ol>
</nav>

<script>
    var units = <?= json_encode($packUnits) ?>;
    var packUnits = <?= json_encode(array_keys($packUnits)) ?>;
</script>

<h2>Заявка поставщику
    <button data-href="<?= $ACTIONPAGE ?>?obj=<?= $_GET['obj'] ?>&r=trader&id=<?= $trader['id'] ?><?= !empty($_GET['for']) ? '&for=' . $_GET['for'] : '' ?>&ref=<?= $ref_url ?>&refmsg=<?= $ref_msg ?>" id="toTraderBtn" class="btn btn-default margin-l20">
        <?= $trader['internal_name'] ?>
    </button>
</h2>
<span class="label label-success"> черновик </span>

<? if($_GET['obj'] == 'all' && isset($businessobj[$order['obj']])) { ?>
    <h3 class="text-info"><?= $businessobj[$order['obj']] ?></h3>
<? } ?>


<form role="form" id="traderForm">

    <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
    <div class="alert alert-warning fade in lockData hidden"></div>

    <br>

    <div class="form-group form-inline">

    <input type="hidden" id="trader" name="trader" value="<?= $trader['id'] ?>">
    <input type="hidden" id="orderId" value="<?= $_GET['id'] ?>">

    <? $arrWeek = array(
        1 => 'Понедельник',
        2 => 'Вторник',
        3 => 'Среда',
        4 => 'Четверг',
        5 => 'Пятница',
        6 => 'Суббота',
        7 => 'Воскресенье'
    ); ?>

        <div class="input-group margin-l20">
            <span class="input-group-addon"> Дата доставки </span>
            <select class="form-control requiredValue" id="dateShipment" name="date_shipment">
                <option value="" disabled readonly<?= $order['date_shipment'] ? '' : ' selected' ?> class="hidden"></option>
                <? for ($i = 0; $i < 10; $i++) { ?>
                    <? $wDay = date('N', strtotime("+" . $i . " day")); ?>
                    <option value="<?= strtotime("+" . $i . " day") ?>"
                            <?= (!empty($order['date_shipment']) && date('d.m.Y', strtotime("+" . $i . " day")) == date('d.m.Y', $order['date_shipment'])) ? ' selected' : '' ?>
                            data-wday="<?= $wDay ?>"
                            class="<?= isset($deliveryDays[$wDay]) ? 'bgC6' : '' ?>"
                            data-from="<?= isset($deliveryDays[$wDay]) ? $deliveryDays[$wDay]['from'] : '' ?>"
                            data-to="<?= isset($deliveryDays[$wDay]) ? $deliveryDays[$wDay]['to'] : '' ?>">
                        <?= $arrWeek[$wDay] . ' ' .date('d.m.Y', strtotime("+" . $i . " day")) ?>
                    </option>
                <? } ?>
            </select>
        </div>

        <div class="input-group deliveryTime margin-l20">
            <span class="input-group-addon"> с </span>
            <input type='time' id="timeFrom" class="form-control deliveryTime<?= isset($trader['obj']['delivery_days'][$wDay]) ? ' requiredValue' : '' ?>" name="wday[<?= $wDay ?>][from]" value="<?= !empty($order['delivery_time']) ? $order['delivery_time']['from'] : '' ?>">
        </div>
        <div class="input-group deliveryTime">
            <span class="input-group-addon"> по </span>
            <input type='time' id="timeTo" class="form-control deliveryTime<?= isset($trader['obj']['delivery_days'][$wDay]) ? ' requiredValue' : '' ?>" name="wday[<?= $wDay ?>][to]" value="<?= !empty($order['delivery_time']) ? $order['delivery_time']['to'] : '' ?>">
        </div>
        <?// $needSave = (empty($order['date_shipment']) || empty($order['delivery_time']) || empty($order['total'])); ?>
        <button type="button" id="saveOrderBtn" class="btn btn-primary canHide margin-l20" data-ctrl="trader">Отправить заявку</button>
    </div>
</form>

<table class="table table-hover prodList">
    <thead>
    <tr>
        <th>&nbsp;</th>
        <th>Наименование</th>
        <th>Ед.изм.</th>
        <th>Цена <span class="help-block" style="font-size:smaller; margin:0;"> (со скидкой) за ед.изм. </span> </th>
        <th>Количество</th>
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <? $totalPrice = 0; ?>
    <? if (!empty($rows)) { ?>
        <? foreach ($rows as $row) {

            $rowPrice = round($row['price'], ($row['amount_unit'] != $row['pack_unit']) ? (strlen($row['unit_value']) - 1 + 2) : 2) * $row['amount'];
            $totalPrice += $rowPrice;

            $bgPrice = '';
            if($row['trader_price_user'] > $row['trader_price']) {
                $bgPrice = 'bgC3';
            } elseif($row['trader_price_user'] < $row['trader_price']) {
                $bgPrice = 'bgC6';
            }
        ?>

            <tr class="prodListItem" data-row-price="<?= $rowPrice ?>" data-id="<?= $row['id'] ?>" data-pack-id="<?= $row['pack_id']?>">
                <td class="dragAnchor" style="width: 30px"><i class="fa fa-ellipsis-v"></i></td>
                <td style="width: 40%"><input type="hidden" value="<?= $row['prod_name'] ?>"><span class="prodName"><?= $row['prod_name'] ?></span></td>
                <td style="width: 20%">
                    <input type="hidden" class="form-control requiredValue packUnit" value="<?= $row['amount_unit'] ?>" data-pack-unit="<?= $row['pack_unit'] ?>">
                    <div class="dropdown form-control canLock" style="width: 100px">
                        <span class="variable packUnitText"><?= $row['amount_unit'] ?></span>
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle pull-right"><b class="caret"></b></a>
                        <ul class="dropdown-menu unitList" data-unit="<?= $row['unit'] ?>">
                            <? if($row['unit'] != $row['pack_unit']) { ?>
                                <li><a href="#"><?= $row['unit'] ?></a></li>
                            <? } ?>
                            <li><a href="#"><?= $row['pack_unit'] ?></a></li>
                        </ul>
                    </div>
                </td>
                <td style="width: 20%">
                    <input type="text"
                            class="onlyUnSignFloatNumbers text-right price hidden"
                            value="<?= $row['trader_price_user'] ?>"
                            data-origin="<?= $row['trader_price'] ?>"
                            data-user="<?= $row['trader_price_user'] ?>"
                            size="10">
                    <span class="help-block priceLabel hidden" style="font-size: smaller;"> цена без скидки за упаковку </span>
                    <span class="priceText <?= $bgPrice ?>"><?= sprintf('%.' . (($row['amount_unit'] != $row['pack_unit']) ? (strlen($row['unit_value']) - 1 + 2) : 2) . 'f', $row['price']) ?></span>
                </td>
                <? $decimalNum = isset($packUnits[$row['amount_unit']]) ? $packUnits[$row['amount_unit']]['decimal_places'] : 0 ?>
                <td style="width: 20%"><input type="text"
                            class="amount onlyUnSignFloatNumbers"
                            value="<?= sprintf('%.' . $decimalNum . 'f', $row['amount']) ?>"
                            data-unit-value="<?= $row['unit_value'] ?>">
                </td>
                <td class="text-center" style="width: 30px"><a type="button" class="btn btn-link btn-sm delOrderProdBtn" data-id="<?= $row['id'] ?>"><i class="fa fa-remove c2"></i></a></td>
            </tr>
        <? } ?>
    <? } ?>
    <tr class="prodListItem hidden">
        <td class="dragAnchor" style="width: 30px"><i class="fa fa-ellipsis-v"></i></td>
        <td style="width: 40%"><input type="hidden" value=""><span class="prodName"></span></td>
        <td style="width: 20%">
            <input type="hidden" class="form-control requiredValue packUnit" value="">
            <div class="dropdown form-control canLock" style="width: 100px">
                <span class="variable packUnitText"></span>
                <a href="#" data-toggle="dropdown" class="dropdown-toggle pull-right"><b class="caret"></b></a>
                <ul class="dropdown-menu unitList"></ul>
            </div>
        </td>
        <td style="width: 20%">
            <input type="text" class="onlyUnSignFloatNumbers text-right price hidden" value="" size="10">
            <span class="help-block priceLabel hidden" style="font-size: smaller;"> цена без скидки за упаковку </span>
            <span class="priceText"></span>
        </td>
        <td style="width: 20%"><input type="text" class="onlyUnSignFloatNumbers amount" value=""></td>
        <td style="width: 30px"><a type="button" class="btn btn-link btn-sm delOrderProdBtn"><i class="fa fa-remove c2"></i></a></td>
    </tr>
    <tr id="newRow" class="active">
        <td>&nbsp;</td>
        <td><input type="text" id="prodName" style="width: 100%"></td>
        <td>&nbsp;</td>
        <td>&nbsp;<span id="orderTotalPrice" class="h2"><?= sprintf("%.2f", $totalPrice) ?></span></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    </tbody>
</table>

<div id="jstreePanel" class="col-md-9 margin-t20 hidden panel panel-primary">
    <div class="alert alert-warning fade hidden in offline"> Нет связи с сервером… </div>
    <div class="alert alert-warning fade hidden in lockData"></div>
    <?= \Food\Core\View::renderPart('products/tree'); ?>
</div>

<div id="changedPriceModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <table id="priceTable" class="table">
                    <thead>
                    <tr>
                        <th rowspan="2">Наименование</th>
                        <th colspan="2" class="text-center">Цена</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" id="savePriceBtn" class="btn btn-primary">Изменить</button>
                <button type="button" id="refusePriceBtn" class="btn btn-default" data-dismiss="modal">Отказаться</button>
            </div>
        </div>
    </div>
</div>
