<?
    global $ACTIONPAGE, $businessobj;

    $obj = isset($_GET['obj']) ? $_GET['obj'] : 'all';
    $forObj = isset($_GET['for']) ? $_GET['for'] : false;

    $tab = isset($_GET['tab']) ? $_GET['tab'] : 'draft';
?>

<h2>Заявки</h2>
<? if(!empty($permit['canEditOrder'])) { ?>
    <form action="<?= $ACTIONPAGE ?>?obj=<?= $_GET['obj'] ?>&r=order/edit">
        <input type="hidden" name="obj" value="<?= $_GET['obj'] ?>">
        <input type="hidden" name="r" value="order/edit">
        <div class="form-group form-inline">

            <? if($obj == 'all') { ?>
                <div class="input-group<?= $obj == 'all' ? '' : ' hidden'?>">
                    <select class="form-control requiredValue" id="obj">
                        <option value="0" selected class="hidden"> Объект </option>
                        <? foreach ($businessobj as $bObj => $bName) { ?>
                            <option value="<?= $bObj ?>"><?= $bName ?></option>
                        <? } ?>
                    </select>
                </div>
            <? } ?>

            <div class="input-group">
                <select class="form-control requiredValue" id="trader" name="trader_id">
                    <option value="0" selected class="hidden"> Поставщик </option>
                    <? foreach ($objTraders as $trader) { ?>
                        <option value="<?= $trader['id'] ?>"><?= $trader['internal_name'] ?></option>
                    <? } ?>
                </select>
            </div>
            <button type="button" class="btn btn-primary btn-sm canHide addOrderBtn">Добавить заявку</button>
            <span class="help-block hidden">Выберите<?= $obj == 'all' ? ' объект и' : ''?> поставщика</span>
        </div>
    </form>
<? } ?>

<ul class="nav nav-tabs" id="orderStateTab">
    <li class="<?= $tab=='draft' ? 'active' : '' ?>"><a href="#tabDraft">Черновики</a></li>
    <li class="<?= $tab=='work' ? 'active' : '' ?>"><a href="#tabWork">В работе</a></li>
    <li class="<?= $tab=='closed' ? 'active' : '' ?>"><a href="#tabClosed">Закрытые</a></li>
</ul>

<div class="container-fluid">
    <div class="tab-content">
        <div id="tabDraft" class="tab-pane fade <?= $tab=='draft' ? 'in active' : '' ?>">
            <? if(!empty($orders['draft'])) { ?>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Номер</th>
                            <?if($_GET['obj'] == 'all') { ?>
                                <th>Объект</th>
                            <? } ?>
                            <th>Дата создания</th>
                            <th>Дата поставки</th>
                            <th>Сумма</th>
                            <th>Поставщик</th>
                        </tr>
                    </thead>
                    <tbody>
                    <? foreach ($orders['draft'] as $order) { ?>
                        <tr class="viewOrder" data-id="<?= $order['id'] ?>" data-obj="<?= $order['obj'] ?>">
                            <td><?= $order['id'] ?></td>
                            <?if($_GET['obj'] == 'all') { ?>
                                <td><?= $businessobj[$order['obj']] ?></td>
                            <? } ?>
                            <td><?= date('d.m.Y H:i', $order['date_open']) ?></td>
                            <td>
                                <span> <?= date('d.m.Y', $order['date_shipment']) ?> </span>,
                                <span> <?= $order['delivery_time']['from'] ?> </span> -
                                <span> <?= $order['delivery_time']['to'] ?> </span>
                            </td>
                            <td><?= $order['total'] ?></td>
                            <td><?= isset($traders[$order['trader_id']]) ? $traders[$order['trader_id']]['internal_name'] : '' ?></td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            <? } else { ?>
                Нет черновиков
            <? } ?>
        </div>
        <div id="tabWork" class="tab-pane fade <?= $tab=='work' ? 'in active' : '' ?>">
            <? if(!empty($orders['work'])) { ?>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Номер</th>
                            <?if($_GET['obj'] == 'all') { ?>
                                <th>Объект</th>
                            <? } ?>
                            <th>Дата отправки</th>
                            <th>Дата поставки</th>
                            <th>Сумма</th>
                            <th>Поставщик</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($orders['work'] as $order) { ?>
                            <tr class="viewOrder" data-id="<?= $order['id'] ?>" data-obj="<?= $order['obj'] ?>">
                                <td><?= $order['id'] ?></td>
                                <?if($_GET['obj'] == 'all') { ?>
                                    <td><?= $businessobj[$order['obj']] ?></td>
                                <? } ?>
                                <td><?= empty($order['date_send']) ? '-' : date('d.m.Y H:i', $order['date_send']) ?></td>
                                <td>
                                    <span> <?= date('d.m.Y', $order['date_shipment']) ?> </span>,
                                    <span> <?= $order['delivery_time']['from'] ?> </span> -
                                    <span> <?= $order['delivery_time']['to'] ?> </span>
                                </td>
                                <td><?= $order['total'] ?></td>
                                <td><?= isset($traders[$order['trader_id']]) ? $traders[$order['trader_id']]['internal_name'] : '' ?></td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
            <? } else { ?>
                Нет заявок в работе
            <? } ?>
        </div>
        <div id="tabClosed" class="tab-pane fade <?= $tab=='closed' ? 'in active' : '' ?>">
            <? if(!empty($orders['close'])) { ?>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Номер</th>
                            <?if($_GET['obj'] == 'all') { ?>
                                <th>Объект</th>
                            <? } ?>
                            <th>Дата отправки</th>
                            <th>Дата поставки</th>
                            <th>Сумма</th>
                            <th>Поставщик</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($orders['close'] as $order) { ?>
                            <tr class="viewOrder" data-id="<?= $order['id'] ?>" data-obj="<?= $order['obj'] ?>">
                                <td><?= $order['id'] ?></td>
                                <?if($_GET['obj'] == 'all') { ?>
                                    <td><?= $businessobj[$order['obj']] ?></td>
                                <? } ?>
                                <td><?= empty($order['date_send']) ? '-' : date('d.m.Y H:i', $order['date_send']) ?></td>
                                <td><?= empty($order['date_close']) ? '-' : date('d.m.Y H:i', $order['date_close']) ?></td>
                                <td><?= $order['total'] ?></td>
                                <td><?= isset($traders[$order['trader_id']]) ? $traders[$order['trader_id']]['internal_name'] : '' ?></td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
            <? } else { ?>
                Нет закрытых заявок
            <? } ?>
        </div>
    </div>

</div>

<?= \Food\Core\View::renderPart('traders/forms/traderModal'); ?>
