<?
//$rows = array(
//    0 => array(
//        'id' => '1',
//        'prod_name' => 'prod 1',
//        'unit' => 'шт',
//        'price' => '12.34',
//        'amount' => '10'
//    ),
//    1 => array(
//        'id' => '2',
//        'prod_name' => 'prod 2',
//        'unit' => 'л',
//        'price' => '21.44',
//        'amount' => '1'
//    ),
//    2 => array(
//        'id' => '3',
//        'prod_name' => 'prod 3',
//        'unit' => 'кг',
//        'price' => '102.50',
//        'amount' => '6'
//    )
//);
use \Food\App\Models\OrderModel;

global $ACTIONPAGE, $businessobj;

$packUnits = array('г', 'кг', 'мл', 'л', 'шт', 'дес');
$unitRoundTo = array(
    'г' => 0,
    'кг' => 3,
    'мл' => 0,
    'л' => 3,
    'шт' => 0,
    'дес' => 1
);

$ref_url = urlencode($_SERVER['REQUEST_URI']);
$ref_msg = urlencode('к заявке');

$forObj = empty($_GET['for']) ? null : $_GET['for'];
$obj = empty($_GET['obj']) ? '' : $_GET['obj'];

?>

<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= $ACTIONPAGE . '?obj=' . $obj . '&r=order&tab=closed' ?>">Список заявок</a></li>
        <li class="breadcrumb-item active">Заявка закрыта</li>
    </ol>
</nav>

<script>
    var unitRoundTo = <?= json_encode($unitRoundTo) ?>;
</script>

<h2>Заявка поставщику
    <a href="<?= $ACTIONPAGE ?>?obj=<?= $_GET['obj'] ?>&r=trader&id=<?= $trader['id'] ?>&ref=<?= $ref_url ?>&refmsg=<?= $ref_msg ?>" class="btn btn-default canHide margin-l20">
        <?= $trader['internal_name'] ?>
    </a>
</h2>
<span class="label label-danger"> закрыта </span>

<? if($_GET['obj'] == 'all' && isset($businessobj[$order['obj']])) { ?>
    <h3 class="text-info"><?= $businessobj[$order['obj']] ?></h3>
<? } ?>

<form role="form" id="traderForm">

    <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
    <div class="alert alert-warning fade in lockData hidden"></div>

    <br>

    <div class="form-group form-inline">

        <input type="hidden" id="trader" name="trader" value="<?= $trader['id'] ?>">
        <input type="hidden" id="orderId" value="<?= $_GET['id'] ?>">

        <? $arrWeek = array(
            1 => 'Понедельник',
            2 => 'Вторник',
            3 => 'Среда',
            4 => 'Четверг',
            5 => 'Пятница',
            6 => 'Суббота',
            7 => 'Воскресенье'
        ); ?>

        <div class="text-nowrap well">
            <label class="margin-l20"> Создана: </label>
            <span> <?= date('d.m.Y H:i', $order['date_open']) ?> </span>

            <label class="margin-l20"> Отправлена: </label>
            <span> <?= date('d.m.Y H:i', $order['date_send']) ?> </span>

            <label class="margin-l20"> Поставка: </label>
            <span> <?= date('d.m.Y', $order['date_shipment']) ?>, </span>
            <span> <?= $order['delivery_time']['from'] ?> </span> -
            <span> <?= $order['delivery_time']['to'] ?> </span>

            <label class="margin-l20"> Закрыта: </label>
            <span> <?= date('d.m.Y H:i', $order['date_close']) ?> </span>
        </div>

        <table class="table table-hover prodList">
            <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Наименование</th>
                <th>Ед.изм.</th>
                <th>Цена</th>
                <th>Количество</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <? if (!empty($rows)) { ?>
                <? foreach ($rows as $row) { ?>
                    <tr class="prodListItem">
                        <td style="width: 30px"><i class="fa fa-ellipsis-v"></i></td>
                        <td style="width: 40%"><?= $row['prod_name'] ?></td>
                        <td style="width: 20%"><?= $row['amount_unit'] ?></td>
                        <td style="width: 20%"><?= sprintf('%.' . (($row['amount_unit'] != $row['pack_unit']) ? (strlen($row['unit_value']) - 1 + 2) : 2) . 'f', $row['price']) ?></td>
                        <? $decimalNum = isset($packUnits[$row['amount_unit']]) ? $packUnits[$row['amount_unit']]['decimal_places'] : 0 ?>
                        <td style="width: 20%"><?= sprintf('%.' . $decimalNum . 'f', $row['amount']) ?></td>
                        <td style="width: 30px">&nbsp;</td>
                    </tr>
                <? } ?>
            <? } ?>
            <tr class="active">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;<span id="orderTotalPrice" class="h2"><?= sprintf("%.2f", $order['total']) ?></span></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            </tbody>
        </table>

