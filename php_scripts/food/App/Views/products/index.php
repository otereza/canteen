<?

$urlObj = empty($_GET['obj']) ? 'all' : $_GET['obj'];

    // проверка прав пользователя
global $user, $ACTIONPAGE;

$changeAll = $user->havePermit(301, $urlObj);
$changeTmarkPack = $user->havePermit(302, $urlObj);

    if (!$user->havePermit(300, $urlObj)) {
        $info = "Вы не можете просматривать данные";
    }


?>


<? if (isset($error)) { ?>
    <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Error!</strong> <?= $error ?>
    </div>
<? } else { ?>
    <? if (isset($info)) { ?>
        <div class="alert alert-info fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Внимание!</strong> <?= $info ?>
        </div>
    <? } ?>


<div class="container-fluid">

    <!-- ПЕРЕКЛЮЧАЛКА ДЛЯ ТЕСТОВ  -->
    <label>Переключалка для перехода по обектам</label>
    <ul class="nav nav-tabs">
        <? global $businessobj; ?>
        <? $objs = array_merge(array('all'=>'Все объекты'), $businessobj); ?>
        <? foreach ($objs as $obj => $name) { ?>
            <li<?= ($urlObj == $obj) ? ' class="active"' : '' ?> data-obj="<?= $obj ?>"><a href="<?= $ACTIONPAGE ?>?obj=<?= $obj ?>&r=product"><?= $name ?></a></li>
        <? } ?>
    </ul>
    <!-- ПЕРЕКЛЮЧАЛКА ДЛЯ ТЕСТОВ  -->




    <? if (!empty($tree) && is_array($tree)) { ?>
        <div class="just-padding keyEvents">
            <button id="addGroup" type="button" class="btn btn-success btn-sm margin-t-5"><i class="fa fa-plus"></i> Группа</button>
            <button id="viewDeleted" type="button" class="btn btn-default btn-sm margin-t-5"><i class="fa fa-eye-slash"></i> Показать удаленные</button>
            <ul class="tree-list well margin-t10">
                <li class="sortableListsOpen noDrag" data-level-root="1">
                    <ul class="noDrag">

                    <? foreach ($tree as $group) { ?>

                    <!-- Group -->
                    <? $haveTypes = (isset($group['types']) && is_array($group['types'])); ?>
                    <li data-level="1" data-level-root="2" class="<?= ($group['deleted'] == 1) ? 'deleted hidden' : '' ?>">
                    <div href="#item-<?= $group['id'] ?>" class="<?= $haveTypes ? '' : ' no-child' ?>" data-toggle="collapse">
                        <?= $group['name'] ?>
                        <span class="pull-right margin-t-5 actions noDrag <?= ($group['deleted'] == 1) ? 'hidden' : '' ?>" data-g-name="<?= $group['name'] ?>" data-g-id="<?= $group['id'] ?>">
                            <button type="button" class="btn btn-success btn-sm noDrag addType" title="Добавить вид продукта"><i class="fa fa-plus noDrag"></i> Вид </button>
                            <button type="button" class="btn btn-info btn-sm noDrag copyGroup" title="Копировать"><i class="fa fa-clone noDrag"></i></button>
                            <button type="button" class="btn btn-warning btn-sm noDrag editGroup" title="Редактировать"><i class="fa fa-edit noDrag"></i></button>
                            <button type="button" class="btn btn-danger btn-sm noDrag deleteGroup" title="Удалить"><i class="fa fa-remove noDrag"></i></button>
                        </span>
                    </div>

                    <ul>

                        <? if ($haveTypes) { ?>
                            <? foreach ($group['types'] as $type) { ?>

                                <!-- Type -->
                                <li data-level="2" data-level-root="3" class="<?= ($type['deleted'] == 1) ? 'deleted hidden' : '' ?>">
                                <? $haveTmarks = (isset($type['tmarks']) && is_array($type['tmarks'])); ?>
                                <div class="<?= $haveTmarks ? '' : ' no-child' ?>" data-toggle="collapse">
                                    <?= $type['name'] ?>
                                    <span class="pull-right actions noDrag <?= ($type['deleted'] == 1) ? 'hidden' : '' ?>"
                                         data-group-name="<?= $group['name'] ?>"
                                         data-group-id="<?= $group['id'] ?>"
                                         data-type-name="<?= $type['name'] ?>"
                                         data-type-id="<?= $type['id'] ?>"
                                         data-pack-unit="<?= $type['pack_unit'] ?>"
                                    >
                                        <button type="button" class="btn btn-success btn-sm margin-t-5 noDrag addTMark" title="Добавить продукт"><i class="fa fa-plus noDrag"></i> Продукт </button>
                                        <button type="button" class="btn btn-info btn-sm margin-t-5 noDrag copyType" title="Копировать"><i class="fa fa-clone noDrag"></i></button>
                                        <button type="button" class="btn btn-warning btn-sm margin-t-5 noDrag editType" title="Редактировать"><i class="fa fa-edit noDrag"></i></button>
                                        <button type="button" class="btn btn-danger btn-sm margin-t-5 noDrag deleteType" title="Удалить"><i class="fa fa-remove noDrag"></i></button>
                                    </span>
                                </div>
                                <ul>

                                    <? if ($haveTmarks) { ?>
                                        <? foreach ($type['tmarks'] as $tmark) { ?>

                                            <!-- Trade Marks -->
                                            <li data-level="3" data-level-root="4">
                                            <? $havePacks = (isset($tmark['packs']) && is_array($tmark['packs'])); ?>
                                            <div class="<?= $havePacks ? '' : ' no-child' ?>" data-toggle="collapse">
                                                <?= $tmark['name'] ?>
                                                <span class="pull-right actions noDrag"
                                                     data-tmark-name="<?= $tmark['name'] ?>"
                                                     data-tmark-id="<?= $tmark['id'] ?>"
                                                     data-pack-unit="<?= $tmark['pack_unit'] ?>"
                                                     data-life="<?= $tmark['life'] ?>"
                                                     data-temperature="<?= $tmark['temperature'] ?>"
                                                >
                                                    <button type="button" class="btn btn-success btn-sm margin-t-5 noDrag addPack" title="Добавить фасовку"><i class="fa fa-plus noDrag"></i> Фасовка </button>
                                                    <button type="button" class="btn btn-info btn-sm margin-t-5 noDrag copyTMark" title="Копировать"><i class="fa fa-clone noDrag"></i></button>
                                                    <button type="button" class="btn btn-warning btn-sm margin-t-5 noDrag editTMark" title="Редактировать"><i class="fa fa-edit noDrag"></i></button>
                                                    <button type="button" class="btn btn-danger btn-sm margin-t-5 noDrag deleteTMark" title="Удалить"><i class="fa fa-remove noDrag"></i></button>
                                                </span>
                                            </div>
                                            <ul>

                                                <? if ($havePacks) { ?>
                                                    <? foreach ($tmark['packs'] as $pack) { ?>

                                                        <!-- Pack -->
                                                        <li data-level="4">
                                                        <div href="#" class=""><?= $pack['name'] ?>
                                                            <span class="pull-right actions noDrag" data-pack-id="<?= $pack['id'] ?>" data-pack-name="<?= $pack['name'] ?>">
                                                                <button type="button" class="btn btn-info btn-sm margin-t-5 noDrag copyPack" title="Копировать"><i class="fa fa-clone noDrag"></i></button>
                                                                <button type="button" class="btn btn-warning btn-sm margin-t-5 noDrag editPack" title="Редактировать"><i class="fa fa-edit noDrag"></i></button>
                                                                <button type="button" class="btn btn-danger btn-sm margin-t-5 noDrag deletePack" title="Удалить"><i class="fa fa-remove noDrag"></i></button>
                                                            </span>
                                                        </div>
                                                        </li>
                                                    <? } ?>
                                                <? } ?>
                                            </ul>
                                            </li>
                                        <? } ?>
                                    <? } ?>
                                </ul>
                                </li>
                            <? } ?>
                        <? } ?>
                    </ul>
                    </li>
                <? } ?>

                    </ul>
                </li>
            </ul>

        </div>
    <? } ?>


</div>



<!--  ФОРМЫ  -->

<div id="tmarkModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Торговая марка</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="tmarkForm">

                    <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
                    <div class="alert alert-warning fade in lockData hidden"></div>

                    <input type="hidden" class="form-control field-id" name="id">
                    <input type="hidden" class="form-control field-pos" name="position">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="input-group margin-b10">
                                    <label for="tmark-groupId" class="control-label">Группа:</label>
                                    <input type="hidden" class="form-control field-groupId" name="group_id">
                                    <input type="text" class="field-groupName" disabled>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="input-group">
                                    <label for="tmark-typeId" class="control-label">Вид:</label>
                                    <input type="hidden" class="form-control field-typeId" name="type_id">
                                    <input type="text" class="pull-right field-typeName" disabled>
                                </div>
                            </div>
                            <div class="col-xs-6">
                            </div>
                        </div>


                    </div>

                    <div class="form-group">
                        <label for="tmark-name" class="control-label">Название:</label>
                        <input type="text" class="form-control field-name requiredValue canLock" name="name">
                    </div>

                    <div class="form-group form-inline">
                        <label for="tmark-packUnit" class="control-label">Единица измерения:</label>
                        <input type="hidden" class="form-control requiredValue field-packUnit" name="pack_unit">
                        <div class="dropdown form-control canLock">
                            <span id="tmark-packUnitText" class="variable"></span>
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b class="caret"></b></a>
                            <ul class="dropdown-menu" id="tmark-packUnitList">
                                <li><a href="#">г/кг</a></li>
                                <li><a href="#">мл/л</a></li>
                                <li><a href="#">шт/уп</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="form-group form-inline">
                        <label for="tmark-life" class="control-label">Срок годности (суток):</label>
                        <input type="text" class="form-control field-life canLock" name="life" size="5">
                    </div>

                    <div class="form-group">
                        <label for="type-temperature" class="control-label">Хранение:</label>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="input-group margin-b10">
                                    <span class="input-group-addon"> от </span>
                                    <input type="text" class="form-control field-tempFrom canLock" name="temp_from">
                                    <span class="input-group-addon">&#8451</span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="input-group">
                                    <span class="input-group-addon"> до </span>
                                    <input type="text" class="form-control field-tempTo canLock" name="temp_to">
                                    <span class="input-group-addon">&#8451</span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-primary saveBtn canHide" data-ctrl="prodTMark">Сохранить</button>
            </div>
        </div>
    </div>
</div>

<?//= \Food\Core\View::renderPart('products/forms/tmark'); ?>

<div id="packModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Упаковка</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="packForm">
                    <input type="hidden" class="form-control" name="id">
                    <input type="hidden" class="form-control" name="position">

                    <div class="form-group">
                        <div class="input-group margin-b10">
                            <label for="pack-productId" class="control-label">Продукт:</label>
                            <input type="hidden" class="form-control" id="pack-productId" name="product_id">
                            <input type="text" id="pack-productName" disabled>
                        </div>
                    </div>

                    <input type="hidden" class="form-control" id="pack-name" name="name">

                    <div class="form-group form-inline">
                        <label for="pack-value" class="control-label">Количество:</label>
                        <input type="text" class="form-control" id="pack-value" name="pack_value">
<!--                        <label for="tmark-packUnit" class="control-label">Единица измерения:</label>-->
                        <input type="hidden" class="form-control" id="pack-packUnit" name="pack_unit">
                        <div class="dropdown form-control">
                            <span id="pack-packUnitText" class="variable"></span>
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b class="caret"></b></a>
                            <ul class="dropdown-menu" id="pack-packUnitList">
                                <li><a href="#">кг</a></li>
                                <li><a href="#">г</a></li>
                                <li><a href="#">л</a></li>
                                <li><a href="#">мл</a></li>
                                <li><a href="#">уп</a></li>
                                <li><a href="#">шт</a></li>
                            </ul>
                        </div>
                        <div class="checkbox pull-right">
                            <label><input type="checkbox" id="pack-byWeight" name="by_weight"> На вес </label>
                        </div>
                    </div>


                    <div class="form-group form-inline">
                        <label for="pack-life" class="control-label">Срок годности (суток):</label>
                        <input type="text" class="form-control" id="pack-life" name="life" size="5">
                    </div>

                    <div class="form-group">
                        <label for="pack-temperature" class="control-label">Хранение:</label>
                        <input type="hidden" class="form-control" id="pack-temperature" name="temperature"><br>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="input-group margin-b10">
                                    <span class="input-group-addon"> от </span>
                                    <input type="text" class="form-control" id="pack-temperatureFrom">
                                    <span class="input-group-addon">&#8451</span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="input-group">
                                    <span class="input-group-addon"> до </span>
                                    <input type="text" class="form-control" id="pack-temperatureTo">
                                    <span class="input-group-addon">&#8451</span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" id="pack-saveBtn" class="btn btn-primary">Сохранить</button>
            </div>
        </div>
    </div>
</div>

<div id="typeModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Вид продукта</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="typeForm">

                    <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
                    <div class="alert alert-warning fade in lockData hidden"></div>

                    <input type="hidden" class="form-control field-id" name="id">
                    <input type="hidden" class="form-control field-pos" name="position">

                    <div class="form-group form-inline">
                        <label for="type-groupId" class="control-label">Группа:</label>
                        <input type="hidden" class="form-control field-groupId" name="group_id">
                        <input type="text" class="field-groupName" disabled>
                    </div>

                    <div class="form-group">
                        <label for="type-name" class="control-label">Название:</label>
                        <input type="text" class="form-control field-name requiredValue canLock" name="name">
                    </div>

                    <div class="form-group form-inline">
                        <label for="type-packUnit" class="control-label">Единица измерения:</label>
                        <input type="hidden" class="form-control requiredValue field-packUnit" name="pack_unit">
                        <div class="dropdown form-control canLock">
                            <span id="type-packUnitText" class="variable"></span>
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b class="caret"></b></a>
                            <ul class="dropdown-menu" id="type-packUnitList">
                                <li><a href="#">кг</a></li>
                                <li><a href="#">г</a></li>
                                <li><a href="#">л</a></li>
                                <li><a href="#">мл</a></li>
                                <li><a href="#">уп</a></li>
                                <li><a href="#">шт</a></li>
                            </ul>
                        </div>
                        <div class="checkbox pull-right">
                            <label><input type="checkbox" class="field-roundToPack canLock" name="round_to_pack"> Округлять до упаковки</label>
                        </div>
                    </div>

                    <div class="form-group form-inline">
                        <label for="type-life" class="control-label">Срок годности (суток):</label>
                        <input type="text" class="form-control requiredValue field-life canLock" name="life" size="5">
                    </div>

                    <div class="form-group">
                        <label for="type-temperature" class="control-label">Хранение:</label>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="input-group margin-b10">
                                    <span class="input-group-addon"> от </span>
                                    <input type="text" class="form-control requiredValue field-tempFrom canLock" name="temp_from">
                                    <span class="input-group-addon">&#8451</span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="input-group">
                                    <span class="input-group-addon"> до </span>
                                    <input type="text" class="form-control requiredValue field-tempTo canLock" name="temp_to">
                                    <span class="input-group-addon">&#8451</span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="checkbox">
                                <label><input type="checkbox" class="field-deliveryPossible canLock" name="delivery_possible"> Возможна поставка в день выдачи до </label>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="input-group hidden field-deliveryHour-block">
                                <input type="time" class="form-control field-deliveryHour canLock" name="delivery_to_hour">
                                <span class="input-group-addon"> часов </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-primary saveBtn canHide" data-ctrl="prodType">Сохранить</button>
            </div>
        </div>
    </div>
</div>

<div id="groupModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Группа</h4>
            </div>
            <div class="modal-body">

                <form role="form" id="groupForm" action="" method="post">
                    <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
                    <div class="alert alert-warning fade in lockData hidden"></div>

                    <input type="hidden" class="form-control field-id" name="id">
                    <input type="hidden" class="form-control field-pos" name="position">

                    <div class="form-group">
                        <label for="group-name" class="control-label">Название</label>
                        <input type="text" class="form-control field-name requiredValue canLock" name="name">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-primary saveBtn canHide" data-ctrl="prodGroup">Сохранить</button>
            </div>
        </div>
    </div>
</div>


<? } ?>