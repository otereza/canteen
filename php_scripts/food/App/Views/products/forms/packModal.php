<div id="packModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Упаковка</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="packForm">

                    <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
                    <div class="alert alert-warning fade in lockData hidden"></div>

                    <input type="hidden" class="form-control field-id" name="id">
                    <input type="hidden" class="form-control field-pos" name="position">
                    <input type="hidden" class="form-control field-groupId" name="group_id">
                    <input type="hidden" class="form-control field-typeId" name="type_id">

                    <div class="form-group">
                        <div class="input-group margin-b10">
                            <label for="pack-productId" class="control-label">Торговая марка:</label>
                            <input type="hidden" class="form-control field-tmarkId" name="tmark_id">
                            <input type="text" class="pull-right field-tmarkName" disabled>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tmark-name" class="control-label">Название:</label>
                        <input type="text" class="form-control field-name canLock" name="name">
                    </div>

                    <div class="form-group form-inline">
                        <label for="pack-value" class="control-label">Количество:</label>
                        <div class="row" id="pack-value">
                            <div class="col-xs-6">
                                <input type="text" class="form-control onlyUnSignNumbers canLock requiredValue field-packValue" name="pack_value">
                            </div>
                            <div class="col-xs-3">
                                <input type="hidden" class="form-control requiredValue field-packUnit" name="pack_unit">
                                <div class="dropdown form-control canLock">
                                    <span id="pack-packUnitText" class="variable"></span>
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b class="caret"></b></a>
                                    <ul class="dropdown-menu" id="pack-packUnitList">
                                        <li class="weight"><a href="#">кг</a></li>
                                        <li class="weight"><a href="#">г</a></li>
                                        <li class="volume"><a href="#">л</a></li>
                                        <li class="volume"><a href="#">мл</a></li>
                                        <li class="piece"><a href="#">дес</a></li>
                                        <li class="piece"><a href="#">шт</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="checkbox pull-right">
                                    <label><input type="checkbox" id="pack-byWeight" class="canLock field-byWeight" name="by_weight"> На вес </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group form-inline">
                        <label for="tmark-life" class="control-label">Срок годности (суток):</label>
                        <input type="text" class="form-control field-life canLock" name="life" size="5">
                    </div>

                    <div class="form-group">
                        <label for="pack-temperature" class="control-label">Хранение:</label>
                        <div class="row" id="pack-temperature">
                            <div class="col-xs-6">
                                <div class="input-group margin-b10">
                                    <span class="input-group-addon"> от </span>
                                    <input type="text" class="form-control field-tempFrom canLock" name="temp_from">
                                    <span class="input-group-addon">&#8451</span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="input-group">
                                    <span class="input-group-addon"> до </span>
                                    <input type="text" class="form-control field-tempTo canLock" name="temp_to">
                                    <span class="input-group-addon">&#8451</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary saveBtn canHide" data-ctrl="prodPack">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>
