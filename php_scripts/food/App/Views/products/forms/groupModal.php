<div id="groupModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Группа</h4>
            </div>
            <div class="modal-body">

                <form role="form" id="groupForm" action="" method="post">
                    <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
                    <div class="alert alert-warning fade in lockData hidden"></div>

                    <input type="hidden" class="form-control field-id" name="id">
                    <input type="hidden" class="form-control field-pos" name="position">

                    <div class="form-group">
                        <label for="group-name" class="control-label">Название</label>
                        <input type="text" class="form-control field-name requiredValue canLock" name="name">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary saveBtn canHide" data-ctrl="prodGroup">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>
