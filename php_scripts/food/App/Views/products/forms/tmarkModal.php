<div id="tmarkModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Торговая марка</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="tmarkForm">

                    <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
                    <div class="alert alert-warning fade in lockData hidden"></div>

                    <input type="hidden" class="form-control field-id" name="id">
                    <input type="hidden" class="form-control field-pos" name="position">
                    <input type="hidden" class="form-control field-hasChaild" name="has_child">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="input-group margin-b10">
                                    <label for="tmark-groupId" class="control-label">Группа:</label>
                                    <input type="hidden" class="form-control field-groupId" name="group_id">
                                    <input type="text" class="field-groupName" disabled>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="input-group">
                                    <label for="tmark-typeId" class="control-label">Вид:</label>
                                    <input type="hidden" class="form-control field-typeId" name="type_id">
                                    <input type="text" class="pull-right field-typeName" disabled>
                                </div>
                            </div>
                            <div class="col-xs-6">
                            </div>
                        </div>


                    </div>

                    <div class="form-group">
                        <label for="tmark-name" class="control-label">Название:</label>
                        <input type="text" class="form-control field-name requiredValue canLock" name="name">
                    </div>

                    <div class="form-group form-inline">
                        <label for="tmark-packUnit" class="control-label">Единица измерения:</label>
                        <input type="hidden" class="form-control requiredValue field-packUnit" name="pack_unit">
                        <div class="dropdown form-control canLock">
                            <span id="tmark-packUnitText" class="variable"></span>
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b class="caret"></b></a>
                            <ul class="dropdown-menu" id="tmark-packUnitList">
                                <li class="weight"><a href="#">кг</a></li>
                                <li class="weight"><a href="#">г</a></li>
                                <li class="volume"><a href="#">л</a></li>
                                <li class="volume"><a href="#">мл</a></li>
                                <li class="piece"><a href="#">дес</a></li>
                                <li class="piece"><a href="#">шт</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="form-group form-inline">
                        <label for="tmark-life" class="control-label">Срок годности (суток):</label>
                        <input type="text" class="form-control onlyUnSignNumbers field-life canLock" name="life" size="5">
                    </div>

                    <div class="form-group">
                        <label for="type-temperature" class="control-label">Хранение:</label>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="input-group margin-b10">
                                    <span class="input-group-addon"> от </span>
                                    <input type="text" class="form-control onlySignNumbers field-tempFrom canLock" name="temp_from">
                                    <span class="input-group-addon">&#8451</span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="input-group">
                                    <span class="input-group-addon"> до </span>
                                    <input type="text" class="form-control onlySignNumbers field-tempTo canLock" name="temp_to">
                                    <span class="input-group-addon">&#8451</span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary saveBtn canHide" data-ctrl="prodTMark">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>
