
<button id="viewDeleted" type="button" class="btn btn-default btn-sm margin-t-5"><i class="fa fa-eye-slash"></i> Удаленные позиции</button>

<div class="container-fluid">
    <div id="jstree-product"></div>
</div>


<!--  ФОРМЫ  -->
<? use \Food\Core\View; ?>

<?= View::renderPart('products/forms/groupModal'); ?>
<?= View::renderPart('products/forms/typeModal'); ?>
<?= View::renderPart('products/forms/tmarkModal'); ?>
<?= View::renderPart('products/forms/packModal'); ?>

<? View::cssFile('jstree/themes/default/style.min.css'); ?>
<? View::jsFile('jstree/jstree.js', View::TO_END); ?>
<? View::jsFile('jstree/jstreegrid.js', View::TO_END);?>

