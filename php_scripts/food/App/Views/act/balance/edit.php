<?
global $ACTIONPAGE, $businessobj;

use Food\Core\View;
use \Food\App\Helpers\Units;
use \Food\App\Models\ActBalanceModel;

$packUnits = Units::getAll();

$ref_url = urlencode($_SERVER['REQUEST_URI']);
$ref_msg = urlencode('к приходной накладной');

// переменные передаются с контроллера, поэтому здесь они как неопределенные. Определим )))
$trader = isset($trader) ? $trader : array();
$order = isset($order) ? $order : array();
$act = isset($act) ? $act : array();
$rows = isset($rows) ? $rows : array();

$forObj = empty($_GET['for']) ? null : $_GET['for'];
$obj = empty($_GET['obj']) ? '' : $_GET['obj'];

//dd($invoice);
?>

<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= $ACTIONPAGE . '?obj=' . $obj . '&r=actBalance' ?>">Список актов снятия остатков</a></li>
        <li class="breadcrumb-item active">Редактирование акта</li>
    </ol>
</nav>

<? if(empty($permit['canEditActBalance'])) { ?>
    <div class="alert alert-warning fade in lockData"> Нет разрешения на редактирование. Только просмотр. </div>
<? } ?>

<h2 class="inline"> Акт <?= $act['type_balance'] == ActBalanceModel::BALANCE_FULL ? 'полного' : 'выборочного' ?> снятия остатков № <?= isset($act['act_number']) ? $act['act_number'] : 'без номера' ?></h2>

<? if($_GET['obj'] == 'all' && isset($businessobj[$act['obj']])) { ?>
    <h3 class="text-info"><?= $businessobj[$act['obj']] ?></h3>
<? } ?>


    <form action="<?= $ACTIONPAGE ?>?obj=<?= $_GET['obj'] ?>&r=actWritingOff/edit">
        <div class="form-group form-inline">

            <div class="alert alert-warning fade in offline hidden"> Нет связи с сервером… </div>
            <div class="alert alert-warning fade in lockData hidden"></div>

            <input type="hidden" value="<?= isset($act['act_number']) ? $act['act_number'] : '' ?>">

            <div class="input-group">
                <? if(!empty($permit['canEditActBalance'])) { ?>
                    <select class="form-control requiredValue" id="date" name="date">
                        <? for ($i = 0; $i < 60; $i++) { ?>
                            <? $day = date('d.m.Y', strtotime("-" . $i . " day")); ?>
                            <option value="<?= $day ?>"<?= (!empty($act['act_date']) && date('d.m.Y', $act['act_date']) == $day) ? ' selected' : '' ?>>
                                <?= $day ?>
                            </option>
                        <? } ?>
                    </select>
                <? } else { ?>
                    <div class="form-control">
                        <?= isset($act['act_date']) ? date('d.m.Y', $act['act_date']) : '' ?>
                    </div>
                <? } ?>
            </div>

            <div class="input-group">
                <div class="dropdown form-control">
                    <? if(!empty($permit['canEditActBalance'])) { ?>
                        <span data-toggle="dropdown" class="dropdown-toggle" id="timePeriodText"><?= empty($act['act_time_period']) ? '00:00 - 00:30' : $act['act_time_period'] ?></span>
                        <ul class="dropdown-menu scroll-y" id="timePeriodList"></ul>
                    <? } else { ?>
                            <?= isset($act['act_time_period']) ? $act['act_time_period'] : '' ?>
                    <? } ?>
                </div>
            </div>

            <? if(!empty($permit['canEditActBalance'])) { ?>
                <button type="button" id="saveBalanceActBtn" class="btn btn-primary btn-sm margin-l20">Сохранить</button>
            <? } ?>
        </div>
        <div class="form-group form-inline">
            <div class="input-group">
                <span class="input-group-addon"> Причина снятия остатков </span>
                <textarea id="reason" class="form-control <?= !empty($permit['canEditActBalance']) ? '' : 'bgC0'?>" cols="80" rows="4" <?= !empty($permit['canEditActBalance']) ? 'placeholder="Введите по какой причине списывается товар"' : 'readonly' ?>><?= isset($act['reason']) ? $act['reason'] : '' ?></textarea>
            </div>
        </div>
        <div class="form-group form-inline">
            <div class="input-group">
                <span class="input-group-addon"> ФИО членов комиссии </span>
                <textarea id="commission" class="form-control <?= !empty($permit['canEditActBalance']) ? '' : 'bgC0'?>" cols="80" rows="4" <?= !empty($permit['canEditActBalance']) ? 'placeholder="Введите ФИО членов комиссии"' : 'readonly' ?>><?= isset($act['commission']) ? $act['commission'] : '' ?></textarea>
            </div>
        </div>
    </form>



<script>
    var units = <?= json_encode($packUnits) ?>;
    var packUnits = <?= json_encode(array_keys($packUnits)) ?>;
    var actObj = "<?= $act['obj'] ?>";
</script>

<table class="table table-hover prodList">
    <thead>
    <tr>
        <th rowspan="2">&nbsp;</th>
        <? if($act['type_balance'] == ActBalanceModel::BALANCE_PART) { ?>
            <th rowspan="2">&nbsp;</th>
        <? } ?>
        <th rowspan="2" style="width: 500px">Наименование</th>
        <th colspan="4" class="text-center"> На складе <span class="help-block" style="font-size:smaller; margin:0;"> текущее состояние </span> </th>
        <th rowspan="2" class="text-center l-border""> Фактическое <span class="help-block" style="font-size:smaller; margin:0;"> количество </span> </th>
    </tr>
    <tr>
        <th class="text-center">Ед.изм.</th>
        <th class="text-center">Цена <span class="help-block" style="font-size:smaller; margin:0;"> за ед.изм. </span> </th>
        <th class="text-center">Количество </th>
        <th class="text-center">Срок годности </th>
    </tr>
    </thead>
    <tbody>
    <? $totalPrice = 0; ?>
    <? if(!empty($rows)) { ?>
        <? foreach ($rows as $row) {
            $rowPrice = $row['price'] * (isset($row['realAmount']) ? ($row['realAmount'] - $row['amount']) : 0);
            $totalPrice += $rowPrice;
            ?>
            <tr class="prodListItem" data-row-price="<?= $rowPrice ?>" data-pack-id="<?= $row['pack_id']?>">
                <td class="" style="width: 30px"><i class="fa fa-ellipsis-v"></i></td>
                <? if($act['type_balance'] == ActBalanceModel::BALANCE_PART) { ?>
                    <td style="width: 25px;"><input type="checkbox" class="check" <?= !empty($row['checked']) ? 'checked' : '' ?>></td>
                <? } ?>
                <td style="width: 500px"><span class="help-block" style="font-size:smaller; margin:0;"><?= $row['group_name'] ?></span><span class="prodName"><?= $row['prod_name'] ?></span></td>
                <td class="text-center packUnit"><?= $row['pack_unit'] ?></td>
                <td class="text-center inPrice"><?= sprintf("%.2f", $row['price']) ?></td>
                <td class="text-center inAmount"><?= $row['amount'] ?></td>
                <td class="text-center expDate"><?= date("d.m.Y", $row['exp_date']) ?></td>
                <? $decimalNum = isset($packUnits[$row['unit']]) ? $packUnits[$row['unit']]['decimal_places'] : 0 ?>
                <td class="text-center l-border">
                    <? if(!empty($permit['canEditActBalance'])) { ?>
                        <input type="number" min="0"
                                   class="realAmount border-0 onlyUnSignFloatNumbers"
                                   value="<?= isset($row['realAmount']) ? $row['realAmount'] : '' ?>"
                                   style="width: 5em">
                    <? } else { ?>
                        <?= isset($row['amount']) ? $row['amount'] : '' ?>
                    <? } ?>
                </td>
            </tr>
        <? } ?>
    <? } ?>

    <tr id="newRow" class="active">
        <td>&nbsp;</td>
        <? if($act['type_balance'] == ActBalanceModel::BALANCE_PART) { ?>
            <td>&nbsp;</td>
        <? } ?>
        <td>&nbsp;</td>
        <td colspan="4" class="h3 text-right"> Расхождение баланса: </td>
        <td class="text-center">&nbsp;<span id="docTotalPrice" class="h2 text-center"><?= sprintf("%.2f", $totalPrice) ?></span></td>
    </tr>

    </tbody>
</table>

<div id="jstreePanel" class="col-md-9 margin-t20 hidden panel panel-primary">
    <div class="alert alert-warning fade hidden in offline"> Нет связи с сервером… </div>
    <div class="alert alert-warning fade hidden in lockData"></div>
    <?= View::renderPart('products/tree'); ?>
</div>
