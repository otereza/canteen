<?
global $ACTIONPAGE, $businessobj;

use \Food\App\Models\ActBalanceModel;

$forObj = empty($_GET['for']) ? null : $_GET['for'];
$obj = empty($_GET['obj']) ? '' : $_GET['obj'];

$tab = isset($_GET['tab']) ? $_GET['tab'] : 'draft';

?>

<h2>Акты снятия остатков</h2>

<div class="container-fluid">
    <? if(!empty($permit['canEditActBalance'])) { ?>
        <form action="<?= $ACTIONPAGE ?>">
            <div class="form-group form-inline">
                <? if($obj == 'all') { ?>
                    <div class="input-group<?= $obj == 'all' ? '' : ' hidden'?>">
                        <select class="form-control requiredValue" id="obj">
                            <option value="" selected class="hidden"> Объект </option>
                            <? foreach ($businessobj as $bObj => $bName) { ?>
                                <option value="<?= $bObj ?>"><?= $bName ?></option>
                            <? } ?>
                        </select>
                    </div>
                <? } else { ?>
                    <input type="hidden" name="obj" id="obj" value="<?= $obj ?>">
                <? } ?>
                <div class="input-group">
                    <span class="input-group-text" style="position:absolute;top:-20px;">Тип снятия остатков</span>
                    <select class="form-control requiredValue" id="typeBalance">
                        <option value="<?= ActBalanceModel::BALANCE_FULL ?>" selected> полное </option>
                        <option value="<?= ActBalanceModel::BALANCE_PART ?>"> выборочное </option>
                    </select>
                </div>
                <button type="button" id="addActBtn" class="btn btn-primary btn-sm canHide" data-cnt="actBalance">Создать акт</button>
                <span class="help-block hidden">Выберите объект</span>
            </div>
        </form>
    <? } ?>

            <? if(!empty($list)) { ?>

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="text-left">Дата акта</th>
                        <?if($_GET['obj'] == 'all') { ?>
                            <th>Объект</th>
                        <? } ?>
                        <th>Номер</th>
                        <th>Типа снятия остатков</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($list as $act) { ?>
                        <tr class="viewAct" data-id="<?= $act['id'] ?>" data-obj="<?= $act['obj'] ?>" data-cnt="actBalance">
                            <td class="text-left">
                                <span> <?= date('d.m.Y H:i', $act['act_date']) ?> </span>
                            </td>
                            <?if($_GET['obj'] == 'all') { ?>
                                <td class="text-center"><?= $businessobj[$act['obj']] ?></td>
                            <? } ?>
                            <td class="text-center"><?= $act['act_number'] ?></td>
                            <td class="text-center"><?= ActBalanceModel::getNameTypeBalance($act['type_balance']) ?></td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            <? } else { ?>
                Нет актов
            <? } ?>

    </div>
