<?
global $ACTIONPAGE, $businessobj;

$forObj = empty($_GET['for']) ? null : $_GET['for'];
$obj = empty($_GET['obj']) ? '' : $_GET['obj'];

$tab = isset($_GET['tab']) ? $_GET['tab'] : 'draft';

?>

<h2>Акты списания</h2>


<div class="container-fluid">
    <? if(!empty($permit['canEditAct'])) { ?>
        <form action="<?= $ACTIONPAGE ?>">
            <div class="form-group form-inline">
                <? if($obj == 'all') { ?>
                    <div class="input-group<?= $obj == 'all' ? '' : ' hidden'?>">
                        <select class="form-control requiredValue" id="obj">
                            <option value="" selected class="hidden"> Объект </option>
                            <? foreach ($businessobj as $bObj => $bName) { ?>
                                <option value="<?= $bObj ?>"><?= $bName ?></option>
                            <? } ?>
                        </select>
                    </div>
                <? } else { ?>
                    <input type="hidden" name="obj" id="obj" value="<?= $obj ?>">
                <? } ?>
                <button type="button" id="addActBtn" class="btn btn-primary btn-sm canHide" data-cnt="actWritingOff">Создать акт</button>
                <span class="help-block hidden">Выберите объект</span>
            </div>
        </form>
    <? } ?>

    <ul class="nav nav-tabs" id="actStateTab">
        <li class="<?= $tab=='draft' ? 'active' : '' ?>"><a href="#tabDraft">Черновики</a></li>
        <li class="<?= $tab=='approved' ? 'active' : '' ?>"><a href="#tabApproved">Проведенные</a></li>
    </ul>

    <div class="tab-content">
        <div id="tabDraft" class="tab-pane fade <?= $tab=='draft' ? 'in active' : '' ?>">
            <? if(!empty($list['draft'])) { ?>

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="text-left">Дата акта</th>
                        <?if($_GET['obj'] == 'all') { ?>
                            <th>Объект</th>
                        <? } ?>
                        <th>Номер</th>
                        <th>Сумма</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($list['draft'] as $act) { ?>
                        <tr class="viewAct" data-id="<?= $act['id'] ?>" data-obj="<?= $act['obj'] ?>" data-cnt="actWritingOff">
                            <td class="text-left">
                                <span> <?= date('d.m.Y H:i', $act['act_date']) ?> </span>
                            </td>
                            <?if($_GET['obj'] == 'all') { ?>
                                <td class="text-center"><?= $businessobj[$act['obj']] ?></td>
                            <? } ?>
                            <td class="text-center"><?= $act['act_number'] ?></td>
                            <td class="text-center"><?= $act['total'] ?></td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            <? } else { ?>
                Нет актов
            <? } ?>
        </div>
        <div id="tabApproved" class="tab-pane fade <?= $tab=='approved' ? 'in active' : '' ?>">
        <? if(!empty($list['approved'])) { ?>

            <table class="table table-hover">
                <thead>
                <tr>
                    <th class="text-left">Дата акта</th>
                    <?if($_GET['obj'] == 'all') { ?>
                        <th>Объект</th>
                    <? } ?>
                    <th>Номер</th>
                    <th>Сумма</th>
                </tr>
                </thead>
                <tbody>
                <? foreach ($list['approved'] as $act) { ?>
                    <tr class="viewAct" data-id="<?= $act['id'] ?>" data-obj="<?= $act['obj'] ?>" data-cnt="actWritingOff">
                        <td class="text-left">
                            <span> <?= date('d.m.Y H:i', $act['act_date']) ?> </span>
                        </td>
                        <?if($_GET['obj'] == 'all') { ?>
                            <td class="text-center"><?= $businessobj[$act['obj']] ?></td>
                        <? } ?>
                        <td class="text-center"><?= $act['act_number'] ?></td>
                        <td class="text-center"><?= $act['total'] ?></td>
                    </tr>
                <? } ?>
                </tbody>
            </table>
        <? } else { ?>
            Нет актов
        <? } ?>
    </div>
    </div>


    </div>
