<?
global $ACTIONPAGE, $businessobj;

use Food\Core\View;
use \Food\App\Helpers\Units;

$packUnits = Units::getAll();

$ref_url = urlencode($_SERVER['REQUEST_URI']);
$ref_msg = urlencode('к приходной накладной');

// переменные передаются с контроллера, поэтому здесь они как неопределенные. Определим )))
$trader = isset($trader) ? $trader : array();
$order = isset($order) ? $order : array();
$act = isset($act) ? $act : array();
$rows = isset($rows) ? $rows : array();

$forObj = empty($_GET['for']) ? null : $_GET['for'];
$obj = empty($_GET['obj']) ? '' : $_GET['obj'];


//dd($act);
?>

<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= $ACTIONPAGE . '?obj=' . $obj . '&r=actWritingOff&tab=approved' ?>">Список актов списания</a></li>
        <li class="breadcrumb-item active">Просмотр акта</li>
    </ol>
</nav>

<h2 class="inline">Акт списания № <?= isset($act['act_number']) ? $act['act_number'] : 'без номера' ?></h2>

<? if($_GET['obj'] == 'all' && isset($businessobj[$act['obj']])) { ?>
    <h3 class="text-info"><?= $businessobj[$act['obj']] ?></h3>
<? } ?>


<? if(!empty($permit['canEditAct'])) { ?>
        <div class="text-nowrap well">
            <label class="margin-l20"> Акт списания №: </label>
            <span> <?= isset($act['act_number']) ? $act['act_number'] : '' ?> </span>

            <label class="margin-l20"> Дата создания: </label>
            <span> <?= isset($act['act_date']) ? date('d.m.Y', $act['act_date']) : '' ?>, <?= isset($act['act_time_period']) ? $act['act_time_period'] : '' ?> </span>

            <label class="margin-l20"> Дата списания: </label>
            <span> <?= isset($act['date_approve']) ? date('d.m.Y H:i', $act['date_approve']) : '' ?> </span>
        </div>

        <div class="form-group form-inline">
            <div class="input-group">
                <span class="input-group-addon"> Причина списания </span>
                <textarea id="reason" class="form-control bgC0" cols="80" rows="4" readonly><?= isset($act['reason']) ? $act['reason'] : '' ?></textarea>
            </div>
        </div>
        <div class="form-group form-inline">
            <div class="input-group">
                <span class="input-group-addon"> ФИО членов комиссии </span>
                <textarea id="commission" class="form-control bgC0" cols="80" rows="4" readonly><?= isset($act['commission']) ? $act['commission'] : '' ?></textarea>
            </div>
        </div>
    </form>
<? } ?>



<script>
    var units = <?= json_encode($packUnits) ?>;
    var packUnits = <?= json_encode(array_keys($packUnits)) ?>;
    var actObj = "<?= $act['obj'] ?>";
</script>

<table class="table table-hover">
    <thead>
    <tr>
        <th rowspan="2">&nbsp;</th>
        <th rowspan="2" style="width: 500px">Наименование</th>
        <th colspan="4" class="text-center"> На складе <span class="help-block" style="font-size:smaller; margin:0;"> текущее состояние </span> </th>
        <th rowspan="2" class="text-center l-border""> Списано <span class="help-block" style="font-size:smaller; margin:0;"> количество </span> </th>
        <th rowspan="2">&nbsp;</th>
    </tr>
    <tr>
        <th class="text-center">Ед.изм.</th>
        <th class="text-center">Цена <span class="help-block" style="font-size:smaller; margin:0;"> за ед.изм. </span> </th>
        <th class="text-center">Количество </th>
        <th class="text-center">Сумма </th>
    </tr>
    </thead>
    <tbody>
    <? if(!empty($rows)) { ?>
        <? foreach ($rows as $row) { ?>
            <tr>
                <td style="width: 30px"><i class="fa fa-ellipsis-v"></i></td>
                <td style="width:500px"><span class="prodName"><?= $row['prod_name'] ?></span></td>
                <td class="text-center packUnit"><?= $row['amount_unit'] ?></td>
                <td class="text-center inPrice"><?= sprintf("%.2f", $row['price']) ?></td>
                <td class="text-center inAmount"><?= $row['in_amount'] ?></td>
                <td class="text-center inTotalPrice"><?= sprintf("%.2f", $row['price'] * $row['in_amount']) ?></td>
                <td class="text-center l-border"><?= $row['amount'] ?></td>
                <td class="text-center return" style="width: 30px">&nbsp;</td>
            </tr>
        <? } ?>
    <? } ?>

    <tr id="newRow" class="active">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="4" class="h3 text-right"> Сумма списания: </td>
        <td class="text-center">&nbsp;<span class="h2 text-center"><?= sprintf("%.2f", $act['total']) ?></span></td>
        <td>&nbsp;</td>
    </tr>

    </tbody>
</table>
