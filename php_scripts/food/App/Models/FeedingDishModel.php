<?php

namespace Food\App\Models;

use Food\App\Helpers\Converter;
use Food\Core\Json;
use Food\Core\Model;
use Food\Core\View;

class FeedingDishModel extends Model
{

    protected $tableName = 'feeding_dish_outlet';

    public $dish_id;
    public $feeding_id;
    public $obj;
    public $calculation;
    public $working;
    public $unit;


    public function getAll()
    {
        if(empty($this->dish_id) || empty($this->obj)) {
            return array();
        }

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE dish_id = :dish_id AND obj = :obj";
        $params = array(
            'dish_id' => $this->dish_id,
            'obj' => $this->obj,
        );

        $feeding = $this->getDB()->query($sql, $params);
        $result = array();
        if(!empty($feeding) && is_array($feeding)) {
            foreach($feeding as $feed) {
                $feed['calculation'] = Converter::getAmountForView($feed['calculation'], $feed['unit']);
                $feed['working'] = Converter::getAmountForView($feed['working'], $feed['unit']);
                $result[$feed['feeding_id']] = $feed;
            }
        }

        return $result;
    }

    public function save()
    {

        if(empty($this->dish_id) || empty($this->obj) || empty($this->feeding_id)) {
            $this->setError("Ошибка добавления категории. Недостаточно данных");
            return false;
        }

        try {


            $sql = "SELECT * FROM " . $this->getTableName() . " WHERE dish_id = :dish_id AND obj = :obj AND feeding_id = :feeding_id";
            $params = array(
                'dish_id' => $this->dish_id,
                'obj' => $this->obj,
                'feeding_id' => $this->feeding_id,
            );
            $data = $this->getDB()->query($sql, $params);

            if (empty($data)) {
                // добавим запись
                $data = array(
                    'dish_id' => $this->dish_id,
                    'obj' => $this->obj,
                    'feeding_id' => $this->feeding_id,
                    'calculation' => Converter::getAmountForDb($this->calculation, $this->unit),
                    'working' => Converter::getAmountForDb($this->working, $this->unit),
                    'unit' => $this->unit,
                );
                $this->getDB()->insert($this->getTableName(), $data);
                $this->id = $this->getDB()->getLastId();
            } else {
                // обновим запись
                $data = array(
                    'calculation' => Converter::getAmountForDb($this->calculation, $this->unit),
                    'working' => Converter::getAmountForDb($this->working, $this->unit),
                );
                $this->getDB()->update($this->getTableName(), $data,
                    'dish_id = :dish_id AND obj = :obj AND feeding_id = :feeding_id',
                    array(
                        'dish_id' => $this->dish_id,
                        'obj' => $this->obj,
                        'feeding_id' => $this->feeding_id,
                ));
            }

        } catch (\PDOException $e) {
            $this->setError("Ошибка добавления категории");
            return false;
        }

        return true;
    }

}
