<?php

namespace Food\App\Models;

use Food\App\Helpers\Converter;
use Food\App\Helpers\Units;
use Food\Core\Json;
use Food\Core\Model;
use Food\Core\View;


class DishesModel extends Model
{
    protected $tableName = 'dishes';

    public $id;
    public $group_id;
    public $dish_name;
    public $objs;
    public $collection_recipe_id;
    public $recipe_number;
    public $creator_technological_map;
    public $standart_calculation_output;
    public $standart_working_output;
    public $output_unit;
    public $cooking;


    // данные блюда для All или обьекта ( ?obj=all ... / ?obj=<obj> ... )
    public function get($id)
    {
        // всегда выбираем общие данные по блюду (all)
        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE id=:id";
        $params = array('id' => $id);
        $dish = $this->getDB()->fetchRow($sql, $params);
        if(!empty($dish)) {
            $dish['standart_calculation_output'] = Converter::getAmountForView($dish['standart_calculation_output'], $dish['output_unit']);
            $dish['standart_working_output'] = Converter::getAmountForView($dish['standart_working_output'], $dish['output_unit']);
            $dish['objs'] = json_decode($dish['objs'], true);
        }

        // если нужны данные по конкретному обьекту
        if(!empty($this->obj) && $this->obj !== 'all') {
            $dishObj = $this->getForObj($id, $this->obj);
            $dish = $dishObj + $dish;
        }

        return $dish;
    }

    // Данные блюда по конкретному обьекту в контексте All ( ?obj=all& ... &for=<obj> )
    public function getForObj($id, $obj)
    {
        $sql = "SELECT * FROM " . $this->getTableName('dishes_obj') . " WHERE dish_id=:dish_id AND obj=:obj";
        $params = array('dish_id' => $id, 'obj' => $obj);
        $dish = $this->getDB()->fetchRow($sql, $params);

        if(!empty($dish)) {
            $dish['standart_calculation_output'] = Converter::getAmountForView($dish['standart_calculation_output'], $dish['output_unit']);
            $dish['standart_working_output'] = Converter::getAmountForView($dish['standart_working_output'], $dish['output_unit']);
        } else {
            $dish = array();
        }
        return $dish;
    }


    public function getAllGroups()
    {
        $groups = array();
        $sql = "SELECT * FROM " . $this->getTableName('dishes_group');
        $res = $this->getDB()->fetchAll($sql);
        foreach ($res as $row) {
            $groups[$row['id']] = $row;
        }
        return $groups;
    }

    public function saveGroup($name, $id = null)
    {
        if (empty($id)) {
            // добавим запись
            $this->getDB()->insert($this->getTableName('dishes_group'), array('name' => $name));
            $id = $this->getDB()->getLastId();
        } else {
            // обновим запись
            $this->getDB()->update($this->getTableName('dishes_group'), array('name' => $name), 'id = :id',  array('id' => $id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения группы");
            return false;
        }

        return $id;
    }

    public function getList($obj)
    {
        $dishes = array();
        $groups = $this->getAllGroups();
        $sql = "SELECT * FROM " . $this->getTableName() . " ORDER BY group_id";
        $res = $this->getDB()->fetchAll($sql);
        foreach ($res as $row) {
            $dishes[$row['group_id']]['name'] = $groups[$row['group_id']]['name'];
            $dishes[$row['group_id']]['dishes'][$row['id']] = $row;
        }
        return $dishes;
    }



    public function save()
    {
        $data = array(
            'group_id' => $this->group_id,
            'dish_name' => $this->dish_name,
            'objs' => $this->objs,
            'collection_recipe_id' => $this->collection_recipe_id,
            'recipe_number' => $this->recipe_number,
            'creator_technological_map' => $this->creator_technological_map,
            'standart_calculation_output' => $this->standart_calculation_output,
            'standart_working_output' => $this->standart_working_output,
            'output_unit' => $this->output_unit,
            'cooking' => $this->cooking,
        );

        if (empty($this->id)) {
            // добавим запись
            $this->getDB()->insert($this->getTableName(), $data);
            $this->id = $this->getDB()->getLastId();
        } else {
            // обновим запись
            $this->getDB()->update($this->getTableName(), $data, 'id = :id',  array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения блюда");
            return false;
        }

        return true;
    }

    public function saveForObj($obj)
    {
        $data = array(
            'dish_id' => $this->id,
            'obj' => $obj,
            'group_id' => $this->group_id,
            'dish_name' => $this->dish_name,
            'collection_recipe_id' => $this->collection_recipe_id,
            'recipe_number' => $this->recipe_number,
            'creator_technological_map' => $this->creator_technological_map,
            'standart_calculation_output' => $this->standart_calculation_output,
            'standart_working_output' => $this->standart_working_output,
            'output_unit' => $this->output_unit,
            'cooking' => $this->cooking,
        );

        $sql = "SELECT * FROM " . $this->getTableName('dishes_obj') . " WHERE dish_id=:dish_id AND obj=:obj";
        $params = array('dish_id' => $this->id, 'obj' => $obj);
        $dish = $this->getDB()->fetchRow($sql, $params);

        if (empty($dish)) {
            // добавим запись
            $this->getDB()->insert($this->getTableName('dishes_obj'), $data);
        } else {
            // обновим запись
            $params = array('dish_id' => $this->id, 'obj' => $obj);
            $conditions = 'dish_id=:dish_id AND obj=:obj';
            $this->getDB()->update($this->getTableName('dishes_obj'), $data, $conditions,  $params);
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения блюда");
            return false;
        }

        return true;
    }

    public function addGroup($name) {
        $this->getDB()->insert($this->getTableName('dishes_group'), array('name' => $name));
        return $this->getDB()->getLastId();
    }

//    public function delete()
//    {
//        if (empty($this->id)) {
//            $this->setError('Неизвестный документ');
//        } else {
//            $this->getDB()->delete($this->getTableName(),
//                'id = :id',
//                array(
//                    'id' => $this->id
//                ));
//        }
//
//        if($this->hasErrors()) {
//            $this->setError("Ошибка удаления продукта документа");
//            return false;
//        }
//
//        return true;
//    }

}
