<?php

namespace Food\App\Models;

use Food\Core\Json;
use Food\Core\Model;
use Food\Core\View;

class OrderModel extends Model
{
    const STATE_DRAFT   = 0;
    const STATE_WORK    = 1;
    const STATE_CLOSED  = 2;

    protected $tableName = 'order';

    public $id;
    public $trader_id;
    public $obj;
    public $state;
    public $date_open;
    public $date_shipment;
    public $delivery_time;
    public $date_send;
    public $date_close;
    public $total;



    public function get($id, $setParams = true)
    {
        if(empty($id)) {
            return null;
        }

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE id = :id";
        $params = array('id' => $id);
        $order = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($order) && is_array($order)) {
            $result = reset($order);
            if($setParams) {
                $this->setAttributes($result);
            }
            $result['delivery_time'] = json_decode($result['delivery_time'], true);
        }

        return $result;
    }

    public function getAll()
    {
        if($this->obj == 'all') {
            $orders = $this->getDB()->query("SELECT * FROM " . $this->getTableName());
        } else {
            $sql = "SELECT * FROM " . $this->getTableName('order') . " WHERE obj = :obj";
            $params = array('obj' => $this->obj);
            $orders = $this->getDB()->query($sql, $params);
        }
        if (!empty($orders) && is_array($orders)) {
            foreach ($orders as &$order) {
                $order['delivery_time'] = json_decode($order['delivery_time'], true);
            }
        }
        return $orders;
    }

    public function getAllForInvoice()
    {
        if($this->obj == 'all') {
            $orders = $this->getDB()->query("SELECT * FROM " . $this->getTableName() . " WHERE state = 1");
        } else {
            $sql = "SELECT * FROM " . $this->getTableName('order') . " WHERE obj = :obj AND state = 1";
            $params = array('obj' => $this->obj);
            $orders = $this->getDB()->query($sql, $params);
        }
        if (!empty($orders) && is_array($orders)) {
            foreach ($orders as &$order) {
                $order['delivery_time'] = json_decode($order['delivery_time'], true);
            }
        }
        return $orders;
    }

    public function getForTrader()
    {
        $sql = "SELECT * FROM " . $this->getTableName('order') . " WHERE obj = :obj AND trader_id = :trader_id AND state = 1";
        $params = array(
            'obj' => $this->obj,
            'trader_id' => $this->trader_id
        );
        $orders = $this->getDB()->query($sql, $params);

        return $orders;
    }


    public function getForSend($id)
    {
        if(empty($id)) {
            return array();
        }

        $sql = "SELECT IFNULL(tro.fio, tr.fio) as fio, IFNULL(tro.phone, tr.phone) as phone, IFNULL(tro.email, tr.email) as email, o.date_shipment, o.delivery_time, o.id
                    FROM " . $this->getTableName() . " AS o
                    INNER JOIN " . $this->getTableName('trader') . " AS tr ON tr.id = o.trader_id
                    LEFT JOIN " . $this->getTableName('trader_obj') . " AS tro ON tro.trader_id = o.trader_id
                    WHERE o.id = :id";
        $params = array('id' => $id);
        $order = $this->getDB()->fetchRow($sql, $params);
        return $order;
    }



    public function save()
    {

        if (empty($this->id)) {
            // добавим запись
            $this->getDB()->insert($this->getTableName(), array(
                'trader_id' => $this->trader_id,
                'obj' => $this->obj,
                'state' => $this->state,
                'date_open' => $this->date_open,
            ));
            $this->id = $this->getDB()->getLastId();
        } else {
            // обновим запись
            $this->getDB()->update($this->getTableName(),
                array(
                    'trader_id' => $this->trader_id,
                    'obj' => $this->obj,
                    'state' => $this->state,
                    'date_open' => $this->date_open,
                    'date_shipment' => $this->date_shipment,
                    'delivery_time' => $this->delivery_time,
                    'date_send' => $this->date_send,
                    'date_close' => $this->date_close,
                    'total' => $this->total,
                ),
                'id = :id',
                array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения заявки");
            return false;
        }

        return true;
    }

    public function delete()
    {
        return false;

        if(empty($this->id) || empty($this->obj)) {
            $this->setError("Field 'id' or 'obj' is empty");
            return false;
        }

        $this->getDB()->update($this->getTableName(),
            array(
                'deleted' => 1
            ),
            'id = :id',
            array('id' => $this->id));

        $this->getDB()->update($this->getTableName('prod_type'),
            array(
                'deleted' => 1
            ),
            'group_id = :id',
            array('id' => $this->id));

        $this->getDB()->update($this->getTableName('prod_obj'),
            array(
                'tmark_deleted' => 1,
                'pack_deleted' => 1
            ),
            'obj = :obj AND group_id = :id',
            array('obj' => $this->obj, 'id' => $this->id));

        if($this->hasErrors()) {
            $this->setError("Ошибка удаления группы");
            return false;
        }

        return true;
    }

    public function close($id)
    {
        $this->getDB()->update($this->getTableName(),
            array(
                'state' => self::STATE_CLOSED,
                'date_close' => time()
            ),
            'id = :id',
            array('id' => $id));
    }
}
