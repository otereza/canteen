<?php

namespace Food\App\Models;

use Food\Core\Model;

class TraderPriceModel extends Model
{

    protected $tableName = 'trader_price';

    public $trader_id;
    public $obj;
    public $tmark_id;
    public $pack_id;
    public $price;
    public $smallest_unit_price;
    public $active;

    static private $statPrices;



    public function get($trader_id)
    {
        if(empty($this->obj)) {
            return null;
        }

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE trader_id = :id AND obj = :obj AND active = 1";
        $params = array('id' => $trader_id, 'obj' => $this->obj);
        $prices = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($prices) && is_array($prices)) {
            foreach ($prices as $price) {
                $result[$price['pack_id']] = $price;
            }
        }

        return $result;
    }

    public function getAll($trader_id)
    {
        $sql = "SELECT tpa.trader_id, tpa.tmark_id, tpa.pack_id, IFNULL(tpo.price, tpa.price) as price, tpa.price as main_price, tpo.active as `active`, tpa.active as main_active,
                        IFNULL((SELECT price FROM " . $this->getTableName() . " WHERE trader_id <> tpa.trader_id AND pack_id = poa.pack_id ORDER BY price ASC LIMIT 1), 0) AS min_other_trader,
                        ROUND(IFNULL((SELECT (smallest_unit_price * pp.smallest_unit_value) FROM " . $this->getTableName() . " WHERE trader_id = tpa.trader_id AND obj = po.obj AND tmark_id = po.tmark_id ORDER BY smallest_unit_price ASC LIMIT 1), 0), 2) AS min_other_pack,
                        ROUND(IFNULL((SELECT (tp2.smallest_unit_price * pp.smallest_unit_value) FROM " . $this->getTableName() . " AS tp2 INNER JOIN " . $this->getTableName('prod_obj') . " AS po2 ON po2.pack_id = tp2.pack_id WHERE tp2.trader_id = tpa.trader_id AND tp2.obj = tpa.obj AND po2.type_id = po.type_id ORDER BY smallest_unit_price ASC LIMIT 1), 0), 2) AS min_this_type,
                        IFNULL((SELECT price FROM " . $this->getTableName() . " WHERE trader_id = tpa.trader_id AND obj <> 'all' AND pack_id = poa.pack_id AND price IS NOT NULL ORDER BY price ASC LIMIT 1), 0) AS min_trader,
                        IFNULL((SELECT obj FROM " . $this->getTableName() . " WHERE trader_id = tpa.trader_id AND obj <> 'all' AND pack_id = poa.pack_id AND price IS NOT NULL ORDER BY price ASC LIMIT 1), '-') AS min_trader_obj,
                        IFNULL((SELECT price FROM " . $this->getTableName() . " WHERE trader_id = tpa.trader_id AND obj <> 'all' AND pack_id = poa.pack_id AND price IS NOT NULL ORDER BY price DESC LIMIT 1), 0) AS max_trader,
                        IFNULL((SELECT obj FROM " . $this->getTableName() . " WHERE trader_id = tpa.trader_id AND obj <> 'all' AND pack_id = poa.pack_id AND price IS NOT NULL ORDER BY price DESC LIMIT 1), '-') AS max_trader_obj

                FROM " . $this->getTableName() . " AS tpa
                LEFT JOIN " . $this->getTableName() . " AS tpo ON tpo.pack_id = tpa.pack_id AND tpo.trader_id = tpa.trader_id AND tpo.obj = :obj
                LEFT JOIN " . $this->getTableName('prod_obj') . " as po ON po.pack_id = tpa.pack_id AND po.obj = tpa.obj
                INNER JOIN " . $this->getTableName('prod_obj') . " as poa ON poa.pack_id = tpa.pack_id AND poa.obj = 'all'
                INNER JOIN " . $this->getTableName('prod_pack') . " as pp ON pp.id = tpa.pack_id
                WHERE tpa.obj = 'all' AND tpa.trader_id = :trader_id";

        $params = array('trader_id' => $trader_id, 'obj' => $this->obj);
        $prices = $this->getDB()->query($sql, $params);
//dd($prices);
        return $prices;
    }


    public function getAllWithMain()
    {
        $sql = "SELECT po.pack_id, IFNULL(tpo.price, tpa.price) as trader_price, IFNULL(tpa.price, tpo.price) as trader_price_main
                    FROM " . $this->getTableName('prod_obj') . " AS po
                    LEFT JOIN " . $this->getTableName() . " AS tpo ON tpo.trader_id = :obj_trader_id AND tpo.obj = :obj AND tpo.pack_id = po.pack_id
                    LEFT JOIN " . $this->getTableName() . " AS tpa ON tpa.trader_id = :all_trader_id AND tpa.obj = 'all' AND tpa.pack_id = po.pack_id
                    WHERE po.obj = 'all' AND po.pack_id IS NOT NULL
                    HAVING trader_price IS NOT NULL;";
        $params = array(
            'obj_trader_id' => $this->trader_id,
            'all_trader_id' => $this->trader_id,
            'obj' => $this->obj);
        $prices = $this->getDB()->query($sql, $params);
        $res = array();
        foreach ($prices as &$price) {
            if(!empty($price['trader_price']) || !empty($price['trader_price_main'])) {
                $res[$price['pack_id']] = $price;
            }
        }

        return $res;
    }


    public function save($changePriceForActive = true)
    {
        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE trader_id = :trader_id AND obj = :obj AND tmark_id = :tmark_id AND pack_id = :pack_id";
        $params = array(
            'trader_id' => $this->trader_id,
            'obj' => $this->obj,
            'tmark_id' => $this->tmark_id,
            'pack_id' => $this->pack_id
        );
        $isPresent = $this->getDB()->fetchRow($sql, $params);

        if($this->obj !== 'all' && $isPresent['active'] == 1 && !$changePriceForActive) {
            return true;
        }

        if (empty($isPresent)) {
            // добавим запись
            $params = array(
                'trader_id' => $this->trader_id,
                'obj' => $this->obj,
                'tmark_id' => $this->tmark_id,
                'pack_id' => $this->pack_id,
                'price' => $this->price,
                'smallest_unit_price' => $this->smallest_unit_price,
            );
            $this->getDB()->insert($this->getTableName(), $params);
        } else {
            // обновим запись
            $params = array(
                'price' => $this->price,
                'smallest_unit_price' => $this->smallest_unit_price,
            );
            if(!is_null($this->active)) {
                $params['active'] = $this->active;
            }
            $this->getDB()->update($this->getTableName(), $params,
                'trader_id = :trader_id AND obj = :obj AND tmark_id = :tmark_id AND pack_id = :pack_id',
                array(
                    'trader_id' => $this->trader_id,
                    'obj' => $this->obj,
                    'tmark_id' => $this->tmark_id,
                    'pack_id' => $this->pack_id,
                ));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения цены");
            return false;
        }

        return true;
    }

    public function saveMainPriceForObj($trader_id, $obj)
    {
        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE trader_id = :id AND obj = :obj AND active = 1";
        $params = array('id' => $trader_id, 'obj' => 'all');
        $prices = $this->getDB()->query($sql, $params);

        if(!empty($prices) && is_array($prices)) {
            $values = array();
            foreach ($prices as $key => $price) {
                $values[] = sprintf('(%d,"%s",%d,%d,"%s","%s", 0)', $price['trader_id'], $obj, $price['tmark_id'], $price['pack_id'], $price['price'], $price['smallest_unit_price']);
            }

            $sql = "INSERT IGNORE INTO " . $this->getTableName() . " (trader_id, obj, tmark_id, pack_id, price, smallest_unit_price, active) 
                        VALUES" . implode(',', $values);
            $this->getDB()->execute($sql);
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения цены");
            return false;
        }

        return true;
    }

    public function deletePriceForObj($trader_id, $obj)
    {
        $this->getDB()->delete($this->getTableName(), 'trader_id = :trader_id AND obj = :obj', array('trader_id' => $trader_id,'obj'=>$obj));

        if($this->hasErrors()) {
            $this->setError("Ошибка удаления цен");
            return false;
        }

        return true;
    }

    public function delete()
    {
        return false;

        if(empty($this->id) || empty($this->obj)) {
            $this->setError("Field 'id' or 'obj' is empty");
            return false;
        }

        $this->getDB()->update($this->getTableName(),
            array(
                'deleted' => 1
            ),
            'id = :id',
            array('id' => $this->id));

        $this->getDB()->update($this->getTableName('prod_type'),
            array(
                'deleted' => 1
            ),
            'group_id = :id',
            array('id' => $this->id));

        $this->getDB()->update($this->getTableName('prod_obj'),
            array(
                'tmark_deleted' => 1,
                'pack_deleted' => 1
            ),
            'obj = :obj AND group_id = :id',
            array('obj' => $this->obj, 'id' => $this->id));

        if($this->hasErrors()) {
            $this->setError("Ошибка удаления группы");
            return false;
        }

        return true;
    }

    public function changePrices($data)
    {
        foreach ($data as $row) {
            $this->getDB()->update($this->getTableName(),
                array(
                    'price'     => $row['price'],
                    'smallest_unit_price' => $row['smallest_unit_price']
                ),
                'trader_id = :trader_id AND obj = :obj AND pack_id = :pack_id',
                array(
                    'trader_id'   => $this->trader_id,
                    'obj'   => $this->obj,
                    'pack_id'   => $row['pack_id']
                )
            );
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка удаления группы");
            return false;
        }

        return true;
    }

}
