<?php

namespace Food\App\Models;

use Food\Core\Model;

class ProdObjectModel extends Model
{

    protected $tableName = 'prod_obj';

    public $id;
    public $obj;
    public $group_id;
    public $type_id;
    public $tmark_id;
    public $tmark_pos;
    public $tmark_deleted = 0;
    public $pack_id;
    public $pack_pos;
    public $pack_deleted = 0;



    /*
     * Получить продукт по ID
     */
//    public function getProduct($id)
//    {
//        if(empty($id)) {
//            return null;
//        }
//
//        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE id = :id";
//        $params = array('id' => $id);
//
//        $product = $this->getDB()->query($sql, $params);
//
//        $result = array();
//        if(!empty($product) && is_array($product)) {
//            $result = reset($product);
//            $this->id = $result['id'];
//            $this->obj = $result['obj'];
//            $this->group_id = $result['group_id'];
//            $this->group_pos = $result['group_pos'];
//            $this->type_id = $result['type_id'];
//            $this->type_pos = $result['type_pos'];
//            $this->tmark_id = $result['tmark_id'];
//            $this->tmark_pos = $result['tmark_pos'];
//            $this->pack_id = $result['pack_id'];
//            $this->pack_pos = $result['pack_pos'];
//            $this->deleted = $result['deleted'];
//        }
//
//        return $result;
//    }

    public function getAllProductsForObj($obj)
    {
        $sql = "SELECT p_obj.id, p_obj.obj,"
            . " p_obj.group_id, gr.group_pos,"
            . " p_obj.type_id, tp.type_pos,"
            . " p_obj.tmark_id, p_obj.tmark_pos, p_obj.tmark_deleted,"
            . " p_obj.pack_id, p_obj.pack_pos, p_obj.pack_deleted"
            . " FROM " . $this->getTableName() . " AS p_obj "
            . " INNER JOIN " . $this->getTableName('prod_group') . " AS gr  ON  gr.id = p_obj.group_id"
            . " INNER JOIN " . $this->getTableName('prod_type') .  " AS tp  ON  tp.id = p_obj.type_id"
            . " LEFT JOIN " . $this->getTableName('prod_tmark') . " AS tm  ON  tm.id = p_obj.tmark_id"
            . " LEFT JOIN " . $this->getTableName('prod_pack') .  " AS pk  ON  pk.id = p_obj.pack_id"
            . " WHERE p_obj.obj = :obj"
            . " ORDER BY gr.group_pos ASC, tp.type_pos ASC, p_obj.tmark_pos ASC, p_obj.pack_pos ASC";
        $params = array('obj' => $obj);

        $products = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($products) && is_array($products)) {
            foreach($products as $product) {
                $result[$product['id']] = $product;
            }
        }

        return $result;
    }

    // Добавление продукта в список продуктов обьекта (не для all)
    // Для all, продукты добаляются в разделе модуля "Продукты" на странице сайта)
    public function addProductForObj($pack_id, $obj)
    {
        $res = null;

        // Проверим, есть ли запись продукта в таблице
        $sql = "SELECT id FROM d_food_prod_obj WHERE obj = :obj AND pack_id = :pack_id";
        $params = array('obj' => $obj, 'pack_id' => $pack_id);
        $objProd = $this->getDB()->fetchColumn($sql, $params);

        if(empty($objProd['id'])) {
            // получим id группы, вида и торговой марки продукта для обьекта all
            $sql = "SELECT pg.id AS group_id, po.group_id AS main_group_id, pt.id AS type_id, po.type_id AS main_type_id, po.tmark_id, po.pack_id
                        FROM d_food_prod_obj AS po
                        LEFT JOIN d_food_prod_group AS pg ON pg.obj= \"" . $obj . "\" AND pg.main_id = po.group_id
                        LEFT JOIN d_food_prod_type AS pt ON pt.obj= \"" . $obj . "\" AND pt.main_id = po.type_id
                        WHERE po.obj = 'all' AND po.pack_id = :pack_id";
            $params = array('pack_id' => $pack_id);
            $objAllProd = $this->getDB()->fetchRow($sql, $params);

            $this->group_id = $objAllProd['group_id'];
            if (empty($objAllProd['group_id'])) {
                $groupModel = new ProdGroupModel();
                $groupModel->get($objAllProd['main_group_id']);

                $groupModel->id = null;
                $groupModel->group_pos = null;
                $groupModel->obj = $obj;
                $groupModel->main_id = $objAllProd['main_group_id'];
                if ($groupModel->save()) {
                    $this->group_id = $groupModel->id;
                } else {
                    $this->setError('Ошибка записи Группы продукта для обьекта ' . $obj);
                }
            }

            $this->type_id = $objAllProd['type_id'];
            if (empty($objAllProd['type_id']) && $this->group_id) {
                $typeModel = new ProdTypeModel();
                $typeModel->get($objAllProd['main_type_id']);

                $typeModel->id = null;
                $typeModel->type_pos = null;
                $typeModel->obj = $obj;
                $typeModel->group_id = $this->group_id;
                $typeModel->main_id = $objAllProd['main_type_id'];
                if ($typeModel->save()) {
                    $this->type_id = $typeModel->id;
                } else {
                    $this->setError('Ошибка записи Вида продукта для обьекта ' . $obj);
                }
            }

            if (!empty($this->group_id) && !empty($this->type_id)) {
                $this->obj = $obj;
                $this->tmark_id = $objAllProd['tmark_id'];
                $tmarkPosition = $this->getTmarkPosition() || $this->getLastTmarkPosition() + 1;
                $packPosition = $this->getLastPackPosition() + 1;
                $res = $this->getDB()->insert($this->getTableName(), array(
                    'obj' => $this->obj,
                    'group_id' => $this->group_id,
                    'type_id' => $this->type_id,
                    'tmark_id' => $this->tmark_id,
                    'tmark_pos' => $tmarkPosition,
                    'pack_id' => $pack_id,
                    'pack_pos' => $packPosition
                ));
                if ($this->getDB()->hasError()) {
                    $this->setError('Ошибка добавления продукта в список продуктов обьекта ' . $obj);
                }
            }

            if ($this->hasErrors()) {
                return false;
            }
        }
        return true;
    }

    public function getTmarkPosition()
    {
        $sql = "SELECT tmark_pos FROM " . $this->getTableName() . " WHERE obj=:obj AND group_id=:group_id AND type_id=:type_id AND tmark_id=:tmark_id LIMIT 1";
        $res = $this->getDB()->fetchColumn($sql, array(
            'obj' => $this->obj,
            'group_id' => $this->group_id,
            'type_id' => $this->type_id,
            'tmark_id' => $this->tmark_id
        ));
        return empty($res) ? 0 : $res;
    }

    public function getLastTmarkPosition()
    {
        $sql = "SELECT MAX(tmark_pos) FROM " . $this->getTableName() . " WHERE obj=:obj AND group_id=:group_id AND type_id=:type_id";
        $res = $this->getDB()->fetchColumn($sql, array(
            'obj' => $this->obj,
            'group_id' => $this->group_id,
            'type_id' => $this->type_id
        ));
        return empty($res) ? 0 : $res;
    }

    public function getLastPackPosition()
    {
        $sql = "SELECT MAX(pack_pos) FROM " . $this->getTableName() . " WHERE obj=:obj AND group_id=:group_id AND type_id=:type_id AND tmark_id=:tmark_id";
        $res = $this->getDB()->fetchColumn($sql, array(
            'obj' => $this->obj,
            'group_id' => $this->group_id,
            'type_id' => $this->type_id,
            'tmark_id' => $this->tmark_id
        ));
        return empty($res) ? 0 : $res;
    }

}
