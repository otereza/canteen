<?php

namespace Food\App\Models;

use Food\App\Helpers\Converter;
use Food\Core\Model;

class InvoiceReturnModel extends Model
{

    protected $tableName = 'invoice_return';

    public $id;
    public $trader_id;
    public $inv_receipt_id;
    public $obj;
    public $inv_number;
    public $inv_date;
    public $inv_time_period;
    public $total;
    public $reason;
    public $date_approve;
    public $status;

    const STATUS_OPEN = 0;
    const STATUS_APROVED = 1;



    public function get($id, $setParams = true)
    {
        if(empty($id)) {
            return null;
        }

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE id = :id";
        $params = array('id' => $id);
        $order = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($order) && is_array($order)) {
            $result = reset($order);
            if($setParams) {
                $this->setAttributes($result);
            }
        }

        return $result;
    }

    public function getAll()
    {
        if($this->obj == 'all') {
            $orders = $this->getDB()->query("SELECT * FROM " . $this->getTableName());
        } else {
            $sql = "SELECT * FROM " . $this->getTableName() . " WHERE obj = :obj";
            $params = array('obj' => $this->obj);
            $orders = $this->getDB()->query($sql, $params);
        }
        return $orders;
    }

    public function getForSend($id)
    {
        if(empty($id)) {
            return array();
        }

        $sql = "SELECT IFNULL(tro.fio, tr.fio) as fio, IFNULL(tro.phone, tr.phone) as phone, IFNULL(tro.email, tr.email) as email, rec.inv_number as receipt_number, rec.inv_date as receipt_date, rec.inv_time_period as receipt_time, ret.inv_number as return_number, ret.inv_date as return_date, ret.inv_time_period as return_time
                    FROM " . $this->getTableName() . " AS ret
                    INNER JOIN " . $this->getTableName('invoice_receipt') . " AS rec ON rec.id = ret.inv_receipt_id
                    INNER JOIN " . $this->getTableName('trader') . " AS tr ON tr.id = ret.trader_id
                    LEFT JOIN " . $this->getTableName('trader_obj') . " AS tro ON tro.trader_id = ret.trader_id
                    WHERE ret.id = :id";
        $params = array('id' => $id);
        $receipts = $this->getDB()->fetchRow($sql, $params);
        return $receipts;
    }



    public function save()
    {

        $data = array(
            'trader_id' => $this->trader_id,
            'inv_receipt_id' => $this->inv_receipt_id,
            'obj' => $this->obj,
            'inv_number' => $this->inv_number,
            'inv_date' => $this->inv_date,
            'inv_time_period' => $this->inv_time_period,
            'total' => $this->total,
            'reason' => $this->reason,
        );

        if (empty($this->id)) {
            // добавим запись
            $this->getDB()->insert($this->getTableName(), $data);
            $this->id = $this->getDB()->getLastId();
            if(empty($this->inv_number)) {
                $this->getDB()->update($this->getTableName(),
                    array(
                        'inv_number' => DocProdModel::PREFIX_INVOICE_RETURN . '-' . $this->id,
                    ),
                    'id = :id',
                    array('id' => $this->id));
            }
        } else {
            // обновим запись
            $this->getDB()->update($this->getTableName(),
                $data,
                'id = :id',
                array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения накладной");
            return false;
        }

        return true;
    }


    public function approve()
    {
        if (empty($this->id)) {
            return false;
        } else {

            $this->getDB()->update($this->getTableName(),
                array(
                    'date_approve' => time(),
                    'status' => self::STATUS_APROVED,
                ),
                'id = :id',
                array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка проводки накладной");
            return false;
        }

        return true;
    }

    public function getReturnProducts()
    {
        $sql = "SELECT prod_name, pack_unit, price, unit, in_amount, in_amount_unit, amount, amount_unit
                    FROM " . $this->getTableName('doc_prod') . "
                    WHERE doc_type = :docType AND doc_id = :docId AND amount IS NOT NULL
                    ORDER BY pos;";
        $params = array(
            'docType' => DocProdModel::DOC_TYPE_INVOICE_RETURN,
            'docId' => $this->id,
        );
        $prods = $this->getDB()->fetchAll($sql, $params);
        foreach ($prods as &$row) {
            $row['price'] = Converter::getPriceForView($row['price'], $row['pack_unit'], $row['in_amount_unit']);
            $row['in_amount'] = Converter::getAmountForView($row['in_amount'], $row['in_amount_unit']);
            $row['amount'] = Converter::getAmountForView($row['amount'], $row['amount_unit']);
        }

        return $prods;

    }

}
