<?php

namespace Food\App\Models;

use Food\Core\Json;
use Food\Core\Model;
use Food\Core\View;

class ProdGroupModel extends Model
{

    protected $tableName = 'prod_group';

    public $id;
    public $obj;
    public $main_id;
    public $group_pos;
    public $name;
    public $deleted;



    public function get($id, $setParams = true)
    {
        if(empty($id)) {
            return null;
        }

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE id = :id";
        $params = array('id' => $id);

        $groups = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($groups) && is_array($groups)) {
            $result = reset($groups);
            if($setParams) {
                $this->id = $result['id'];
                $this->obj = $result['obj'];
                $this->name = $result['name'];
                $this->group_pos = $result['group_pos'];
                $this->deleted = $result['deleted'];
            }
        }

        return $result;
    }

    public function getAll($obj)
    {
        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE obj = :obj ORDER BY group_pos ASC";

        $params = array('obj' => $obj);

        $groups = $this->getDB()->query($sql, $params);
        $result = array();
        if(!empty($groups) && is_array($groups)) {
            foreach($groups as $group) {
                $result[$group['id']] = $group;
            }
        }

        return $result;
    }

    public function save()
    {

        if(!empty($this->name)) {

            if (empty($this->id)) {
                // добавим запись
                $this->getDB()->insert($this->getTableName(), array(
                    'main_id' => $this->main_id,
                    'obj' => $this->obj,
                    'group_pos' => $this->getLastPosition() + 1,
                    'name' => $this->name,
                ));
                $this->id = $this->getDB()->getLastId();
            } else {
                // обновим запись
                $curData = $this->get($this->id, false);
                if($this->group_pos == null) {
                    $this->group_pos = $curData['group_pos'];
                } elseif ($this->group_pos != $curData['group_pos']) {
                    // перемещаем позиции, если старая и новая позиции отличаются
                    // перемещаем на бОльшую позицию, смещаем все позиции от старой до новой позиции на единицу вверх и, на освободившееся место, вставляем новую позицию
                    if($this->group_pos > $curData['group_pos']) {
                        $sql = "UPDATE " . $this->getTableName() . " SET group_pos = group_pos - 1 WHERE obj = :obj AND group_pos > :old_group_pos AND group_pos <= :new_group_pos";
                    } else {
                        $sql = "UPDATE " . $this->getTableName() . " SET group_pos = group_pos + 1 WHERE obj = :obj AND group_pos >= :new_group_pos AND group_pos < :old_group_pos";
                    }
                    $params = array(
                        'obj' => $this->obj,
                        'old_group_pos' => $curData['group_pos'],
                        'new_group_pos' => $this->group_pos
                    );
                    $this->getDB()->execute($sql, $params);
                }

                $this->getDB()->update($this->getTableName(),
                    array(
                        'group_pos' => $this->group_pos,
                        'name' => $this->name,
                    ),
                    'id = :id',
                    array('id' => $this->id));
            }

            return true;
        } else {
            return false;
        }
    }

    public function delete()
    {
        if(empty($this->id) || empty($this->obj)) {
            $this->setError("Field 'id' or 'obj' is empty");
            return false;
        }

        try {
            $this->getDB()->begin();

            $this->getDB()->update($this->getTableName(),
                array(
                    'deleted' => 1
                ),
                'id = :id',
                array('id' => $this->id));

            $this->getDB()->update($this->getTableName('prod_type'),
                array(
                    'deleted' => 1
                ),
                'group_id = :id',
                array('id' => $this->id));

            $this->getDB()->update($this->getTableName('prod_obj'),
                array(
                    'tmark_deleted' => 1,
                    'pack_deleted' => 1
                ),
                'obj = :obj AND group_id = :id',
                array('obj' => $this->obj, 'id' => $this->id));

            $this->getDB()->commit();

        } catch (\PDOException $e) {
            $this->getDB()->rollBack();
            $this->setError("Ошибка удаления группы");
            return false;
        }

        return true;
    }

    public function undelete()
    {
        if(empty($this->id) || empty($this->obj)) {
            $this->setError("Field 'id' or 'obj' is empty");
            return false;
        }

        try {
            $this->getDB()->begin();

            $this->getDB()->update($this->getTableName(),
            array(
                'deleted' => 0
            ),
            'id = :id',
            array('id' => $this->id));

        $this->getDB()->update($this->getTableName('prod_type'),
            array(
                'deleted' => 0
            ),
            'group_id = :id',
            array('id' => $this->id));

        $this->getDB()->update($this->getTableName('prod_obj'),
            array(
                'tmark_deleted' => 0,
                'pack_deleted' => 0
            ),
            'obj = :obj AND group_id = :id',
            array('obj' => $this->obj, 'id' => $this->id));

            $this->getDB()->commit();

        } catch (\PDOException $e) {
            $this->getDB()->rollBack();
            $this->setError("Ошибка восстановления группы");
            return false;
        }

        return true;
    }

    public function changePos()
    {
        try {
            $this->getDB()->begin();

            $oldPos = $this->getPosition();
            if ($this->group_pos > $oldPos) {
                $sql = "UPDATE " . $this->getTableName() . " SET group_pos = group_pos - 1 WHERE obj = :obj AND group_pos > :old_group_pos AND group_pos <= :new_group_pos";
            } else {
                $sql = "UPDATE " . $this->getTableName() . " SET group_pos = group_pos + 1 WHERE obj = :obj AND group_pos >= :new_group_pos AND group_pos < :old_group_pos";
            }
            $params = array(
                'obj' => $this->obj,
                'old_group_pos' => $oldPos,
                'new_group_pos' => $this->group_pos
            );
            $this->getDB()->execute($sql, $params);

            $this->getDB()->update($this->getTableName(),
                array(
                    'group_pos' => $this->group_pos
                ),
                'id = :id AND obj = :obj',
                array(
                    'obj' => $this->obj,
                    'id' => $this->id
                )
            );
            $this->getDB()->commit();

        } catch (\PDOException $e) {
            $this->getDB()->rollBack();
            $this->setError("Ошибка перемещения группы");
            return false;
        }
        return true;
    }

    public function getPosition()
    {
        $sql = "SELECT group_pos FROM " . $this->getTableName() . " WHERE obj=:obj AND id=:id";
        $res = $this->getDB()->fetchColumn($sql, array(
            'obj' => $this->obj,
            'id' => $this->id
        ));
        return isset($res) ? $res : null;
    }

    public function getLastPosition()
    {
        $sql = "SELECT MAX(group_pos) as pos FROM " . $this->getTableName() . " WHERE obj=:obj";
        $res = $this->getDB()->fetchColumn($sql, array(
            'obj' => $this->obj,
        ));
        return empty($res) ? 0 : $res;
    }

//    private function setPosition()
//    {
//        //TODO: нужно делать. Не работает.
//        return;
//        $this->group_pos = $this->getPosition();
//
//        if (!empty($this->group_pos)) {
//            $this->getDB()->update($this->getTableName('position_group'), array(
//                'obj' => $_GET['obj'],
//                'group_id' => $this->id,
//                'position' => $this->group_pos,
//            ));
//        } else {
//            $positions = $this->getLastPosition();
//            $this->group_pos = $positions + 1;
//
//            $this->getDB()->insert($this->getTableName('position_group'), array(
//                'obj' => $_GET['obj'],
//                'group_id' => $this->id,
//                'position' => $this->group_pos,
//            ));
//        }
//    }
}
