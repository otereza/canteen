<?php

namespace Food\App\Models;

use Food\App\Helpers\Converter;
use Food\Core\Model;

class InvoiceTransferModel extends Model
{

    protected $tableName = 'invoice_transfer';

    public $id;
    public $obj_from;
    public $obj_to;
    public $inv_number;
    public $inv_date;
    public $inv_time_period;
    public $reason;
    public $total;
    public $date_approve;
    public $status;

    const STATUS_DRAFT = 0;
    const STATUS_NEED_APPROVE = 1;
    const STATUS_APROVED = 2;



    public function get($id, $setParams = true)
    {
        if(empty($id)) {
            return null;
        }

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE id = :id";
        $params = array('id' => intval($id));
        $invoice = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($invoice) && is_array($invoice)) {
            $result = reset($invoice);
            if($setParams) {
                $this->setAttributes($result);
            }
        }

        return $result;
    }

    public function getAll($obj)
    {
        if($obj == 'all') {
            $invoices = $this->getDB()->query("SELECT * FROM " . $this->getTableName());
        } else {
            $sql = "SELECT * FROM " . $this->getTableName() . " WHERE obj_from = :obj_from OR (obj_to = :obj_to AND status <> :status)";
            $params = array('obj_from' => $obj, 'obj_to' => $obj, 'status' => self::STATUS_DRAFT);
            $invoices = $this->getDB()->query($sql, $params);
        }
        return $invoices;
    }

    public function save()
    {
        $data = array(
            'obj_from' => $this->obj_from,
            'obj_to' => $this->obj_to,
            'inv_number' => $this->inv_number,
            'inv_date' => $this->inv_date,
            'inv_time_period' => $this->inv_time_period,
            'reason' => $this->reason,
            'total' => $this->total,
        );

        if (empty($this->id)) {
            // добавим запись
            $this->getDB()->insert($this->getTableName(), $data);
            $this->id = $this->getDB()->getLastId();
            if(empty($this->inv_number)) {
                $this->getDB()->update($this->getTableName(),
                    array(
                        'inv_number' => DocProdModel::PREFIX_INVOICE_TRANSFER . '-' . $this->id,
                    ),
                    'id = :id',
                    array('id' => $this->id));
            }
        } else {
            // обновим запись
            $this->getDB()->update($this->getTableName(), $data,
                'id = :id',
                array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения накладной");
            return false;
        }

        return true;
    }

    public function approve($statusApprove)
    {
        if (empty($this->id)) {
            return false;
        } else {

            $this->getDB()->update($this->getTableName(),
                array(
                    'date_approve' => time(),
                    'status' => $statusApprove,
                ),
                'id = :id',
                array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка проводки накладной");
            return false;
        }

        return true;
    }


}
