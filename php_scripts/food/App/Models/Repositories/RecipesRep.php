<?php


namespace Food\App\Models\Repositories;


use Food\Core\Model;

class RecipesRep extends Model
{
    protected $tableName = 'collect_recipes';

    public function getCollect()
    {
        $recipes = array();
        $sql = "SELECT * FROM " . $this->getTableName();
        $res = $this->getDB()->fetchAll($sql);
        foreach ($res as $row) {
            $recipes[$row['id']] = $row;
        }
        return $recipes;

    }

    public function save($name) {
        $this->getDB()->insert($this->getTableName(), array('name_n_year' => $name));
        return $this->getDB()->getLastId();
    }

}