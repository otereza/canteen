<?php

namespace Food\App\Models\Repositories;

use Food\App\Helpers\Converter;
use Food\Core\Model;

class TechMapProdRep extends Model
{

    const TECH_MAP_TYPE_CALC = 1;
    const TECH_MAP_TYPE_WORK = 2;

    protected $tableName = 'technological_map_prods';

    public $id;
    public $obj;
    public $dish_id;
    public $map_type;
    public $pos;
    public $prod_name;
    public $unit;
    public $brutto;
    public $netto;


    public function getProds($dishId, $obj)
    {

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE dish_id = :dish_id AND obj = :obj ORDER BY map_type, pos";
        $params = array(
            'dish_id' => $dishId,
            'obj' => $obj
        );
        $prods = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($prods) && is_array($prods)) {
            foreach($prods as $prod) {
                $prod['brutto'] = Converter::getAmountForView($prod['brutto'], $prod['unit']);
                $prod['netto'] = Converter::getAmountForView($prod['netto'], $prod['unit']);
                $result[$prod['id']] = $prod;
            }
        }

        return $result;
    }


    public function copyFromAllIntoObj($dishId, $obj)
    {
        $sql = "INSERT INTO " . $this->getTableName() . "(obj,dish_id,map_type,pos,prod_name,unit,brutto,netto)
                (SELECT :obj, dish_id, map_type, pos, prod_name, unit, brutto, netto FROM " . $this->getTableName() . " WHERE obj='all' AND dish_id = :dish_id)";
        $params = array(
            'dish_id' => $dishId,
            'obj' => $obj
        );
        $this->getDB()->execute($sql, $params);
        return $this->getProds($dishId, $obj);
    }

    public function save()
    {

        $data = array(
            'obj' => $this->obj,
            'dish_id' => $this->dish_id,
            'map_type' => $this->map_type,
            'pos' => $this->pos,
            'prod_name' => $this->prod_name,
            'unit' => $this->unit,
            'brutto' => empty($this->brutto) ? null : Converter::getAmountForDb($this->brutto, $this->unit),
            'netto' => empty($this->netto) ? null : Converter::getAmountForDb($this->netto, $this->unit),
        );
        if (empty($this->id)) {
            // добавим запись
            $this->getDB()->insert($this->getTableName(), $data);
            $this->id = $this->getDB()->getLastId();
        } else {
            // обновим запись
            $this->getDB()->update($this->getTableName(), $data,
                'id = :id',
                array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения продукта документа");
            return false;
        }

        return true;
    }

    public function delete()
    {
        if (empty($this->id)) {
            $this->setError('Неизвестный документ');
        } else {
            $this->getDB()->delete($this->getTableName(),
                'id = :id',
                array(
                    'id' => $this->id
                ));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка удаления продукта документа");
            return false;
        }

        return true;
    }

    public function deleteWorkMapProds($dish_id)
    {
        if (empty($dish_id) || empty($this->obj)) {
            $this->setError('Неизвестный документ');
        } else {
            $this->getDB()->delete($this->getTableName(),
                'obj = :obj AND dish_id = :dish_id AND map_type = :map_type',
                array(
                    'obj' => $this->obj,
                    'dish_id' => $dish_id,
                    'map_type' => self::TECH_MAP_TYPE_WORK
                ));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка удаления продукта документа");
            return false;
        }

        return true;
    }

}
