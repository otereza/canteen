<?php
/**
 * Created by PhpStorm.
 * User: otereza
 * Date: 20.01.19
 * Time: 20:26
 */

namespace Food\App\Models\Repositories;



use Food\App\Models\ProdObjectModel;
use Food\App\Models\ProdGroupModel;
use Food\App\Models\ProdTMarkModel;
use Food\App\Models\ProdPackModel;
use Food\App\Models\ProdTypeModel;
use Food\App\Models\TraderPriceModel;

class ProductRep
{
    public $addingPackParams = array();

    public function getTree($obj, $for = false, $forObj = null) {

        $objModel = new ProdObjectModel();
        $groupModel = new ProdGroupModel();
        $typeModel = new ProdTypeModel() ;
        $tmarkModel = new ProdTMarkModel();
        $packModel = new ProdPackModel();

        $groups = $groupModel->getAll($obj);
//dd($groups);
        $types = $typeModel->getAll($obj);
        $tmarks = $tmarkModel->getAll();
        $packs = $packModel->getAll();

        $products = $objModel->getAllProductsForObj($obj);

        if($for == 'trader') {
//            if(isset($_GET['trader_id'])) {
                $priceModel = new TraderPriceModel();
                $priceModel->obj = $forObj ? $forObj : $obj;
                $objPrices = $priceModel->getAll($_GET['trader_id']);
                $prices = array();
                if($objPrices) {
                    foreach($objPrices as $k => $price) {
                        if(is_null($price['active'])) {
                            $checked = 2;
                        } else {
                            $checked = intval($price['active']);
                        }
                        $prices[$price['tmark_id']][$price['pack_id']] = array(
                            'price' => $price['price'],
                            'main_price' => $price['main_price'],
                            'otherTrader' => $price['min_other_trader'],
                            'otherPack' => $price['min_other_pack'],
                            'thisType' => $price['min_this_type'],
                            'min' => $price['min_trader'],
                            'min_obj' => $price['min_trader_obj'],
                            'max' => $price['max_trader'],
                            'max_obj' => $price['max_trader_obj'],
                            'checkboxState' => $checked,
                        );
                    }
                }
//            }
        }
//dd($prices);
        $res = array();

        foreach ($groups as $group) {
            $res[$group['group_pos']] = array(
                'id' => 'gr' . $group['id'],
                'text' => $group['name'],
                'type' => 'group',
                'icon' => 'fa fa-envira c1',
                'data' => $group
            );
            if($group['deleted']) {
                $res[$group['group_pos']]['li_attr'] = array('class' => "hidden deleted");
//                $res[$group['group_pos']]['a_attr'] = array('style' => "text-decoration: line-through;");
            }
            if($for == 'trader') {
//                $res[$group['group_pos']]['state']['checkbox_disabled'] = true;
//                $res[$group['group_pos']]['a_attr']['class'] = "no_checkbox";
            }

            foreach ($types as $tpKey => $type) {
                if($type['group_id'] == $group['id']) {
                    $res[$group['group_pos']]['children'][$type['type_pos']] = array(
                        'id' => 'tp' . $type['id'],
                        'text' => $type['name'],
                        'type' => 'type',
                        'icon' => 'fa fa-tag c2',
                        'data' => $type
                    );
                    if($type['deleted']) {
                        $res[$group['group_pos']]['children'][$type['type_pos']]['li_attr'] = array('class' => "hidden deleted");
//                        $res[$group['group_pos']]['children'][$type['type_pos']]['a_attr'] = array('style' => "text-decoration: line-through;");
                    }
                    if($for == 'trader') {
//                        $res[$group['group_pos']]['children'][$type['type_pos']]['state']['checkbox_disabled'] = true;
//                        $res[$group['group_pos']]['children'][$type['type_pos']]['a_attr']['class'] = "no_checkbox";
                    }

                    foreach($products as $prKey => $product) {
                        if (!empty($product['type_id']) && $product['type_id'] == $type['id']) {
                            if (!empty($product['tmark_id'])) {

                                if(!isset($res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']])) {
                                    $res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']] = array(
                                        'id' => 'tm' . $tmarks[$product['tmark_id']]['id'],
                                        'text' => $tmarks[$product['tmark_id']]['name'],
                                        'type' => 'tmark',
                                        'icon' => 'fa fa-trademark c3',
                                        'data' => $tmarks[$product['tmark_id']]
                                    );
                                    if($product['tmark_deleted']) {
                                        $res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']]['li_attr'] = array('class' => "hidden deleted");
//                                        $res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']]['a_attr'] = array('style' => "text-decoration: line-through;");
                                        $res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']]['data']['deleted'] = '1';
                                    }
                                    if($for == 'trader') {
//                                        $res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']]['state']['checkbox_disabled'] = true;
//                                        $res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']]['a_attr']['class'] = "no_checkbox";
                                    }
                                }

                                if (!empty($product['pack_id'])) {
                                    $pName = $packs[$product['pack_id']]['name'];
                                    $pValue = $packs[$product['pack_id']]['value'];
                                    $pUnit = $packs[$product['pack_id']]['pack_unit'];
                                    $pByWeight = $packs[$product['pack_id']]['by_weight'];
                                    if($pByWeight != '1') {
                                        $pName .= (empty($pName) ? '' : ', ') . $pValue . ' ' . $pUnit;
                                    } else {
                                        $pName .= (empty($pName) ? '' : ', ') . $pUnit . ' (на вес)';
                                    }

                                    // если есть цены, добавим их к упаковке
                                    if(isset($prices[$product['tmark_id']][$product['pack_id']])) {
                                        $packs[$product['pack_id']] = array_merge($packs[$product['pack_id']], $prices[$product['tmark_id']][$product['pack_id']]);
                                    }

                                    $res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']]['children'][$product['pack_pos']] = array(
                                        'id' => 'pk' . $packs[$product['pack_id']]['id'],
                                        'text' => $pName,
                                        'type' => 'pack',
                                        'icon' => 'fa fa-cube c4',
                                        'data' => $packs[$product['pack_id']],
                                    );
                                    if($product['pack_deleted']) {
                                        $res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']]['children'][$product['pack_pos']]['li_attr'] = array('class' => "hidden deleted");
//                                        $res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']]['children'][$product['pack_pos']]['a_attr'] = array('style' => "text-decoration: line-through;");
                                        $res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']]['children'][$product['pack_pos']]['data']['deleted'] = '1';
                                    }
                                    if($for == 'trader') {
                                        $res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']]['children'][$product['pack_pos']]['text'] = '<input type="checkbox" class="p-check-box" name="check['.$product['pack_id'].']" data-id="'.$product['pack_id'].'"> ' . $pName;
//                                        $res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']]['children'][$product['pack_pos']]['a_attr']['class'] = "stretch";
                                    }
                                    if(!empty($this->addingPackParams) && isset($this->addingPackParams[$product['pack_id']])) {
                                        $res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']]['children'][$product['pack_pos']]['data'] += $this->addingPackParams[$product['pack_id']];
                                    }
                                }

                            }
                            unset($products[$prKey]);
                        }
                    }
                    if($for == 'trader' && empty($res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']]['children'])) {
                        unset($res[$group['group_pos']]['children'][$type['type_pos']]['children'][$product['tmark_pos']]);
                    }
                    unset($types[$tpKey]);
                }
                if($for == 'trader' && empty($res[$group['group_pos']]['children'][$type['type_pos']]['children'])) {
                    unset($res[$group['group_pos']]['children'][$type['type_pos']]);
                }
            }
            if($for == 'trader' && empty($res[$group['group_pos']]['children'])) {
                unset($res[$group['group_pos']]);
            }
//            dd($res);
           $this->makeIndexed($res);
        }
//dd($res);
        return array(
            array(
                'id' => 'root_0',
                'text' => 'Продукты',
                'type' => 'root',
                'icon' => false,
                'state' => array(
                    'opened'  => true,  // is the node open
                    'disabled' => true,  // is the node disabled
                    'selected' => false,  // is the node selected
                    'checkbox_disabled' => true,
                ),
                'a_attr' => array('class' => "no_checkbox"),
                'children' => array_values($res)
            )
        );
    }

    function makeIndexed(array &$arr, array &$par = null){
        if(is_null($par)){
            $arr = array_values($arr);
        } else {
            $par['children'] = array_values($arr);
        }

        for($i=0; $i<count($arr); $i++) {
            $temp = @$arr[$i]['children'];
            if(isset($temp)) {
                $this->makeIndexed($arr[$i]['children'], $arr[$i]);
            }
        }
    }


}