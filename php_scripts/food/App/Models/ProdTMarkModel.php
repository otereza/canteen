<?php

namespace Food\App\Models;

use Food\Core\Model;

class ProdTMarkModel extends Model
{

    protected $tableName = 'prod_tmark';

    public $id;
    public $obj;
    public $group_id;
    public $type_id;
    public $tmark_pos;
    public $name;
    public $pack_unit;
    public $life;
    public $temp_from;
    public $temp_to;
    public $deleted;


    public function get($id, $setParams = true)
    {
        if(empty($id)) {
            return null;
        }

        $sql = "SELECT tm.*, p_obj.group_id, p_obj.type_id, p_obj.obj, p_obj.tmark_pos, p_obj.tmark_deleted FROM " . $this->getTableName() . " AS tm
INNER JOIN " . $this->getTableName('prod_obj') . " AS p_obj ON p_obj.tmark_id = tm.id
WHERE tm.id = :id AND p_obj.obj = :obj LIMIT 1";

        $params = array('id' => $id, 'obj' => $this->obj);

        $tmarks = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($tmarks) && is_array($tmarks)) {
            $result = reset($tmarks);
            if($setParams) {
                $this->id = $result['id'];
                $this->obj = $result['obj'];
                $this->group_id = $result['group_id'];
                $this->type_id = $result['type_id'];
                $this->tmark_pos = $result['tmark_pos'];
                $this->name = $result['name'];
                $this->pack_unit = $result['pack_unit'];
                $this->life = $result['life'];
                $this->temp_from = $result['temp_from'];
                $this->temp_to = $result['temp_to'];
                $this->deleted = $result['tmark_deleted'];
            }
        }

        return $result;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM " . $this->getTableName();
        $products = $this->getDB()->query($sql);
        $result = array();
        if(!empty($products) && is_array($products)) {
            foreach($products as $product) {
                $result[$product['id']] = $product;
            }
        }
        return $result;
    }

    public function save()
    {
        if(empty($this->group_id)) {
            $this->setError("Field 'group_id' is empty");
        }
        if(empty($this->type_id)) {
            $this->setError("Field 'type_id' is empty");
        }
        if(empty($this->name)) {
            $this->setError("Field 'name' is empty");
        }
        if(empty($this->pack_unit)) {
            $this->setError("Field 'pack_unit' is empty");
        }

        if($this->hasErrors()) {
            return false;
        }

        try {
            $this->getDB()->begin();

            if (empty($this->id)) {
                // добавим запись в таблицу торговых марок
                $this->getDB()->insert($this->getTableName(), array(
                    'name'              => $this->name,
                    'pack_unit'         => $this->pack_unit,
                    'life'              => $this->life,
                    'temp_from'         => $this->temp_from,
                    'temp_to'           => $this->temp_to,
                ));
                $this->id = $this->getDB()->getLastId();
                // добавим запись в таблицу продуктов обьекта
                // найдем связку обьект + группа + вид с пустым полем торговой марки
                $sql = "SELECT id FROM " . $this->getTableName('prod_obj') . " WHERE obj=:obj AND group_id=:group_id AND type_id=:type_id AND tmark_id IS NULL";
                $params = array(
                    'obj' => $this->obj,
                    'group_id' => $this->group_id,
                    'type_id' => $this->type_id
                );
                $prodObjId = $this->getDB()->fetchColumn($sql, $params);
                // найдена строка с пустым полем торговой марик
                if($prodObjId) {    // обновляем запись
                    $this->getDB()->update($this->getTableName('prod_obj'),
                        array(
                            'tmark_id' => $this->id,
                            'tmark_pos' => $this->getLastPosition() + 1
                        ),
                        'id = :id',
                        array('id' => $prodObjId)
                    );
                } else {    // добаляем новую торговую марку
                    $this->getDB()->insert($this->getTableName('prod_obj'),
                        array(
                            'obj' => $this->obj,
                            'group_id'          => $this->group_id,
                            'type_id'           => $this->type_id,
                            'tmark_id' => $this->id,
                            'tmark_pos' => $this->getLastPosition() + 1
                        )
                    );
                }


            } else {
                // обновим запись
                $this->getDB()->update($this->getTableName(),
                    array(
                        'name'              => $this->name,
                        'pack_unit'         => $this->pack_unit,
                        'life'              => $this->life,
                        'temp_from'         => $this->temp_from,
                        'temp_to'           => $this->temp_to,
                    ),
                    'id= :id',
                    array('id' => $this->id));  // WHERE
            }
            $this->getDB()->commit();

        } catch (\PDOException $e) {
            $this->getDB()->rollBack();
            $this->setError("Ошибка сохранения торговой марки: " . $e->getMessage() . $e->getTraceAsString());
            return false;
        }

        return true;
    }

    public function delete()
    {
        if(empty($this->id) || empty($this->obj)) {
            $this->setError("Field 'id' or 'obj' is empty");
            return false;
        }

        $this->getDB()->update($this->getTableName('prod_obj'),
            array(
                'tmark_deleted' => 1,
                'pack_deleted' => 1
            ),
            'obj = :obj AND tmark_id = :id',
            array('obj' => $this->obj, 'id' => $this->id));

        if($this->hasErrors()) {
            $this->setError("Ошибка удаления торговой марки");
            return false;
        }
        return true;
    }

    public function undelete()
    {
        if(empty($this->id) || empty($this->obj)) {
            $this->setError("Field 'id' or 'obj' is empty");
            return false;
        }

        $this->getDB()->update($this->getTableName('prod_obj'),
            array(
                'tmark_deleted' => 0,
                'pack_deleted' => 0
            ),
            'obj = :obj AND tmark_id = :id',
            array('obj' => $this->obj, 'id' => $this->id));

        if($this->hasErrors()) {
            $this->setError("Ошибка восстановления торговой марки");
            return false;
        }
        return true;
    }

    public function changePos()
    {
        try {
            $this->getDB()->begin();

            $curData = $this->getDB()->fetchRow("SELECT * FROM " . $this->getTableName('prod_obj') . " WHERE obj = :obj AND tmark_id = :tmark_id",
                array('tmark_id' => $this->id, 'obj' => $this->obj)
            );
            if($curData['type_id'] == $this->type_id) {
                // тот же вид
                if ($this->tmark_pos > $curData['tmark_pos']) {
                    $sql = "UPDATE " . $this->getTableName('prod_obj') . " SET tmark_pos = tmark_pos - 1 WHERE obj = :obj AND type_id = :type_id AND tmark_pos > :old_tmark_pos AND tmark_pos <= :new_tmark_pos";
                } else {
                    $sql = "UPDATE " . $this->getTableName('prod_obj') . " SET tmark_pos = tmark_pos + 1 WHERE obj = :obj AND type_id = :type_id AND tmark_pos >= :new_tmark_pos AND tmark_pos < :old_tmark_pos";
                }
                $params = array(
                    'obj' => $this->obj,
                    'type_id' => $this->type_id,
                    'old_tmark_pos' => $curData['tmark_pos'],
                    'new_tmark_pos' => $this->tmark_pos
                );
                $this->getDB()->execute($sql, $params);
            } else {
                // другой вид
                // смещаем позиции в старом виде продукта
                $sql = "UPDATE " . $this->getTableName('prod_obj') . " SET tmark_pos = tmark_pos - 1 WHERE obj = :obj AND group_id = :group_id AND type_id = :type_id AND tmark_pos > :tmark_pos";
                $params = array(
                    'obj' => $this->obj,
                    'group_id' => $curData['group_id'],
                    'type_id' => $curData['type_id'],
                    'tmark_pos' => $curData['tmark_pos']
                );
                $this->getDB()->execute($sql, $params);

                // смещаем позиции в новом виде продукта
                $sql = "UPDATE " . $this->getTableName('prod_obj') . " SET tmark_pos = tmark_pos + 1 WHERE obj = :obj AND group_id = :group_id AND type_id = :type_id AND tmark_pos >= :tmark_pos";
                $params = array(
                    'obj' => $this->obj,
                    'group_id' => $this->group_id,
                    'type_id' => $this->type_id,
                    'tmark_pos' => $this->tmark_pos
                );
                $this->getDB()->execute($sql, $params);
            }

            $this->getDB()->update($this->getTableName('prod_obj'),
                array(
                    'group_id' => $this->group_id,
                    'type_id' => $this->type_id,
                    'tmark_pos' => $this->tmark_pos,
                ),
                'tmark_id = :id AND obj = :obj',
                array(
                    'obj' => $this->obj,
                    'id' => $this->id
                )
            );

            $this->getDB()->commit();

        } catch (\PDOException $e) {
            $this->getDB()->rollBack();
            $this->setError("Ошибка перемещения торговой марки: " . $e->getMessage() . $e->getTraceAsString());
            return false;
        }
        return true;
    }

//    public function getPosition()
//    {
//        $sql = "SELECT tmark_pos FROM " . $this->getTableName('prod_obj') . " WHERE obj=:obj AND tmark_id=:tmark_id";
//        $res = $this->getDB()->fetchColumn($sql, array(
//            'obj' => $this->obj,
//            'tmark_id' => $this->id
//        ));
//        return isset($res['tmark_pos']) ? $res['tmark_pos'] : null;
//    }

    public function getLastPosition()
    {
        $sql = "SELECT MAX(tmark_pos) FROM " . $this->getTableName('prod_obj') . " WHERE obj=:obj AND group_id=:group_id AND type_id=:type_id";
        $res = $this->getDB()->fetchColumn($sql, array(
            'obj' => $this->obj,
            'group_id' => $this->group_id,
            'type_id' => $this->type_id
        ));
        return empty($res) ? 0 : $res;
    }

//    public function setPosition()
//    {
//        $this->tmark_pos = $this->getPosition();
//
//        if (!empty($this->tmark_pos)) {
//            $this->getDB()->update($this->getTableName('prod_obj'), array(
//                'obj' => $this->obj,
//                'tmark_id' => $this->id,
//                'tmark_pos' => $this->tmark_pos,
//            ));
//        } else {
//            $positions = $this->getLastPosition();
//            $this->tmark_pos = $positions + 1;
//            $this->getDB()->insert($this->getTableName('prod_obj'), array(
//                'obj' => $_GET['obj'],
//                'tmark_id' => $this->id,
//                'tmark_pos' => $this->tmark_pos,
//            ));
//        }
//        if($this->hasErrors()) {
//            $this->setError("Ошибка сохранения позиции продукта");
//            return false;
//        }
//        return true;
//    }

}
