<?php

namespace Food\App\Models;

use Food\App\Helpers\Converter;
use Food\App\Helpers\Units;
use Food\Core\Json;
use Food\Core\Model;
use Food\Core\View;


/**
 * Склад.
 *
 * Количество хранится в минимальных единицах (г, мл, шт)
 * Цена хранится целым числом за минимальную единицу (г, мл, шт),
 *      точность до 3-х знаков (делитель на 1000 000 (3 знака на максимальную упаковку(кг,мл) и 3 знака после запятой))
 *
 * Class WarehouseModel
 * @package Food\App\Models
 */
class WarehouseModel extends Model
{
    const PRICE_COEFF = 1000000;

    protected $tableName = 'warehouse';

    public $id;
    public $trans_doc_type; // тип документа по которому был изменен склад (накладная приход/расход/перемещение, акты)
    public $trans_doc_id;   // id документа по которому был изменен склад
    public $lot_id;         // номер партии, аналогичный номеру приходной накладной
    public $trans_date;     // дата-время проводки документа
    public $obj;
    public $prod_name;
    public $tmark_id;
    public $pack_id;
    public $unit;
    public $pack_unit;
    public $amount_unit;
    public $amount;
    public $price;
    public $exp_date;


    /**
     * Получить продукты на складе с сохранением истории изменений
     * (документ, дата, количество и цена, по которым поступ или списан со склада)
     *
     * @param $obj
     * @return array|null
     */
    public function getProds($obj)
    {

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE obj=:obj ORDER BY trans_date ASC";
        $params = array(
            'obj' => $obj
        );
        $prods = $this->getDB()->query($sql, $params);

        return $prods;
    }

    /**
     * Получить продукты на складе сгрупированиые по партиям. Показывает партии, которые еще не списаны
     *
     * @param $obj
     * @return array|null
     */
    public function getProdsBalance($obj)
    {

        $sql = "SELECT g.name as group_name, w.obj, w.lot_id, MIN(w.date_add) as date_add, w.prod_name, w.tmark_id, w.pack_id, w.unit, w.pack_unit, SUM(w.amount) as amount, ROUND(SUM(w.price)/COUNT(w.pack_id), 0) as price, w.exp_date
                FROM (SELECT obj, lot_id, IF(trans_doc_id = lot_id, trans_date, null) as date_add, prod_name, tmark_id, pack_id, unit, pack_unit, SUM(amount) as amount, ROUND(SUM(price)/COUNT(pack_id), 0) as price, exp_date
                      FROM " . $this->getTableName() . "
                      WHERE obj=:obj
                      GROUP BY lot_id, pack_id, exp_date
                      ORDER BY date_add ASC ) as w
                JOIN " . $this->getTableName('prod_obj') . " as p ON p.pack_id = w.pack_id AND p.obj=w.obj
                JOIN " . $this->getTableName('prod_group') . " as g ON g.id = p.group_id AND p.obj=w.obj
                GROUP BY w.pack_id, exp_date
                ORDER BY g.group_pos, w.prod_name ASC, w.exp_date ASC";
        $params = array(
            'obj' => $obj
        );
        $prods = $this->getDB()->query($sql, $params);

        return $prods;
    }

    /**
     * Получить продукты на складе сгрупированиые по (продукт + фасовка)
     *
     * @param $obj
     * @return array|null
     */
    public function getProdsGroupByPackId($obj)
    {

        $sql = "SELECT t.obj, MIN(t.date_add) as first_date_add, MAX(t.date_add) as last_date_add, prod_name, tmark_id, pack_id, unit, pack_unit, amount, price, MIN(exp_date) as exp_date
                    FROM (SELECT obj, lot_id, IF(trans_doc_id = lot_id, trans_date, null) as date_add, prod_name, tmark_id, pack_id, unit, pack_unit, SUM(amount) as amount, ROUND(SUM(price)/COUNT(pack_id), 0) as price, exp_date
                            FROM " . $this->getTableName() . "
                            WHERE obj=:obj
                            GROUP BY lot_id, pack_id, exp_date
                            ORDER BY date_add ASC ) as t
                    WHERE t.amount > 0
                    GROUP BY t.pack_id";
        $params = array(
            'obj' => $obj
        );
        $prods = $this->getDB()->query($sql, $params);

        return $prods;
    }

    /**
     * добавить продукт на склад (количество положительное)
     *
     * @return bool
     */
    public function putProduct()
    {
        if($this->obj != 'all') {
            $prodObjModel = new ProdObjectModel();
            $prodObjModel->addProductForObj($this->pack_id, $this->obj);
        }
        return $this->save();
    }


    /**
     * взять продукт со склада (количество отрицательное)
     *
     * @return bool
     */
    public function takeProduct()
    {
        $this->amount *= -1;
        return $this->save();
    }


    /**
     * Сохранение продукта на складе
     *
     * @return bool
     */
    private function save()
    {
        $data = array(
            'trans_doc_type' => $this->trans_doc_type,
            'trans_doc_id' => $this->trans_doc_id,
            'lot_id' => $this->lot_id,
            'trans_date' => $this->trans_date,
            'obj' => $this->obj,
            'prod_name' => $this->prod_name,
            'tmark_id' => $this->tmark_id,
            'pack_id' => $this->pack_id,
            'unit' => $this->unit,
            'pack_unit' => $this->pack_unit,
            'amount' => $this->amount,
            'price' => $this->price,
            'exp_date' => $this->exp_date
        );

        if (empty($this->id)) {
            // добавим запись
            $this->getDB()->insert($this->getTableName(), $data);
            $this->id = $this->getDB()->getLastId();
        } else {
            // обновим запись
            $this->getDB()->update($this->getTableName(), $data, 'id = :id',  array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения продукта документа");
            return false;
        }

        return true;
    }

    /**
     * Удаление всех продуктов документа
     *
     * @return bool
     */
    public function deleteDocProds()
    {
        if (empty($this->trans_doc_type) || empty($this->trans_doc_id)) {
            $this->setError('Неизвестный документ');
        } elseif(!empty($this->obj)) {
            $this->getDB()->delete($this->getTableName(),
                'trans_doc_type = :trans_doc_type AND trans_doc_id = :trans_doc_id AND obj=:obj',
                array(
                    'trans_doc_type' => $this->trans_doc_type,
                    'trans_doc_id' => $this->trans_doc_id,
                    'obj' => $this->obj
                ));
        } else {
            $this->getDB()->delete($this->getTableName(),
                'trans_doc_type = :trans_doc_type AND trans_doc_id = :trans_doc_id',
                array(
                    'trans_doc_type' => $this->trans_doc_type,
                    'trans_doc_id' => $this->trans_doc_id
                ));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка удаления продуктов документа");
            return false;
        }

        return true;
    }

}
