<?php
/**
 * Created by PhpStorm.
 * User: terezao
 * Date: 28.01.19
 * Time: 11:54
 */

namespace Food\App\Models;


use Food\Core\Model;

class LockTablesModel extends Model
{
    protected $tableName = 'lock_tables';

    public $lockTableName;
    public $rowId;
    public $userId;

    public function __construct($locTableName)
    {
        $this->lockTableName = $locTableName;
    }

    public function getUserBlockedTable()
    {
        $this->getDB()->delete($this->getTableName(), 'time_expired < :currTime', array('currTime' => time()));
        if(!empty($this->rowId)) {
            $sql = "SELECT user_id FROM " . $this->getTableName() . " WHERE table_name = :table_name AND row_id = :row_id AND time_expired > :time_expired";
            $user_id = $this->getDB()->fetchColumn($sql, array(
                'table_name' => $this->lockTableName,
                'row_id' => $this->rowId,
                'time_expired' => time(),
            ));
            return $user_id;
        }
        return false;
    }

    public function setLock()
    {
        global $user;

        if(!empty($this->rowId) && !empty($this->userId)) {
            $id = $this->getDB()->fetchColumn("SELECT id FROM " . $this->getTableName() . " WHERE table_name = :table_name AND row_id = :row_id AND user_id = :user_id",
                array(
                'table_name' => $this->lockTableName,
                'row_id' => $this->rowId,
                'user_id' => $this->userId,
            ));
            if($id) {
                return $this->getDB()->update($this->getTableName(),
                    array( 'time_expired' => time() + 60 * 15 ),
                    'id = :id',
                    array( 'id' => $id )
                );
            } else {
                return $this->getDB()->insert($this->getTableName(),
                    array(
                        'table_name' => $this->lockTableName,
                        'row_id' => $this->rowId,
                        'user_id' => $this->userId,
                        'time_expired' => time() + 60 * 15,
                    )
                );
            }
        }
        return false;
    }

    public function unLock()
    {
        if(!empty($this->rowId) && !empty($this->userId)) {
            $id = $this->getDB()->fetchColumn("SELECT id FROM " . $this->getTableName() . " WHERE table_name = :table_name AND row_id = :row_id AND user_id = :user_id",
                array(
                    'table_name' => $this->lockTableName,
                    'row_id' => $this->rowId,
                    'user_id' => $this->userId,
                ));
            if($id) {
                $this->getDB()->delete($this->getTableName(),
                    'id = :id',
                    array( 'id' => $id )
                );
                return true;
            }
        }
        return false;
    }

}