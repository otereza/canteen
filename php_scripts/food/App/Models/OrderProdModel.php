<?php

namespace Food\App\Models;

use Food\Core\Json;
use Food\Core\Model;
use Food\Core\View;

class OrderProdModel extends Model
{

    protected $tableName = 'order_product';

    public $id;
    public $order_id;
    public $pos;
    public $prod_name;
    public $tmark_id;
    public $pack_id;
    public $unit;
    public $pack_unit;
    public $amount_unit;
    public $amount;
    public $price;



    public function get($id, $setAttributes = false)
    {
        if(empty($id)) {
            return null;
        }

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE id = :id";
        $params = array('id' => $id);
        $order = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($order) && is_array($order)) {
            $result = reset($order);
            if($setAttributes ) {
                $this->setAttributes($result);
            }
        }

        return $result;
    }

    public function getAllForOrder($order_id)
    {

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE order_id = :order_id ORDER BY pos ASC";
        $params = array('order_id' => $order_id);
        $products = $this->getDB()->query($sql, $params);

        return (!empty($products) && is_array($products)) ? $products : array();
    }

    public function getAllForSendOrder($order_id)
    {

        $sql = "SELECT tm.name, CONCAT(IFNULL(pk.value, 'на вес,'), pk.pack_unit) as pack, op.price, pk.pack_unit, op.amount, op.amount_unit
                    FROM " . $this->getTableName() . " AS op
                    JOIN " . $this->getTableName('prod_tmark') . " AS tm ON tm.id = op.tmark_id
                    JOIN " . $this->getTableName('prod_pack') . " AS pk ON pk.id = op.pack_id
                    WHERE op.order_id = :order_id";
        $params = array('order_id' => $order_id);
        $products = $this->getDB()->query($sql, $params);

        return (!empty($products) && is_array($products)) ? $products : array();
    }



    public function save()
    {

        $data = array(
            'order_id' => $this->order_id,
            'pos' => $this->pos,
            'prod_name' => $this->prod_name,
            'tmark_id' => $this->tmark_id,
            'pack_id' => $this->pack_id,
            'unit' => $this->unit,
            'pack_unit' => $this->pack_unit,
            'amount_unit' => $this->amount_unit,
            'amount' => $this->amount,
            'price' => $this->price
        );

        if (empty($this->id)) {
            // добавим запись
            $data['pos'] = $this->getLastPosition() + 1;
            $this->getDB()->insert($this->getTableName(), $data);
            $this->id = $this->getDB()->getLastId();
        } else {
            // обновим запись
            $this->getDB()->update($this->getTableName(), $data,
                'id = :id', array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения продукта в заявке");
            return false;
        }

        return true;
    }

    public function update($data)
    {
        $this->getDB()->update($this->getTableName(), $data,
            'id = :id', array('id' => $this->id));

        if($this->hasErrors()) {
            $this->setError("Ошибка обновления продукта в заявке");
            return false;
        }

        return true;
    }

    public function delete()
    {

        if(empty($this->id)) {
            $this->setError("Field 'id' is empty");
            return false;
        }

        $curOrderProd = $this->get($this->id);
        $this->getDB()->begin();
        try {
            $this->getDB()->delete($this->getTableName(), 'id = :id', array('id' => $this->id));

            $sql = "UPDATE " . $this->getTableName() . " SET pos = pos - 1 WHERE order_id = :order_id AND pos > :oldPos";
            $params = array(
                'order_id' => $curOrderProd['order_id'],
                'oldPos' => $curOrderProd['pos']
            );
            $this->getDB()->execute($sql, $params);

            $this->getDB()->commit();
        } catch (\PDOException $e) {
            $this->errors = $this->getDB()->getErrors();
            $this->getDB()->rollBack();
            return false;
        }

        return true;
    }

    public function changePosition()
    {
        if($this->id && $this->pos) {
            $curOrderProd = $this->get($this->id);
            if ($this->pos != $curOrderProd['pos']) {
                $this->getDB()->begin();
                try {
                    if ($this->pos > $curOrderProd['pos']) {
                        $sql = "UPDATE " . $this->getTableName() . " SET pos = pos - 1 WHERE order_id = :order_id AND pos > :oldPos AND pos <= :newPos";
                        $params = array(
                            'order_id' => $curOrderProd['order_id'],
                            'oldPos' => $curOrderProd['pos'],
                            'newPos' => $this->pos
                        );
                        $this->getDB()->execute($sql, $params);
                    } elseif ($this->pos < $curOrderProd['pos']) {
                        $sql = "UPDATE " . $this->getTableName() . " SET pos = pos + 1 WHERE order_id = :order_id AND pos >= :newPos AND pos < :oldPos";
                        $params = array(
                            'order_id' => $curOrderProd['order_id'],
                            'newPos' => $this->pos,
                            'oldPos' => $curOrderProd['pos'],
                        );
                        $this->getDB()->execute($sql, $params);
                    }

                    $this->getDB()->update($this->getTableName(),
                        array('pos' => $this->pos),
                        'id = :id',
                        array('id' => $this->id)
                    );

                    $this->getDB()->commit();
                } catch (\PDOException $e) {
                    $this->errors = $this->getDB()->getErrors();
                    $this->getDB()->rollBack();
                    return false;
                }
            }
        }
        return true;
    }

    private function getLastPosition()
    {
        $sql = "SELECT MAX(pos) FROM " . $this->getTableName() . " WHERE order_id = :order_id";
        $params = array('order_id' => $this->order_id);
        return $this->getDB()->fetchColumn($sql, $params);
    }

}
