<?php

namespace Food\App\Models;

use Food\App\Helpers\Converter;
use Food\Core\Model;

class ActWritingOffModel extends Model
{

    protected $tableName = 'act_writingoff';

    public $id;
    public $obj;
    public $act_number;
    public $act_date;
    public $act_time_period;
    public $total;
    public $reason;
    public $commission;
    public $date_approve;
    public $status;

    const STATUS_OPEN = 0;
    const STATUS_APROVED = 1;



    public function get($id, $setParams = true)
    {
        if(empty($id)) {
            return null;
        }

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE id = :id";
        $params = array('id' => intval($id));
        $act = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($act) && is_array($act)) {
            $result = reset($act);
            if($setParams) {
                $this->setAttributes($result);
            }
        }

        return $result;
    }

    public function getAll()
    {
        if($this->obj == 'all') {
            $acts = $this->getDB()->query("SELECT * FROM " . $this->getTableName());
        } else {
            $sql = "SELECT * FROM " . $this->getTableName() . " WHERE obj = :obj";
            $params = array('obj' => $this->obj);
            $acts = $this->getDB()->query($sql, $params);
        }
        return $acts;
    }

//    public function getForSend($id)
//    {
//        if(empty($id)) {
//            return array();
//        }
//
//        $sql = "SELECT IFNULL(tro.fio, tr.fio) as fio, IFNULL(tro.phone, tr.phone) as phone, IFNULL(tro.email, tr.email) as email, o.date_shipment, o.delivery_time, o.id
//                    FROM " . $this->getTableName() . " AS o
//                    INNER JOIN " . $this->getTableName('trader') . " AS tr ON tr.id = o.trader_id
//                    LEFT JOIN " . $this->getTableName('trader_obj') . " AS tro ON tro.trader_id = o.trader_id
//                    WHERE o.id = :id";
//        $params = array('id' => $id);
//        $order = $this->getDB()->fetchRow($sql, $params);
//        return $order;
//    }



    public function save()
    {
        $data = array(
            'obj' => $this->obj,
            'act_number' => $this->act_number,
            'act_date' => $this->act_date,
            'act_time_period' => $this->act_time_period,
            'total' => $this->total,
            'reason' => $this->reason,
            'commission' => $this->commission,
        );

        if (empty($this->id)) {
            // добавим запись
            $this->getDB()->insert($this->getTableName(), $data);
            $this->id = $this->getDB()->getLastId();
            if(empty($this->act_number)) {
                $this->getDB()->update($this->getTableName(),
                    array(
                        'act_number' => DocProdModel::PREFIX_ACT_WRITING_OFF . '-' . $this->id,
                    ),
                    'id = :id',
                    array('id' => $this->id));
            }
        } else {
            // обновим запись
            $this->getDB()->update($this->getTableName(), $data,
                'id = :id',
                array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения акта");
            return false;
        }

        return true;
    }


    public function approve()
    {
        if (empty($this->id)) {
            return false;
        } else {

            $this->getDB()->update($this->getTableName(),
                array(
                    'date_approve' => time(),
                    'status' => self::STATUS_APROVED,
                ),
                'id = :id',
                array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка проводки накладной");
            return false;
        }

        return true;
    }

}
