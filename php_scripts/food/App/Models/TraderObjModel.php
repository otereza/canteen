<?php

namespace Food\App\Models;

use Food\Core\Model;

class TraderObjModel extends Model
{

    protected $tableName = 'trader_obj';

    public $trader_id;
    public $obj;
    public $delivery_days;  // массив времени работы from, to. Формат: день недели(Пн-1...Вс-7) + ЧЧ + ММ
    public $min_price;
    public $contract;
    public $fio;
    public $phone;
    public $email;
    public $prepayment;
    public $payment_delay;
    public $discount;



    public function get($id, $setParams = true)
    {
        if(empty($id)) {
            return null;
        }

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE trader_id = :id AND obj = :obj";
        $params = array('id' => $id, 'obj' => $this->obj);
        $trader = $this->getDB()->query($sql, $params);

        $sql = "SELECT * FROM " . $this->getTableName('trader_delivery_days') . " WHERE trader_id = :id AND obj = :obj";
        $params = array('id' => $id, 'obj' => $this->obj);
        $days = $this->getDB()->query($sql, $params);
        $delivery_days = array();
        if($days) {
            foreach ($days as $dTime) {
                $delivery_days[$dTime['week_day']] = array(
                    'from' => sprintf("%02d:%02d", intval($dTime['time_from'] / 100), intval($dTime['time_from'] % 100)),
                    'to' => sprintf("%02d:%02d", intval($dTime['time_to'] / 100), intval($dTime['time_to'] % 100))
                );
            }
        }

        $result = array();
        if(!empty($trader) && is_array($trader)) {
            $result = reset($trader);
            $result['delivery_days'] = $delivery_days;
            if($setParams) {
                $this->trader_id = $result['trader_id'];
                $this->obj = $result['obj'];
                $this->delivery_days = $result['delivery_days'];
                $this->min_price = $result['min_price'];
                $this->contract = $result['contract'];
                $this->fio = $result['fio'];
                $this->phone = $result['phone'];
                $this->email = $result['email'];
                $this->prepayment = $result['prepayment'];
                $this->payment_delay = $result['payment_delay'];
                $this->discount = $result['discount'];
            }
        }

        return $result;
    }


    public function save()
    {
        try {
            $this->getDB()->begin();

            $sql = "SELECT COUNT(*) FROM " . $this->getTableName() . " WHERE trader_id = :id AND obj = :obj";
            $params = array('id' => $this->trader_id, 'obj' => $this->obj);
            $trader = $this->getDB()->fetchColumn($sql, $params);

            if (empty($trader)) {
                // добавим запись
                $this->getDB()->insert($this->getTableName(), array(
                    'trader_id' => $this->trader_id,
                    'obj' => $this->obj,
                    'min_price' => $this->min_price,
                    'contract' => $this->contract,
                    'fio' => $this->fio,
                    'phone' => $this->phone,
                    'email' => $this->email,
                    'prepayment' => $this->prepayment,
                    'payment_delay' => $this->payment_delay,
                    'discount' => $this->discount,
                ));
                $this->id = $this->getDB()->getLastId();
            } else {
                // обновим запись
                $this->getDB()->update($this->getTableName(),
                    array(
                        'min_price' => $this->min_price,
                        'contract' => $this->contract,
                        'fio' => $this->fio,
                        'phone' => $this->phone,
                        'email' => $this->email,
                        'prepayment' => $this->prepayment,
                        'payment_delay' => $this->payment_delay,
                        'discount' => $this->discount,
                    ),
                    'trader_id = :trader_id AND obj = :obj',
                    array('trader_id' => $this->trader_id, 'obj' => $this->obj));

                // Удалим время доставки товара поставщиком на обьект
                $this->getDB()->delete($this->getTableName('trader_delivery_days'),
                    'trader_id = :trader_id AND obj = :obj',
                    array(
                        'trader_id' => $this->trader_id,
                        'obj' => $this->obj
                    )
                );
            }

            // Запись времени работы
            foreach ($this->delivery_days as $wDay => $time) {
                $this->getDB()->insert($this->getTableName('trader_delivery_days'), array(
                    'trader_id' => $this->trader_id,
                    'obj' => $this->obj,
                    'week_day' => $wDay,
                    'time_from' => $time['from'],
                    'time_to' => $time['to'],
                ));
            }

            $this->getDB()->commit();

        } catch (\PDOException $e) {
            $this->getDB()->rollBack();
            return false;
        }

        return true;
    }

    public function delete()
    {
        return false;

        if(empty($this->id) || empty($this->obj)) {
            $this->setError("Field 'id' or 'obj' is empty");
            return false;
        }

        $this->getDB()->update($this->getTableName(),
            array(
                'deleted' => 1
            ),
            'id = :id',
            array('id' => $this->id));

        $this->getDB()->update($this->getTableName('prod_type'),
            array(
                'deleted' => 1
            ),
            'group_id = :id',
            array('id' => $this->id));

        $this->getDB()->update($this->getTableName('prod_obj'),
            array(
                'tmark_deleted' => 1,
                'pack_deleted' => 1
            ),
            'obj = :obj AND group_id = :id',
            array('obj' => $this->obj, 'id' => $this->id));

        if($this->hasErrors()) {
            $this->setError("Ошибка удаления группы");
            return false;
        }

        return true;
    }

    public function getDiscount($trader_id)
    {
        $sql = "SELECT discount FROM " . $this->getTableName() . " WHERE trader_id = :id AND obj = :obj";
        $params = array('id' => $trader_id, 'obj' => $this->obj);
        $discount = $this->getDB()->fetchColumn($sql, $params);

        return empty($discount) ? 0 : $discount;
    }

}
