<?php

namespace Food\App\Models;

use Food\Core\Json;
use Food\Core\Model;
use Food\Core\View;

class TraderModel extends Model
{

    protected $tableName = 'trader';

    public $id;
    public $obj;
    public $internal_name;
    public $legal_name;
    public $bank_details;
    public $address;
    public $main_phone;
    public $main_email;
    public $contract;
    public $fio;
    public $phone;
    public $email;
    public $prepayment;
    public $payment_delay;
    public $discount;
    public $business_obj;
    public $obj_data;



    public function get($id, $setParams = true, $for = null)
    {
        if(empty($id)) {
            return null;
        }

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE id = :id";
        $params = array('id' => $id);
        $traders = $this->getDB()->query($sql, $params);

        if($for) {
            $traderObjModel = new TraderObjModel();
            $traderObjModel->obj = $for;
            $objData = $traderObjModel->get($id);
        }
        $result = array();
        if(!empty($traders) && is_array($traders)) {
            $result = reset($traders);
            $result['business_obj'] = json_decode($result['business_obj'], true);
            $result['discount'] = $result['discount'] / 100;
            $result['obj'] = isset($objData) ? $objData : array();
            if($setParams) {
                $this->setAttributes($result);
            }
        }

//        dd($result);
        return $result;
    }

    // Получение всех поставщиков.
    // Если указан обьект, то получение почтавщиков для обьекта
    public function getAll()
    {
        $traders = $this->getDB()->query("SELECT * FROM " . $this->getTableName());

        if(!empty($this->obj) && $this->obj != 'all' && !empty($traders)) {
            foreach ($traders as $key => $trader) {
                $trader['business_obj'] = json_decode($trader['business_obj'], true);
                if(!in_array($this->obj, $trader['business_obj'])) {
                    unset($traders[$key]);
                }
            }
        }

        $result = array();
        if(!empty($traders) && is_array($traders)) {
            foreach($traders as $trader) {
                $result[$trader['id']] = $trader;
            }
        }
//dd($result);
        return $result;
    }


    // Получение обьектов, с которыми работает поставщик
    public function getTraderBusinessObjs($id)
    {
        $sql = "SELECT business_obj FROM " . $this->getTableName() . " WHERE id = :id";
        $params = array('id' => $id);
        $objs = $this->getDB()->fetchColumn($sql, $params);
        if(!empty($objs)) {
//            $arrObjs = json_decode($objs, true);
//            return array_keys( array_filter($arrObjs, function($item) { return $item; }) );
            return json_decode($objs, true);
        }
        return array();
    }

    public function deliveryDays($trader_id)
    {
        $result = array();
        $sql = "SELECT * FROM " . $this->getTableName('trader_delivery_days') . " WHERE trader_id = :trader_id";
        $params = array('trader_id' => $trader_id);
        $deliveryDays = $this->getDB()->query($sql, $params);
        if(!empty($deliveryDays) && is_array($deliveryDays)) {
            foreach ($deliveryDays as $day) {
                $result[$day['week_day']] = array(
                    'from' => sprintf("%02d:%02d", intval($day['time_from'] / 100), intval($day['time_from'] % 100)),
                    'to' => sprintf("%02d:%02d", intval($day['time_to'] / 100), intval($day['time_to'] % 100)),
                );
            }
        }
        return $result;
    }



    public function save()
    {

        if (empty($this->id)) {
            // добавим запись
            $this->getDB()->insert($this->getTableName(), array(
                'internal_name' => $this->internal_name,
                'legal_name' => $this->legal_name,
                'bank_details' => $this->bank_details,
                'address' => $this->address,
                'main_phone' => $this->main_phone,
                'main_email' => $this->main_email,
                'contract' => $this->contract,
                'fio' => $this->fio,
                'phone' => $this->phone,
                'email' => $this->email,
                'prepayment' => $this->prepayment,
                'payment_delay' => $this->payment_delay,
                'discount' => $this->discount * 100,
                'business_obj' => $this->business_obj,
            ));
            $this->id = $this->getDB()->getLastId();
        } else {
            // обновим запись
            $this->getDB()->update($this->getTableName(),
                array(
                    'internal_name' => $this->internal_name,
                    'legal_name' => $this->legal_name,
                    'bank_details' => $this->bank_details,
                    'address' => $this->address,
                    'main_phone' => $this->main_phone,
                    'main_email' => $this->main_email,
                    'contract' => $this->contract,
                    'fio' => $this->fio,
                    'phone' => $this->phone,
                    'email' => $this->email,
                    'prepayment' => $this->prepayment,
                    'payment_delay' => $this->payment_delay,
                    'discount' => $this->discount * 100,
                    'business_obj' => $this->business_obj,
                ),
                'id = :id',
                array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения поставщика");
            return false;
        }

        return true;
    }

    public function delete()
    {
        return false;

        if(empty($this->id) || empty($this->obj)) {
            $this->setError("Field 'id' or 'obj' is empty");
            return false;
        }

        $this->getDB()->update($this->getTableName(),
            array(
                'deleted' => 1
            ),
            'id = :id',
            array('id' => $this->id));

        $this->getDB()->update($this->getTableName('prod_type'),
            array(
                'deleted' => 1
            ),
            'group_id = :id',
            array('id' => $this->id));

        $this->getDB()->update($this->getTableName('prod_obj'),
            array(
                'tmark_deleted' => 1,
                'pack_deleted' => 1
            ),
            'obj = :obj AND group_id = :id',
            array('obj' => $this->obj, 'id' => $this->id));

        if($this->hasErrors()) {
            $this->setError("Ошибка удаления группы");
            return false;
        }

        return true;
    }

}
