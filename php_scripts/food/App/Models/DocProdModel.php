<?php

namespace Food\App\Models;

use Food\Core\Json;
use Food\Core\Model;
use Food\Core\View;

class DocProdModel extends Model
{

    const DOC_TYPE_INVOICE_RECEIPT = 1;
    const DOC_TYPE_INVOICE_RETURN = 2;
    const DOC_TYPE_INVOICE_TRANSFER = 3;
    const DOC_TYPE_ACT_WRITING_OFF = 4;
    const DOC_TYPE_ACT_WAREHOUSE_BALANCE = 5;

    const PREFIX_INVOICE_RECEIPT = 'НакПр';
    const PREFIX_INVOICE_RETURN = 'НакВз';
    const PREFIX_INVOICE_TRANSFER = 'НакПер';
    const PREFIX_ACT_WRITING_OFF = 'АктСпис';
    const PREFIX_ACT_WAREHOUSE_BALANCE = 'АктОст';

    const DOC_STATUS_NOT_APPROVED = 0;
    const DOC_STATUS_APPROVED = 1;


    protected $tableName = 'doc_prod';

    public $id;
    public $doc_type;
    public $doc_id;
    public $obj;
    public $pos;
    public $prod_name;
    public $tmark_id;
    public $pack_id;
    public $unit;
    public $pack_unit;
    public $in_amount;
    public $in_amount_unit;
    public $in_price;
    public $amount;
    public $amount_unit;
    public $price;
    public $exp_date;


    public static function getDocName($doc_type)
    {
        $docTypeNames = array(
            self::DOC_TYPE_INVOICE_RECEIPT => 'Накладная (приход)',
            self::DOC_TYPE_INVOICE_RETURN => 'Накладная (возврат)',
            self::DOC_TYPE_INVOICE_TRANSFER => 'Накладная (перемещение)',
            self::DOC_TYPE_ACT_WRITING_OFF => 'Акт списания',
            self::DOC_TYPE_ACT_WAREHOUSE_BALANCE => 'Акт снятия остатков',
        );
         return isset($docTypeNames[$doc_type]) ? $docTypeNames[$doc_type] : null;
    }

    public function getProds($docType, $docId, $obj=false)
    {
        if($obj) {
            $sql = "SELECT * FROM " . $this->getTableName() . " WHERE doc_type = :doc_type AND doc_id = :doc_id AND obj=:obj ORDER BY pos";
            $params = array(
                'doc_type' => $docType,
                'doc_id' => $docId,
                'obj' => $obj
            );
        } else {
            $sql = "SELECT * FROM " . $this->getTableName() . " WHERE doc_type = :doc_type AND doc_id = :doc_id ORDER BY pos";
            $params = array(
                'doc_type' => $docType,
                'doc_id' => $docId
            );
        }
        $docs = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($docs) && is_array($docs)) {
            foreach($docs as $doc) {
                $result[$doc['id']] = $doc;
            }
        }

        return $result;
    }

    public function getProdsForInvReturn($invReceiptId, $invReturnId)
    {

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE doc_type = :doc_type AND doc_id = :doc_id ORDER BY pos";
        $params = array(
            'doc_type' => $docType,
            'doc_id' => $docId
        );
        $docs = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($docs) && is_array($docs)) {
            foreach($docs as $doc) {
                $result[$doc['id']] = $doc;
            }
        }

        return $result;
    }

    public function getProdsHasBeenReceivedByInvoiceOnOrder($orderId)
    {

        $sql = "SELECT dp.prod_name, dp.pack_unit, dp.tmark_id, dp.pack_id, dp.in_amount, SUM(dp.amount) as amount
                    FROM " . $this->getTableName() . " AS dp
                    WHERE dp.doc_type = :docType AND dp.doc_id IN (SELECT id FROM " . $this->getTableName('invoice_receipt') . " AS ir WHERE ir.order_id = :orderId AND ir.status = :status)
                    GROUP BY dp.pack_id
                    HAVING amount < in_amount";
        $params = array(
            'docType' => self::DOC_TYPE_INVOICE_RECEIPT,
            'orderId' => $orderId,
            'status' => self::DOC_STATUS_APPROVED
        );
        $docs = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($docs) && is_array($docs)) {
            foreach($docs as $doc) {
                $result[$doc['pack_id']] = $doc;
            }
        }

        return $result;
    }

    public function save()
    {

        $data = array(
            'doc_type' => $this->doc_type,
            'doc_id' => $this->doc_id,
            'obj' => $this->obj,
            'pos' => $this->pos,
            'prod_name' => $this->prod_name,
            'tmark_id' => $this->tmark_id,
            'pack_id' => $this->pack_id,
            'unit' => $this->unit,
            'pack_unit' => $this->pack_unit,
            'in_amount' => empty($this->in_amount) ? null : $this->in_amount,
            'in_amount_unit' => empty($this->in_amount_unit) ? null : $this->in_amount_unit,
            'in_price' => empty($this->in_price) ? null : $this->in_price,
            'amount' => $this->amount,
            'amount_unit' => $this->amount_unit,
            'price' => $this->price,
            'exp_date' => $this->exp_date
        );
        if (empty($this->id)) {
            // добавим запись
            $this->getDB()->insert($this->getTableName(), $data);
            $this->id = $this->getDB()->getLastId();
        } else {
            // обновим запись
            $this->getDB()->update($this->getTableName(), $data,
                'id = :id',
                array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения продукта документа");
            return false;
        }

        return true;
    }

    public function delete()
    {
        if (empty($this->id)) {
            $this->setError('Неизвестный документ');
        } else {
            $this->getDB()->delete($this->getTableName(),
                'id = :id',
                array(
                    'id' => $this->id
                ));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка удаления продукта документа");
            return false;
        }

        return true;
    }

    /**
     * Удаление всех продуктов документа
     *
     * @return bool
     */
    public function deleteDocProds()
    {
        if (empty($this->doc_type) || empty($this->doc_id)) {
            $this->setError('Неизвестный документ');
        } else {
            $this->getDB()->delete($this->getTableName(),
                'doc_type = :doc_type AND doc_id = :doc_id',
                array(
                    'doc_type' => $this->doc_type,
                    'doc_id' => $this->doc_id
                ));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка удаления продуктов документа");
            return false;
        }

        return true;
    }

}
