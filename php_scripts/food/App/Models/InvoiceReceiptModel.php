<?php

namespace Food\App\Models;

use Food\App\Helpers\Converter;
use Food\Core\Model;

class InvoiceReceiptModel extends Model
{

    protected $tableName = 'invoice_receipt';

    public $id;
    public $trader_id;
    public $order_id;
    public $obj;
    public $inv_number;
    public $inv_date;
    public $inv_time_period;
    public $total;
    public $date_approve;
    public $status;

    const STATUS_OPEN = 0;
    const STATUS_APROVED = 1;



    public function get($id, $setParams = true)
    {
        if(empty($id)) {
            return null;
        }

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE id = :id";
        $params = array('id' => intval($id));
        $order = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($order) && is_array($order)) {
            $result = reset($order);
            if($setParams) {
                $this->setAttributes($result);
            }
        }

        return $result;
    }

    public function getAll()
    {
        if($this->obj == 'all') {
            $orders = $this->getDB()->query("SELECT * FROM " . $this->getTableName());
        } else {
            $sql = "SELECT * FROM " . $this->getTableName() . " WHERE obj = :obj";
            $params = array('obj' => $this->obj);
            $orders = $this->getDB()->query($sql, $params);
        }
        return $orders;
    }

    public function getForSend($id)
    {
        if(empty($id)) {
            return array();
        }

        $sql = "SELECT IFNULL(tro.fio, tr.fio) as fio, IFNULL(tro.phone, tr.phone) as phone, IFNULL(tro.email, tr.email) as email, o.date_shipment, o.delivery_time, o.id
                    FROM " . $this->getTableName() . " AS o
                    INNER JOIN " . $this->getTableName('trader') . " AS tr ON tr.id = o.trader_id
                    LEFT JOIN " . $this->getTableName('trader_obj') . " AS tro ON tro.trader_id = o.trader_id
                    WHERE o.id = :id";
        $params = array('id' => $id);
        $order = $this->getDB()->fetchRow($sql, $params);
        return $order;
    }

    public function getForTrader()
    {
        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE obj = :obj AND trader_id = :trader_id AND status = :status";
        $params = array(
            'obj' => $this->obj,
            'trader_id' => $this->trader_id,
            'status' => self::STATUS_APROVED
        );
        $invoices = $this->getDB()->query($sql, $params);

        return $invoices;
    }


    public function save()
    {
        $data = array(
            'trader_id' => $this->trader_id,
            'order_id' => $this->order_id,
            'obj' => $this->obj,
            'inv_number' => $this->inv_number,
            'inv_date' => $this->inv_date,
            'inv_time_period' => $this->inv_time_period,
            'total' => $this->total,
        );

        if (empty($this->id)) {
            // добавим запись
            $this->getDB()->insert($this->getTableName(), $data);
            $this->id = $this->getDB()->getLastId();
            if(empty($this->inv_number)) {
                $this->getDB()->update($this->getTableName(),
                    array(
                        'inv_number' => DocProdModel::PREFIX_INVOICE_RECEIPT . '-' . $this->id,
                    ),
                    'id = :id',
                    array('id' => $this->id));
            }
        } else {
            // обновим запись
            $this->getDB()->update($this->getTableName(), $data,
                'id = :id',
                array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения накладной");
            return false;
        }

        return true;
    }


    public function approve()
    {
        if (empty($this->id)) {
            return false;
        } else {

            $this->getDB()->update($this->getTableName(),
                array(
                    'date_approve' => time(),
                    'status' => self::STATUS_APROVED,
                ),
                'id = :id',
                array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка проводки накладной");
            return false;
        }

        return true;
    }

    public function getClimeProducts()
    {
        $sql = "SELECT dp.prod_name, dp.pack_unit, dp.in_price, dp.unit, dp.in_amount, dp.in_amount_unit, SUM(dp.amount) as amount, dp.amount_unit, IF(SUM(dp.amount) < dp.in_amount, 0, 1) as is_all_received
                    FROM " . $this->getTableName('doc_prod') . " AS dp
                    INNER JOIN " . $this->getTableName() . " AS ir ON ir.id = dp.doc_id AND ir.order_id = :orderId
                    WHERE dp.doc_type = :docType AND (ir.id = :docId OR ir.status = :status)
                    GROUP BY dp.pack_id, dp.amount_unit
                    HAVING dp.in_amount IS NOT NULL;";
        $params = array(
            'docType' => DocProdModel::DOC_TYPE_INVOICE_RECEIPT,
            'docId' => $this->id,
            'orderId' => $this->order_id,
            'status' => DocProdModel::DOC_STATUS_APPROVED
        );
        $prods = $this->getDB()->fetchAll($sql, $params);
        $isAllReceived = true;
        foreach ($prods as &$row) {
            $row['in_price'] = Converter::getPriceForView($row['in_price'], $row['pack_unit'], $row['in_amount_unit']);
            $row['in_amount'] = Converter::getAmountForView($row['in_amount'], $row['in_amount_unit']);
            $row['amount'] = Converter::getAmountForView($row['amount'], $row['amount_unit']);
            $isAllReceived &= $row['is_all_received'];
            unset($row['is_all_received']);
        }

        return array('isAllReceived' => $isAllReceived, 'prods' => $prods);

    }

}
