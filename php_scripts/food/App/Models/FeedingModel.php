<?php

namespace Food\App\Models;

use Food\Core\Json;
use Food\Core\Model;
use Food\Core\View;

class FeedingModel extends Model
{

    protected $tableName = 'feeding';

    const STATUS_PRESENT = 0;
    const STATUS_DELETED = 1;

    public $id;
    public $parent_id;
    public $pos;
    public $name;
    public $objs;
    public $status;




    public function get($id, $setParams = true)
    {
        global $businessobj;

        if(empty($id)) {
            return null;
        }

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE id = :id";
        $params = array('id' => $id);

        $feeding = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($feeding) && is_array($feeding)) {
            $result = reset($feeding);
            $objs = empty($result['objs']) ? array() : json_decode($result['objs'], true);
            $result['objs'] = array();
            foreach(array_keys($businessobj) as $bObj) {
                $result['objs'][$bObj] = in_array($bObj, $objs) ? 1 : 0;
            }
            if($setParams) {
                $this->setAttributes($result);
            }
        }

        return $result;
    }

    public function getAll()
    {
        global $businessobj;

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE status=" . self::STATUS_PRESENT . " ORDER BY pos ASC";

        $feeding = $this->getDB()->query($sql);
        $result = array();
        if(!empty($feeding) && is_array($feeding)) {
            foreach($feeding as $feed) {
                $objs = empty($feed['objs']) ? array() : json_decode($feed['objs'], true);
                $feed['objs'] = array();
                foreach(array_keys($businessobj) as $bObj) {
                    $feed['objs'][$bObj] = !empty($objs[$bObj]) ? 1 : 0;
                }
                $result[$feed['id']] = $feed;
            }
        }

        return $result;
    }

    public function saveCategory()
    {

        if(!empty($this->name)) {
            $this->getDB()->begin();
            try {
                if (empty($this->pos) && $this->parent_id > 0) {
                    $sql = "SELECT MAX(pos) FROM " . $this->getTableName() . " WHERE id = :id OR parent_id = :parent_id";
                    $params = array('parent_id' => $this->parent_id, 'id' => $this->parent_id);
                    $res = $this->getDB()->fetchColumn($sql, $params);
                    $this->pos = $res + 1;
                    $sql = "UPDATE " . $this->getTableName() . " SET pos = pos + 1 WHERE pos >= :pos";
                    $params = array('pos' => $this->pos);
                    $this->getDB()->execute($sql, $params);
                } elseif(empty($this->pos)) {
                    $sql = "SELECT MAX(pos) FROM " . $this->getTableName();
                    $res = $this->getDB()->fetchColumn($sql);
                    $this->pos = $res + 1;
                }

                $data = array(
                    'parent_id' => $this->parent_id,
                    'pos' => $this->pos,
                    'name' => $this->name,
                    'objs' => is_array($this->objs) ? json_encode($this->objs) : $this->objs,
                    'status' => self::STATUS_PRESENT,
                );

                if (empty($this->id)) {
                    // добавим запись
                    $this->getDB()->insert($this->getTableName(), $data);
                    $this->id = $this->getDB()->getLastId();
                } else {
                    // обновим запись
                    $this->getDB()->update($this->getTableName(), $data,
                        'id = :id',
                        array('id' => $this->id));
                }

                $this->getDB()->commit();

            } catch (\PDOException $e) {
                $this->getDB()->rollBack();
                $this->setError("Ошибка добавления категории");
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

    public function deleteCategory()
    {
        if(empty($this->id)) {
            $this->setError("Field 'id' is empty");
            return false;
        }

        $sql = "SELECT id FROM " . $this->getTableName() . " WHERE parent_id = :parent_id";
        $params = array(
            'parent_id' => $this->id
        );
        $res = $this->getDB()->fetchAll($sql,$params);
        $ids = array();
        if(!empty($res)) {
            foreach ($res as $row) {
                if ($this->canDeleteOrMove($row['id'])) {
                    $ids[] = $row['id'];
                }
            }
            // если все найденные категории можно удалять
            if(count($res) == count($ids)) {
                $ids[] = $this->id;
            }
        } else {
            // если у группы нет категорий, или это категория
            $ids[] = $this->id;
        }
        try {
            $this->getDB()->update($this->getTableName(),
                array('status' => self::STATUS_DELETED), 'id IN ('.implode(',', $ids).')');
        } catch (\PDOException $e) {
            $this->getDB()->rollBack();
            $this->setError("Ошибка удаления питающегося");
            return false;
        }

        return $ids;
    }

    public function canDeleteOrMove($id)
    {
        //TODO: Проверить может ли быть удален
        return true;
    }
}
