<?php

namespace Food\App\Models;

use Food\Core\Model;

class ProdTypeModel extends Model
{

    protected $tableName = 'prod_type';

    public $id;
    public $obj;
    public $main_id;
    public $group_id;
    public $type_pos;
    public $name;
    public $pack_unit;
    public $round_to_pack;
    public $life;
    public $temp_from;
    public $temp_to;
    public $delivery_possible;
    public $delivery_to_hour;
    public $deleted;



    public function get($id, $setParams = true)
    {
        if(empty($id)) {
            return null;
        }

        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE id = :id";
        $params = array('id' => $id);

        $types = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($types) && is_array($types)) {
            $result = reset($types);
            if($setParams) {
                $this->id = $result['id'];
                $this->obj = $result['obj'];
                $this->group_id = $result['group_id'];
                $this->type_pos = $result['type_pos'];
                $this->name = $result['name'];
                $this->pack_unit = $result['pack_unit'];
                $this->round_to_pack = $result['round_to_pack'];
                $this->life = $result['life'];
                $this->temp_from = $result['temp_from'];
                $this->temp_to = $result['temp_to'];
                $this->delivery_possible = $result['delivery_possible'];
                $this->delivery_to_hour = $result['delivery_to_hour'];
                $this->deleted = $result['deleted'];
            }
        }

        return $result;
    }

    public function getAll($obj)
    {
        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE obj = :obj ORDER BY group_id ASC, type_pos ASC";
        $params = array('obj' => $obj);

        $types = $this->getDB()->query($sql, $params);
        $result = array();
        if(!empty($types) && is_array($types)) {
            foreach($types as $type) {
                $result[$type['id']] = $type;
            }
        }

        return $result;
    }

    public function save()
    {
        if(empty($this->group_id)) {
            $this->setError("Field 'group_id' is empty");
        }
        if(empty($this->name)) {
            $this->setError("Field 'name' is empty");
        }
        if(empty($this->pack_unit)) {
            $this->setError("Field 'pack_unit' is empty");
        }

        if($this->hasErrors()) {
            return false;
        }

        if (empty($this->id)) {
            // добавим запись
            $this->getDB()->insert($this->getTableName(), array(
                'obj'               => $this->obj,
                'main_id'           => $this->main_id,
                'group_id'          => $this->group_id,
                'type_pos'          => $this->getLastPosition() + 1,
                'name'              => $this->name,
                'pack_unit'         => $this->pack_unit,
                'round_to_pack'     => $this->round_to_pack,
                'life'              => $this->life,
                'temp_from'         => $this->temp_from,
                'temp_to'           => $this->temp_to,
                'delivery_possible' => $this->delivery_possible,
                'delivery_to_hour'  => $this->delivery_to_hour,
            ));
            $this->id = $this->getDB()->getLastId();
        } else {
            // обновим запись
            $curData = $this->get($this->id, false);
            if($this->type_pos == null) {
                $this->type_pos = $curData['type_pos'];
            } elseif ($this->type_pos != $curData['type_pos']) {
                // перемещаем позиции, если старая и новая позиции отличаются
                // перемещаем на бОльшую позицию, смещаем все позиции от старой до новой позиции на единицу вверх и, на освободившееся место, вставляем новую позицию
                if($this->type_pos > $curData['type_pos']) {
                    $sql = "UPDATE " . $this->getTableName() . " SET type_pos = type_pos - 1 WHERE obj = :obj AND group_id = :group_id AND type_pos > :old_type_pos AND type_pos <= :new_type_pos";
                } else {
                    $sql = "UPDATE " . $this->getTableName() . " SET type_pos = type_pos + 1 WHERE obj = :obj AND group_id = :group_id AND type_pos >= :new_type_pos AND type_pos < :old_type_pos";
                }
                $params = array(
                    'obj' => $this->obj,
                    'group_id' => $this->group_id,
                    'old_type_pos' => $curData['type_pos'],
                    'new_type_pos' => $this->type_pos
                );
                $this->getDB()->execute($sql, $params);
            }
            $this->getDB()->update($this->getTableName(),
                array(
                    'obj'               => $this->obj,
                    'group_id'          => $this->group_id,
                    'type_pos'          => $this->type_pos,
                    'name'              => $this->name,
                    'pack_unit'         => $this->pack_unit,
                    'round_to_pack'     => $this->round_to_pack,
                    'life'              => $this->life,
                    'temp_from'         => $this->temp_from,
                    'temp_to'           => $this->temp_to,
                    'delivery_possible' => $this->delivery_possible,
                    'delivery_to_hour'  => $this->delivery_to_hour,
                ),
                'id = :id',
                array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения типа продукта");
            return false;
        }

        return true;
    }

    public function delete()
    {
        if(empty($this->id) || empty($this->obj)) {
            $this->setError("Field 'id' or 'obj' is empty");
            return false;
        }

        try {
            $this->getDB()->begin();

            $this->getDB()->update($this->getTableName(),
                array(
                    'deleted' => 1
                ),
                'obj = :obj AND id = :id',
                array('obj' => $this->obj, 'id' => $this->id));

            $this->getDB()->update($this->getTableName('prod_obj'),
                array(
                    'tmark_deleted' => 1,
                    'pack_deleted' => 1
                ),
                'obj = :obj AND type_id = :id',
                array('obj' => $this->obj, 'id' => $this->id));

            $this->getDB()->commit();

        } catch (\PDOException $e) {
            $this->getDB()->rollBack();
            $this->setError("Ошибка удаления типа продукта");
            return false;
        }

        return true;
    }

    public function undelete()
    {
        if(empty($this->id) || empty($this->obj)) {
            $this->setError("Field 'id' or 'obj' is empty");
            return false;
        }

        try {
            $this->getDB()->begin();

            $this->getDB()->update($this->getTableName(),
            array(
                'deleted' => 0
            ),
            'obj = :obj AND id = :id',
            array('obj' => $this->obj, 'id' => $this->id));

        $this->getDB()->update($this->getTableName('prod_obj'),
            array(
                'tmark_deleted' => 0,
                'pack_deleted' => 0
            ),
            'obj = :obj AND type_id = :id',
            array('obj' => $this->obj, 'id' => $this->id));

            $this->getDB()->commit();

        } catch (\PDOException $e) {
            $this->getDB()->rollBack();
            $this->setError("Ошибка восстановления типа продукта");
            return false;
        }

        return true;
    }

    public function changePos()
    {
        try {
            $this->getDB()->begin();

            $curData = $this->get($this->id, false);
            if($curData['group_id'] == $this->group_id) {
                // та же группа
                if ($this->type_pos > $curData['type_pos']) {
                    $sql = "UPDATE " . $this->getTableName() . " SET type_pos = type_pos - 1 WHERE obj = :obj AND type_pos > :old_type_pos AND type_pos <= :new_type_pos";
                } else {
                    $sql = "UPDATE " . $this->getTableName() . " SET type_pos = type_pos + 1 WHERE obj = :obj AND type_pos >= :new_type_pos AND type_pos < :old_type_pos";
                }
                $params = array(
                    'obj' => $this->obj,
                    'old_type_pos' => $curData['type_pos'],
                    'new_type_pos' => $this->type_pos
                );
                $this->getDB()->execute($sql, $params);
            } else {
                // другая группа
                // смещаем позиции в старой группе
                $sql = "UPDATE " . $this->getTableName() . " SET type_pos = type_pos - 1 WHERE obj = :obj AND group_id = :group_id AND type_pos > :type_pos";
                $params = array(
                    'obj' => $this->obj,
                    'group_id' => $curData['group_id'],
                    'type_pos' => $curData['type_pos']
                );
                $this->getDB()->execute($sql, $params);

                // смещаем позиции в новой группе
                $sql = "UPDATE " . $this->getTableName() . " SET type_pos = type_pos + 1 WHERE obj = :obj AND group_id = :group_id AND type_pos >= :type_pos";
                $params = array(
                    'obj' => $this->obj,
                    'group_id' => $this->group_id,
                    'type_pos' => $this->type_pos
                );
                $this->getDB()->execute($sql, $params);
            }

            $this->getDB()->update($this->getTableName(),
                array(
                    'type_pos' => $this->type_pos,
                    'group_id' => $this->group_id
                ),
                'id = :id AND obj = :obj',
                array(
                    'obj' => $this->obj,
                    'id' => $this->id
                )
            );

            $this->getDB()->update($this->getTableName('prod_obj'),
                array(
                    'group_id' => $this->group_id
                ),
                'type_id = :id AND obj = :obj',
                array(
                    'obj' => $this->obj,
                    'id' => $this->id
                )
            );

            $this->getDB()->commit();

        } catch (\PDOException $e) {
            $this->getDB()->rollBack();
            $this->setError("Ошибка перемещения вида продукта");
            return false;
        }
        return true;
    }

    public function getPosition()
    {
        $sql = "SELECT type_pos FROM " . $this->getTableName() . " WHERE obj=:obj AND id=:id";
        $res = $this->getDB()->fetchColumn($sql, array(
            'obj' => $this->obj,
            'id' => $this->id
        ));
        return isset($res['position']) ? $res['position'] : null;
    }

    public function getLastPosition()
    {
        $sql = "SELECT MAX(type_pos) as pos FROM " . $this->getTableName() . " WHERE obj=:obj AND group_id=:group_id";
        $res = $this->getDB()->fetchColumn($sql, array(
            'obj' => $this->obj,
            'group_id' => $this->group_id,
        ));
        return empty($res) ? 0 : $res;
    }

    private function setPosition()
    {
        //TODO: нужно делать. Не работает.
        return;
        $this->position = $this->getPosition();

        if (!empty($this->position)) {
            $this->getDB()->update($this->getTableName('position_type'), array(
                'obj' => $this->obj,
                'group_id' => $this->group_id,
                'type_id' => $this->id,
                'position' => $this->position,
            ));
        } else {
            $positions = $this->getLastPosition();
            $this->position = $positions + 1;
            $this->getDB()->insert($this->getTableName('position_type'), array(
                'obj' => $_GET['obj'],
                'group_id' => $this->group_id,
                'type_id' => $this->id,
                'position' => $this->position,
            ));
        }
        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения позиции типа продукта");
            return false;
        }
        return true;
    }
}
