<?php

namespace Food\App\Models;

use Food\Core\Model;

class ProdPackModel extends Model
{

    protected $tableName = 'prod_pack';

    public $id;
    public $obj;
    public $group_id;
    public $type_id;
    public $tmark_id;
    public $pack_pos;
    public $name;
    public $by_weight;
    public $value;
    public $pack_unit;
    public $smallest_unit_value;
    public $life;
    public $temp_from;
    public $temp_to;
    public $deleted;



    public function get($id, $setParams = true)
    {
        if(empty($id)) {
            return null;
        }

        $sql = "SELECT pk.*, p_obj.group_id, p_obj.type_id, p_obj.obj, p_obj.tmark_id, p_obj.pack_pos, p_obj.pack_deleted 
                    FROM " . $this->getTableName() . " AS pk
                    INNER JOIN " . $this->getTableName('prod_obj') . " AS p_obj ON p_obj.pack_id = pk.id
                    WHERE pk.id = :id AND p_obj.obj = :obj";
        $params = array('id' => $id, 'obj' => $this->obj );

        $packs = $this->getDB()->query($sql, $params);

        $result = array();
        if(!empty($packs) && is_array($packs)) {
            $result = reset($packs);
            if($setParams) {
                $this->id = $result['id'];
                $this->obj = $result['obj'];
                $this->group_id = $result['group_id'];
                $this->type_id = $result['type_id'];
                $this->tmark_id = $result['tmark_id'];
                $this->pack_pos = $result['pack_pos'];
                $this->name = $result['name'];
                $this->by_weight = $result['by_weight'];
                $this->value = $result['value'];
                $this->pack_unit = $result['pack_unit'];
                $this->smallest_unit_value = $result['smallest_unit_value'];
                $this->life = $result['life'];
                $this->temp_from = $result['temp_from'];
                $this->temp_to = $result['temp_to'];
                $this->deleted = $result['pack_deleted'];
            }
        }

        return $result;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM " . $this->getTableName();

        $packs = $this->getDB()->query($sql);
        $result = array();
        if(!empty($packs) && is_array($packs)) {
            foreach($packs as $pack) {
                $result[$pack['id']] = $pack;
            }
        }
        return $result;
    }

    public function getAllIdForProduct($product_id)
    {
        $sql = "SELECT id FROM " . $this->getTableName() . " WHERE product_id = :id ";

        $ids = $this->getDB()->query($sql, array('id' => $product_id));
        return $ids;
    }

    public function save()
    {
        if(empty($this->group_id)) {
            $this->setError("Field 'group_id' is empty");
        }
        if(empty($this->type_id)) {
            $this->setError("Field 'type_id' is empty");
        }
        if(empty($this->tmark_id)) {
            $this->setError("Field 'tmark_id' is empty");
        }
        if(empty($this->pack_unit)) {
            $this->setError("Field 'pack_unit' is empty");
        }

        if($this->hasErrors()) {
            return false;
        }

        if (empty($this->id)) {
            // добавим запись
            $this->getDB()->insert($this->getTableName(), array(
                'name'              => $this->name,
                'by_weight'         => $this->by_weight,
                'value'             => $this->value,
                'pack_unit'         => $this->pack_unit,
                'smallest_unit_value' => $this->getSmallestUnitValue($this->value, $this->pack_unit),
                'life'              => $this->life,
                'temp_from'         => $this->temp_from,
                'temp_to'           => $this->temp_to,
            ));
            $this->id = $this->getDB()->getLastId();

            // добавим запись в таблицу продуктов обьекта
            // найдем связку обьект + группа + вид с пустым полем торговой марки
            $sql = "SELECT id FROM " . $this->getTableName('prod_obj') . " WHERE obj=:obj AND group_id=:group_id AND type_id=:type_id AND tmark_id=:tmark_id AND pack_id IS NULL";
            $params = array(
                'obj' => $this->obj,
                'group_id' => $this->group_id,
                'type_id' => $this->type_id,
                'tmark_id' => $this->tmark_id
            );
            $prodObjId = $this->getDB()->fetchColumn($sql, $params);
            // найдена строка с пустым полем упаковки
            if($prodObjId) {    // обновляем запись
                $this->getDB()->update($this->getTableName('prod_obj'),
                    array(
                        'pack_id' => $this->id,
                        'pack_pos' => $this->getLastPosition() + 1
                    ),
                    'id = :id',
                    array('id' => $prodObjId)
                );
            } else {    // добаляем новую упаковку
                $this->getDB()->insert($this->getTableName('prod_obj'),
                    array(
                        'obj' => $this->obj,
                        'group_id'  => $this->group_id,
                        'type_id'   => $this->type_id,
                        'tmark_id'  => $this->tmark_id,
                        'tmark_pos' => $this->getTMarkPosition(),
                        'pack_id'   => $this->id,
                        'pack_pos'  => $this->getLastPosition() + 1
                    )
                );
            }

        } else {
            // обновим запись
            $this->getDB()->update($this->getTableName(),
                array(
                    'name'              => $this->name,
                    'by_weight'         => $this->by_weight,
                    'value'             => $this->value,
                    'pack_unit'         => $this->pack_unit,
                    'smallest_unit_value' => $this->getSmallestUnitValue($this->value, $this->pack_unit),
                    'life'              => $this->life,
                    'temp_from'         => $this->temp_from,
                    'temp_to'           => $this->temp_to,
                ),
                'id = :id',
                array('id' => $this->id));
        }

        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения упаковки");
            return false;
        }
        return true;
    }

    public function delete()
    {
        if(empty($this->id) || empty($this->obj)) {
            $this->setError("Field 'id' or 'obj' is empty");
            return false;
        }

        $this->getDB()->update($this->getTableName('prod_obj'),
            array(
                'pack_deleted' => 1
            ),
            'obj = :obj AND pack_id = :id',
            array('obj' => $this->obj, 'id' => $this->id));

        if($this->hasErrors()) {
            $this->setError("Ошибка удаления упаковки");
            return false;
        }
        return true;
    }

    public function undelete()
    {
        if(empty($this->id) || empty($this->obj)) {
            $this->setError("Field 'id' or 'obj' is empty");
            return false;
        }

        $this->getDB()->update($this->getTableName('prod_obj'),
            array(
                'pack_deleted' => 0
            ),
            'obj = :obj AND pack_id = :id',
            array('obj' => $this->obj, 'id' => $this->id));

        if($this->hasErrors()) {
            $this->setError("Ошибка восстановления упаковки");
            return false;
        }
        return true;
    }

    public function changePos()
    {
        try {
            $this->getDB()->begin();

            $curData = $this->getDB()->fetchRow("SELECT * FROM " . $this->getTableName('prod_obj') . " WHERE obj = :obj AND pack_id = :pack_id",
                array('pack_id' => $this->id, 'obj' => $this->obj)
            );
            if($curData['tmark_id'] == $this->tmark_id) {
                // тот же вид
                if ($this->pack_pos > $curData['pack_pos']) {
                    $sql = "UPDATE " . $this->getTableName('prod_obj') . " SET pack_pos = pack_pos - 1 WHERE obj = :obj AND tmark_id = :tmark_id AND pack_pos > :old_pack_pos AND pack_pos <= :new_pack_pos";
                } else {
                    $sql = "UPDATE " . $this->getTableName('prod_obj') . " SET pack_pos = pack_pos + 1 WHERE obj = :obj AND tmark_id = :tmark_id AND pack_pos >= :new_pack_pos AND pack_pos < :old_pack_pos";
                }
                $params = array(
                    'obj' => $this->obj,
                    'tmark_id' => $this->tmark_id,
                    'old_pack_pos' => $curData['pack_pos'],
                    'new_pack_pos' => $this->pack_pos
                );
                $this->getDB()->execute($sql, $params);
            }

            $this->getDB()->update($this->getTableName('prod_obj'),
                array(
                    'pack_pos' => $this->pack_pos
                ),
                'pack_id = :id AND obj = :obj',
                array(
                    'obj' => $this->obj,
                    'id' => $this->id
                )
            );

            $this->getDB()->commit();

        } catch (\PDOException $e) {
            $this->getDB()->rollBack();
            $this->setError("Ошибка перемещения упаковки: " . $e->getMessage() . $e->getTraceAsString());
            return false;
        }
        return true;
    }

//    public function getPosition()
//    {
//        return null;
//        $sql = "SELECT position FROM " . $this->getTableName('position_pack') . " WHERE obj=:obj AND pack_id=:pack_id";
//        $res = $this->getDB()->fetchColumn($sql, array(
//            'obj' => $_GET['obj'],
//            'pack_id' => $this->id
//        ));
//        return isset($res['position']) ? $res['position'] : null;
//    }

    public function getTMarkPosition()
    {
        $sql = "SELECT tmark_pos FROM " . $this->getTableName('prod_obj') . " WHERE obj=:obj AND group_id=:group_id AND type_id=:type_id AND tmark_id=:tmark_id";
        $res = $this->getDB()->fetchColumn($sql, array(
            'obj' => $this->obj,
            'group_id' => $this->group_id,
            'type_id' => $this->type_id,
            'tmark_id' => $this->tmark_id
        ));
        return empty($res) ? 0 : $res;
    }

    public function getLastPosition()
    {
        $sql = "SELECT MAX(pack_pos) FROM " . $this->getTableName('prod_obj') . " WHERE obj=:obj AND group_id=:group_id AND type_id=:type_id AND tmark_id=:tmark_id";
        $res = $this->getDB()->fetchColumn($sql, array(
            'obj' => $this->obj,
            'group_id' => $this->group_id,
            'type_id' => $this->type_id,
            'tmark_id' => $this->tmark_id
        ));
        return empty($res) ? 0 : $res;
    }

    private function setPosition()
    {
        return null;
        $this->position = $this->getPosition();

        if (!empty($this->position)) {
            $this->getDB()->update($this->getTableName('position_pack'), array(
                'obj' => $_GET['obj'],
                'pack_id' => $this->id,
                'position' => $this->position,
            ));
        } else {
            $positions = $this->getLastPosition();
            $this->position = $positions + 1;
            $this->getDB()->insert($this->getTableName('position_pack'), array(
                'obj' => $_GET['obj'],
                'pack_id' => $this->id,
                'position' => $this->position,
            ));
        }
        if($this->hasErrors()) {
            $this->setError("Ошибка сохранения позиции упаковки");
            return false;
        }
        return true;
    }

    public static function getSmallestUnitValue($value, $unit)
    {
        $coef = array(
            'г' => 1, 'кг' => 1000,
            'мл' => 1, 'л' => 1000,
            'шт' => 1, 'дес' => 10
        );
        return isset($coef[$unit]) ? $value * $coef[$unit] : $value;
    }
}
