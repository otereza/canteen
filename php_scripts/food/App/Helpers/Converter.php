<?php


namespace Food\App\Helpers;


class Converter
{
    const PRICE_COEFF = 1000;     // цену храним целым числом за упаковку (или за ед.изм, если "на вес") с точностью до 3-х знаков

    /**
     * Получение цены для вывода пользователю
     * Цена в базе данных хранится как целое число за упаковку с точностью до 3-х знаков
     *
     * @param $price - цена
     * @param $packUnit - фасовка
     * @param $amountUnit - фасовка или ед.изм, за которую указана цена
     * @return float|int
     */
    public static function getPriceForDb($price, $packUnit, $amountUnit = null)
    {
        if (!empty($price) && preg_match('/([\d]+)?(.*)/', $packUnit, $matches)) {
            $unitValue = empty($matches[1]) ? 1 : $matches[1];
            if ($amountUnit && $packUnit != $amountUnit) {
                $price = intval($price * $unitValue * self::PRICE_COEFF);
            } else {
                $price = intval($price * self::PRICE_COEFF);
            }
        }

        return $price;
    }

    /**
     * Получение цены для вывода пользователю
     * Цена в базе данных хранится как целое число за упаковку с точностью до 3-х знаков
     *
     * @param $price - цена
     * @param $packUnit - фасовка
     * @param $amountUnit - фасовка или ед.изм, за которую указана цена
     * @return float|int
     */
    public static function getPriceForView($price, $packUnit, $amountUnit = null)
    {
        if (!empty($price) && preg_match('/([\d]+)?(.*)/', $packUnit, $matches)) {
            $unitValue = empty($matches[1]) ? 1 : $matches[1];
            if ($amountUnit && $packUnit != $amountUnit) {
                $price = round($price / $unitValue / self::PRICE_COEFF, (strlen($unitValue) - 1 + 2));
            } else {
                $price = round($price / self::PRICE_COEFF, 2);
            }
        }

        return $price;
    }


    /**
     * Преобразование количества для записи в базу данных
     * В базе данных количество хранится в минимальных единицах измерения
     *
     * @param $amount
     * @param $amountUnit
     * @return float|int
     */
    public static function getAmountForDb($amount, $amountUnit)
    {
        if(!empty($amount) && preg_match('/([\d]+)?(.*)/', $amountUnit, $matches)) {
            $unitValue = empty($matches[1]) ? 1 : $matches[1];
            $unit = $matches[2];
            $amount = $amount * $unitValue * Units::getSmallest($unit);
        }
        return $amount;
    }

    /**
     * Преобразование количества для вывода пользователю
     * В базе данных количество хранится в минимальных единицах измерения
     *
     * @param $amount
     * @param $amountUnit
     * @return float|int
     */
    public static function getAmountForView($amount, $amountUnit)
    {
        if(!empty($amount) && preg_match('/([\d]+)?(.*)/', $amountUnit, $matches)) {
            $unitValue = empty($matches[1]) ? 1 : $matches[1];
            $unit = $matches[2];
            $amount = $amount / $unitValue / Units::getSmallest($unit);
        }
        return $amount;
    }

}