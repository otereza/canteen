<?php
/**
 * Created by PhpStorm.
 * User: terezao
 * Date: 26.03.19
 * Time: 22:12
 */

namespace Food\App\Helpers;


use Food\Core\Db;

class Units
{
    static $units;

    private static function init()
    {
        if(!self::$units) {
            $db = Db::getInstance();
            $units = $db->query("SELECT * FROM d_food_units");
            foreach ($units as $unit) {
                $unit['step'] = pow(0.1, $unit['decimal_places']);
                self::$units[$unit['name']] = $unit;
            }
        }
        return self::$units;
    }

    public static function getAll()
    {
        self::init();
        return self::$units;
    }

    // получить id единицы измерения
    public static function getId($name)
    {
        self::init();
        return isset(self::$units[$name]) ? self::$units[$name]['id'] : null;
    }

    // получить множитель для меньшей единицы измерения (для г - 1000, для кг - 1)
    public static function getSmallest($name)
    {
        self::init();
        return isset(self::$units[$name]) ? self::$units[$name]['to_smallest'] : 1;
    }

    // получить множитель для большей единицы измерения (для г - 1000, для кг - 1)
    public static function getLargest($name)
    {
        self::init();
        return isset(self::$units[$name]) ? self::$units[$name]['to_largest'] : 1;
    }

    // получить количество знаков после запятой для единицы измерения
    public static function getDecimal($name)
    {
        self::init();
        return isset(self::$units[$name]) ? self::$units[$name]['decimal_places'] : null;
    }

    // получить шаг изменения поля типа number для единицы измерения
    public static function getStep($name)
    {
        self::init();
        return isset(self::$units[$name]) ? self::$units[$name]['step'] : null;
    }


    // получить список единиц измерения
    public static function getUnits()
    {
        self::init();
        return array_keys(self::$units);
    }

    // получить список единиц измерения группы
    public static function getGroupUnits($name)
    {
        $groupUnits = array();
        self::init();
        if(isset(self::$units[$name])) {
            $group = self::$units[$name]['group'];
            $groupUnits = array_filter(self::$units, function($item) use ($group) {
                return $item['group'] == $group;
            });
        };
        return $groupUnits;
    }

    // получить группу единицы измерения (вес, обьем, количество)
    public static function getUnitGroup($name)
    {
        self::init();
        return isset(self::$units[$name]) ? self::$units[$name]['group'] : null;
    }

    // получить шаг изминения количества
    public static function getUnitStep($name)
    {
        self::init();
        return isset(self::$units[$name]) ? pow(0.1, self::$units[$name]['decimal_places']) : null;
    }
}