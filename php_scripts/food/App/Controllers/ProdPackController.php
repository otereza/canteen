<?php

namespace Food\App\Controllers;

use Food\App\Models\LockTablesModel;
use Food\App\Models\ProdGroupModel;
use Food\App\Models\ProdPackModel;
use Food\App\Models\ProdTypeModel;
use Food\App\Models\Repositories\ProductRep;
use Food\Core\Json;
use Food\Core\View;
use Food\Core\Controller;
use Food\App\Models\ProdTMarkModel;


class ProdPackController extends Controller
{

    public function saveAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(empty($_POST['group_id'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty group_id';
            $page = new Json($res);
        }

        if(empty($_POST['type_id'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty type_id';
            $page = new Json($res);
        }

        if(empty($_POST['tmark_id'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty tmark_id';
            $page = new Json($res);
        }

        if(empty($_POST['pack_unit'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty pack_unit';
            $page = new Json($res);
        }

        if(empty($_POST['pack_value']) && !empty($_POST['by_weight'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty pack_value';
            $page = new Json($res);
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
        }

        $model = new ProdPackModel();
        $model->id = isset($_POST['id']) ? intval($_POST['id']) : null;
        $model->obj = $this->obj;
        $model->group_id = $_POST['group_id'];
        $model->type_id = $_POST['type_id'];
        $model->tmark_id = $_POST['tmark_id'];
        $model->pack_pos = isset($_POST['position']) ? $_POST['position'] : null;
        $model->name = stripslashes($_POST['name']);
        $model->pack_unit = $_POST['pack_unit'];
        $model->by_weight = isset($_POST['by_weight']) ? true : false;
        $model->value = $model->by_weight ? null : $_POST['pack_value'];
        $model->life = isset($_POST['life']) ? $_POST['life'] : null;
        $model->temp_from = isset($_POST['temp_from']) ? $_POST['temp_from'] : null;
        $model->temp_to = isset($_POST['temp_to']) ? $_POST['temp_to'] : null;
        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);

    }

    public function editAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(empty($_POST['id'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty id';
            $page = new Json($res);
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
        }

        $model = new ProdPackModel();
        $model->id = $_POST['id'];
        $model->obj = $this->obj;
        if (!$model->get($model->id)) {
            $res['error'] = 1;
            $res['content'] = $model->getErrors();
        } else {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $model->id;
            $lockTablesModel->userId = $user->uid;
            $userId = $lockTablesModel->getUserBlockedTable();
            $res['error'] = 0;
            $res['content'] = $model;
            if(!empty($userId) && $userId != $user->uid) {
                $screenName = $user->getScreenName(array($userId));
                $res['content']->lock = "Данные редактируются другим пользователем: " . $screenName[0];
            } else {
                $lockTablesModel->setLock();
            }
        }
        $page = new Json($res);
    }

    public function deleteAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(empty($_POST['id'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty id';
            $page = new Json($res);
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
        }

        $model = new ProdPackModel();
        $model->id = intval($_POST['id']);
        $model->obj = $this->obj;
        if($model->id && $this->canDelete()) {
            if(!$model->delete()) {
                $res['error'] = 1;
                $res['content'] = $model->getErrors();
            } else {
                $res['content'] = 'ok';
            }
        }

        $page = new Json($res);
    }

    private function canDelete()
    {
        //TODO: Проверки на возможность удалить упаковку
        return true;
    }

    public function undeleteAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(empty($_POST['id'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty id';
            $page = new Json($res);
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
        }

        $model = new ProdPackModel();
        $model->id = intval($_POST['id']);
        $model->obj = $this->obj;
        if(!$model->undelete()) {
            $res['error'] = 1;
            $res['content'] = $model->getErrors();
        } else {
            $res['content'] = 'ok';
        }

        $page = new Json($res);
    }

    public function changePosAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(empty($_POST['id'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty id';
            $page = new Json($res);
        }

        if(empty($_POST['pos'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty pos';
            $page = new Json($res);
        }

        if(empty($_POST['tmark_id'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty tmark_id';
            $page = new Json($res);
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
        }

        $model = new ProdPackModel();
        $model->id = $_POST['id'];
        $model->pack_pos = $_POST['pos'];
        $model->tmark_id = $_POST['tmark_id'];
        $model->obj = $this->obj;
        if(!$model->changePos()) {
            $res['error'] = 1;
            $res['content'] = $model->getErrors();
        } else {
            $res['content'] = 'ok';
        }
        $page = new Json($res);
    }

    public function lockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new ProdPackModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            $userId = $lockTablesModel->getUserBlockedTable();
            if($userId && $userId != $user->uid) {
                $screenName = $user->getScreenName(array($userId));
                $res['content']['lock'] = "Данные редактируются другим пользователем: " . $screenName[0];
            } else {
                if ($lockTablesModel->setLock()) {
                    $res['error'] = 0;
                    $res['content'] = $lockTablesModel;
                } else {
                    $res['error'] = 1;
                    $res['content'] = $lockTablesModel->getErrors();
                }
            }
        }
        $page = new Json($res);
    }

    public function unlockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new ProdPackModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            if(!$lockTablesModel->unLock()) {
                $res['error'] = 0;
                $res['content'] = $lockTablesModel;
            } else {
                $res['error'] = 1;
                $res['content'] = $lockTablesModel->getErrors();
            }
        }
        $page = new Json($res);
    }

}
