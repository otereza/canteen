<?php

namespace Food\App\Controllers;

use Food\Core\Controller;
use Food\Core\Json;
use Food\Core\View;

/**
 * Home controller
 *
 */
class HomeController extends Controller
{

    public function indexAction()
    {
        global $user;
        View::render('home/index', array('user' => $user->screenName));
    }


    /**
     * For DEBUG mode
     */
    public function savePermitAction()
    {
        global $page, $FILES_ROOT;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error: obj Empty");
            $page = new Json($res);
            return;
        }

        $data = array();
        $userPermitFile = $FILES_ROOT . '/userpermits.txt';
        if(file_exists($userPermitFile)) {
            $data = file_get_contents($userPermitFile);
            if(!empty($data)) {
                $data = json_decode($data, true);
            }
        }

        unset($data[$this->obj]);
        if(isset($_POST['permit'])) {
            foreach ($_POST['permit'] as $permit => $changed) {
                $data[$this->obj][$permit] = 1;
            }
        }

        if(file_put_contents($userPermitFile, json_encode($data)) === false) {
            $res['error'] = 1;
            $res['content'] = array("Save Error: Ошибка записи файла");
        }

        $page = new Json($res);

    }

}
