<?php


namespace Food\App\Controllers;


use Food\App\Helpers\Converter;
use Food\App\Helpers\Units;
use Food\App\Models\DishesModel;
use Food\App\Models\FeedingDishModel;
use Food\App\Models\FeedingModel;
use Food\App\Models\Repositories\RecipesRep;
use Food\App\Models\Repositories\TechMapProdRep;
use Food\Core\Controller;
use Food\Core\Json;
use Food\Core\View;

class DishesController extends Controller
{

    public function indexAction()
    {
        global $page, $user, $businessobj;

        View::setTitle('Готовые блюда');

        View::cssFile('jstree/themes/default/style.min.css');
        View::jsFile('app/dashes.js', View::TO_END);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditReadyDishes' => $user->havePermit(311, $this->obj),
            'canEditTechnologicalMap' => $user->havePermit(312, $this->obj),
        );
        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        $dishesModel = new DishesModel();
        $groups = $dishesModel->getAllGroups();
        $list = $dishesModel->getList($this->obj);
        if(empty($_GET['id'])) {
            View::render('dishes/index', array(
                'obj' => $this->obj,
                'groups' => empty($groups) ? array() : $groups,
                'list' => empty($list) ? array() : $list,
                'permit' => $permit
            ));
        } else {
            $for = null;
            if(!empty($_GET['for']) || $this->obj != 'all') {
                $for = isset($_GET['for']) ? $_GET['for'] : $this->obj;
                View::jsText('var tripleState = true;', View::TO_END);
            }
            View::jsText('var treeWithCheckboxes = true;', View::TO_END);
            View::jsText('var dataUrl = "trader/getData";', View::TO_END);
            View::jsText('var businessobj = ' . json_encode($businessobj) . ';' , View::TO_END);
            View::render('dishes/edit', array(
                'obj' => $this->obj,
                'groups' => empty($groups) ? array() : $groups,
                'permit' => $permit
            ));
        }

        return;
    }

    public function newCollectionRecipesAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('name');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $RecipesModel = new RecipesRep();
        if(!($id = $RecipesModel->save($_POST['name']))) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$RecipesModel->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $id;
        }
        $page = new Json($res);
        return;
    }


    public function newDishGroupAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('name');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $dishesModel = new DishesModel();
        if(!($id = $dishesModel->saveGroup(stripslashes($_POST['name'])))) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$dishesModel->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $id;
        }
        $page = new Json($res);
        return;
    }

    public function newAction()
    {

        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('name');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $dishesModel = new DishesModel();
        $groupId = $_POST['group'];
        if(empty($_POST['group']) && !empty($_POST['newGroupName'])) {
            $groupId = $dishesModel->addGroup($_POST['newGroupName']);
        }
        $dishesModel->dish_name = stripslashes($_POST['name']);
        $dishesModel->group_id = $groupId;

        if(!$dishesModel->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$dishesModel->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $dishesModel;
        }
        $page = new Json($res);
        return;
    }

    public function editAction()
    {
        global $page, $user, $businessobj, $ACTIONPAGE;

        View::setTitle('Новое блюдо');

        View::cssFile('jstree/themes/default/style.min.css');
        View::jsFile('app/dashes.js', View::TO_END);
//        View::jsFile('tinymce/tinymce.min.js', View::TO_END);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditDishes' => $user->havePermit(311, $this->obj),
            'canEditTechnologicalMap' => $user->havePermit(312, $this->obj),
        );
        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        $for = null;
        if(!empty($_GET['for']) || $this->obj != 'all') {
            $for = isset($_GET['for']) ? $_GET['for'] : $this->obj;
        }

        $dishId = $_GET['id'];

        // параметры блюда
        $dishesModel = new DishesModel();
        $groups = $dishesModel->getAllGroups();
        $dishesModel->obj = $this->obj;
        $dish = $dishesModel->get($dishId);
        if($this->obj == 'all' && !empty($for)) {
            $objDish = $dishesModel->getForObj($dishId, empty($for) ? $this->obj : $for);
        }

        // технологическая карта
        $techMapProdRep = new TechMapProdRep();
        $prods = $techMapProdRep->getProds($dishId, 'all');
        if(($this->obj == 'all' && !empty($for)) || $this->obj != 'all') {
            $mapRows = $techMapProdRep->getProds($dishId, empty($for) ? $this->obj : $for);
            if(empty($mapRows)) {
                $mapRows = $techMapProdRep->copyFromAllIntoObj($dishId, empty($for) ? $this->obj : $for);
            }

            if($this->obj == 'all') {
                $forCheckMapAll = array_map(function ($item) {
                    unset($item['id'], $item['obj']);
                    return $item;
                }, array_values($prods));
                $forCheckMapObj = array_map(function ($item) {
                    unset($item['id'], $item['obj']);
                    return $item;
                }, array_values($mapRows));
                $objDish['isChangedTechMap'] = md5(json_encode($forCheckMapAll)) != md5(json_encode($forCheckMapObj));
            }
        } else {
            $mapRows = $prods;
        }

        // список книг рецептов
        $recipesRep = new RecipesRep();
        $collectRecipes = $recipesRep->getCollect();

        // категории питающихся
        $feedingModel = new FeedingModel();
        $feeds = $feedingModel->getAll();

        // выход блюда для категорий питающихся
        $feedingDishModel = new FeedingDishModel();
        $feedingDishModel->dish_id = $dishId;
        $feedingDishModel->obj = empty($for) ? $this->obj : $for;
        $feedingDishOutlet = $feedingDishModel->getAll();

        $feedingDishModel->obj = 'all';
        $feedingDishOutletAll = $feedingDishModel->getAll();

        $feeding_dish_outlet = array();
        $standart_calculation_output = empty($objDish['standart_calculation_output']) ? $dish['standart_calculation_output'] : $objDish['standart_calculation_output'];
        $standart_working_output = empty($objDish['standart_working_output']) ? $dish['standart_working_output'] : $objDish['standart_working_output'];
        foreach ($feeds as $feed) {
            if(isset($feedingDishOutlet[$feed['id']])) {
                $feeding_dish_outlet[$feed['id']] = $feedingDishOutlet[$feed['id']];
                if($this->obj == 'all') {
                    $feeding_dish_outlet[$feed['id']]['isChangedCalculation'] = isset($feedingDishOutletAll[$feed['id']]['calculation']) && $feedingDishOutlet[$feed['id']]['calculation'] != $feedingDishOutletAll[$feed['id']]['calculation'];
                    $feeding_dish_outlet[$feed['id']]['isChangedWorking'] = isset($feedingDishOutletAll[$feed['id']]['working']) && $feedingDishOutlet[$feed['id']]['working'] != $feedingDishOutletAll[$feed['id']]['working'];
                }
            } elseif(!empty($feed['objs'][$feedingDishModel->obj])) {
                $feeding_dish_outlet[$feed['id']] = array(
                    'dish_id' => $dishId,
                    'obj' => empty($for) ? $this->obj : $for,
                    'feeding_id' => $feed['id'],
                    'calculation' => $standart_calculation_output,
                    'working' => $standart_working_output,
                );
            } elseif($this->obj == 'all') {
                $feeding_dish_outlet[$feed['id']] = array(
                    'dish_id' => $dishId,
                    'obj' => $this->obj,
                    'feeding_id' => $feed['id'],
                    'calculation' => $standart_calculation_output,
                    'working' => $standart_working_output,
                );
            }
        }

        // параметры для обьектов JS
        View::jsText('var businessobj = ' . json_encode($businessobj) . ';' , View::TO_END);
        View::jsText('window.dish = ' . json_encode($dish) . ';'
            . 'window.dish.items = ' . (empty($mapRows) ? '{}' : json_encode($mapRows)) . ';'
            . 'window.dish.objs = ' . (empty($dish['objs']) ? '{}' : json_encode($dish['objs'])) . ';'
            . 'window.dish.outlet = ' . (empty($feeding_dish_outlet) ? '{}' : json_encode($feeding_dish_outlet)) . ';'
            . 'window.packUnits = ' . json_encode(Units::getAll()) . ';', View::TO_END);

        View::render('dishes/edit', array(
//            'obj' => $this->obj,
            'objDish' => empty($objDish) ? array() : $objDish,
            'dish' => $dish,
            'groups' => $groups,
            'rows' => $mapRows,
            'recipes' => $collectRecipes,
            'feeds' => $feeds,
            'feeding_dish_outlet' => $feeding_dish_outlet,
            'permit' => $permit
        ));

        return;
    }



    public function saveProdAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

//        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $reqFields = array('dish_id', 'map_type', 'pos', 'prod_name', 'unit');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new TechMapProdRep();
        $model->id = isset($_POST['id']) ? $_POST['id'] : null;
        $model->obj = empty($_POST['for']) ? $this->obj : $_POST['for'];
        $model->dish_id = $_POST['dish_id'];
        $model->map_type = $_POST['map_type'];
        $model->pos = $_POST['pos'];
        $model->prod_name = stripslashes($_POST['prod_name']);
        $model->unit = $_POST['unit'];
        $model->brutto = isset($_POST['brutto']) ? $_POST['brutto'] : null;
        $model->netto = isset($_POST['netto']) ? $_POST['netto'] : null;

        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $model->brutto = Converter::getAmountForView($model->brutto, $model->unit);
            $model->netto = Converter::getAmountForView($model->netto, $model->unit);
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);

    }

    // копирование строк с калькуляционной карты
    public function copyCalculationMapProdAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('rows', 'dish_id');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        if (is_array($_POST['rows'])) {
            $model = new TechMapProdRep();
            $model->obj = $this->obj;
            if($model->deleteWorkMapProds($_POST['dish_id'])) {
                $result = array();
                // обновим данные по калькуляционной карте
                foreach ($_POST['rows'] as $row) {
                    $model = new TechMapProdRep();
                    $model->id = $row['id'];
                    $model->obj = $this->obj;
                    $model->dish_id = $row['dish_id'];
                    $model->map_type = $row['map_type'];
                    $model->pos = $row['pos'];
                    $model->prod_name = stripslashes($row['prod_name']);
                    $model->unit = $row['unit'];
                    $model->brutto = isset($row['brutto']) ? $row['brutto'] : null;
                    $model->netto = isset($row['netto']) ? $row['netto'] : null;

                    if (!$model->save()) {
                        $res['error'] = 1;
                        $res['content'] = array("Save Error", $model->getErrors());
                        $page = new Json($res);
                        return;
                    }
                }
                // добавим данные по рабочей карте
                foreach ($_POST['rows'] as $row) {
                    $model = new TechMapProdRep();
                    $model->obj = $this->obj;
                    $model->dish_id = $row['dish_id'];
                    $model->map_type = TechMapProdRep::TECH_MAP_TYPE_WORK;
                    $model->pos = $row['pos'];
                    $model->prod_name = stripslashes($row['prod_name']);
                    $model->unit = $row['unit'];
                    $model->brutto = isset($row['brutto']) ? $row['brutto'] : null;
                    $model->netto = isset($row['netto']) ? $row['netto'] : null;

                    if (!$model->save()) {
                        $res['error'] = 1;
                        $res['content'] = array("Save Error", $model->getErrors());
                        $page = new Json($res);
                        return;
                    } else {
                        $result[] = $model;
                    }
                }
                $res['content'] = array(
                    'rows' => $result
                );
            } else {
                $res['error'] = 1;
                $res['content'] = array("Delete Error", $model->getErrors());
            }
        } else {
            $res['error'] = 1;
            $res['content'] = "Not data for copy";
        }
        $page = new Json($res);

    }

    public function deleteProdAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('id');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new TechMapProdRep();
        $model->id = $_POST['id'];

        if(!$model->delete()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);
    }

    public function saveAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('id', 'group_id', 'dish_name', 'objs', 'collection_recipe_id', 'standart_calculation_output', 'standart_working_output', 'output_unit', 'items','outlet');
        $for = null;
        if(!empty($_GET['for']) || $this->obj != 'all') {
            $for = isset($_GET['for']) ? $_GET['for'] : $this->obj;
            $reqFields = array('id', 'group_id', 'dish_name');
        }

        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        // сохраним блюдо
        $dishesModel = new DishesModel();
        $dishesModel->id = isset($_POST['id']) ? $_POST['id'] : null;
        $dishesModel->obj = $this->obj;
        $dishesModel->group_id = $_POST['group_id'];
        $dishesModel->dish_name = $_POST['dish_name'];
        $dishesModel->objs = is_array($_POST['objs']) ? json_encode($_POST['objs']) : null;
        $dishesModel->collection_recipe_id = $_POST['collection_recipe_id'];
        $dishesModel->recipe_number = isset($_POST['recipe_number']) ? $_POST['recipe_number'] : null;
        $dishesModel->creator_technological_map = isset($_POST['creator_technological_map']) ? $_POST['creator_technological_map'] : null;
        $dishesModel->standart_calculation_output = $_POST['standart_calculation_output'];
        $dishesModel->standart_working_output = $_POST['standart_working_output'];
        $dishesModel->output_unit = $_POST['output_unit'];
        $dishesModel->cooking = isset($_POST['cooking']) ? stripslashes($_POST['cooking']) : null;

        if($this->obj == 'all') {
            $saveRes = $dishesModel->save();
        } else {
            $saveRes = $dishesModel->saveForObj($this->obj);
        }

        if(!$saveRes) {
            $res['error'] = 1;
            $res['content'] = array("Save Error", $dishesModel->getErrors());
        } else {
            // сохраним продукты
            foreach ($_POST['items'] as $row) {
                $model = new TechMapProdRep();
                $model->id = $row['id'];
                $model->obj = empty($for) ? $this->obj : $for;;
                $model->dish_id = $row['dish_id'];
                $model->map_type = $row['map_type'];
                $model->pos = $row['pos'];
                $model->prod_name = stripslashes($row['prod_name']);
                $model->unit = $row['unit'];
                $model->brutto = isset($row['brutto']) ? $row['brutto'] : null;
                $model->netto = isset($row['netto']) ? $row['netto'] : null;

                if (!$model->save()) {
                    $res['error'] = 1;
                    $res['content'] = array("Save Error", $model->getErrors());
                    $page = new Json($res);
                    return;
                }
            }

            // сохраним выходы блюда по категориям питающихся
            if(isset($_POST['outlet']) && is_array($_POST['outlet'])) {
                foreach ($_POST['outlet'] as $row) {
                    $model = new FeedingDishModel();
                    $model->dish_id = $_POST['id'];
                    $model->obj = empty($for) ? $this->obj : $for;;
                    $model->feeding_id = $row['feeding_id'];
                    $model->calculation = $row['calculation'];
                    $model->working = $row['working'];
                    $model->unit = $_POST['output_unit'];

                    if (!$model->save()) {
                        $res['error'] = 1;
                        $res['content'] = array("Save Error", $model->getErrors());
                        $page = new Json($res);
                        return;
                    }
                }
            }

            $res['error'] = 0;
            $res['content'] = $dishesModel;
        }
        $page = new Json($res);

    }

}