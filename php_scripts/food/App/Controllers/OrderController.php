<?php

namespace Food\App\Controllers;

use Food\App\Helpers\Converter;
use Food\App\Models\LockTablesModel;
use Food\App\Models\OrderModel;
use Food\App\Models\OrderProdModel;
use Food\App\Models\ProdObjectModel;
use Food\App\Models\Repositories\ProductRep;
use Food\App\Models\Repositories\Units;
use Food\App\Models\TraderModel;
use Food\App\Models\TraderObjModel;
use Food\App\Models\TraderPriceModel;
use Food\Core\Json;
use Food\Core\View;
use Food\Core\Controller;


class OrderController extends Controller
{
    public function indexAction()
    {
        global $page, $user, $businessobj;

        View::setTitle('Заявки');

        View::jsFile('app/order.js', View::TO_END);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditFullProducts' => $user->havePermit(301, $this->obj),
            'canEditTmarkPackProducts' => $user->havePermit(302, $this->obj),
            'canEditTrader' => $user->havePermit(303, $this->obj),
            'canCheckedProduct' => $user->havePermit(305, $this->obj),
            'canChangePriceProduct' => $user->havePermit(306, $this->obj),
            'canEditOrder' => $user->havePermit(307, $this->obj),
        );
        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        $model = new OrderModel();
        $model->obj = $this->obj;
        $allOrders = $model->getAll();
        $orders = array();
        foreach ($allOrders as $order) {
            if($order['state'] == OrderModel::STATE_DRAFT) {
                $orders['draft'][$order['id']] = $order;
            } elseif($order['state'] == OrderModel::STATE_WORK) {
                $orders['work'][$order['id']] = $order;
            } elseif($order['state'] == OrderModel::STATE_CLOSED) {
                $orders['close'][$order['id']] = $order;
            }
        }

        $model = new TraderModel();
        $model->obj = $this->obj;
        $traders = $model->getAll();
        $objTraders = array();
        if($this->obj != 'all') {
            foreach ($traders as $key => $trader) {
                $trader['business_obj'] = json_decode($trader['business_obj'], true);
                if (in_array($this->obj, $trader['business_obj'])) {
                    $objTraders[$key] = $trader;
                }
            }
        }

        View::render('orders/index', array(
            'obj' => $this->obj,
            'orders' => $orders,
            'objTraders' => $objTraders,
            'traders' => $traders,
            'permit' => $permit
        ));

        return;
    }

    public function editAction()
    {
        global $page, $user, $businessobj;

        View::setTitle('Заявки');

        View::jsFile('app/order.js', View::TO_END);
        View::jsText(
            'var tripleState = false;' .
            'var treeWithCheckboxes = false;' .
            'var dataUrl = "order/getData";', View::TO_HEAD);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        if(empty($_GET['id'])) {
            View::render('error404', array(
                'error' => 'Неизвестный номер заявки'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditFullProducts' => $user->havePermit(301, $this->obj),
            'canEditTmarkPackProducts' => $user->havePermit(302, $this->obj),
            'canEditTrader' => $user->havePermit(303, $this->obj),
            'canCheckedProduct' => $user->havePermit(305, $this->obj),
            'canChangePriceProduct' => $user->havePermit(306, $this->obj),
            'canEditOrder' => $user->havePermit(307, $this->obj),
        );
        if(!empty($_GET['for'])) {
            $permit['canChangeObjPriceProduct'] = $user->havePermit(306, $_GET['for']);
        }

        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        $model = new OrderModel();
        $model->obj = $this->obj;
        $order = $model->get($_GET['id']);

        if(empty($order['trader_id'])) {
            View::render('error404', array(
                'error' => 'Заявка ' . $_GET['id'] . ' не найдена'
            ));
            return;
        }

        $model = new TraderModel();
        $model->obj = $this->obj;
        $trader = $model->get($order['trader_id'], false);

        if(empty($trader['id'])) {
            View::render('error404', array(
                'error' => 'У заявки неизвестный поставщик ' . $order['trader_id']
            ));
            return;
        }
        $deliveryDays = $model->deliveryDays($trader['id']);

        $model = new TraderObjModel();
        $model->obj = empty($_GET['for']) ? $this->obj : $_GET['for'];
        $discount = $model->getDiscount($trader['id']);
        $priceCoef = 1 - ($discount / 100);
        $order['priceCoef'] = $priceCoef;

        $model = new OrderProdModel();
        $model->obj = $this->obj;
        $productRows = $model->getAllForOrder($order['id']);

        $priceModel = new TraderPriceModel();
        $priceModel->obj = empty($_GET['for']) ? $this->obj : $_GET['for'];
        $priceModel->trader_id = $order['trader_id'];
        $prices = $priceModel->getAllWithMain();

        $rows = array();
        foreach ($productRows as $row) {
            $unitValue = preg_replace('/[^\d]+/', '', $row['pack_unit']);
            $unitValue = empty($unitValue) ? 1 : $unitValue;
            $userTraderPrice = sprintf('%.2f', Converter::getPriceForView($row['price'], $row['pack_unit']) / $priceCoef);

            $rows[$row['id']] = $row;
            $rows[$row['id']]['price'] = Converter::getPriceForView($row['price'], $row['pack_unit'], $row['amount_unit']);
            $rows[$row['id']]['amount'] = Converter::getAmountForView($row['amount'], $row['amount_unit']);
            $rows[$row['id']]['unit_value'] = $unitValue;
            $rows[$row['id']]['trader_price'] = $prices[$row['pack_id']]['trader_price'];
            $rows[$row['id']]['trader_price_user'] = $userTraderPrice;
            $rows[$row['id']]['trader_price_main'] = $prices[$row['pack_id']]['trader_price_main'];
        }

//dd($rows);
        $view = 'orders/draft';
        if($order['state'] == OrderModel::STATE_WORK) {
            $view = 'orders/work';
        } elseif($order['state'] == OrderModel::STATE_CLOSED) {
            $view = 'orders/closed';
        }

        View::jsText('window.order = ' . json_encode($order) . ';'
                   . 'window.order.items = ' . (empty($rows) ? '{}' : json_encode($rows)) . ';'
                   . 'window.prices = ' . json_encode($prices) . ';', View::TO_END);

        View::render($view, array(
            'obj' => $this->obj,
            'trader' => $trader,
            'order' => $order,
            'rows' => $rows,
            'deliveryDays' => $deliveryDays,
            'permit' => $permit
        ));

    }

    public function getDataAction()
    {

        global $page, $businessobj;

        if(!$this->isRightObject()) {
            View::render('products/index', array(
                'error' => $this->getError()
            ));
            return;
        }

        // если данные для конкретного объекта
        if($this->obj == 'all' && (isset($_GET['for']) && array_key_exists($_GET['for'], $businessobj)) ) {
            $forObj = $_GET['for'];
        } else {
            $forObj = null;
        }

        $model = new ProductRep();
        $tree = $model->getTree($this->obj, 'trader', $forObj);
        $page = new Json($tree );
    }

    public function getTraderOrdersAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('trader_id', 'forObj');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => $this->getError()
            ));
            return;
        }

        $model = new OrderModel();
        $model->obj = $_POST['forObj'];
        $model->trader_id = $_POST['trader_id'];
        $orders = $model->getForTrader();
        $traderOrders = array();
        foreach ($orders as $order) {
            $traderOrders[] = array(
                'id' => $order['id'],
                'name' => 'Заявка от ' . date('d.m.Y', $order['date_shipment']) . ' №' . $order['id'],
                'delivery_time' => json_decode($order['delivery_time'], true),
                'total' => $order['total']
            );
        }

        if($model->hasErrors()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $traderOrders;
        }
        $page = new Json($res);
    }


    public function newAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('trader_id', 'obj');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new OrderModel();
        $model->trader_id = $_POST['trader_id'];
        $model->obj = $_POST['obj'];
        $model->state = 0;
        $model->date_open = time();

        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);

    }

    public function saveProdAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('order_id', 'prod_name','tmark_id', 'pack_id', 'unit', 'pack_unit', 'amount_unit', 'amount', 'price');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new OrderProdModel();
        $model->id = isset($_POST['id']) ? $_POST['id'] : null;
        $model->order_id = $_POST['order_id'];
        $model->pos = isset($_POST['pos']) ? $_POST['pos'] : null ;
        $model->prod_name = stripslashes($_POST['prod_name']);
        $model->tmark_id = $_POST['tmark_id'];
        $model->pack_id = $_POST['pack_id'];
        $model->unit = $_POST['unit'];
        $model->pack_unit = $_POST['pack_unit'];
        $model->amount_unit = $_POST['amount_unit'];
        $model->amount = Converter::getAmountForDb($_POST['amount'], $_POST['amount_unit']);
        $model->price = Converter::getPriceForDb($_POST['price'], $_POST['pack_unit'], $_POST['amount_unit']);

        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $model->amount = Converter::getAmountForView($model->amount, $model->amount_unit);
            $model->price = Converter::getPriceForView($model->price, $model->pack_unit, $model->amount_unit);
            $res['error'] = 0;
            $res['content'] = $model;

            //  добавим продукт в список продуктов обьекта
            if($this->obj != 'all') {
                $prodObjModel = new ProdObjectModel();
                $saved = $prodObjModel->addProductForObj($_POST['pack_id'], $this->obj);
                if (!$saved) {
                    $res['error'] = 1;
                    $res['content'] = array("Ошибка записи продукта в список продуктов обьектов", $prodObjModel->getErrors());
                }
            }

        }
        $page = new Json($res);
    }

    public function updateProdAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('id');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new OrderProdModel();
        $model->id = $_POST['id'];
        unset($_POST['id']);
        if(!$model->update($_POST)) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);
    }


    public function delProdAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('id');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new OrderProdModel();
        $model->id = $_POST['id'];

        if(!$model->delete()) {
            $res['error'] = 1;
            $res['content'] = array("Delete Error",$model->getErrors());
        }
        $page = new Json($res);

    }


    public function saveAction()
    {
        global $page, $businessobj;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('id');
        if(empty($_POST['preSave'])) {
            $reqFields = array('id', 'date_shipment', 'delivery_time', 'total', 'items');
        }
        foreach ($reqFields as $field) {
            if (empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new OrderModel();
        $model->get($_POST['id']);
        $model->date_shipment = $_POST['date_shipment'];
        $model->delivery_time = json_encode($_POST['delivery_time']);
        $model->total = $_POST['total'];
        if(empty($_POST['preSave'])) {
            $model->date_send = time();
            $model->state = OrderModel::STATE_WORK;
        }

        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;

            // обновим продукты
            if(!empty($_POST['items'])) {
                foreach($_POST['items'] as $row) {
                    $prodModel = new OrderProdModel();
                    $prodModel->id = $row['id'];
                    $prodModel->order_id = $row['order_id'];
                    $prodModel->pos = $row['pos'];
                    $prodModel->prod_name = stripslashes($row['prod_name']);
                    $prodModel->tmark_id = $row['tmark_id'];
                    $prodModel->pack_id = $row['pack_id'];
                    $prodModel->unit = $row['unit'];
                    $prodModel->pack_unit = $row['pack_unit'];
                    $prodModel->amount_unit = $row['amount_unit'];
                    $prodModel->amount = Converter::getAmountForDb($row['amount'], $row['amount_unit']);
                    $prodModel->price = Converter::getPriceForDb($row['price'], $row['pack_unit'], $row['amount_unit']);
                    if(!$prodModel->save()) {
                        $res['error'] = 1;
                        $res['content'] = array("Save Error",$prodModel->getErrors());
                    }
                }
            }

            if(empty($_POST['preSave'])) {
                $this->foodSend($_POST['id']);
            }
        }

        $page = new Json($res);

    }

    public function saveInfoAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('orderId', 'dateShipment', 'deliveryTime');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new OrderModel();
        $model->get($_POST['orderId']);
        $model->date_shipment = $_POST['dateShipment'];
        $model->delivery_time = json_encode($_POST['deliveryTime']);

        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);

    }

    public function changeStateAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('orderId', 'state');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new OrderModel();
        $model->get($_POST['orderId']);
        $model->state = $_POST['state'];
//        dd($model);
        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);
    }

    public function lockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new TraderModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            $userId = $lockTablesModel->getUserBlockedTable();
            if($userId && $userId != $user->uid) {
                $screenName = $user->getScreenName(array($userId));
                $res['content']['lock'] = "Данные редактируются другим пользователем: " . $screenName[0];
            } else {
                if ($lockTablesModel->setLock()) {
                    $res['error'] = 0;
                    $res['content'] = $lockTablesModel;
                } else {
                    $res['error'] = 1;
                    $res['content'] = $lockTablesModel->getErrors();
                }
            }
        }
        $page = new Json($res);
    }

    public function unlockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new TraderModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            if($lockTablesModel->unLock()) {
                $res['error'] = 0;
                $res['content'] = $lockTablesModel;
            } else {
                $res['error'] = 1;
                $res['content'] = $lockTablesModel->getErrors();
            }
        }
        $page = new Json($res);
    }

    // вызов функции внешнего модуля
    private function foodSend($orderId)
    {
        if(function_exists('model_dispatches')) {
            $model = new OrderModel();
            $order = $model->getForSend($orderId);
            if (!empty($order)) {
                $params = array(
                    'fio' => $order['fio'],
                    'phone' => $order['phone'],
                    'email' => $order['email']
                );

                $orderProdModel = new OrderProdModel();
                $prods = $orderProdModel->getAllForSendOrder($orderId);
                $params['items'] = $prods;

                $params['date'] = date('d.m.Y', $order['date_shipment']);
                $delivery_time = json_decode($order['delivery_time'], true);
                $params['time'] = $delivery_time['from'] . '-' . $delivery_time['to'];
                $params['order'] = $order['id'];

                model_dispatches('food_send', $params);
            }
        }
    }
}
