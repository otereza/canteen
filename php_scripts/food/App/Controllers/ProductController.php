<?php

namespace Food\App\Controllers;

use Food\App\Models\ProdGroupModel;
use Food\App\Models\ProdPackModel;
use Food\App\Models\ProdTypeModel;
use Food\App\Models\Repositories\ProductRep;
use Food\Core\Json;
use Food\Core\View;
use Food\Core\Controller;
use Food\App\Models\ProdTMarkModel;


class ProductController extends Controller
{
    public function indexAction()
    {
        global $user;

        View::setTitle('Продукты питания');

        View::cssFile('jstree/themes/default/style.min.css');
        View::jsFile('jstree/jstree.js', View::TO_END);
        View::jsText('var dataUrl = "product/getData";', View::TO_END);
        View::jsFile('app/product.js', View::TO_END);
        View::jsFile('app/forms/products.js', View::TO_END);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditFullProducts' => $user->havePermit(301, $this->obj),
            'canEditTmarkPackProducts' => $user->havePermit(302, $this->obj),
        );
        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        View::render('products/tree', array());
    }

    public function getDataAction()
    {

        global $page;

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => $this->getError()
            ));
            return;
        }

        $model = new ProductRep();
        $tree = $model->getTree($this->obj);
        $page = new Json($tree );
    }

}
