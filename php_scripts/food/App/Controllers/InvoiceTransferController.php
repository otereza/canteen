<?php

namespace Food\App\Controllers;

use Food\App\Models\DocProdModel;
use Food\App\Models\InvoiceTransferModel;
use Food\App\Models\LockTablesModel;
use Food\App\Models\Repositories\ProductRep;
use Food\App\Models\TraderModel;
use Food\App\Models\WarehouseModel;
use Food\App\Helpers\Converter;
use Food\Core\Json;
use Food\Core\View;
use Food\Core\Controller;


class InvoiceTransferController extends Controller
{
    public function indexAction()
    {
        global $page, $user, $businessobj;

        View::setTitle('Список Накладных (перемещение)');

        View::jsFile('app/invoiceTransfer.js', View::TO_END);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditInvoice' => $user->havePermit(308, $this->obj),
        );
        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        $model = new InvoiceTransferModel();
        $invoices = $model->getAll($this->obj);

        $stateInvoices = array();
        foreach ($invoices as $invoice) {
            if($invoice['status'] == InvoiceTransferModel::STATUS_DRAFT) {
                $stateInvoices['draft'][] = $invoice;           // черновики
            } elseif($invoice['status'] == InvoiceTransferModel::STATUS_NEED_APPROVE) {
                $invoice['canEditInvoiceObjFrom'] = $user->havePermit(308, $invoice['obj_from']);
                $invoice['canEditInvoiceObjTo'] = $user->havePermit(308, $invoice['obj_to']);
                if($this->obj == 'all' || $this->obj == $invoice['obj_to'])
                $stateInvoices['needApprove'][] = $invoice;     // требуют подтверждения получения
            } elseif($invoice['status'] == InvoiceTransferModel::STATUS_APROVED) {
                if($this->obj == $invoice['obj_from']) {
                    $stateInvoices['approvedFrom'][] = $invoice;    // отгруженнные и проведенные накладные
                } elseif($this->obj == $invoice['obj_to']) {
                        $stateInvoices['approvedTo'][] = $invoice;    // отгруженнные и проведенные накладные
                } else {
                    // показываем одну и ту же проведенную накладную для отгруженных и полученных
                    $stateInvoices['approvedFrom'][] = $invoice;    // отгруженнные и проведенные накладные
                    $stateInvoices['approvedTo'][] = $invoice;      // полученные и проведенные накладные
                }
            }
        }

        View::render('invoice/transfer/index', array(
            'obj' => $this->obj,
            'list' => $stateInvoices,
            'permit' => $permit
        ));

        return;
    }

    /**
     * @throws \Food\App\Exceptions\ConvertException
     */
    public function editAction()
    {
        global $page, $user, $businessobj;

        View::setTitle('Накладная (перемещения)');

        View::cssFile('jstree/themes/default/style.min.css');
        View::jsFile('jstree/jstree.js', View::TO_END);
        View::jsFile('jstree/jstreegrid.js', View::TO_END);
        View::jsFile('app/invoiceTransfer.js', View::TO_END);
        View::jsText('var tripleState = false;', View::TO_END);
        View::jsText('var treeWithCheckboxes = false;', View::TO_END);
        View::jsText('var dataUrl = "trader/getData";', View::TO_END);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        if(empty($_GET['id'])) {
            View::render('error404', array(
                'error' => 'Неизвестный номер Приходной Накладной'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditInvoice' => $user->havePermit(308, $this->obj),
        );


        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        $model = new InvoiceTransferModel();
        $invoice = $model->get($_GET['id']);
        // права на проводку с/на обьект
        $invoice['canEditInvoiceObjFrom'] = $user->havePermit(308, $invoice['obj_from']);
        $invoice['canEditInvoiceObjTo'] = $user->havePermit(308, $invoice['obj_to']);

        $warehouseModel = new WarehouseModel();
        $prods = $warehouseModel->getProdsGroupByPackId(isset($_GET['for']) ? $_GET['for'] : $this->obj);
        $warehoseProds = array();
        foreach ($prods as $prod) {
            $warehoseProds[$prod['pack_id']] = $prod;
        }

        $prodDocModel = new DocProdModel();
        $productRows = $prodDocModel->getProds(DocProdModel::DOC_TYPE_INVOICE_TRANSFER, $_GET['id']);
        foreach ($productRows as &$row) {
            $row['in_amount'] = isset($warehoseProds[$row['pack_id']]) ? Converter::getAmountForView($warehoseProds[$row['pack_id']]['amount'], $row['pack_unit']) : 0;
            $row['inTotalPrice'] = isset($warehoseProds[$row['pack_id']]) ? Converter::getPriceForView($warehoseProds[$row['pack_id']]['price'], $row['pack_unit']) : 0;
            $row['amount'] = Converter::getAmountForView($row['amount'], $row['pack_unit']);
            $row['price'] = Converter::getPriceForView($row['price'], $row['pack_unit']);
        }

        View::jsText('window.invoice = ' . json_encode($invoice) . ';'
            . 'window.invoice.items = ' . (empty($productRows) ? '{}' : json_encode($productRows)) . ';'
            . 'window.docType = ' . DocProdModel::DOC_TYPE_INVOICE_TRANSFER . ';', View::TO_END);

        View::render('invoice/transfer/edit', array(
            'invoice' => $invoice,
            'rows' => $productRows,
            'permit' => $permit
        ));

    }

    public function viewAction()
    {
        global $page, $user, $businessobj;

        View::setTitle('Накладная (перемещения)');

        View::cssFile('jstree/themes/default/style.min.css');
        View::jsFile('app/invoiceTransfer.js', View::TO_END);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        if(empty($_GET['id'])) {
            View::render('error404', array(
                'error' => 'Неизвестный номер Приходной Накладной'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditInvoice' => $user->havePermit(308, $this->obj),
        );


        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        $model = new InvoiceTransferModel();
        $invoice = $model->get($_GET['id']);
        // права на проводку с/на обьект
        $invoice['canEditInvoiceObjFrom'] = $user->havePermit(308, $invoice['obj_from']);
        $invoice['canEditInvoiceObjTo'] = $user->havePermit(308, $invoice['obj_to']);

        $warehouseModel = new WarehouseModel();
        $prods = $warehouseModel->getProdsGroupByPackId(isset($_GET['for']) ? $_GET['for'] : $this->obj);
        $warehoseProds = array();
        foreach ($prods as $prod) {
            $warehoseProds[$prod['pack_id']] = $prod;
        }

        $prodDocModel = new DocProdModel();
        $productRows = $prodDocModel->getProds(DocProdModel::DOC_TYPE_INVOICE_TRANSFER, $_GET['id']);
        foreach ($productRows as &$row) {
            $row['in_amount'] = isset($warehoseProds[$row['pack_id']]) ? Converter::getAmountForView($warehoseProds[$row['pack_id']]['amount'], $row['pack_unit']) : 0;
            $row['inTotalPrice'] = isset($warehoseProds[$row['pack_id']]) ? Converter::getPriceForView($warehoseProds[$row['pack_id']]['price'], $row['pack_unit']) : 0;
            $row['amount'] = Converter::getAmountForView($row['amount'], $row['pack_unit']);
            $row['price'] = Converter::getPriceForView($row['price'], $row['pack_unit']);
        }

        View::jsText('window.invoice = ' . json_encode($invoice) . ';'
            . 'window.invoice.items = ' . (empty($productRows) ? '{}' : json_encode($productRows)) . ';'
            . 'window.docType = ' . DocProdModel::DOC_TYPE_INVOICE_TRANSFER . ';', View::TO_END);

        View::render('invoice/transfer/view', array(
            'invoice' => $invoice,
            'rows' => $productRows,
            'permit' => $permit
        ));

    }


    public function getDataAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $warehouseModel = new WarehouseModel();
        $prods = $warehouseModel->getProdsGroupByPackId($this->obj);
        $addingPackParams = array();
        if(!empty($prods) && is_array($prods)) {
            foreach($prods as $prod) {
                $amount = Converter::getAmountForView($prod['amount'], $prod['pack_unit']);
                $price = Converter::getPriceForView($prod['price'], $prod['pack_unit']);
                $addingPackParams[$prod['pack_id']]['amount'] = (string) $amount;
                $addingPackParams[$prod['pack_id']]['price'] = (string) $price;
                $addingPackParams[$prod['pack_id']]['exp_date'] = (string) $prod['exp_date'];
            }
        }

        $model = new ProductRep();
        $model->addingPackParams = $addingPackParams;
        $tree = $model->getTree($this->obj);
//        dd($tree);
        $page = new Json($tree);

    }


    public function newAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('objFrom', 'objTo');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new InvoiceTransferModel();
        $model->obj_from = $_POST['objFrom'];
        $model->obj_to = $_POST['objTo'];
        $model->inv_date = time();
        if((date('i') > 30)) {
            $timeFrom = date('H:i', mktime(date('H') + date('I'), 30));
            $timeTo = date('H:i', mktime(date('H') + date('I') + 1, 00));
        } else {
            $timeFrom = date('H:i', mktime(date('H') + date('I'), 00));
            $timeTo = date('H:i', mktime(date('H') + date('I'), 30));
        }
        $model->inv_time_period = $timeFrom . ' - ' . $timeTo;

        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);
    }

    public function saveAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('id', 'inv_number', 'inv_date', 'inv_time_period', 'obj_from', 'obj_to');
        if(empty($_POST['preSave'])) {
            $reqFields = array('id', 'inv_number', 'inv_date', 'inv_time_period', 'total', 'items', "canEditInvoiceObjTo");
        }
        foreach ($reqFields as $field) {
            if (empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $invoiceTransferModel = new InvoiceTransferModel();
        $invoiceTransferModel->get($_POST['id']);
        $invoiceTransferModel->obj_from = $_POST['obj_from'];
        $invoiceTransferModel->obj_to = $_POST['obj_to'];
        $invoiceTransferModel->inv_number = $_POST['inv_number'];
        $invoiceTransferModel->inv_date = $_POST['inv_date'];
        $invoiceTransferModel->inv_time_period = $_POST['inv_time_period'];
        $invoiceTransferModel->reason = empty($_POST['reason']) ? '' : stripslashes($_POST['reason']);
        $invoiceTransferModel->total = $_POST['total'];

        if(!$invoiceTransferModel->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$invoiceTransferModel->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $invoiceTransferModel;

            // обновим продукты
            if(!empty($_POST['items'])) {
                foreach($_POST['items'] as $row) {
                    $prodModel = new DocProdModel();
                    $prodModel->id = $row['id'];
                    $prodModel->doc_type = DocProdModel::DOC_TYPE_INVOICE_TRANSFER;
                    $prodModel->doc_id = $_POST['id'];
                    $prodModel->obj = $this->obj;
                    $prodModel->pos = $row['pos'];
                    $prodModel->prod_name = stripslashes($row['prod_name']);
                    $prodModel->tmark_id = $row['tmark_id'];
                    $prodModel->pack_id = $row['pack_id'];
                    $prodModel->unit = $row['unit'];
                    $prodModel->pack_unit = $row['pack_unit'];
                    $prodModel->in_amount = Converter::getAmountForDb($row['in_amount'], $row['amount_unit']);
                    $prodModel->amount = Converter::getAmountForDb($row['amount'], $row['amount_unit']);
                    $prodModel->amount_unit = $row['amount_unit'];
                    $prodModel->price = Converter::getPriceForDb($row['price'], $row['pack_unit'], $row['amount_unit']);
                    $prodModel->exp_date = $row['exp_date'];
                    if(!$prodModel->save()) {
                        $res['error'] = 1;
                        $res['content'] = array("Save Error", $prodModel->getErrors());
                    }
                }
                if(!empty($_POST['approve'])) {
                    if(!$this->approveFrom($invoiceTransferModel, $_POST['items'])) {
                        $res['error'] = 1;
                        $res['content'] = array("Approve Error", $this->getError());
                    }
                }
            }

        }

        $page = new Json($res);

    }

    /**
     * @param $docModel InvoiceTransferModel
     * @param $items
     * @return bool
     */
    public function approveFrom($docModel, $items)
    {
        // перед сохранением, удалим все продукты данного документа со склада. Чтобы не дублировались
        $warehouseModel = new WarehouseModel();
        $warehouseModel->trans_doc_type = DocProdModel::DOC_TYPE_INVOICE_TRANSFER;
        $warehouseModel->trans_doc_id = $docModel->id;
        $warehouseModel->obj = $docModel->obj_from;

        // временное решение для тестов
        $warehouseModel->deleteDocProds();

        foreach($items as $row) {
            $warehouseModel = new WarehouseModel();
            $warehouseModel->trans_doc_type = DocProdModel::DOC_TYPE_INVOICE_TRANSFER;
            $warehouseModel->trans_doc_id = $docModel->id;
            $warehouseModel->trans_date = time();
            $warehouseModel->obj = $docModel->obj_from;
            $warehouseModel->prod_name = stripslashes($row['prod_name']);
            $warehouseModel->tmark_id = $row['tmark_id'];
            $warehouseModel->pack_id = $row['pack_id'];
            $warehouseModel->unit = $row['unit'];
            $warehouseModel->pack_unit = $row['pack_unit'];
            $warehouseModel->amount = Converter::getAmountForDb($row['amount'], $row['amount_unit']);
            $warehouseModel->price = Converter::getPriceForDb($row['price'], $row['pack_unit'], $row['amount_unit']);
            $warehouseModel->exp_date = $row['exp_date'];
            $warehouseModel->takeProduct();
            if($warehouseModel->hasErrors()) {
                $this->setError($warehouseModel->getErrors());
                return false;
            }
        }
        $docModel->approve(InvoiceTransferModel::STATUS_NEED_APPROVE);

        return true;
    }

    public function approveDistAction()
    {

        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('doc_id', 'obj_from', 'obj_to');
        foreach ($reqFields as $field) {
            if (empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $invoiceModel = new InvoiceTransferModel();
        $invoiceModel->get($_POST['doc_id']);

        // перед сохранением, удалим все продукты данного документа со склада. Чтобы не дублировались
        $warehouseModel = new WarehouseModel();
        $warehouseModel->trans_doc_type = DocProdModel::DOC_TYPE_INVOICE_TRANSFER;
        $warehouseModel->trans_doc_id = $_POST['doc_id'];
        $warehouseModel->obj = $_POST['obj_to'];
        $warehouseModel->deleteDocProds();

        // получим списанные продукты накладной перемещения для добавление на склад получателя
        $prodModel = new DocProdModel();
        $items = $prodModel->getProds(DocProdModel::DOC_TYPE_INVOICE_TRANSFER, $_POST['doc_id'], $_POST['obj_from']);

        foreach($items as $row) {
            $warehouseModel = new WarehouseModel();
            $warehouseModel->trans_doc_type = DocProdModel::DOC_TYPE_INVOICE_TRANSFER;
            $warehouseModel->trans_doc_id = $_POST['doc_id'];
            $warehouseModel->trans_date = time();
            $warehouseModel->obj = $_POST['obj_to'];
            $warehouseModel->prod_name = stripslashes($row['prod_name']);
            $warehouseModel->tmark_id = $row['tmark_id'];
            $warehouseModel->pack_id = $row['pack_id'];
            $warehouseModel->unit = $row['unit'];
            $warehouseModel->pack_unit = $row['pack_unit'];
            $warehouseModel->amount = $row['amount'];
            $warehouseModel->price = $row['price'];
            $warehouseModel->exp_date = $row['exp_date'];
            $warehouseModel->putProduct();
            if($warehouseModel->hasErrors()) {
                $this->setError($warehouseModel->getErrors());
                $res['error'] = 1;
                $res['content'] = array("Approve Error", $this->getError());
                $page = new Json($res);
                return;
            }
        }
        $invoiceModel->approve(InvoiceTransferModel::STATUS_APROVED);

        $page = new Json($res);
    }


    public function lockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new TraderModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            $userId = $lockTablesModel->getUserBlockedTable();
            if($userId && $userId != $user->uid) {
                $screenName = $user->getScreenName(array($userId));
                $res['content']['lock'] = "Данные редактируются другим пользователем: " . $screenName[0];
            } else {
                if ($lockTablesModel->setLock()) {
                    $res['error'] = 0;
                    $res['content'] = $lockTablesModel;
                } else {
                    $res['error'] = 1;
                    $res['content'] = $lockTablesModel->getErrors();
                }
            }
        }
        $page = new Json($res);
    }

    public function unlockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new TraderModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            if($lockTablesModel->unLock()) {
                $res['error'] = 0;
                $res['content'] = $lockTablesModel;
            } else {
                $res['error'] = 1;
                $res['content'] = $lockTablesModel->getErrors();
            }
        }
        $page = new Json($res);
    }

}
