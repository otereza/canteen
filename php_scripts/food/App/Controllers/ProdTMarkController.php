<?php

namespace Food\App\Controllers;

use Food\App\Models\LockTablesModel;
use Food\App\Models\ProdGroupModel;
use Food\App\Models\ProdPackModel;
use Food\App\Models\ProdTypeModel;
use Food\App\Models\Repositories\ProductRep;
use Food\Core\Json;
use Food\Core\View;
use Food\Core\Controller;
use Food\App\Models\ProdTMarkModel;


class ProdTMarkController extends Controller
{

    public function saveAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('group_id', 'type_id', 'name', 'pack_unit');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new ProdTMarkModel();
        $model->id = isset($_POST['id']) ? $_POST['id'] : null;
        $model->obj = $this->obj;
        $model->group_id = $_POST['group_id'];
        $model->type_id = $_POST['type_id'];
        $model->tmark_pos = isset($_POST['position']) ? $_POST['position'] : null;
        $model->name = stripslashes($_POST['name']);
        $model->pack_unit = $_POST['pack_unit'];
        $model->round_to_pack = isset($_POST['round_to_pack']) ? true : false;
        $model->life = isset($_POST['life']) ? $_POST['life'] : null;
        $model->temp_from = isset($_POST['temp_from']) ? $_POST['temp_from'] : null;
        $model->temp_to = isset($_POST['temp_to']) ? $_POST['temp_to'] : null;

        if(!empty($_POST['has_child'])) {   // НЕ сохраняем, если есть упаковки (дети)
            $res['error'] = 0;
            $res['content'] = $model;
            $res['has_child'] = true;
        } else {
            if (!$model->save()) {
                $res['error'] = 1;
                $res['content'] = array("Save Error", $model->getErrors());
            } else {
                $res['error'] = 0;
                $res['content'] = $model;
            }
        }
        $page = new Json($res);

    }

    public function editAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(empty($_POST['id'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty id';
            $page = new Json($res);
            return;
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new ProdTMarkModel();
        $model->id = $_POST['id'];
        $model->obj = $this->obj;
        if (!$model->get($model->id)) {
            $res['error'] = 1;
            $res['content'] = $model->getErrors();
        } else {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $model->id;
            $lockTablesModel->userId = $user->uid;
            $userId = $lockTablesModel->getUserBlockedTable();
            $res['error'] = 0;
            $res['content'] = $model;
            if(!empty($userId) && $userId != $user->uid) {
                $screenName = $user->getScreenName(array($userId));
                $res['content']->lock = "Данные редактируются другим пользователем: " . $screenName[0];
            } else {
                $lockTablesModel->setLock();
            }
        }
        $page = new Json($res);
    }


    public function deleteAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(empty($_POST['id'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty id';
            $page = new Json($res);
            return;
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new ProdTMarkModel();
        $model->id = intval($_POST['id']);
        $model->obj = $this->obj;

        if($model->id && $this->canDelete()) {
            if(!$model->delete()) {
                $res['error'] = 1;
                $res['content'] = $model->getErrors();
            } else {
                $res['content'] = 'ok';
            }
        }

        $page = new Json($res);
    }

    private function canDelete()
    {
        //TODO: Проверки на возможность удалить торговую марку
        return true;
    }

    public function undeleteAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(empty($_POST['id'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty id';
            $page = new Json($res);
            return;
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new ProdTMarkModel();
        $model->id = intval($_POST['id']);
        $model->obj = $this->obj;
        if(!$model->undelete()) {
            $res['error'] = 1;
            $res['content'] = $model->getErrors();
        } else {
            $res['content'] = 'ok';
        }

        $page = new Json($res);
    }

    public function changePosAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('id', 'pos', 'group_id', 'type_id');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new ProdTMarkModel();
        $model->id = $_POST['id'];
        $model->group_id = $_POST['group_id'];
        $model->type_id = $_POST['type_id'];
        $model->tmark_pos = $_POST['pos'];
        $model->obj = $this->obj;
        if(!$model->changePos()) {
            $res['error'] = 1;
            $res['content'] = $model->getErrors();
        } else {
            $res['content'] = 'ok';
        }
        $page = new Json($res);
    }

    public function lockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new ProdTMarkModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            $userId = $lockTablesModel->getUserBlockedTable();
            if($userId && $userId != $user->uid) {
                $screenName = $user->getScreenName(array($userId));
                $res['content']['lock'] = "Данные редактируются другим пользователем: " . $screenName[0];
            } else {
                if ($lockTablesModel->setLock()) {
                    $res['error'] = 0;
                    $res['content'] = $lockTablesModel;
                } else {
                    $res['error'] = 1;
                    $res['content'] = $lockTablesModel->getErrors();
                }
            }
        }
        $page = new Json($res);
    }

    public function unlockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new ProdTMarkModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            if(!$lockTablesModel->unLock()) {
                $res['error'] = 0;
                $res['content'] = $lockTablesModel;
            } else {
                $res['error'] = 1;
                $res['content'] = $lockTablesModel->getErrors();
            }
        }
        $page = new Json($res);
    }
}
