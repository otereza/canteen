<?php

namespace Food\App\Controllers;

use Food\App\Models\LockTablesModel;
use Food\App\Models\TraderObjModel;
use Food\Core\Json;
use Food\Core\Controller;


class TraderObjController extends Controller
{

    public function saveAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('contract', 'fio', 'phone', 'email');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return false;
            }
        }

        if(empty($_POST['payment_delay']) && !isset($_POST['prepayment'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty payment_delay';
            $page = new Json($res);
            return false;
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return false;
        }

        $deliveryDays = array();
        if(isset($_POST['wday']) && is_array($_POST['wday'])) {
            foreach ($_POST['wday'] as $wDay => $time) {
                if (!empty($time['from']) && !empty($time['to'])) {
                    $deliveryDays[$wDay]['from'] = str_replace(':', '', $time['from']);
                    $deliveryDays[$wDay]['to'] = str_replace(':', '', $time['to']);
                }
            }
        }

        $model = new TraderObjModel();
        $model->trader_id = isset($_POST['id']) ? intval($_POST['id']) : null;
        $model->obj = $_POST['obj'];
        $model->delivery_days = $deliveryDays;
        $model->min_price = !empty($_POST['min_price']) ? $_POST['min_price'] : null;
        $model->contract = $_POST['contract'];
        $model->fio = $_POST['fio'];
        $model->phone = $_POST['phone'];
        $model->email = $_POST['email'];
        $model->prepayment = isset($_POST['prepayment']) ? true : false;
        $model->payment_delay = $model->prepayment ? null : $_POST['payment_delay'];
        $model->discount = isset($_POST['discount']) ? str_replace(',', '.', $_POST['discount']) : null;
        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);

    }

    public function lockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new TraderObjModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            $userId = $lockTablesModel->getUserBlockedTable();
            if($userId && $userId != $user->uid) {
                $screenName = $user->getScreenName(array($userId));
                $res['content']['lock'] = "Данные редактируются другим пользователем: " . $screenName[0];
            } else {
                if ($lockTablesModel->setLock()) {
                    $res['error'] = 0;
                    $res['content'] = $lockTablesModel;
                } else {
                    $res['error'] = 1;
                    $res['content'] = $lockTablesModel->getErrors();
                }
            }
        }
        $page = new Json($res);
    }

    public function unlockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new TraderObjModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            if($lockTablesModel->unLock()) {
                $res['error'] = 0;
                $res['content'] = $lockTablesModel;
            } else {
                $res['error'] = 1;
                $res['content'] = $lockTablesModel->getErrors();
            }
        }
        $page = new Json($res);
    }


}
