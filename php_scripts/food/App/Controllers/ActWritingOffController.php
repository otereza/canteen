<?php


namespace Food\App\Controllers;


use Food\App\Helpers\Converter;
use Food\App\Models\ActWritingOffModel;
use Food\App\Models\DocProdModel;
use Food\App\Models\InvoiceReturnModel;
use Food\App\Models\Repositories\ProductRep;
use Food\App\Models\TraderModel;
use Food\App\Models\WarehouseModel;
use Food\Core\Controller;
use Food\Core\Json;
use Food\Core\View;

class ActWritingOffController extends Controller
{
    public function indexAction()
    {
        global $page, $user, $businessobj;

        View::setTitle('Акт списания');

        View::jsFile('app/act.js', View::TO_END);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditAct' => $user->havePermit(309, $this->obj),
        );
        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        $model = new ActWritingOffModel();
        $model->obj = $this->obj;
        $acts = $model->getAll();
        $list = array();
        foreach ($acts as $act) {
            if($act['status'] == 0) {
                $list['draft'][] = $act;
            } elseif($act['status'] == 1) {
                $list['approved'][] = $act;
            }
        }


        View::render('act/writingOff/index', array(
            'list' => $list,
            'permit' => $permit
        ));

        return;
    }

    public function editAction()
    {
        global $page, $user, $businessobj;

        View::setTitle('Акт списания');

        View::jsFile('app/act.js', View::TO_END);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditAct' => $user->havePermit(309, $this->obj),
        );
        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        $model = new ActWritingOffModel();
        $act = $model->get($_GET['id']);

        $warehouseModel = new WarehouseModel();
        $prods = $warehouseModel->getProdsGroupByPackId(isset($_GET['for']) ? $_GET['for'] : $this->obj);
        $warehoseProds = array();
        foreach ($prods as $prod) {
            $warehoseProds[$prod['pack_id']] = $prod;
        }

        $prodDocModel = new DocProdModel();
        $productRows = $prodDocModel->getProds(DocProdModel::DOC_TYPE_ACT_WRITING_OFF, $_GET['id']);
        foreach ($productRows as &$row) {
            $row['in_amount'] = isset($warehoseProds[$row['pack_id']]) ? Converter::getAmountForView($warehoseProds[$row['pack_id']]['amount'], $row['pack_unit']) : 0;
            $row['inTotalPrice'] = isset($warehoseProds[$row['pack_id']]) ? Converter::getPriceForView($warehoseProds[$row['pack_id']]['price'], $row['pack_unit']) : 0;
            $row['amount'] = Converter::getAmountForView($row['amount'], $row['pack_unit']);
            $row['price'] = Converter::getPriceForView($row['price'], $row['pack_unit']);
        }

        View::jsText('window.act = ' . json_encode($act) . ';'
            . 'window.act.items = ' . (empty($productRows) ? '{}' : json_encode($productRows)) . ';'
            . 'window.docType = ' . DocProdModel::DOC_TYPE_ACT_WRITING_OFF . ';', View::TO_END);


        View::render('act/writingOff/'.($act['status'] == 0 ? 'edit' : 'view'), array(
            'act' => $act,
            'permit' => $permit,
            'rows' => $productRows
        ));

        return;
    }

    public function newAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('obj');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new ActWritingOffModel();
        $model->obj = $_POST['obj'];
        $model->act_date = time();
        if((date('i') > 30)) {
            $timeFrom = date('H:i', mktime(date('H') + date('I'), 30));
            $timeTo = date('H:i', mktime(date('H') + date('I') + 1, 00));
        } else {
            $timeFrom = date('H:i', mktime(date('H') + date('I'), 00));
            $timeTo = date('H:i', mktime(date('H') + date('I'), 30));
        }
        $model->act_time_period = $timeFrom . ' - ' . $timeTo;
        $model->total = 0;

        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);

    }

    public function getDataAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $warehouseModel = new WarehouseModel();
        $prods = $warehouseModel->getProdsGroupByPackId($this->obj);
        $addingPackParams = array();
        if(!empty($prods) && is_array($prods)) {
            foreach($prods as $prod) {
                $amount = Converter::getAmountForView($prod['amount'], $prod['pack_unit']);
                $price = Converter::getPriceForView($prod['price'], $prod['pack_unit']);
                $addingPackParams[$prod['pack_id']]['amount'] = (string) $amount;
                $addingPackParams[$prod['pack_id']]['price'] = (string) $price;
                $addingPackParams[$prod['pack_id']]['exp_date'] = (string) $prod['exp_date'];
            }
        }

        $model = new ProductRep();
        $model->addingPackParams = $addingPackParams;
        $tree = $model->getTree($this->obj);

//        dd($tree);
        $page = new Json($tree);

    }


    public function saveAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('id', 'act_number', 'act_date', 'act_time_period');
        if(empty($_POST['preSave'])) {
            $reqFields = array('id', 'act_number', 'act_date', 'act_time_period', 'total', 'items');
        }
        foreach ($reqFields as $field) {
            if (empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new ActWritingOffModel();
        $model->get($_POST['id']);
        $model->act_number = $_POST['act_number'];
        $model->act_date = $_POST['act_date'];
        $model->act_time_period = $_POST['act_time_period'];
        $model->reason = stripslashes($_POST['reason']);
        $model->commission = stripslashes($_POST['commission']);
        $model->total = $_POST['total'];

        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;

            // обновим продукты
            if(!empty($_POST['items'])) {
                foreach($_POST['items'] as $row) {
                    $prodModel = new DocProdModel();
                    $prodModel->id = $row['id'];
                    $prodModel->doc_type = DocProdModel::DOC_TYPE_ACT_WRITING_OFF;
                    $prodModel->doc_id = $_POST['id'];
                    $prodModel->obj = $this->obj;
                    $prodModel->pos = $row['pos'];
                    $prodModel->prod_name = stripslashes($row['prod_name']);
                    $prodModel->tmark_id = $row['tmark_id'];
                    $prodModel->pack_id = $row['pack_id'];
                    $prodModel->unit = $row['unit'];
                    $prodModel->pack_unit = $row['pack_unit'];
                    $prodModel->amount = Converter::getAmountForDb($row['amount'], $row['amount_unit']);
                    $prodModel->amount_unit = $row['amount_unit'];
                    $prodModel->price = Converter::getPriceForDb($row['price'], $row['pack_unit'], $row['amount_unit']);
                    $prodModel->exp_date = $row['exp_date'];
                    if(!$prodModel->save()) {
                        $res['error'] = 1;
                        $res['content'] = array("Save Error", $prodModel->getErrors());
                        $page = new Json($res);
                        return;
                    }
                }
                if(!empty($_POST['approve'])) {
                    if(!$this->approve($model, $_POST['items'])) {
                        $res['error'] = 1;
                        $res['content'] = array("Approve Error", $this->getError());
                        $page = new Json($res);
                        return;
                    }
                }
            }

        }

        $page = new Json($res);

    }

    /**
     * @param $docModel ActWritingOffModel
     * @param $items
     * @return bool
     */
    public function approve($docModel, $items)
    {
        // перед сохранением, удалим все продукты данного документа со склада. Чтобы не дублировались
        $warehouseModel = new WarehouseModel();
        $warehouseModel->trans_doc_type = DocProdModel::DOC_TYPE_ACT_WRITING_OFF;
        $warehouseModel->trans_doc_id = $docModel->id;
        // временное решение для тестов
        $warehouseModel->deleteDocProds();

        // получим список продуктов на складе для списания нужного количества
        $prods = $warehouseModel->getProds($this->obj);
        $warehouseProds = array();
        foreach ($prods as $prod) {
            if($prod['amount'] > 0) {
                $warehouseProds[$prod['pack_id']][] = $prod;
            }
        }

        foreach($items as $row) {
            if($row['amount'] > 0) {
                $needAmount = Converter::getAmountForDb($row['amount'], $row['amount_unit']);
                foreach ($warehouseProds[$row['pack_id']] as $lotRow) {
                    $warehouseModel = new WarehouseModel();
                    $warehouseModel->trans_doc_type = DocProdModel::DOC_TYPE_ACT_WRITING_OFF;
                    $warehouseModel->trans_doc_id = $docModel->id;
                    $warehouseModel->lot_id = $lotRow['lot_id'];
                    $warehouseModel->trans_date = time();
                    $warehouseModel->obj = $this->obj;
                    $warehouseModel->prod_name = $lotRow['prod_name'];
                    $warehouseModel->tmark_id = $lotRow['tmark_id'];
                    $warehouseModel->pack_id = $lotRow['pack_id'];
                    $warehouseModel->unit = $lotRow['unit'];
                    $warehouseModel->pack_unit = $lotRow['pack_unit'];
                    $warehouseModel->amount = ($needAmount >= $lotRow['amount']) ? $lotRow['amount'] : ($lotRow['amount'] - $needAmount);
                    $warehouseModel->price = $lotRow['price'];
                    $warehouseModel->exp_date = $lotRow['exp_date'];

                    $warehouseModel->takeProduct();
                    if ($warehouseModel->hasErrors()) {
                        $this->setError($warehouseModel->getErrors());
                        return false;
                    }
                    $needAmount -= $lotRow['amount'];
                    if($needAmount <= 0) {
                        break;
                    }
                }
            }
        }
        $docModel->approve();

        return true;
    }


    public function changeStateAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('orderId', 'state');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new OrderModel();
        $model->get($_POST['orderId']);
        $model->state = $_POST['state'];
//        dd($model);
        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);
    }

}