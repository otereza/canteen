<?php


namespace Food\App\Controllers;


use Food\App\Helpers\Converter;
use Food\App\Models\ActBalanceModel;
use Food\App\Models\ActWritingOffModel;
use Food\App\Models\DocProdModel;
use Food\App\Models\InvoiceReturnModel;
use Food\App\Models\Repositories\ProductRep;
use Food\App\Models\TraderModel;
use Food\App\Models\WarehouseModel;
use Food\Core\Controller;
use Food\Core\Json;
use Food\Core\View;

class ActBalanceController extends Controller
{
    public function indexAction()
    {
        global $page, $user, $businessobj;

        View::setTitle('Акты снятия остатков');

        View::jsFile('app/act.js', View::TO_END);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditActBalance' => $user->havePermit(310, $this->obj),
        );
        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        $model = new ActBalanceModel();
        $model->obj = $this->obj;
        $acts = $model->getAll();


        View::render('act/balance/index', array(
            'list' => $acts,
            'permit' => $permit
        ));

        return;
    }

    public function editAction()
    {
        global $page, $user, $businessobj;

        View::setTitle('Акт снятия остатков');

        View::jsFile('app/act.js', View::TO_END);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditActBalance' => $user->havePermit(310, $this->obj),
        );
        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        $model = new ActBalanceModel();
        $act = $model->get($_GET['id']);

        $prodDocModel = new DocProdModel();
        $rows = $prodDocModel->getProds(DocProdModel::DOC_TYPE_ACT_WAREHOUSE_BALANCE, $_GET['id']);
        $prodDocRows = array();
        foreach ($rows as $row) {
            $prodDocRows[$row['pack_id'] . '-'. $row['exp_date']] = $row;
        }

        $warehouseModel = new WarehouseModel();
        $productRows = $warehouseModel->getProdsBalance(isset($_GET['for']) ? $_GET['for'] : $this->obj);

        foreach ($productRows as &$row) {
            $key = $row['pack_id'] . '-'. $row['exp_date'];
            if(isset($prodDocRows[$key])) {
                $row['id'] = $prodDocRows[$key]['id'];
                $row['realAmount'] = Converter::getAmountForView($prodDocRows[$key]['amount'], $prodDocRows[$key]['pack_unit']);
                $row['checked'] = 1;
            }
            $row['amount'] = Converter::getAmountForView($row['amount'], $row['pack_unit']);
            $row['price'] = Converter::getPriceForView($row['price'], $row['pack_unit']);
        }

        View::jsText('window.act = ' . json_encode($act) . ';'
            . 'window.act.items = ' . (empty($productRows) ? '{}' : json_encode($productRows)) . ';'
            . 'window.docType = ' . DocProdModel::DOC_TYPE_ACT_WAREHOUSE_BALANCE . ';', View::TO_END);


        View::render('act/balance/edit', array(
            'act' => $act,
            'permit' => $permit,
            'rows' => $productRows
        ));

        return;
    }

    public function newAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('obj');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new ActBalanceModel();
        $model->obj = $_POST['obj'];
        $model->act_date = time();
        if((date('i') > 30)) {
            $timeFrom = date('H:i', mktime(date('H') + date('I'), 30));
            $timeTo = date('H:i', mktime(date('H') + date('I') + 1, 00));
        } else {
            $timeFrom = date('H:i', mktime(date('H') + date('I'), 00));
            $timeTo = date('H:i', mktime(date('H') + date('I'), 30));
        }
        $model->act_time_period = $timeFrom . ' - ' . $timeTo;
        $model->type_balance = isset($_POST['typeBalance']) ? $_POST['typeBalance'] : ActBalanceModel::BALANCE_FULL;

        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);

    }


    public function saveAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('id', 'act_number', 'act_date', 'act_time_period', 'type_balance');
        foreach ($reqFields as $field) {
            if (empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new ActBalanceModel();
        $model->get($_POST['id']);
        $model->act_number = $_POST['act_number'];
        $model->act_date = $_POST['act_date'];
        $model->act_time_period = $_POST['act_time_period'];
        $model->type_balance = $_POST['type_balance'];
        $model->reason = stripslashes($_POST['reason']);
        $model->commission = stripslashes($_POST['commission']);

        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;

            // обновим продукты
            if(!empty($_POST['items'])) {
                $items = array();
                foreach($_POST['items'] as $key => $row) {
                    $prodModel = new DocProdModel();
                    if(!isset($row['realAmount']) || $row['realAmount'] == '') {
                        if(isset($row['id'])) {
                            $prodModel->id = $row['id'];
                            $prodModel->delete();
                        }
                        continue;
                    }
                    $prodModel->id = isset($row['id']) ? $row['id'] : null;
                    $prodModel->doc_type = DocProdModel::DOC_TYPE_ACT_WAREHOUSE_BALANCE;
                    $prodModel->doc_id = $_POST['id'];
                    $prodModel->obj = $this->obj;
                    $prodModel->pos = isset($row['pos']) ? $row['pos'] : $key;
                    $prodModel->prod_name = stripslashes($row['prod_name']);
                    $prodModel->tmark_id = $row['tmark_id'];
                    $prodModel->pack_id = $row['pack_id'];
                    $prodModel->unit = $row['unit'];
                    $prodModel->pack_unit = $row['pack_unit'];
                    $prodModel->in_amount = Converter::getAmountForDb($row['amount'], $row['pack_unit']);
                    $prodModel->amount_unit = $row['pack_unit'];
                    $prodModel->price = Converter::getPriceForDb($row['price'], $row['pack_unit']);
                    $prodModel->amount = Converter::getAmountForDb($row['realAmount'], $row['pack_unit']);
                    $prodModel->exp_date = $row['exp_date'];
                    if(!$prodModel->save()) {
                        $res['error'] = 1;
                        $res['content'] = array("Save Error", $prodModel->getErrors());
                        $page = new Json($res);
                        return;
                    }
                    $items[] = array(
                        'prod_name' => $prodModel->prod_name,
                        'unit' => $prodModel->unit,
                        'pack_unit' => $prodModel->pack_unit,
                        'in_amount' => $row['amount'],
                        'price' => $row['price'],
                        'real_amount' => $row['realAmount'],
                        'exp_date' => date("d.m.Y", $row['exp_date']),
                    );
                }
            }
            $this->callFoodCheck($model, $items);
        }

        $page = new Json($res);

    }

    /**
     * вызов функции внешнего модуля
     *
     * @param $actBalnceModel ActBalanceModel
     * @param $items
     */
    private function callFoodCheck($actBalnceModel, $items)
    {
        if(function_exists('model_dispatches')) {
            $params = array(
                'obj' => $actBalnceModel->obj,
                'act_number' => $actBalnceModel->act_number,
                'act_date' => date("d.m.Y", $actBalnceModel->act_date),
                'act_time_period' => $actBalnceModel->act_time_period,
                'type_balance' => ActBalanceModel::getNameTypeBalance($actBalnceModel->type_balance),
                'reason' => $actBalnceModel->reason,
                'commission' => $actBalnceModel->commission,
            );

            $params['items'] = $items;
            model_dispatches('food_check', $params);
        }
    }


}