<?php

namespace Food\App\Controllers;

use Food\App\Models\LockTablesModel;
use Food\App\Models\ProdGroupModel;
use Food\Core\Json;
use Food\Core\Controller;


class ProdGroupController extends Controller
{

    public function saveAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(empty($_POST['name'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty name';
            $page = new Json($res);
            return;
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new ProdGroupModel();
        $model->id = isset($_POST['id']) ? $_POST['id'] : null;
        $model->obj = $this->obj;
        $model->name = stripslashes($_POST['name']);
        $model->group_pos = isset($_POST['position']) ? $_POST['position'] : null;
        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = $model->getErrors();
        } else {
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);
    }

    public function getAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new ProdGroupModel();
        $model->id = isset($_POST['id']) ? $_POST['id'] : null;
        $model->obj = $this->obj;
        if (!$model->get($model->id)) {
            $res['error'] = 1;
            $res['content'] = $model->getErrors();
        } else {
            if($model->id) {
                $lockTablesModel = new LockTablesModel($model->getTableName());
                $lockTablesModel->rowId = $model->id;
                $userId = $lockTablesModel->getUserBlockedTable();
                $res['error'] = 0;
                $res['content'] = $model;
                if(!empty($userId) && $userId != $user->uid) {
                    $screenName = $user->getScreenName(array($userId));
                    $res['content']->lock = "Данные редактируются другим пользователем: " . $screenName[0];
                }
            }
        }
        $page = new Json($res);
    }

    public function editAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(empty($_POST['id'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty id';
            $page = new Json($res);
            return;
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new ProdGroupModel();
        $model->id = $_POST['id'];
        $model->obj = $this->obj;
        if (!$model->get($model->id)) {
            $res['error'] = 1;
            $res['content'] = $model->getErrors();
        } else {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $model->id;
            $userId = $lockTablesModel->getUserBlockedTable();
            $res['error'] = 0;
            $res['content'] = $model;
            if(!empty($userId) && $userId != $user->uid) {
                $screenName = $user->getScreenName(array($userId));
                $res['content']->lock = "Данные редактируются другим пользователем: " . $screenName[0];
            } else {
                $lockTablesModel->setLock();
            }
        }
        $page = new Json($res);
    }

    public function deleteAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(empty($_POST['id'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty id';
            $page = new Json($res);
            return;
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new ProdGroupModel();
        $model->id = $_POST['id'];
        $model->obj = $this->obj;
        if($model->id && $this->canDelete()) {
            if(!$model->delete()) {
                $res['error'] = 1;
                $res['content'] = $model->getErrors();
            } else {
                $res['content'] = 'ok';
            }
        }
        $page = new Json($res);
    }

    private function canDelete()
    {
        //TODO: Проверки на возможность удалить группу
        return true;
    }

    public function undeleteAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(empty($_POST['id'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty id';
            $page = new Json($res);
            return;
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new ProdGroupModel();
        $model->id = $_POST['id'];
        $model->obj = $this->obj;
        if(!$model->undelete()) {
            $res['error'] = 1;
            $res['content'] = $model->getErrors();
        } else {
            $res['content'] = 'ok';
        }
        $page = new Json($res);
    }

    public function changePosAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('id', 'pos');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new ProdGroupModel();
        $model->id = $_POST['id'];
        $model->group_pos = $_POST['pos'];
        $model->obj = $this->obj;
        if(!$model->changePos()) {
            $res['error'] = 1;
            $res['content'] = $model->getErrors();
        } else {
            $res['content'] = 'ok';
        }
        $page = new Json($res);
    }

    public function lockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new ProdGroupModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            $userId = $lockTablesModel->getUserBlockedTable();
            if($userId && $userId != $user->uid) {
                $screenName = $user->getScreenName(array($userId));
                $res['content']['lock'] = "Данные редактируются другим пользователем: " . $screenName[0];
            } else {
                if ($lockTablesModel->setLock()) {
                    $res['error'] = 0;
                    $res['content'] = $lockTablesModel;
                } else {
                    $res['error'] = 1;
                    $res['content'] = $lockTablesModel->getErrors();
                }
            }
        }
        $page = new Json($res);
    }

    public function unlockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new ProdGroupModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            if(!$lockTablesModel->unLock()) {
                $res['error'] = 0;
                $res['content'] = $lockTablesModel;
            } else {
                $res['error'] = 1;
                $res['content'] = $lockTablesModel->getErrors();
            }
        }
        $page = new Json($res);
    }

}
