<?php

namespace Food\App\Controllers;

use Food\App\Helpers\Converter;
use Food\App\Models\DocProdModel;
use Food\App\Models\ProdObjectModel;
use Food\Core\Json;
use Food\Core\Controller;


class DocProdController extends Controller
{
    public function saveAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('doc_type', 'doc_id', 'pos', 'prod_name', 'tmark_id', 'pack_id', 'unit', 'pack_unit', 'amount', 'amount_unit', 'price', 'exp_date');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new DocProdModel();
        $model->id = isset($_POST['id']) ? $_POST['id'] : null;
        $model->doc_type = $_POST['doc_type'];
        $model->doc_id = $_POST['doc_id'];
        $model->obj = empty($_POST['for']) ? $this->obj : $_POST['for'];
        $model->pos = $_POST['pos'];
        $model->prod_name = stripslashes($_POST['prod_name']);
        $model->tmark_id = $_POST['tmark_id'];
        $model->pack_id = $_POST['pack_id'];
        $model->unit = $_POST['unit'];
        $model->pack_unit = $_POST['pack_unit'];
        $model->amount = Converter::getAmountForDb($_POST['amount'], $_POST['amount_unit']);
        $model->amount_unit = $_POST['amount_unit'];
        $model->price = Converter::getPriceForDb($_POST['price'], $_POST['pack_unit'], $_POST['amount_unit']);
        $model->exp_date = $_POST['exp_date'];

        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $model->amount = Converter::getAmountForView($model->amount, $model->amount_unit);
            $model->price = Converter::getPriceForView($model->price, $model->pack_unit, $model->amount_unit);
            $res['error'] = 0;
            $res['content'] = $model;

            //  добавим продукт в список продуктов обьекта
            if($this->obj != 'all') {
                $prodObjModel = new ProdObjectModel();
                $saved = $prodObjModel->addProductForObj($_POST['pack_id'], $this->obj);
                if (!$saved) {
                    $res['error'] = 1;
                    $res['content'] = array("Ошибка записи продукта в список продуктов обьектов", $prodObjModel->getErrors());
                }
            }

        }
        $page = new Json($res);
    }

    public function deleteAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('id');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new DocProdModel();
        $model->id = $_POST['id'];

        if(!$model->delete()) {
            $res['error'] = 1;
            $res['content'] = array("Delete Error",$model->getErrors());
        }
        $page = new Json($res);

    }

}
