<?php

namespace Food\App\Controllers;

use Food\App\Models\DocProdModel;
use Food\App\Models\InvoiceReceiptModel;
use Food\App\Models\LockTablesModel;
use Food\App\Models\OrderModel;
use Food\App\Models\OrderProdModel;
use Food\App\Models\ProdPackModel;
use Food\App\Models\Repositories\ProductRep;
use Food\App\Models\TraderModel;
use Food\App\Models\TraderObjModel;
use Food\App\Models\TraderPriceModel;
use Food\App\Models\WarehouseModel;
use Food\App\Helpers\Converter;
use Food\Core\Json;
use Food\Core\View;
use Food\Core\Controller;


class InvoiceReceiptController extends Controller
{
    public function indexAction()
    {
        global $page, $user, $businessobj;

        View::setTitle('Список Накладных (приход)');

        View::jsFile('app/invoice.js', View::TO_END);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditInvoice' => $user->havePermit(308, $this->obj),
        );
        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        $model = new InvoiceReceiptModel();
        $model->obj = $this->obj;
        $invoices = $model->getAll();

        $stateInvoices = array();
        foreach ($invoices as $invoice) {
            if(empty($invoice['date_approve'])) {
                $stateInvoices['draft'][] = $invoice;
            } else {
                $stateInvoices['approved'][] = $invoice;
            }
        }

        $model = new TraderModel();
        $model->obj = $this->obj;
        $traders = $model->getAll();
        $objTraders = array();
        if($this->obj != 'all') {
            foreach ($traders as $key => $trader) {
                $trader['business_obj'] = json_decode($trader['business_obj'], true);
                if (in_array($this->obj, $trader['business_obj'])) {
                    $objTraders[$key] = $trader;
                }
            }
        }

        View::render('invoice/receipt/index', array(
            'obj' => $this->obj,
            'invoices' => $stateInvoices,
            'objTraders' => $objTraders,
            'traders' => $traders,
            'permit' => $permit
        ));

        return;
    }

    /**
     * @throws \Food\App\Exceptions\ConvertException
     */
    public function editAction()
    {
        global $page, $user, $businessobj;

        View::setTitle('Накладная (приход)');

        View::cssFile('jstree/themes/default/style.min.css');
        View::jsFile('jstree/jstree.js', View::TO_END);
        View::jsFile('jstree/jstreegrid.js', View::TO_END);
        View::jsFile('app/invoice.js', View::TO_END);
        View::jsText('var tripleState = false;', View::TO_END);
        View::jsText('var treeWithCheckboxes = false;', View::TO_END);
        View::jsText('var dataUrl = "trader/getData";', View::TO_END);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        if(empty($_GET['id'])) {
            View::render('error404', array(
                'error' => 'Неизвестный номер Приходной Накладной'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditFullProducts' => $user->havePermit(301, $this->obj),
            'canEditTmarkPackProducts' => $user->havePermit(302, $this->obj),
            'canEditTrader' => $user->havePermit(303, $this->obj),
            'canCheckedProduct' => $user->havePermit(305, $this->obj),
            'canChangePriceProduct' => $user->havePermit(306, $this->obj),
            'canEditInvoice' => $user->havePermit(308, $this->obj),
        );
        if(!empty($_GET['for'])) {
            $permit['canChangeObjPriceProduct'] = $user->havePermit(306, $_GET['for']);
        }

        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        $model = new InvoiceReceiptModel();
        $model->obj = $this->obj;
        $invoice = $model->get($_GET['id']);

        if(empty($invoice['id'])) {
            View::render('error404', array(
                'error' => 'Накладная ' . $_GET['id'] . ' не найдена'
            ));
            return;
        }

        $model = new TraderModel();
        $model->obj = $this->obj;
        $trader = $model->get($invoice['trader_id']);
        $traderDeliveryDays = $model->deliveryDays($invoice['trader_id']);

        $priceModel = new TraderPriceModel();
        $priceModel->obj = empty($_GET['for']) ? $this->obj : $_GET['for'];
        $priceModel->trader_id = $invoice['trader_id'];
        $prices = $priceModel->getAllWithMain();

        $packModel = new ProdPackModel();
        $prodLife = array();
        foreach($packModel->getAll() as $pack) {
            $prodLife[$pack['id']] = $pack['life'];
        }

        $model = new TraderObjModel();
        $model->obj = empty($_GET['for']) ? $this->obj : $_GET['for'];
        $discount = $model->getDiscount($trader['id']);
        $priceCoef = 1 - ($discount / 100);
        $invoice['priceCoef'] = $priceCoef;

        $model = new DocProdModel();
        $productRows = $model->getProds(DocProdModel::DOC_TYPE_INVOICE_RECEIPT, $_GET['id']);
        $prodsHasBeenReceived = !empty($invoice['order_id']) ? $model->getProdsHasBeenReceivedByInvoiceOnOrder($invoice['order_id']) : array();
        foreach ($productRows as &$row) {
            if($row['in_amount']) {
                if (isset($prodsHasBeenReceived[$row['pack_id']]) && $row['in_amount']) {
                    $row['remained'] = $row['in_amount'] - $prodsHasBeenReceived[$row['pack_id']]['amount'];
                    $row['remained'] = Converter::getAmountForView($row['remained'], $row['in_amount_unit']);
                    unset($prodsHasBeenReceived[$row['pack_id']]);
                } else {
                    $row['remained'] = 0;
                }
            }


            $unitValue = preg_replace('/[^\d]+/', '', $row['pack_unit']);
            $unitValue = empty($unitValue) ? 1 : $unitValue;
            $userTraderPrice = sprintf('%.2f', Converter::getPriceForView($row['price'], $row['pack_unit']) / $priceCoef);

            if(!empty($row['in_price'])) {
                $row['in_price'] = Converter::getPriceForView($row['in_price'], $row['pack_unit'], $row['in_amount_unit']);
            }
            $row['price'] = Converter::getPriceForView($row['price'], $row['pack_unit'], $row['amount_unit']);
            if(!empty($row['in_amount_unit'])) {
                $row['in_amount'] = Converter::getAmountForView($row['in_amount'], $row['in_amount_unit']);
            }
            $row['amount'] = Converter::getAmountForView($row['amount'], $row['amount_unit']);

            $row['unit_value'] = $unitValue;
            $row['trader_price'] = $prices[$row['pack_id']]['trader_price'];
            $row['trader_price_user'] = $userTraderPrice;
            $row['life'] = $prodLife[$row['pack_id']];
            $row['trader_price_main'] = $prices[$row['pack_id']]['trader_price_main'];
        }
//dd($rows);
        if(empty($invoice['order_id'])) {
            $order = array(
              'id' => 0,
              'name' => 'Без заявки'
            );
        } else {
            $model = new OrderModel();
            $order = $model->get($invoice['order_id']);
            $order['name'] = 'от ' . date('d.m.Y', $order['date_shipment']) . ' №' . $order['id'];
        }
        $invoice['items'] = $productRows;
        View::jsText('window.invoice = ' . json_encode($invoice) . ';'
                   . 'window.invoice.items = ' . (empty($productRows) ? '{}' : json_encode($productRows)) . ';'
                   . 'window.docType = ' . DocProdModel::DOC_TYPE_INVOICE_RECEIPT . ';'
                   . 'window.prices = ' . json_encode($prices) . ';'
                   . 'window.traderDeliveryDays = '. json_encode($traderDeliveryDays) . ';', View::TO_END);

        View::render('invoice/receipt/'.($invoice['status'] ? 'approved' : 'edit'), array(
            'obj' => $this->obj,
            'invoice' => $invoice,
            'trader' => $trader,
            'order' => $order,
            'rows' => $productRows,
            'permit' => $permit
        ));

    }


    /**
     * @return string|void
     */
    public function getOrderItemsAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('trader_id', 'order_id', 'obj', 'doc_id');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new OrderProdModel();
        $model->obj = $_POST['obj'];
        $productRows = $model->getAllForOrder($_POST['order_id']);

        $model = new ProdPackModel();
        $prodLife = $model->getAll();

        $priceModel = new TraderPriceModel();
        $priceModel->obj = empty($_GET['for']) ? $this->obj : $_GET['for'];
        $priceModel->trader_id = $_POST['trader_id'];
        $prices = $priceModel->getAllWithMain();

        // Так как мы грузим продукты для накладной согласно заявки,
        // перед записью удаляем все продукты текущей накладной
        $docModel = new DocProdModel();
        $docModel->doc_type = DocProdModel::DOC_TYPE_INVOICE_RECEIPT;
        $docModel->doc_id = $_POST['doc_id'];
        $docModel->deleteDocProds();
        unset($docModel);

        $cnt = 1;
        foreach ($productRows as &$row) {
            $unitValue = preg_replace('/[^\d]+/', '', $row['pack_unit']);
            $unitValue = empty($unitValue) ? 1 : $unitValue;
            $row['unit_value'] = $unitValue;
            $row['trader_price'] = $prices[$row['pack_id']]['trader_price'];
            $row['trader_price_user'] = $prices[$row['pack_id']]['trader_price'];
            $row['trader_price_main'] = $prices[$row['pack_id']]['trader_price_main'];
            $row['life'] = isset($prodLife[$row['pack_id']]) ? $prodLife[$row['pack_id']]['life'] : 1;

            $docModel = new DocProdModel();
            $docModel->doc_type = DocProdModel::DOC_TYPE_INVOICE_RECEIPT;
            $docModel->doc_id = $_POST['doc_id'];
            $docModel->obj = empty($_POST['for']) ? $this->obj : $_POST['for'];
            $docModel->pos = $cnt++;
            $docModel->prod_name = stripslashes($row['prod_name']);
            $docModel->tmark_id = $row['tmark_id'];
            $docModel->pack_id = $row['pack_id'];
            $docModel->unit = $row['unit'];
            $docModel->pack_unit = $row['pack_unit'];
            $docModel->in_amount = $row['amount'];
            $docModel->in_amount_unit = $row['amount_unit'];
            $docModel->in_price = $row['price'];
            $docModel->amount = $row['amount'];
            $docModel->amount_unit = $row['amount_unit'];
            $docModel->price = $row['price'];
            $docModel->exp_date = date('d.m.Y', strtotime('+' . $row['life'] . ' day', mktime(23, 59, 59)));
            $docModel->save();
            if($docModel->hasErrors()) {
                $res['error'] = 1;
                $res['content'] = $docModel->getErrors();
                $page = new Json($res);
                return;
            } else {
                $row['id'] = $docModel->id;
                unset($docModel);
            }

        }

        if(empty($productRows)) {
            $res['error'] = 1;
            $res['content'] = "";
        } else {
            $res['error'] = 0;
            $res['content'] = $productRows;
        }
        $page = new Json($res);
        return;
    }

    public function getTraderInvoiceAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('trader_id', 'forObj');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => $this->getError()
            ));
            return;
        }

        $model = new InvoiceReceiptModel();
        $model->obj = $_POST['forObj'];
        $model->trader_id = $_POST['trader_id'];
        $invoices = $model->getForTrader();
        $traderInvoices = array();
        foreach ($invoices as $invoice) {
            $traderInvoices[] = array(
                'id' => $invoice['id'],
                'name' => 'Накладная от ' . date('d.m.Y', $invoice['inv_date']) . ' № ' . $invoice['inv_number'],
                'total' => $invoice['total']
            );
        }

        if($model->hasErrors()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $traderInvoices;
        }
        $page = new Json($res);
    }


    public function getDataAction()
    {

        global $page, $businessobj;

        if(!$this->isRightObject()) {
            View::render('products/index', array(
                'error' => $this->getError()
            ));
            return;
        }

        // если данные для конкретного объекта
        if($this->obj == 'all' && (isset($_GET['for']) && array_key_exists($_GET['for'], $businessobj)) ) {
            $forObj = $_GET['for'];
        } else {
            $forObj = null;
        }

        $model = new ProductRep();
        $tree = $model->getTree($this->obj, 'trader', $forObj);
        $page = new Json($tree );
    }


    public function newAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('trader_id', 'obj');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new InvoiceReceiptModel();
        $model->obj = $_POST['obj'];
        $model->trader_id = $_POST['trader_id'];
        $model->order_id = $_POST['doc_id'];
        $model->inv_date = time();
        if((date('i') > 30)) {
            $timeFrom = date('H:i', mktime(date('H') + date('I'), 30));
            $timeTo = date('H:i', mktime(date('H') + date('I') + 1, 00));
        } else {
            $timeFrom = date('H:i', mktime(date('H') + date('I'), 00));
            $timeTo = date('H:i', mktime(date('H') + date('I'), 30));
        }
        $model->inv_time_period = $timeFrom . ' - ' . $timeTo;

        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;


            // Добавим продукты из заявки
            if(!empty($_POST['doc_id'])) {
                $orderModel = new OrderProdModel();
                $orderModel->obj = $_POST['obj'];
                $productRows = $orderModel->getAllForOrder($_POST['doc_id']);

                $prodPackModel = new ProdPackModel();
                $prodLife = $prodPackModel->getAll();

                $priceModel = new TraderPriceModel();
                $priceModel->obj = $_POST['obj'];
                $priceModel->trader_id = $_POST['trader_id'];
                $prices = $priceModel->getAllWithMain();

                // Так как мы грузим продукты для накладной согласно заявки,
                // перед записью удаляем все продукты текущей накладной
                $docModel = new DocProdModel();
                $docModel->doc_type = DocProdModel::DOC_TYPE_INVOICE_RECEIPT;
                $docModel->doc_id = $model->id;
                $docModel->deleteDocProds();
                unset($docModel);

                $cnt = 1;
                foreach ($productRows as &$row) {
                    $unitValue = preg_replace('/[^\d]+/', '', $row['pack_unit']);
                    $unitValue = empty($unitValue) ? 1 : $unitValue;
                    $row['unit_value'] = $unitValue;
                    $row['trader_price'] = $prices[$row['pack_id']]['trader_price'];
                    $row['trader_price_user'] = $prices[$row['pack_id']]['trader_price'];
                    $row['trader_price_main'] = $prices[$row['pack_id']]['trader_price_main'];
                    $row['life'] = isset($prodLife[$row['pack_id']]) ? $prodLife[$row['pack_id']]['life'] : 1;

                    $docModel = new DocProdModel();
                    $docModel->doc_type = DocProdModel::DOC_TYPE_INVOICE_RECEIPT;
                    $docModel->doc_id = $model->id;
                    $docModel->obj = $_POST['obj'];
                    $docModel->pos = $cnt++;
                    $docModel->prod_name = stripslashes($row['prod_name']);
                    $docModel->tmark_id = $row['tmark_id'];
                    $docModel->pack_id = $row['pack_id'];
                    $docModel->unit = $row['unit'];
                    $docModel->pack_unit = $row['pack_unit'];
                    $docModel->in_amount = $row['amount'];
                    $docModel->in_amount_unit = $row['amount_unit'];
                    $docModel->in_price = $row['price'];
                    $docModel->amount = $row['amount'];
                    $docModel->amount_unit = $row['amount_unit'];
                    $docModel->price = $row['price'];
                    $docModel->exp_date = strtotime('+' . $row['life'] . ' day', mktime(23, 59, 59));
                    $docModel->save();
                    if ($docModel->hasErrors()) {
                        $res['error'] = 1;
                        $res['content'] = $docModel->getErrors();
                        $page = new Json($res);
                        return;
                    } else {
                        $row['id'] = $docModel->id;
                        unset($docModel);
                    }

                }
            }

        }
        $page = new Json($res);

    }

    public function saveAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('id', 'inv_number', 'inv_date', 'inv_time_period');
        if(empty($_POST['preSave'])) {
            $reqFields = array('id', 'inv_number', 'inv_date', 'inv_time_period', 'total', 'items');
        }
        foreach ($reqFields as $field) {
            if (empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $invoiceReceiptModel = new InvoiceReceiptModel();
        $invoiceReceiptModel->get($_POST['id']);
        $invoiceReceiptModel->inv_number = $_POST['inv_number'];
        $invoiceReceiptModel->inv_date = $_POST['inv_date'];
        $invoiceReceiptModel->inv_time_period = $_POST['inv_time_period'];
        $invoiceReceiptModel->total = $_POST['total'];

        if(!$invoiceReceiptModel->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$invoiceReceiptModel->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $invoiceReceiptModel;

            // обновим продукты
            if(!empty($_POST['items'])) {
                $isAllRecived = array();
                foreach($_POST['items'] as $row) {
                    $prodModel = new DocProdModel();
                    $prodModel->id = $row['id'];
                    $prodModel->doc_type = DocProdModel::DOC_TYPE_INVOICE_RECEIPT;
                    $prodModel->doc_id = $_POST['id'];
                    $prodModel->obj = $this->obj;
                    $prodModel->pos = $row['pos'];
                    $prodModel->prod_name = stripslashes($row['prod_name']);
                    $prodModel->tmark_id = $row['tmark_id'];
                    $prodModel->pack_id = $row['pack_id'];
                    $prodModel->unit = $row['unit'];
                    $prodModel->pack_unit = $row['pack_unit'];
                    $prodModel->in_amount = Converter::getAmountForDb($row['in_amount'], $row['in_amount_unit']);
                    $prodModel->in_amount_unit = !empty($row['in_amount_unit']) ? $row['in_amount_unit'] : null;
                    $prodModel->in_price = Converter::getPriceForDb($row['in_price'], $row['pack_unit'], $row['in_amount_unit']);
                    $prodModel->amount = Converter::getAmountForDb($row['amount'], $row['amount_unit']);
                    $prodModel->amount_unit = $row['amount_unit'];
                    $prodModel->price = Converter::getPriceForDb($row['price'], $row['pack_unit'], $row['amount_unit']);
                    $prodModel->exp_date = $row['exp_date'];
                    if(!$prodModel->save()) {
                        $res['error'] = 1;
                        $res['content'] = array("Save Error", $prodModel->getErrors());
                    }
                }
                if(!empty($_POST['approve'])) {
                    if(!$this->approve($invoiceReceiptModel, $_POST['items'])) {
                        $res['error'] = 1;
                        $res['content'] = array("Approve Error", $this->getError());
                    } else {
                        // проверим все ли продукты получены из заявки
                        $clime = $invoiceReceiptModel->getClimeProducts();
                        // сообщим в основную систему, что заявка не закрыта.
                        if(!$clime['isAllReceived']) {
                            $this->foodClaim($invoiceReceiptModel, $clime['prods']);
                        }
                    }
                }
            }

        }

        $page = new Json($res);

    }

    /**
     * @param $docModel InvoiceReceiptModel
     * @param $items
     * @return bool
     */
    public function approve($docModel, $items)
    {
        // перед сохранением, удалим все продукты данного документа со склада. Чтобы не дублировались
        $warehouseModel = new WarehouseModel();
        $warehouseModel->trans_doc_type = DocProdModel::DOC_TYPE_INVOICE_RECEIPT;
        $warehouseModel->trans_doc_id = $docModel->id;

        // временное решение для тестов
        $warehouseModel->deleteDocProds();

        foreach($items as $row) {
            $warehouseModel = new WarehouseModel();
            $warehouseModel->trans_doc_type = DocProdModel::DOC_TYPE_INVOICE_RECEIPT;
            $warehouseModel->trans_doc_id = $docModel->id;
            $warehouseModel->lot_id = $docModel->id;
            $warehouseModel->trans_date = time();
            $warehouseModel->obj = $this->obj;
            $warehouseModel->prod_name = stripslashes($row['prod_name']);
            $warehouseModel->tmark_id = $row['tmark_id'];
            $warehouseModel->pack_id = $row['pack_id'];
            $warehouseModel->unit = $row['unit'];
            $warehouseModel->pack_unit = $row['pack_unit'];
            $warehouseModel->amount = Converter::getAmountForDb($row['amount'], $row['amount_unit']);
            $warehouseModel->price = Converter::getPriceForDb($row['price'], $row['pack_unit'], $row['amount_unit']);
            $warehouseModel->exp_date = $row['exp_date'];
            $warehouseModel->putProduct();
            if($warehouseModel->hasErrors()) {
                $this->setError($warehouseModel->getErrors());
                return false;
            }
        }
        $docModel->approve();

        $orderModel = new OrderModel();
        $orderModel->close($docModel->order_id);

        return true;
    }


    public function changeStateAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('orderId', 'state');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new OrderModel();
        $model->get($_POST['orderId']);
        $model->state = $_POST['state'];
//        dd($model);
        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);
    }

    public function lockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new TraderModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            $userId = $lockTablesModel->getUserBlockedTable();
            if($userId && $userId != $user->uid) {
                $screenName = $user->getScreenName(array($userId));
                $res['content']['lock'] = "Данные редактируются другим пользователем: " . $screenName[0];
            } else {
                if ($lockTablesModel->setLock()) {
                    $res['error'] = 0;
                    $res['content'] = $lockTablesModel;
                } else {
                    $res['error'] = 1;
                    $res['content'] = $lockTablesModel->getErrors();
                }
            }
        }
        $page = new Json($res);
    }

    public function unlockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new TraderModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            if($lockTablesModel->unLock()) {
                $res['error'] = 0;
                $res['content'] = $lockTablesModel;
            } else {
                $res['error'] = 1;
                $res['content'] = $lockTablesModel->getErrors();
            }
        }
        $page = new Json($res);
    }

    //TODO: нужно допилить
    private function foodClaim($invoiceReceiptModel, $climeProds)
    {
        if(function_exists('model_dispatches')) {
            $model = new OrderModel();
            $order = $model->getForSend($invoiceReceiptModel->orderId);
            if (!empty($order)) {
                $params = array(
                    'fio' => $order['fio'],
                    'phone' => $order['phone'],
                    'email' => $order['email']
                );

                $params['items'] = $climeProds;

                $params['date'] = date('d.m.Y', $order['date_shipment']);
                $delivery_time = json_decode($order['delivery_time'], true);
                $params['time'] = $delivery_time['from'] . '-' . $delivery_time['to'];
                $params['order'] = $order['id'];

                model_dispatches('food_send', $params);
            }
        }
    }

}
