<?php


namespace Food\App\Controllers;


use Food\App\Helpers\Converter;
use Food\App\Helpers\Units;
use Food\App\Models\DishesModel;
use Food\App\Models\FeedingModel;
use Food\App\Models\Repositories\RecipesRep;
use Food\App\Models\Repositories\TechMapProdRep;
use Food\Core\Controller;
use Food\Core\Json;
use Food\Core\View;

class FeedingController extends Controller
{

    public function indexAction()
    {
        global $page, $user, $businessobj;

        View::setTitle('Категории питающихся');

        View::jsFile('app/feeding.js', View::TO_END);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditFeedingCategory' => $user->havePermit(315, $this->obj),
            'canEditFeeding' => $user->havePermit(316, $this->obj),
        );
        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        $feedingModel = new FeedingModel();
        $feeds = $feedingModel->getAll();
        View::jsText('window.feeds = {};'
            . 'window.feeds.items = ' . (empty($feeds) ? '{}' : json_encode($feeds)) . ';', View::TO_END);

        //TODO: реализовать только просмотр и редактирование только категорий питающихся
        View::render('feeding/index', array(
            'feeds' => $feeds,
            'permit' => $permit
        ));

        return;
    }

    public function saveCategoryAction()
    {
        global $page, $businessobj;

        $res = array(
            'error' => 0,
            'content' => ''
        );

//        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $reqFields = array('name');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new FeedingModel();
        $model->id = isset($_POST['id']) ? $_POST['id'] : null;
        $model->parent_id = $_POST['parent_id'];
        $model->pos = isset($_POST['pos']) ? $_POST['pos'] : null;
        $model->name = stripslashes($_POST['name']);
        $model->objs = !empty($_POST['objs']) ? stripslashes($_POST['objs']) : null;

        if(!$model->saveCategory()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $objs = empty($model->objs) ? array() : json_decode($model->objs, true);
            $model->objs = new \stdClass();
            foreach(array_keys($businessobj) as $bObj) {
                $model->objs->$bObj = in_array($bObj, $objs) ? 1 : 0;
            }
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);

    }


    public function deleteCategoryAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('id');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new FeedingModel();
        $model->id = isset($_POST['id']) ? $_POST['id'] : null;
        $ids = $model->deleteCategory();
        if(!$ids) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = array('ids' => $ids);
        }
        $page = new Json($res);

    }

    // TODO: реализовать !!!!
    public function saveAction()
    {
        global $page, $businessobj;

        $res = array(
            'error' => 0,
            'content' => ''
        );

//        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $reqFields = array('items');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        if(is_array($_POST['items'])) {
            foreach ($_POST['items'] as $row) {
                $model = new FeedingModel();
                $model->id = isset($row['id']) ? $row['id'] : null;
                $model->parent_id = $row['parent_id'];
                $model->pos = isset($row['pos']) ? $row['pos'] : null;
                $model->name = stripslashes($row['name']);
                $model->objs = !empty($row['objs']) ? json_encode($row['objs']) : null;

                if (!$model->saveCategory()) {
                    $res['error'] = 1;
                    $res['content'] = array("Save Error", $model->getErrors());
                    $page = new Json($res);
                    return false;
                }
            }
        }
        $res['error'] = 0;
        $res['content'] = 'ok';
        $page = new Json($res);

    }


}