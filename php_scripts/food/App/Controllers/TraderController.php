<?php

namespace Food\App\Controllers;

use Food\App\Models\LockTablesModel;
use Food\App\Models\ProdPackModel;
use Food\App\Models\Repositories\ProductRep;
use Food\App\Models\TraderModel;
use Food\App\Models\TraderObjModel;
use Food\App\Models\TraderPriceModel;
use Food\Core\Json;
use Food\Core\View;
use Food\Core\Controller;


class TraderController extends Controller
{
    public function indexAction()
    {
        global $page, $user, $businessobj;

        View::setTitle('Поставщики');

        View::cssFile('jstree/themes/default/style.min.css');
        View::jsFile('jstree/jstree.js', View::TO_END);
        View::jsFile('jstree/jstreegrid.js', View::TO_END);
        View::jsFile('app/trader.js', View::TO_END);
        View::jsFile('app/forms/products.js', View::TO_END);

        if(!$this->isRightObject()) {
            View::render('error404', array(
                'error' => 'Неизвестный обьект'
            ));
            return;
        }

        $permit = array(
            'canViewData' => $user->havePermit(300, $this->obj),
            'canEditFullProducts' => $user->havePermit(301, $this->obj),
            'canEditTmarkPackProducts' => $user->havePermit(302, $this->obj),
            'canEditTrader' => $user->havePermit(303, $this->obj),
            'canCheckedProduct' => $user->havePermit(305, $this->obj),
            'canChangePriceProduct' => $user->havePermit(306, $this->obj),
        );
        View::jsText('var permit = ' . json_encode($permit) . ';', View::TO_END);

        if(!$permit['canViewData']) {
            View::render('error404', array(
                'error' => 'Вы не можете просматривать данные'
            ));
            return;
        }

        $model = new TraderModel();
        $model->obj = $this->obj;
        $traders = $model->getAll();

        if(empty($_GET['id'])) {
            View::render('traders/index', array(
                'obj' => $this->obj,
                'traders' => empty($traders) ? array() : $traders,
                'permit' => $permit
        ));
        } else {
            $for = null;
            if(!empty($_GET['for']) || $this->obj != 'all') {
                $for = isset($_GET['for']) ? $_GET['for'] : $this->obj;
                View::jsText('var tripleState = true;', View::TO_END);
            }
            View::jsText('var treeWithCheckboxes = true;', View::TO_END);
            View::jsText('var dataUrl = "trader/getData";', View::TO_END);
            View::jsText('var businessobj = ' . json_encode($businessobj) . ';' , View::TO_END);
            View::render('traders/trader', array(
                'obj' => $this->obj,
                'trader' => $model->get($_GET['id'], false, $for),
                'permit' => $permit
            ));
        }

        return;
    }


    public function pricesWithMainAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $reqFields = array('trader_id');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }
        $model = new TraderObjModel();
        $model->obj = $_POST['obj'];
        $discount = $model->getDiscount($_POST['trader_id']);
        $priceCoef = 1 - ($discount / 100);

        $priceModel = new TraderPriceModel();
        $priceModel->trader_id = $_POST['trader_id'];
        $priceModel->obj = $this->obj;
        $prices = $priceModel->getAllWithMain();
        if($priceModel->hasErrors()) {
            $res['error'] = 1;
            $res['content'] = array("Request Error", $priceModel->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = array(
                'prices' => $prices,
                'priceCoef' => $priceCoef
            );
        }
        $page = new Json($res);
    }

    public function getDataAction()
    {

        global $page, $businessobj;

        if(!$this->isRightObject()) {
            View::render('products/index', array(
                'error' => $this->getError()
            ));
            return;
        }

        // если данные для конкретного объекта
        if($this->obj == 'all' && (isset($_GET['for']) && array_key_exists($_GET['for'], $businessobj)) ) {
            $forObj = $_GET['for'];
        } else {
            $forObj = null;
        }

        $model = new ProductRep();
        $tree = $model->getTree($this->obj, 'trader', $forObj);
        $page = new Json($tree );
    }


    public function savePriceAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $reqFields = array('trader_id', 'tmark_id', 'pack_id', 'price', 'smallest_unit_value');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $priceModel = new TraderPriceModel();
        $priceModel->trader_id = $_POST['trader_id'];
        $priceModel->obj = empty($_POST['forObj']) ? $this->obj : $_POST['forObj'];
        $priceModel->tmark_id = $_POST['tmark_id'];
        $priceModel->pack_id = $_POST['pack_id'];
        $priceModel->price = empty($_POST['price']) ? null : $_POST['price'];
        $priceModel->active = isset($_POST['checkboxState']) ? $_POST['checkboxState'] : ($priceModel->obj === 'all' ? 0 : 2);
        $priceModel->smallest_unit_price = empty($_POST['price']) ? null : ($_POST['price'] / $_POST['smallest_unit_value']);
        if(!$priceModel->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$priceModel->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $priceModel;

            // устанавливаем цены для обьектов, если устанавливается цена в общем прайсе
            if($this->obj == 'all' && empty($_POST['forObj'])) {
                $traderModel = new TraderModel();
                $traderObjs = $traderModel->getTraderBusinessObjs($_POST['trader_id']);
                if(!empty($traderObjs)) {
                    foreach ($traderObjs as $obj) {
                        $priceModel->obj = $obj;
                        $priceModel->active = null;
                        if(!$priceModel->save(false)) {
                            $res['error'] = 1;
                            $res['content'][] = array("Save for $obj Error", $priceModel->getErrors());
                        }
                    }
                }
            }
        }
        $page = new Json($res);

    }

    // изменение цен поставщика со страниц заявки или накладной
    public function changePriceAction()
    {
        global $page;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $reqFields = array('trader_id', 'obj', 'items');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $priceModel = new TraderPriceModel();
        $priceModel->trader_id = $_POST['trader_id'];
        $priceModel->obj = $_POST['obj'];

        $prodItems = array();
        foreach ($_POST['items'] as $item) {
            $pack_id = $item['pack_id'];
            $price = $item['trader_price_user'];
            $smallestUnitValue = ProdPackModel::getSmallestUnitValue($item['unit_value'], $item['unit']);
            $smallest_unit_price = ($smallestUnitValue !== 0 ) ? $price / $smallestUnitValue : $price;
            $prodItems[] = array(
                'pack_id'   => $pack_id,
                'price'     => $price,
                'smallest_unit_price' => $smallest_unit_price
            );
        }
        if(!empty($prodItems)) {
            if(!$priceModel->changePrices($prodItems)) {
                $res['error'] = 1;
                $res['content'] = array("Save Error",$priceModel->getErrors());
            } else {
                $res['error'] = 0;
                $res['content'] = '';
            }
        }
        $page = new Json($res);

    }

    public function saveAction()
    {
        global $page, $businessobj;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        $reqFields = array('internal_name', 'legal_name', 'contract', 'fio', 'phone', 'email');
        foreach ($reqFields as $field) {
            if(empty($_POST[$field])) {
                $res['error'] = 1;
                $res['content'] = 'Empty ' . $field;
                $page = new Json($res);
                return;
            }
        }

        if(empty($_POST['payment_delay']) && empty($_POST['prepayment'])) {
            $res['error'] = 1;
            $res['content'] = 'Empty payment_delay';
            $page = new Json($res);
            return;
        }

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new TraderModel();
        $model->id = isset($_POST['id']) ? intval($_POST['id']) : null;
        $model->obj = $this->obj;
        $model->internal_name = stripslashes($_POST['internal_name']);
        $model->legal_name = stripslashes($_POST['legal_name']);
        $model->bank_details = !empty($_POST['bank_details']) ? stripslashes($_POST['bank_details']) : null;
        $model->address = !empty($_POST['address']) ? stripslashes($_POST['address']) : null;
        $model->main_phone = !empty($_POST['main_phone']) ? $_POST['main_phone'] : null;
        $model->main_email = !empty($_POST['main_email']) ? $_POST['main_email'] : null;
        $model->contract = $_POST['contract'];
        $model->fio = $_POST['fio'];
        $model->phone = $_POST['phone'];
        $model->email = $_POST['email'];
        $model->prepayment = isset($_POST['prepayment']) ? true : false;
        $model->payment_delay = $model->prepayment ? null : $_POST['payment_delay'];
        $model->discount = isset($_POST['discount']) ? str_replace(',', '.', $_POST['discount']) : null;
        $arrBObjs = array_keys($_POST['business_obj']);
        $model->business_obj = json_encode($arrBObjs);
        if(!$model->save()) {
            $res['error'] = 1;
            $res['content'] = array("Save Error",$model->getErrors());
        } else {
            $res['error'] = 0;
            $res['content'] = $model;
        }
        $page = new Json($res);

    }

    public function getTradersForObjAction()
    {
        global $page, $businessobj;

        $res = array(
            'error' => 0,
            'content' => ''
        );

        if(!$this->isRightObject()) {
            $res['error'] = 1;
            $res['content'] = $this->getError();
            $page = new Json($res);
            return;
        }

        $model = new TraderModel();
        $model->obj = $this->obj;

        $res['content'] = $model->getAll();
        $page = new Json($res);
    }

    public function lockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new TraderModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            $userId = $lockTablesModel->getUserBlockedTable();
            if($userId && $userId != $user->uid) {
                $screenName = $user->getScreenName(array($userId));
                $res['content']['lock'] = "Данные редактируются другим пользователем: " . $screenName[0];
            } else {
                if ($lockTablesModel->setLock()) {
                    $res['error'] = 0;
                    $res['content'] = $lockTablesModel;
                } else {
                    $res['error'] = 1;
                    $res['content'] = $lockTablesModel->getErrors();
                }
            }
        }
        $page = new Json($res);
    }

    public function unlockAction()
    {
        global $page, $user;

        $res = array(
            'error' => 0,
            'content' => ''
        );
        $model = new TraderModel();
        if (!empty($_POST['id'])) {
            $lockTablesModel = new LockTablesModel($model->getTableName());
            $lockTablesModel->rowId = $_POST['id'];
            $lockTablesModel->userId = $user->uid;
            if($lockTablesModel->unLock()) {
                $res['error'] = 0;
                $res['content'] = $lockTablesModel;
            } else {
                $res['error'] = 1;
                $res['content'] = $lockTablesModel->getErrors();
            }
        }
        $page = new Json($res);
    }


}
