<?php

function dd()
{
    ini_set('default_mimetype', 'text/html');
    ini_set('default_charset', 'utf-8');
    foreach (func_get_args() as $arg ) {
        print_r($arg);
    }
    die;
}

function ErrorPage404($msg = '')
{
    global $page;
    if($msg) {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404');
        $page->title = "Error 404";
        Message($msg);
    }
}

function Message($msg = '')
{
    \Food\Core\View::render('error404', array('error' => $msg));
}

function declineForNum($num, $one, $four, $many)
{
    $declension = $many;
    if ($num == 1) {
        $declension = $one;
    } elseif ($num > 1 && $num < 5) {
        $declension = $four;
    }
    return $num . ' ' . $declension;
}