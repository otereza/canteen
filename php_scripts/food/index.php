<?php

ini_set('default_charset', 'utf-8');
ini_set('default_mimetype', 'text/html');

define('BASE_DIR', __DIR__);
define('APP_DIR', BASE_DIR . '/App');
define('VIEW_DIR', APP_DIR . '/Views');
define('LOG_DIR', BASE_DIR . '/logs');

require "App/functions.php";
require "config.php";
require "includes/loader.php";
require "includes/route.php";



