<?php

namespace Food\Core;

/**
 * Base controller
 *
 */
abstract class Controller
{

    private $error = '';

    protected $obj;

    /**
     * Class constructor
     *
     * @param array $route_params  Parameters from the route
     *
     * @return void
     */
    public function __construct()
    {
//        $this->route_params = $route_params;
    }

    /**
     * Magic method called when a non-existent or inaccessible method is
     * called on an object of this class. Used to execute before and after
     * filter methods on action methods. Action methods need to be named
     * with an "Action" suffix, e.g. indexAction, showAction etc.
     *
     * @param string $name Method name
     * @param array $args Arguments passed to the method
     *
     * @return void
     * @throws \Exception
     */
    public function __call($name, $args)
    {
        $method = $name . 'Action';

        if (method_exists($this, $method)) {
            if ($this->before() !== false) {
                call_user_func_array(array($this, $method), $args);
                $this->after();
            }
        } else {
            throw new \Exception("Method $method not found in controller " . get_class($this));
        }
    }

    /**
     * Before filter - called before an action method.
     *
     * @return void
     */
    protected function before()
    {
    }

    /**
     * After filter - called after an action method.
     *
     * @return void
     */
    protected function after()
    {
    }

    /**
     * Проверка переданного параметра obj
     * @return bool
     */
    protected function isRightObject()
    {
        global $businessobj;

        if(empty($_GET['obj'])) {
            $this->error = "В параметре запроса не указан обьект (obj)";
        } elseif($_GET['obj'] != 'all' && !array_key_exists($_GET['obj'], $businessobj)) {
            $this->error = "Обьект не существует";
        } else {
            $this->obj = $_GET['obj'];
        }
        return empty($this->error);
    }

    function getError()
    {
        return $this->error;
    }

    function setError($msg)
    {
        $this->error = $msg;
    }

}
