<?php

namespace Food\Core;


class Db
{
    private static $instance = null;
    private $pdo;
    private $query;
    private $count = 0;
    private $error = false;
    private $errors = array();
    private $lastId;

    private function __construct() {

        try {
            // Put your database information
            $this->pdo = new \PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD, array(
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_EMULATE_PREPARES => false
            ));
            $this->pdo->exec("set names utf8");
        } catch (\PDOException $e) {
            die($e->getMessage());
        }

    }

    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new Db();
        }
        return self::$instance;
    }

    public function begin()
    {
        $this->pdo->beginTransaction();
    }

    public function commit()
    {
        $this->pdo->commit();
    }

    public function rollBack()
    {
        $this->pdo->rollBack();
    }

    public function query($sql, $placeholders = array()) {
        $this->error = false;
        $results = null;

        if ($query = $this->pdo->prepare($sql)) {

            foreach ($placeholders as $param => $value) {
                $query->bindValue($param, $value);
            }

            if(DEBUG) {
                Log::info($this->getQueryString($sql, $placeholders));
            }
            if ($query->execute()) {
                // You can PDO::FETCH_OBJ instad of assoc, or whatever you like
                $results = $query->fetchAll(\PDO::FETCH_ASSOC);
                $this->count = $query->rowCount();
                $this->lastId = $this->pdo->lastInsertId();
            } else {
                Log::error($this->pdo->errorInfo());
                $this->errors[] = $this->pdo->errorInfo();
                $this->error = true;
            }
        }
        return $results;
    }

    public function execute($sql, $placeholders = array()) {
        $this->error = false;
        $results = null;

        if ($query = $this->pdo->prepare($sql)) {

            foreach ($placeholders as $param => $value) {
                $query->bindValue($param, $value);
            }

            if(DEBUG) {
                Log::info($this->getQueryString($sql, $placeholders));
            }
            if ($query->execute()) {
                // You can PDO::FETCH_OBJ instad of assoc, or whatever you like
                $this->count = $query->rowCount();
                $this->lastId = $this->pdo->lastInsertId();
            } else {
                Log::error($this->pdo->errorInfo());
                $this->errors[] = $this->pdo->errorInfo();
                $this->error = true;
            }
        }
        return $this->error;
    }


    /**
     * Получение всех строк
     * @param $sql
     * @param array $placeholders
     * @return array|null
     */
    public function fetchAll($sql, $placeholders = array())
    {
        $this->error = false;
        $results = null;

        if ($this->query = $this->pdo->prepare($sql)) {

            foreach ($placeholders as $param => $value) {
                $this->query->bindValue($param, $value);
            }

            if(DEBUG) {
                Log::info($this->getQueryString($sql, $placeholders));
            }
            if ($this->query->execute()) {
                // You can PDO::FETCH_OBJ instad of assoc, or whatever you like
                $results = $this->query->fetchAll(\PDO::FETCH_ASSOC);
                $this->count = $this->query->rowCount();
            } else {
                Log::error($this->pdo->errorInfo());
                $this->errors[] = $this->pdo->errorInfo();
                $this->error = true;
            }
        }
        return $results;
    }

    /**
     * Получение одной строки
     * @param $sql
     * @param array $placeholders
     * @return mixed|null
     */
    public function fetchRow($sql, $placeholders = array())
    {
        $this->error = false;
        $results = null;

        if ($query = $this->pdo->prepare($sql)) {

            foreach ($placeholders as $param => $value) {
                $query->bindValue($param, $value);
            }

            if(DEBUG) {
                Log::info($this->getQueryString($sql, $placeholders));
            }
            if ($query->execute()) {
                // You can PDO::FETCH_OBJ instad of assoc, or whatever you like
                $results = $query->fetch(\PDO::FETCH_ASSOC);
            } else {
                Log::error($this->pdo->errorInfo());
                $this->errors[] = $this->pdo->errorInfo();
                $this->error = true;
            }
        }
        return $results;
    }

    /**
     * Получение значения столбца
     * @param $sql
     * @param array $placeholders
     * @param int $column
     * @return mixed|null
     */
    public function fetchColumn($sql, $placeholders = array(), $column = 0)
    {
        $this->error = false;
        $results = null;

        if ($query = $this->pdo->prepare($sql)) {

            foreach ($placeholders as $param => $value) {
                $query->bindValue($param, $value);
            }

            if(DEBUG) {
                Log::info($this->getQueryString($sql, $placeholders));
            }
            if ($query->execute()) {
                // You can PDO::FETCH_OBJ instad of assoc, or whatever you like
                $results = $query->fetchColumn($column);
            } else {
                Log::error($this->pdo->errorInfo());
                $this->errors[] = $this->pdo->errorInfo();
                $this->error = true;
            }
        }
        return $results;
    }


    public function insert($table, $data)
    {
        $columns = array();
        $placeholders = array();
        foreach($data as $column => $value) {
            $columns[] = $column;
            $placeholders[$column] = $value;
        }

        $sql = "INSERT INTO " . $table . " (" . implode(',', $columns) . ") VALUES (:" . implode(", :", $columns) . ")";
        try {
            $this->execute($sql, $placeholders);
        } catch (\PDOException $e) {
            Log::error($this->pdo->errorInfo());
            $this->errors[] = "Insert error! " . $e->getMessage();
            return false;
        }
        return true;
    }

    public function update($table, array $data, $where = '', $placeholders = array())
    {
        $sets = array();
        foreach ($data as $field => $value) {
            if($value === null) {
                $sets[] = $field . '=null';
            } else {
                $sets[] = $field . '="' . addslashes($value) . '"';
            }
        }

        $sql = "UPDATE " . $table . " SET ". implode(',', $sets);
        if($where !== '') {
            $sql .= " WHERE " . $where;
        }
        try {
            $this->execute($sql, $placeholders);
        } catch (\PDOException $e) {
            Log::error($this->pdo->errorInfo());
            $this->errors[] = "Update error! " . $e->getMessage();
            return false;
        }
        return true;
    }

    public function delete($table, $where = null, $placeholders = array())
    {
        $sql = "DELETE FROM " . $table;
        if($where !== null ) {
            $sql .= " WHERE " . $where;
        }
        try {
            $this->execute($sql, $placeholders);
        } catch (\PDOException $e) {
            Log::error($this->pdo->errorInfo());
            $this->errors[] = "Delete error! " . $e->getMessage();
            return false;
        }
        return true;
    }


    public function getQueryString($sql, $placeholders = null)
    {
        if($placeholders) {
            $arrPlaces = array();
            $arrValues = array();
            foreach ($placeholders as $key => $value) {
                $arrPlaces[] = ':' . $key;
                $arrValues[] = '"' . $value . '"';
            }

            $query = str_replace($arrPlaces, $arrValues, $sql);
        } else {
            $query = $sql;
        }
        return $query;
    }

    public function hasError() {
        return (bool)count($this->errors);
    }

    public function getErrors() {
        return $this->errors;
    }

    public function geCount() {
        return $this->count;
    }

    public function getLastId() {
        return $this->lastId;
    }
}