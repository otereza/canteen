<?php

namespace Food\Core;

/**
 * View
 *
 */
class View
{

    const TO_HEAD   = 'head';
    const TO_END    = 'end';

    /**
     * Render a view file
     *
     * @param string $view The view file
     * @param array $args Associative array of data to display in the view (optional)
     *
     * @return void
     */
    public static function render($view, $args = array())
    {
        global $page;

        // Общие стили и скрипты модуля
        array_push($page->css['files'], 'bootstrap.min.css');
        array_push($page->css['files'], 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
        array_push($page->css['files'], 'food.css');

        array_push($page->js['files']['head'], 'jquery.min.js');
        array_push($page->js['files']['end'], 'jquery-ui.min.js');
        array_push($page->js['files']['end'], 'bootstrap.min.js');
        array_push($page->js['files']['end'], 'app.js');

        extract($args, EXTR_SKIP);

        $file = dirname(__DIR__) . "/App/Views/$view.php";  // relative to Core directory

        if (is_readable($file)) {

            ob_start();
            ob_implicit_flush();
            require VIEW_DIR . "/layout.php";
            $layout = ob_get_clean();

            ob_start();
            ob_implicit_flush();
            require $file;
            $content = ob_get_clean();


            $page->html['body'] .= str_replace("{{ CONTENT }}", $content, $layout);
        } else {
            Message("View \"$view\" not found");
        }
    }

    public static function renderPart($view, $args = array())
    {
        extract($args, EXTR_SKIP);
        $file = dirname(__DIR__) . "/App/Views/$view.php";  // relative to Core directory

        if (is_readable($file)) {
            ob_start();
            require $file;
            return ob_get_clean();
        } else {
            Message("View \"$view\" not found");
        }
        return false;
    }

    public static function setTitle($title)
    {
        global $page;
        $page->title = $title;
    }

    public static function jsFile($file, $to)
    {
        global $page;
        if ($to == self::TO_END || $to == self::TO_HEAD) {
            array_push($page->js['files'][$to], $file);
        }
    }

    public static function jsText($text, $to)
    {
        global $page;
        if ($to == self::TO_END || $to == self::TO_HEAD) {
            $page->js['text'][$to] .= $text;
        }
    }

    public static function cssFile($file)
    {
        global $page;
        array_push($page->css['files'], $file);
    }

    public static function cssText($text)
    {
        global $page;
        $page->css['text'] .= $text;
    }

}
