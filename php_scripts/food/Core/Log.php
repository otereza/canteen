<?php
/**
 * Created by PhpStorm.
 * User: terezao
 * Date: 22.01.19
 * Time: 10:15
 */

namespace Food\Core;


class Log
{

    public static function info($msg)
    {
        if(!empty($msg)) {
            self::writeLog($msg, 'INFO');
        }
    }

    public static function warning($msg)
    {
        if(!empty($msg)) {
            self::writeLog($msg, 'WARNING');
        }
    }

    public static function error($msg)
    {
        if(!empty($msg)) {
            self::writeLog(json_encode($msg), 'ERROR');
        }
    }

    private static function writeLog($msg, $severity)
    {
        $msg = "[" . self::udate('d.m.Y H:i:s.u') . "][" . $severity . "]: " . $msg . "\n";
        @file_put_contents(LOG_DIR . '/log.txt', $msg, FILE_APPEND);

    }

    private static function udate($format = 'u', $utimestamp = null) {
        if (is_null($utimestamp))
            $utimestamp = microtime(true);

        $timestamp = floor($utimestamp);
        $milliseconds = round(($utimestamp - $timestamp) * 1000000);

        return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format), $timestamp);
    }
}