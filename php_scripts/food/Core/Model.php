<?php

namespace Food\Core;

use PDO;
use Food\App\Config;

/**
 * Base model
 *
 */
abstract class Model
{

    protected $errors = array();

    protected $tableName;
    public $lock;

    protected function getDB()
    {
        static $db = null;

        if ($db === null) {
            $db = Db::getInstance();
        }
        return $db;
    }

    public function getTableName($tableName = null)
    {
        if($tableName == null) {
            $tableName = $this->tableName;
        }
        return DB_TABLE_PREFIX . $tableName;
    }

    public function hasErrors()
    {
        return !empty($this->errors) || $this->getDB()->hasError();
    }

    public function getErrors()
    {
        return array_merge($this->errors, $this->getDB()->getErrors());
    }

    public function setError($msg)
    {
        return array_push($this->errors, $msg);
    }

    public function setAttributes($data)
    {
        foreach ($data as $attr => $val) {
            $this->$attr = $val;
        }
    }
}
