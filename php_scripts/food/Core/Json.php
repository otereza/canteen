<?php
/**
 * Created by PhpStorm.
 * User: terezao
 * Date: 18.01.19
 * Time: 19:46
 */

namespace Food\Core;

/**
 * Заглушка для отправки JSON ответа без усилий основной системы
 *
 * Class Json
 * @package Food\Core
 */
class Json
{
    public $json = '';

    public function __construct($json)
    {
        $this->json = $json;
    }

    public function go()
    {
//        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode($this->json);
    }
}