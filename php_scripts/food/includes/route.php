<?php

$defController = 'home';
$defAction = 'index';

if(isset($_GET['r'])) {
    $route = explode('/', $_GET['r']);
    unset($_GET['r']);
}

$ctrl = empty($route[0]) ? $defController : $route[0];
$act = empty($route[1]) ? $defAction : $route[1];

unset($_GET['cmd']);

$controllerName = 'Food\\App\\Controllers\\' . ucfirst($ctrl) . 'Controller';
$actionName = $act . 'Action';

if(class_exists($controllerName)) {
    $controller = new $controllerName;
    if(method_exists($controller, $actionName)) {
        $controller->{$act}();
    } else {
        ErrorPage404('Action "'.$actionName.'" not found in class "'.$controllerName.'"');
    }
} else {
    ErrorPage404('Controller "'.$controllerName.'" not found');
}
