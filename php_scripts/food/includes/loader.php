<?php

spl_autoload_register(function ($class) {

    // module-specific namespace prefix
    $prefix = 'Food\\';

    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }

    $file = BASE_DIR . '/'. str_replace('\\', '/', substr($class, $len)) . '.php';

    if (file_exists($file)) {
        require_once $file;
    }
});
